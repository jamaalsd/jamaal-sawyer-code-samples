##Source Code##

I have provided the source code for three projects, **Endurance**, **Going Buggy**, and **SuperJanKenPow**.  *The media and back-end dependencies for these projects have not been provided, however videos of all three in action are contained in the repository.*

**Endurance** is a part of a recent project completed for the NHL, and was written with Phaser.  The main game code is separated into three running states.  The Boot State initializes the game, the Preloader State loaded all the assets, and the Game State represents the actual game.  Custom Controllers, Classes, and JSON objects are loaded in from separate files in the Preloader state.

**Super JanKenPow** is a personal project that I released as a mobile app for iOS in early 2015.  ([https://itunes.apple.com/us/app/id952548697](https://itunes.apple.com/us/app/id952548697))

It has been written using ImpactJS, which follows closely the inheritence model written discussed by John Resig here: http://ejohn.org/blog/simple-javascript-inheritance/ In addition, ImpactJS supports the concept of file dependencies, enabling the developer to separate their code and classes into organized and reusable files.

**GoingBuggy** was written for Palm Ventures and is a conversion of a Flash game to HTML5 with the PhaserJS engine.  It follows a similar state pattern as Endurance.

##Videos##

**Javascript**

GoingBuggy, Super JanKenPow,AnimalRoundup,Aquarium,Tubin' Trouble, and Endurance.  

**Unity3D**

I've included video of two mobile games I created with Unity3D, Slerp and Runner.  Neither was ever officially released.  

Runner was an extreme upgrade and expansion on this tutorial: http://catlikecoding.com/unity/tutorials/runner/ that I wrote in 2012.  

I created Slerp early in 2014.  Fun fact: I produced the music as well!

**C_Sound_Processing**

This is a video in which I tested the sound processing algorithm that I wrote enabling a computer to take the sound input of a microphone array and triangulate the position of the sound sources within the array's hearing.  The video shows me using my prototype GUI to interface with the array and successfully produce the position of two separate sound sources.  I am co-inventor of patent detailing this process. *(Patent No. 8,704,070, “System and method for mapping and displaying audio source locations”)*