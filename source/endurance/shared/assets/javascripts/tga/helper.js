var TGA = TGA || {};
TGA.Shared = TGA.Shared || {};
TGA.Shared.initHelperFunctions = function(state){
    state.calculateTimeFromSpeedAndDistance = function(x1, y1, x2, y2, speed){
        var dist = Math.sqrt(Math.pow(x1-x2,2)+Math.pow(y1-y2,2));
        return (dist/speed)*1000;
    };

    state.calculateSpeedFromTimeAndDistance = function(x1, y1, x2, y2, time){
        var dist = Math.sqrt(Math.pow(x1-x2,2)+Math.pow(y1-y2,2));
        return (dist/time);
    };

    state.tweenClear = function (name) {
        var filterArr = _.filter(this.tweenArray, function(tween){return tween.name === name;});
        for(var i = 0; i < filterArr.length; i++){
            filterArr[i].stop();
            filterArr[i] = undefined;
        }
        this.tweenArray = _.compact(this.tweenArray);
    };

    state.getWordWrapHeight = function (textObject) {
        var result = '';
        var lines = textObject.text.split('\n');
        for (var i = 0; i < lines.length; i++)
        {
            var spaceLeft = textObject.wordWrapWidth;
            var words = lines[i].split(' ');
            for (var j = 0; j < words.length; j++)
            {
                var wordWidth = textObject.context.measureText(words[j]).width;
                var wordWidthWithSpace = wordWidth + textObject.context.measureText(' ').width;
                if(j === 0 || wordWidthWithSpace > spaceLeft)
                {
                    // Skip printing the newline if it's the first word of the line that is
                    // greater than the word wrap width.
                    if(j > 0)
                    {
                        result += '\n';
                    }
                    result += words[j];
                    spaceLeft = textObject.wordWrapWidth - wordWidth;
                }
                else
                {
                    spaceLeft -= wordWidthWithSpace;
                    result += ' ' + words[j];
                }
            }

            if (i < lines.length-1)
            {
                result += '\n';
            }
        }
        var returnLines = result.split('\n').length;
        var fontSize = parseInt(textObject.style.font.split('px')[0],10);
        var spacing = ( (textObject.lineSpacing * returnLines) + (fontSize * returnLines-1) );
        return spacing;
    };

    state.replaceInString = function (str, find, replace) {
        var re = new RegExp(find, 'g');
        str = str.replace(re, replace);
        return str;
    };

    state.getString = function(key){
        if(TGA.inEverFiApp || this.game.HTML5GameService !== undefined){
            return this.game.HTML5GameService.getString(key).toString();
        }else{
            return this.gameText[key];
        }
    };

    state.getTextStyle = function(key){
        return TGA.TextStyles.styles[key];
    };

    state.saveData = function(key, results){
        if(TGA.inEverFiApp || this.game.HTML5GameService !== undefined){
            this.game.HTML5GameService.saveData(key, results);
        }
    };

    state.endGame = function (){
        console.log('game ended');
        if(TGA.inEverFiApp || this.game.HTML5GameService !== undefined){
            this.game.HTML5GameService.endGame();
        }
    };

    state.playSound = function (audio, settings){
        settings = settings || {};
        if(settings.loop){
            settings.loop = audio.loop;
        }
        if(settings.volume){
            settings.volume = audio.volume;
        }
        if(settings.fadeIn){
            var vol = audio.volume;
            console.log(vol);
            console.log(audio);
            audio.fadeTo(settings.fadeIn, vol);
            return;
        }
        if(settings.delay){
            this.game.time.events.add(settings.delay, function(){
                audio.play();
            }.bind(this));
            return;
        }
        audio.play();

    };
};
