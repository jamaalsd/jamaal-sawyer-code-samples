var TGA = TGA || {};
TGA.Shared = TGA.Shared || {};
///////////////////////////////////////////////////////////////////////
// Timer: Countdown Timer that triggers event
///////////////////////////////////////////////////////////////////////
TGA.Shared.Timer = function(game, x, y, parent){
	// Super call to Phaser.Sprite
	Phaser.Sprite.call(this, game, x, y, 'timer');
	this.zI = 100;
	this.anchor.setTo(0.5);
	this.completeCallbackFunction = undefined;
	this.completeCallback = function(){
		var timerTween = this.game.add.tween(this).to({ alpha: 0.0}, 150, Phaser.Easing.Linear.None,true);
		if(this.completeCallbackFunction){
			timerTween.onComplete.add(this.completeCallbackFunction);
		}
		this.animations.stop();
	}.bind(this);
	var anim = this.animations.add('countdown',[0,1,2,3],1);
	anim.onComplete.add(this.completeCallback);
	this.alpha = 0;
	parent.add(this);
};
TGA.Shared.Timer.prototype = Object.create(Phaser.Sprite.prototype);
TGA.Shared.Timer.constructor = TGA.Shared.Timer;

TGA.Shared.Timer.prototype.countdown = function (callback){
	if(callback){
		this.completeCallbackFunction = callback;
	}
	var startAnimation = function(){
		this.animations.play('countdown');
		var state = this.game.currentActiveState;
		if(state.sfx && state.sfx.countdown){
			state.playSound(state.sfx.countdown);
		}
	}.bind(this);
	var timerTween = this.game.add.tween(this).to({ alpha: 1.0}, 150, Phaser.Easing.Linear.None,true);
	timerTween.onComplete.add(startAnimation);
	this.animations.currentAnim.frame = 0;
};

///////////////////////////////////////////////////////////////////////
// Big Button: User Big Button
///////////////////////////////////////////////////////////////////////
TGA.Shared.BigButton = function(game, settings){
	// Super call to Phaser.Sprite
	Phaser.Sprite.call(this, game, settings.x, settings.y, settings.key);
	if(settings.callback){
		this.callback = settings.callback;
	}else{
		this.callback = function (){};
	}
	this.zI = 2;
	this.instructionBool = false;
	this.anchor.setTo(0.5);
	this.animations.add('disabled',[2]);
	this.animations.add('out',[1]);
	this.animations.add('over',[0]);
	this.animations.play('out');
	this.enabled = true;
	this.inputEnabled = true;
	this.events.onInputOver.add(this.buttonOver, this);
	this.events.onInputOut.add(this.buttonOut, this);
	this.events.onInputDown.add(this.buttonPress, this);
	this.events.onInputUp.add(this.buttonClick, this);
	this.input.useHandCursor = true;
	this.input.pixelPerfectOver = true;
	this.textArr = [];

	var inFrench = $('body').hasClass('fr');

	var textStyle =  (inFrench? TGA.TextStyles.getStyle('bigButtonTopFr'):TGA.TextStyles.getStyle('bigButtonTop'));

	var textTop = '';
	var textBottom = '';
	if(settings.text){
		if(settings.text[0]){
			textTop = settings.text[0];
		}
		if(settings.text[1]){
			textBottom = settings.text[1];
		}
	}

	var tTopY = (inFrench? -8: -12);

	var text = game.add.text(1, tTopY, textTop.toUpperCase(), textStyle);
	text.anchor.setTo(0.5);
	this.addChild(text);
	this.textArr.push(text);

	textStyle =  (inFrench? TGA.TextStyles.getStyle('bigButtonBottomFr'):TGA.TextStyles.getStyle('bigButtonBottom'));
	text = game.add.text(1, 14, textBottom.toUpperCase(), textStyle);
	text.anchor.setTo(0.5);
	this.addChild(text);
	this.textArr.push(text);
	settings.layer.add(this);
};
TGA.Shared.BigButton.prototype = Object.create(Phaser.Sprite.prototype);
TGA.Shared.BigButton.constructor = TGA.Shared.BigButton;

TGA.Shared.BigButton.prototype.setState = function (state){
	switch (state){
		case 'disabled':
			for(var i = 0; i < this.textArr.length; i++){
				this.textArr[i].alpha = 0.25;
			}
			this.enabled = false;
			this.animations.play('disabled');
			break;
		case 'enabled':
			for(var i = 0; i < this.textArr.length; i++){
				this.textArr[i].alpha = 1;
			}
			this.enabled = true;
			this.animations.play('out');
			break;
	}
};

TGA.Shared.BigButton.prototype.setButtonText = function (text){
	var textTop = '';
	var textBottom = '';
	if(text){
		if(text[0]){
			textTop = text[0];
		}
		if(text[1]){
			textBottom = text[1];
		}
	}
	this.textArr[0].setText(textTop);
	this.textArr[1].setText(textBottom);
};

TGA.Shared.BigButton.prototype.buttonOver = function (button){
	var state = this.game.currentActiveState;
	if((state.canClick && !state.isPaused && button.enabled) || this.instructionBool){
		button.animations.play('over');
		if(state.sfx && state.sfx.buttonOver){
			state.playSound(state.sfx.buttonOver);
		}
	}
};

TGA.Shared.BigButton.prototype.buttonOut = function (button){
	var state = this.game.currentActiveState;
	if((state.canClick && !state.isPaused && button.enabled) || this.instructionBool){
		button.animations.play('out');
		button.scale.setTo(1);
	}
};

TGA.Shared.BigButton.prototype.buttonPress = function (button){
	var state = this.game.currentActiveState;
	if((state.canClick && !state.isPaused && button.enabled) || this.instructionBool){
		button.scale.setTo(0.9);
	}
};

TGA.Shared.BigButton.prototype.buttonClick = function (button){
	// Check if pointer is still over button
	var state = this.game.currentActiveState;
	var checkOver = Phaser.Circle.intersectsRectangle(this.game.input.activePointer.circle,button.getBounds());
	if((checkOver && state.canClick && !state.isPaused && button.enabled) || this.instructionBool){
		if(state.sfx && state.sfx.buttonClick){
			state.playSound(state.sfx.buttonClick);
		}
		this.callback();
	}
	button.scale.setTo(1);
};

///////////////////////////////////////////////////////////////////////
// DisplayBox:  box based on a vertical line scaled horizontally
///////////////////////////////////////////////////////////////////////
TGA.Shared.DisplayBox = function(game, settings){
	// Super call to Phaser.Sprite
	Phaser.Sprite.call(this, game, settings.x, settings.y, settings.key);
	this.scale.x = settings.scaleX;
	this.zI = 2;
	this.textArr = [];
	var body = settings.body || '0';
	this.textOffset = settings.textOffset || 0;

	var textStyle =  {
		font: "12px exomedium",
		fill: "#ffffff",
		align: "left"
		};
	if(settings.header){
		this.textHeader = game.add.text(this.x+this.width/2 + this.textOffset, this.y+22, settings.header, textStyle);
		this.textHeader.zI = 2.1;
		this.textHeader.lineSpacing = 3;
		settings.layer.add(this.textHeader);
	}

	textStyle =  {
		font: "42px exolight",
		fill: "#ffffff",
		align: "right"
	};

	this.textBody = game.add.text(this.x + this.width/2 - 10 + this.textOffset, this.y+16, body, textStyle);
	this.textBody.zI = 2.1;
	this.textBody.anchor.x = 1;
	settings.layer.add(this.textBody);

	this.update = function () {
		if(this.textHeader){
			this.textHeader.x = this.x+this.width/2+this.textOffset;
			this.textHeader.y = this.y + 22;
			this.textHeader.alpha = this.alpha;
		}
		if(this.textBody){
		}

	}.bind(this);

	// this.textArr.push(text);

	// textStyle =  {
	// font: "27px exolight",
	// fill: "#ffffff",
	// align: "center"
	// };
	// text = game.add.text(1, 14, settings.text[1].toUpperCase(), textStyle);
	// text.anchor.setTo(0.5);
	// this.addChild(text);
	// this.textArr.push(text);
	settings.layer.add(this);
};
TGA.Shared.DisplayBox.prototype = Object.create(Phaser.Sprite.prototype);
TGA.Shared.DisplayBox.constructor = TGA.Shared.DisplayBox;
