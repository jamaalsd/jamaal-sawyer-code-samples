var TGA = TGA || {};
TGA.Shared = TGA.Shared || {};
///////////////////////////////////////////////////////////////////////
// PauseMenu: A customizable Pause Menu created when the game is paused
///////////////////////////////////////////////////////////////////////
TGA.Shared.PauseMenu = function(game, parent, pauseButtonKey){
    // Super call to Phaser.Group
    Phaser.Group.call(this, game, parent);

    //add overlay
    this.overlay = new Phaser.Sprite(game, 0, 0, 'black');
    this.overlay.scale = { x: game.width/2, y: game.height/2};
    this.overlay.alpha = 0.33;
    this.add(this.overlay);

    // pause text
    var textStyle =  TGA.TextStyles.getStyle('pauseText');
    this.pauseText = this.game.add.text(game.world.centerX, game.world.centerY, "PAUSED", textStyle);
    this.pauseText.anchor.set(0.5);
    this.add(this.pauseText);

    this.resumeButton = new Phaser.Button(
        this.game,
        this.game.currentActiveState.pauseButton.x,
        this.game.currentActiveState.pauseButton.y,
        pauseButtonKey,
        this.resumeGame,
        this,
        0, 1, 1, 0
    );
    this.resumeButton.input.useHandCursor = true;
    this.resumeButton.events.onInputOver.add(function(){
        var state = this.game.currentActiveState;
        if(state.sfx.buttonOver){
            state.playSound(state.sfx.buttonOver);
        }
    }.bind(this));
    this.add(this.resumeButton);
};
TGA.Shared.PauseMenu.prototype = Object.create(Phaser.Group.prototype);
TGA.Shared.PauseMenu.constructor = TGA.Shared.PauseMenu;
TGA.Shared.PauseMenu.prototype.resumeGame = function (){
    var state = this.game.currentActiveState;
    if(state.resumeGame){
        state.resumeGame();
        if(state.sfx.buttonClick){
            state.playSound(state.sfx.buttonClick);
        }
        this.destroy(true);
    }else{
        console.error('There is no resumeGame function for the current state!');
    }
};
///////////////////////////////////////////////////////////////////////
// InstructionMenu:An Instruction Menu, pauses the game until complete
// arguments are 
// -game-
// the game object
// -parent-
// the layer group to add the menu touch
// -settings-
// instructions, callback, keepAlive, header, firstTime, delay, firstTimeHandler
// delay:delay before start
// instructions: load a set of instruction objects in an array for each instruction
// clicking the next button or triggering the continueInstructions function of this
// object will move through them
// instruction object properties:
//     text:(string)the text to display
//     textPos:({x,y}) where to position the text
//     headerText:(string)if the header needs to change
//     showHeader:(bool)show the header (default is false after first instruction)
//     headerPos:({x,y}) where to position the header
//     showArrow:show the arrow (default is false)
//     arrowPos:({x,y})where to place the arrow
//     arrowRot:(float)rotate arrow in radians
//     arrowScale:({x,y})flip the arrow image
//     noNextButton:dont display the next button
//     wordWrap:set the word wrap of the text
// keepAlive:(bool) shows a single instruction with no way to dismiss
// header:(string) the header text for the first instruction at least
// firstTime:(bool) will route instructions through a first time instruction handler (used for tutorials)
// firstTimeHandler:(function) something that will be triggered after every instruction if firstTime is set

///////////////////////////////////////////////////////////////////////
// instructions, callback, keepAlive, header, firstTime
TGA.Shared.InstructionMenu = function(game, parent, settings){
    // Super call to Phaser.Group
    Phaser.Group.call(this, game, parent);
    this.keepAlive =  settings.keepAlive || false;
    this.instructions = settings.instructions;
    this.fadeTime = 250;
    this.firstTime = settings.firstTime || false;
    this.firstTimeIter = 0;
    this.firstTimeHandler = settings.firstTimeHandler || false;
    var delay = settings.delay || 0;
    //add overlay
    if(settings.callback){
        this.resumeCallback = settings.callback;
    }else{
        this.resumeCallback = function(){};
    }
    
    this.overlay = new Phaser.Sprite(this.game, 0, 0, 'black');
    this.overlay.scale = { x: this.game.width/2, y: this.game.height/2};
    this.overlay.alpha = 0.66;
    this.overlay.inputEnabled = true;
    // this.overlay.events.onInputUp.add(this.continueInstructions, this);
    this.add(this.overlay);

    // instruction text
    var textStyle =  TGA.TextStyles.getStyle('instructionBody');
    this.instructionText = this.game.add.text(this.game.world.centerX - 305, this.game.world.centerY + 110, this.instructions[0], textStyle);
    this.instructionText.wordWrap = true;
    this.instructionText.lineSpacing = 20;

    //header
    textStyle =   TGA.TextStyles.getStyle('instructionHeader');
    this.headerText = this.game.add.text(this.game.world.centerX - 305, this.game.world.centerY + 40, settings.header.toUpperCase(), textStyle);
    this.headerText.wordWrap = true;
    this.headerText.wordWrapWidth = 530;
    this.headerText.lineSpacing = 24;
    this.add(this.headerText);

    this.arrow = new Phaser.Sprite(this.game, 0, 0, 'arrow');
    this.add(this.arrow);
    this.arrow.alpha = 0;

    if(!this.keepAlive){
        //next button
        this.nextButton = new Phaser.Button(
            this.game,
            this.game.world.centerX - 305 + 19*1.33,
            this.game.world.centerY + 220 + 19*1.33,
            'nextButton',
            this.continueInstructions,
            this,
            1,0,1,0
        );
        this.add(this.nextButton);
        this.nextButton.anchor.setTo(0.5);
        this.nextButton.scale.setTo(1.33);
        this.nextButton.input.useHandCursor = true;
        this.nextButton.events.onInputOver.add(function(){
            var state = this.game.currentActiveState;
            if(state.sfx && state.sfx.buttonOver && this.nextButton.alpha !== 0){
                state.playSound(state.sfx.buttonOver);
            }
        }.bind(this));
        this.nextButton.events.onInputDown.add(function(){this.nextButton.scale.setTo(1.23);}.bind(this));
        this.nextButton.events.onInputUp.add(function(){this.nextButton.scale.setTo(1.33);}.bind(this));
        this.nextButton.alpha = 0.0;


        this.nextButton.updatePosition = function () {
            // console.log(this.instructionText.getWordWrapHeight());
            this.nextButton.x = this.instructionText.x + this.instructionText.width - this.nextButton.width/2;
            this.nextButton.y = this.instructionText.y + this.game.currentActiveState.getWordWrapHeight(this.instructionText) + this.nextButton.height/2 + 10;
        }.bind(this);
        this.nextButton.updatePosition();

    }

    this.game.currentActiveState.canClick = false;

    this.updateInstructions();

    this.headerText.alpha = this.instructionText.alpha = this.alpha = 0.0;

    var fadeIn = this.game.add.tween(this).to({ alpha: 1.0}, this.fadeTime, Phaser.Easing.Linear.None, false, delay);
    this.headerText.x += 50;
    var headerIn = this.game.add.tween(this.headerText).to({alpha:1, x: this.headerText.x - 50}, 500, Phaser.Easing.Exponential.Out, false, delay);
    this.instructionText.x += 50;
    var instructionIn = this.game.add.tween(this.instructionText).to({alpha:1, x: this.instructionText.x - 50}, 500, Phaser.Easing.Exponential.Out, false, 200);
    headerIn.onStart.add(function(){instructionIn.start();}.bind(this));
    if(this.nextButton){
        this.nextButton.x += 50;
        var buttonIn = this.game.add.tween(this.nextButton).to({alpha:1, x: this.nextButton.x - 50}, 500, Phaser.Easing.Exponential.Out, false, 400);
        instructionIn.onStart.add(function(){
            buttonIn.start();
            this.game.currentActiveState.canClick = true;
        }.bind(this));
    }
    fadeIn.chain(headerIn);
    fadeIn.start();
    // fadeIn.onComplete.add(function () {
    // this.game.currentActiveState.canClick = true;
    // }.bind(this));

};
TGA.Shared.InstructionMenu.prototype = Object.create(Phaser.Group.prototype);
TGA.Shared.InstructionMenu.constructor = TGA.Shared.InstructionMenu;
TGA.Shared.InstructionMenu.prototype.updateInstructions = function (){
    var iData = this.instructions.shift();
    this.instructionText.x = iData.textPos.x;
    this.instructionText.y = iData.textPos.y;
    this.instructionText.setText(iData.text);
    this.instructionText.wordWrapWidth = iData.wordWrap;
    if(iData.showHeader === undefined || iData.showHeader === false){
        this.headerText.alpha = 0;
    }else{
        this.headerText.alpha = 1;
        this.headerText.x = iData.headerPos.x;
        this.headerText.y = iData.headerPos.y;
        if(iData.headerText){
            this.headerText.setText(iData.headerText);
        }
    }

    if(iData.showArrow === undefined || iData.showArrow === false){
        this.arrow.alpha = 0;
    }else{
        this.arrow.alpha = 1;
        this.arrow.x = iData.arrowPos.x;
        this.arrow.y = iData.arrowPos.y;
        if(iData.arrowRot){
            this.arrow.rotation = iData.arrowRot;
        }else{
            this.arrow.rotation = 0;
        }
        if(iData.arrowScale){
            this.arrow.scale.x = iData.arrowScale.x;
            this.arrow.scale.y = iData.arrowScale.y;
        }
    }

    if(this.nextButton){
        this.nextButton.updatePosition();
    }
    if(iData.noNextButton){
        this.nextButton.x = 10000;
    }
    this.addChild(this.instructionText);
};
TGA.Shared.InstructionMenu.prototype.continueInstructions = function (button){
    var state = this.game.currentActiveState;
    if(button !== undefined){
        if(state.sfx && state.sfx.buttonClick){
            state.playSound(state.sfx.buttonClick);
        }
    }
    if(state.canClick){
        if(this.instructions.length < 1){
            this.resumeGame();
        }else{
            this.updateInstructions();
        }
        if(this.firstTime && this.firstTimeHandler){
            this.firstTimeHandler();
        }
    }
};
TGA.Shared.InstructionMenu.prototype.resumeGame = function (){
    // There is a bug in Phaser that will register a TypeError for cache here sometimes
    
    if(this.game !== null){
        if(!this.keepAlive) {
        this.game.currentActiveState.canClick = false;
        var fadeOut = this.game.add.tween(this).to({ alpha: 0.0}, this.fadeTime, Phaser.Easing.Linear.None,true);
        fadeOut.onComplete.add(function () {
            this.game.currentActiveState.canClick = true;
            this.resumeCallback();
            this.destroy(true);

        }.bind(this));
    }else{
        this.resumeCallback();
    }
    }
};


