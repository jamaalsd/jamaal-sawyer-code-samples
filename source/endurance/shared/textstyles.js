var TGA = TGA || {};
TGA.TextStyles = TGA.TextStyles || {};
TGA.TextStyles.styles = {
    introGameNumber:{
        font: "42px exolight",
        fill: "#ffffff",
        align: "center"
    },

    introGameTitle:{
        font: "100px exoitalic",
        fill: "#ffffff",
        align: "center"
    },

    pauseText:{
        font: "100px exosemibold",
        fill: "#ffffff",
        align: "center"
    },
    
    instructionBody:{
        font: "20px robotolight",
        fill: "#ffffff",
        align: "left"
    },

    instructionHeader:{
        font: "40px exothin",
        fill: "#ffffff",
        align: "left"
    },

    bigButtonTop:{
        font: "26px exoblack",
        fill: "#ffffff",
        align: "center"
    },

    bigButtonBottom:{
        font: "27px exolight",
        fill: "#ffffff",
        align: "center"
    },
    roundCounter:{
        font: "24px exolight",
        fill: "#ffffff",
        align: "left"
    }
};

TGA.TextStyles.getStyle = function (key){
    return TGA.TextStyles.styles[key];
};

module.exports = TGA.TextStyles;