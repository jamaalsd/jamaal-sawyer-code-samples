var TGA = TGA || {};
TGA.Endurance = TGA.Endurance || {};
///////////////////////////////////////////////////////////////////////
// DisplayBox:  box based on a vertical line scaled horizontally
///////////////////////////////////////////////////////////////////////
TGA.Endurance.DisplayBox = function(game, settings){
	// Super call to Phaser.Sprite
	Phaser.Sprite.call(this, game, settings.x, settings.y, settings.key);
	this.scale.x = settings.scaleX;
	this.zI = 2;
	this.textArr = [];
	var body = settings.body || '0';
	var title = settings.title || '';
	this.textOffset = settings.textOffset || 0;
	this.yOffset = settings.yOffset || 0;


	var textStyle =  {
		font: "12px exomedium",
		fill: "#ffffff",
		align: "left"
		};
	if(settings.header){
		this.textHeader = game.add.text(this.x+this.width/2 + this.textOffset, this.y+22+this.yOffset, settings.header, textStyle);
		this.textHeader.zI = 2.1;
		this.textHeader.lineSpacing = 3;
		settings.layer.add(this.textHeader);
	}

	textStyle =  {
		font: "42px exolight",
		fill: "#ffffff",
		align: "right"
	};

	this.textBody = game.add.text(this.x + this.width/2 - 10 + this.textOffset, this.y+16+this.yOffset, body, textStyle);
	this.textBody.zI = 2.1;
	this.textBody.anchor.x = 1;
	settings.layer.add(this.textBody);

	textStyle =  {
		font: "14px exobolditalic",
		fill: "#000000",
		align: "center"
	};

	this.textTitle = game.add.text(this.x + this.width/2, this.y + 7, title, textStyle);
	this.textTitle.zI = 2.1;
	this.textTitle.anchor.x = 0.5;
	settings.layer.add(this.textTitle);

	this.update = function () {
		if(this.textHeader){
			this.textHeader.x = this.x+this.width/2+this.textOffset;
			this.textHeader.y = this.y + 22 + this.yOffset;
			this.textHeader.alpha = this.alpha;
		}
		if(this.textBody){
		}

	}.bind(this);

	// this.textArr.push(text);

	// textStyle =  {
	// font: "27px exolight",
	// fill: "#ffffff",
	// align: "center"
	// };
	// text = game.add.text(1, 14, settings.text[1].toUpperCase(), textStyle);
	// text.anchor.setTo(0.5);
	// this.addChild(text);
	// this.textArr.push(text);
	settings.layer.add(this);
};
TGA.Endurance.DisplayBox.prototype = Object.create(Phaser.Sprite.prototype);
TGA.Endurance.DisplayBox.constructor = TGA.Endurance.DisplayBox;