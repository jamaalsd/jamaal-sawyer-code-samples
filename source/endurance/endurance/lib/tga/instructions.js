var TGA = TGA || {};
TGA.Endurance = TGA.Endurance || {};
////////////////////////////////////////////////////////////////////////////
// firstTimeInstructionHandler: controls the tutorial at the beg of the game
////////////////////////////////////////////////////////////////////////////
TGA.Endurance.firstTimeInstructionHandler = function (){
    var state = this.game.currentActiveState;
    state.firstTimeIter = state.firstTimeIter || 0;
    // console.log(state.firstTimeIter);
    switch(state.firstTimeIter){
        case 0:
            state.transitionLayer.addMultiple([
                state.gridController.grid,
                state.gridController.gridLine,
                state.gridController.heartRateBox.box,
                ]);
            for(var i = 0; i < state.gridController.intervalMarkers.length; i++){
                var m = state.gridController.intervalMarkers[i];
                state.transitionLayer.add(m);
            }
            state.gridController.gridLine.alpha = 1;
            state.gridController.setState('active');
            break;
        case 1:

            break;
        case 2:
            state.gridController.gridLine.alpha = 0;
            state.baseLayer.addMultiple([
                state.gridController.grid,
                state.gridController.gridLine,
                state.gridController.heartRateBox.box,
                ]);
            for(var i = 0; i < state.gridController.intervalMarkers.length; i++){
                var m = state.gridController.intervalMarkers[i];
                state.baseLayer.add(m);
            }

            state.transitionLayer.add(state.sliderController.sliderBack);
            state.sliderController.sliderDragButton.input.draggable = false;
            state.gridController.setState('inactive');
            break;
        case 3:

            break;
        case 4:
            state.transitionLayer.addMultiple([
                state.gridController.grid,
                state.gridController.gridLine,
                state.gridController.heartRateBox.box,
                ]);
            for(var i = 0; i < state.gridController.intervalMarkers.length; i++){
                var m = state.gridController.intervalMarkers[i];
                state.transitionLayer.add(m);
            }
            state.baseLayer.add(state.sliderController.sliderBack);
            state.gridController.setState('active');
            break;
        case 5:
            state.baseLayer.addMultiple([
                state.gridController.grid,
                state.gridController.gridLine,
                state.gridController.heartRateBox.box,
                ]);
            for(var i = 0; i < state.gridController.intervalMarkers.length; i++){
                var m = state.gridController.intervalMarkers[i];
                state.baseLayer.add(m);
            }
            state.gridController.setState('inactive');
            state.baseLayer.sort('zI');
            break;
    }
    state.firstTimeIter++;
};