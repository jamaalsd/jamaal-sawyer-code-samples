var TGA = TGA || {};
TGA.Endurance = TGA.Endurance || {};
///////////////////////////////////////////////////////////////////////
// GridController:  Controls the grid in Endurance
///////////////////////////////////////////////////////////////////////
TGA.Endurance.GridController = function (game, settings){
    // INIT
    this.game = game;

    // GRID
    this.grid = new Phaser.Sprite(this.game, settings.x, settings.y, 'grid_inactive');
    this.grid.zI = 0;
    this.gridBlocks = {x:30,y:24};
    this.gridSpacing = {
        x:this.grid.width/this.gridBlocks.x,
        y:this.grid.height/this.gridBlocks.y

    };
    this.intervalSpacing = this.gridSpacing.x * (this.game.currentActiveState.totalIntervals-1);
    settings.layer.add(this.grid);

    // HEARTRATEBOX
    this.heartRateBox = {};
    this.heartRateBox.box = new Phaser.Sprite(this.game,this.grid.x,this.grid.y,'hrBox');
    this.heartRateBox.box.zI = 5;
    this.heartRateBox.box.animations.add('active',[0]);
    this.heartRateBox.box.animations.add('inactive',[1]);
    this.heartRateBox.box.animations.add('correct',[2]);
    this.heartRateBox.box.animations.add('incorrect',[3]);
    this.heartRateBox.box.animations.play('inactive');

    this.heartRateBox.heart = new Phaser.Sprite(this.game,0,0,'hrBoxHeart');
    this.heartRateBox.heart.anchor.setTo(0.5);
    this.heartRateBox.heart.x = this.heartRateBox.box.width/2;
    this.heartRateBox.heart.y = this.heartRateBox.box.height/2;

    this.heartRateBox.heart.animations.add('active',[0]);
    this.heartRateBox.heart.animations.add('inactive',[1]);
    this.heartRateBox.heart.animations.add('correct',[2]);
    this.heartRateBox.heart.animations.add('incorrect',[3]);
    this.heartRateBox.heart.animations.play('inactive');

    this.heartRateBox.box.addChild(this.heartRateBox.heart);


    var textStyle =  {
        font: "14px exomedium",
        fill: "#ffffff",
        align: "center"
    };
    this.heartRateBox.text = this.game.add.text(0,0,'100',textStyle);
    this.heartRateBox.text.inactiveTextStyle =  {
        font: "14px exomedium",
        fill: "#ffffff",
        align: "center"
    };
    this.heartRateBox.text.correctTextStyle =  {
        font: "14px exomedium",
        fill: "#00FF7C",
        align: "center"
    };
    this.heartRateBox.text.incorrectTextStyle =  {
        font: "14px exomedium",
        fill: "#FF2E36",
        align: "center"
    };
    this.heartRateBox.text.anchor.setTo(0.5);
    this.heartRateBox.text.x = this.heartRateBox.box.width/2+2;
    this.heartRateBox.text.y = this.heartRateBox.box.height/2;
    this.heartRateBox.box.addChild(this.heartRateBox.text);

    settings.layer.add(this.heartRateBox.box);

    // GRID LINE
    this.gridLine = new TGA.Endurance.GridMarkerLine(this.game, {
        x:this.grid.x,
        y:this.grid.y + (this.gridSpacing.y * 15)
    });
    settings.layer.add(this.gridLine);

    // INTERVAL MARKERS
    this.intervalMarkers = [];

    var xOffset = 0;
    var yOffset = 25;
    for(var i = 0; i < this.game.currentActiveState.totalIntervals + 1; i++){
        var pos = {
            x:this.grid.x + i * this.intervalSpacing + xOffset,
            y:this.grid.y + this.grid.height + yOffset
        };
        var interval = new TGA.Endurance.IntervalMarker(this.game,{
            x: pos.x,
            y: pos.y,
            intervalNumber: i
        });
        settings.layer.add(interval);
        this.intervalMarkers.push(interval);
    }

    // LINE CONTROL
    this.lineController = new TGA.Endurance.IntervalLineController(this.game, {
        gridStartPos: {
            x:this.grid.x,
            y:this.grid.y + this.grid.height
        },
        gridSpacing: this.gridSpacing.x,
        intervalSpacing: this.intervalSpacing
    });

    // FUNCTIONS
    this.setIntervalMarker = function(interval){
        for(var i = 0; i < this.intervalMarkers.length; i++){
            var marker = this.intervalMarkers[i];
            if(i === interval){
                marker.isCurrentMarker = true;
            }else{
                marker.isCurrentMarker = false;
            }
        }
    };

    this.setState = function(state){
        this.grid.loadTexture('grid_'+state);
        this.gridLine.setState(state);
        this.lineController.setState(this.game.currentActiveState.currentInterval, state);
        this.heartRateBox.box.animations.play(state);
        this.heartRateBox.heart.animations.play(state);
        var s = (state === 'active'?'inactive':state);
        this.heartRateBox.text.setStyle(this.heartRateBox.text[s+'TextStyle']);
        for(var i = 0; i < this.intervalMarkers.length; i++){
            var marker = this.intervalMarkers[i];
            if(marker.isCurrentMarker){
                marker.setState(state);
                if(state !== 'active'){
                    this.gridLine.checkmark.animations.play(state);
                }else{
                    this.gridLine.checkmark.animations.play('inactive');
                }
                this.gridLine.setCheckmark(this.game.currentActiveState.currentInterval, this.intervalSpacing);
            }else{
                var postCorrect = this.game.currentActiveState.intervalCorrect[marker.intervalNumber-1];
                this.intervalMarkers[i].setState('inactive', this.game.currentActiveState.currentInterval, postCorrect);
            }
        }
    };

    this.setGoal = function(speed){
        this.gridLine.y = this.grid.y +  this.grid.height - (this.gridSpacing.y * 2 * speed);
        this.gridLine.meter.text.setText(this.game.currentActiveState.currentHeartRateDisplay[speed]);
    };

    this.drawLine = function(interval, level){
        return this.lineController.drawLine(interval,level);
    };

    this.killLines = function (){
        this.lineController.killLines();
    };

};
TGA.Endurance.GridController.constructor = TGA.Endurance.GridController;
///////////////////////////////////////////////////////////////////////
// IntervalMarker:  Creates an interval marker for the grid
///////////////////////////////////////////////////////////////////////
TGA.Endurance.IntervalMarker = function(game, settings){
    // Super call to Phaser.Sprite
    Phaser.Sprite.call(this, game, settings.x, settings.y, 'intervalMarker');
    this.intervalNumber = settings.intervalNumber;
    this.zI = 0;
    this.animations.add('inactive',[0]);
    this.animations.add('active',[1]);
    this.animations.add('correct',[2]);
    this.animations.add('incorrect',[3]);
    this.animations.add('inactiveCorrect',[4]);
    this.animations.add('inactiveIncorrect',[5]);
    this.animations.play('inactive');
    this.anchor.setTo(0.5);
    this.darkTextStyle =  {
        font: "14px exomedium",
        fill: "#000000",
        align: "center"
    };
    this.lightTextStyle =  {
        font: "14px exomedium",
        fill: "#ffffff",
        align: "center"
    };
    this.inactivePostTextStyle = {
        font:"14px exomedium",
        fill:"#8B8B8B",
        align:"center"
    };
    this.correctTextStyle =  {
        font: "14px exomedium",
        fill: "#00FF7C",
        align: "center"
    };
    this.incorrectTextStyle =  {
        font: "14px exomedium",
        fill: "#FF2E36",
        align: "center"
    };
    this.text = this.game.add.text(2,4,this.intervalNumber+' ',this.lightTextStyle);
    this.text.anchor.setTo(0.5);
    this.addChild(this.text);
};
TGA.Endurance.IntervalMarker.prototype = Object.create(Phaser.Sprite.prototype);
TGA.Endurance.IntervalMarker.constructor = TGA.Endurance.IntervalMarker;

TGA.Endurance.IntervalMarker.prototype.setState = function (state, currentInterval, intervalCorrect) {
    this.animations.play(state);
    this.text.alpha = 1;
    switch(state){
        case 'inactive':
            if(this.intervalNumber < currentInterval){
                if(this.intervalNumber === 0){
                    this.text.setStyle(this.inactivePostTextStyle);
                }else{
                    this.text.alpha = 0;
                    if(intervalCorrect){
                        this.animations.play('inactiveCorrect');
                    }else{
                        this.animations.play('inactiveIncorrect');
                    }
                }

            }else{
                this.text.setStyle(this.lightTextStyle);
            }
            break;
        case 'active':
            this.text.setStyle(this.darkTextStyle);
            break;
        case 'correct':
            this.text.setStyle(this.correctTextStyle);
            break;
        case 'incorrect':
            this.text.setStyle(this.incorrectTextStyle);
            break;
    }
};
///////////////////////////////////////////////////////////////////////
// GridMarkerLine:  Creates a markerline for the grid
///////////////////////////////////////////////////////////////////////
TGA.Endurance.GridMarkerLine = function(game, settings){
    // Super call to Phaser.Sprite
    Phaser.Sprite.call(this, game, settings.x, settings.y, 'gridMarkerLine');
    this.zI = 3;
    this.anchor.y = 0.5;
    this.animations.add('inactive',[0]);
    this.animations.add('correct',[1]);
    this.animations.add('incorrect',[2]);
    this.animations.play('inactive');

    this.checkmark = new Phaser.Sprite(this.game,0,0,'gridCheckmark');
    this.checkmark.anchor.setTo(0.5);
    this.checkmark.animations.add('inactive',[0]);
    this.checkmark.animations.add('correct',[1]);
    this.checkmark.animations.add('incorrect',[2]);
    this.addChild(this.checkmark);

    this.meter = new Phaser.Sprite(this.game, -5, 0, 'hrGoalBox');
    this.meter.animations.add('inactive',[0]);
    this.meter.animations.add('correct',[1]);
    this.meter.animations.add('incorrect',[2]);
    this.meter.animations.play('inactive');
    this.meter.anchor.x = 1;
    this.meter.anchor.y = 0.5;
    this.addChild(this.meter);

    var textStyle =  {
        font: "20px exomedium",
        fill: "#000000",
        align: "right"
    };

    this.meter.text = this.game.add.text(-20,-14,'100',textStyle);
    this.meter.text.inactiveTextStyle = {
        font:"20px exomedium",
        fill:"#000000",
        align:"right"
    };
    this.meter.text.correctTextStyle =  {
        font: "20px exomedium",
        fill: "#00FF7C",
        align: "right"
    };
    this.meter.text.incorrectTextStyle =  {
        font: "20px exomedium",
        fill: "#FF2E36",
        align: "right"
    };
    this.meter.text.anchor.x = 1;
    this.meter.addChild(this.meter.text);

};
TGA.Endurance.GridMarkerLine.prototype = Object.create(Phaser.Sprite.prototype);
TGA.Endurance.GridMarkerLine.constructor = TGA.Endurance.GridMarkerLine;

TGA.Endurance.GridMarkerLine.prototype.setCheckmark = function (interval, spacing){
    this.checkmark.x = interval * spacing;
};

TGA.Endurance.GridMarkerLine.prototype.setState = function (state){
    if(state === 'active'){
        state = 'inactive';
    }
    this.animations.play(state);
    this.meter.animations.play(state);
    this.checkmark.animations.play(state);
    this.meter.text.setStyle(this.meter.text[state+'TextStyle']);

};




