var TGA = TGA || {};
TGA.Endurance = TGA.Endurance || {};
///////////////////////////////////////////////////////////////////////
// IntervalLineController:  Controls the lines between intervals
///////////////////////////////////////////////////////////////////////
TGA.Endurance.IntervalLineController = function(game, settings){
    // VARIABLES
    this.game = game;
    this.gridStartPos = settings.gridStartPos;
    this.gridSpacing = settings.gridSpacing;
    this.intervalSpacing = settings.intervalSpacing;
    this.lines = [];

// FUNCTIONS
    this.setState = function (activeInterval, state){
        for(var i = 0; i < this.lines.length; i++){
            if(i === activeInterval-1){
                this.lines[i].setState('active',state);
            }else{
                this.lines[i].setState('inactive',state);
            }
        }
    };

    this.drawLine = function(interval, level){
        var state = this.game.currentActiveState;
        var line = new TGA.Endurance.IntervalLine(this.game, {
            x: this.gridStartPos.x + (this.intervalSpacing * (interval - 1)),
            y: this.gridStartPos.y - state.lastLevel * (this.gridSpacing * state.gridsPerSpeedUnit),
            layer: state.baseLayer
        });
        state.baseLayer.sort('zI');
        this.lines.push(line);

        var speed = state.baseLineSpeed + (state.lineSpeedMod) * (level-1);
        var newPos = {
            x: line.x + this.intervalSpacing,
            y: this.gridStartPos.y - (this.gridSpacing * state.gridsPerSpeedUnit)*level
        };
        // var time = state.calculateTimeFromSpeedAndDistance(line.x, line.y, newPos.x, newPos.y, speed);
        var time = state.speedTime[level-1];
        this.setState(interval, 'active');
        line.drawLine(newPos, time);
        return time;
    };

    this.killLastDrawnLine = function(){
        if(this.lines.length > 0){
            var line = this.lines.pop();
            line.destroy();
        }
    };

    this.killLines = function(){
        for(var i = 0; i < this.lines.length; i++){
            this.lines[i].destroy();
        }
        this.lines = [];
    };

};
TGA.Endurance.IntervalLineController.constructor = TGA.Endurance.IntervalLineController;

///////////////////////////////////////////////////////////////////////
// IntervalLine:  The lines between intervals
///////////////////////////////////////////////////////////////////////
TGA.Endurance.IntervalLine = function(game, settings){
    // Super call to Phaser.Sprite
    Phaser.Sprite.call(this, game, settings.x, settings.y, 'line_inactive_inactive');
    this.zI = 2.9;
    this.anchor.y = 0.5;
    settings.layer.add(this);
};
TGA.Endurance.IntervalLine.prototype = Object.create(Phaser.Sprite.prototype);
TGA.Endurance.IntervalLine.constructor = TGA.Endurance.IntervalLine;

TGA.Endurance.IntervalLine.prototype.setState = function (active, state) {
    this.loadTexture('line_'+state+'_'+active);
};

TGA.Endurance.IntervalLine.prototype.drawLine = function (endPos, time){
    this.rotation = this.game.physics.arcade.angleToXY(this,endPos.x,endPos.y);
    var line = new Phaser.Line(this.x, this.y, endPos.x, endPos.y);
    var tween = this.game.add.tween(this.scale).to(
        {x:line.length},
        time,
        Phaser.Easing.Default,
        true
    );
    tween.onComplete.add(function(){
        this.game.currentActiveState.lineDrawnCallback();
    }.bind(this));
};
