var TGA = TGA || {};
TGA.Endurance = TGA.Endurance || {};
TGA.Endurance.SliderController = function (game, settings){
    this.game = game;
    this.state = this.game.currentActiveState;
    // Slider Background
    this.sliderBack = new Phaser.Sprite(this.game, settings.x, settings.y, 'sliderBack');
    this.sliderBack.zI = 0;
    settings.layer.add(this.sliderBack);

    // Slider Start Button
    this.sliderButton = new Phaser.Sprite(this.game, 0, this.sliderBack.height - 50, 'sliderButton');
    this.sliderBack.addChild(this.sliderButton);
    this.sliderButton.animations.add('out',[0]);
    this.sliderButton.animations.add('over',[1]);
    this.sliderButton.animations.play('out');

    var size = ($('body').hasClass('fr')?'20px':'24px');

    this.sliderButton.outTextStyle =  {
        font: size+" exoblack",
        fill: "#FFFFFF",
        align: "center"
    };
    this.sliderButton.overTextStyle =  {
        font: size+" exoblack",
        fill: "#000000",
        align: "center"
    };
    this.sliderButton.pressTextStyle =  {
        font: size+" exoblack",
        fill: "#FFFFFF",
        align: "center"
    };
    this.sliderButton.text =
        this.game.add.text(
            this.sliderButton.width/2, this.sliderButton.height/2,
            this.state.getString('sliderButtonText'), this.sliderButton.outTextStyle);
        this.sliderButton.text.anchor.setTo(0.5);
    this.sliderButton.addChild(this.sliderButton.text);

    this.sliderButton.inputEnabled = true;
    this.sliderButton.enabled = true;
    this.sliderButton.input.useHandCursor = true;
    this.sliderButton.events.onInputOver.add(this.sliderButtonOver, this);
    this.sliderButton.events.onInputOut.add(this.sliderButtonOut, this);
    this.sliderButton.events.onInputDown.add(this.sliderButtonPress, this);
    this.sliderButton.events.onInputUp.add(this.sliderButtonUp, this);

    this.sliderLines = new Phaser.Sprite(this.game, this.sliderBack.width/2, 140, 'sliderLines');
    this.sliderBack.addChild(this.sliderLines);
    this.sliderLines.anchor.x = 0.5;

    this.sliderDragButton = new Phaser.Sprite(this.game, 0, 0, 'sliderDragButton');
    this.sliderDragButton.anchor.setTo(0.5);
    this.sliderLines.addChild(this.sliderDragButton);

    var bounds = new Phaser.Rectangle(0, 0, this.sliderLines.width, this.sliderLines.height);
    this.snapSpacing = 20.25;
    this.sliderDragButton.inputEnabled = true;
    this.sliderDragButton.input.useHandCursor = true;
    this.sliderDragButton.input.enableDrag(false, false, true, 255, bounds);
    this.sliderDragButton.input.enableSnap(60, this.snapSpacing,true,true,0,0);
    this.sliderDragButton.input.setDragLock(false,true);
    this.sliderDragButton.events.onDragStop.add(function(){
        var state = this.game.currentActiveState;
        state.playSound(state.sfx.buttonClick);
    }.bind(this));

    // Slider Text
    this.sliderText = {};
    var textStyle =  {
        font: "12px exomedium",
        fill: "#ffffff",
        align: "center"
    };
    this.sliderText.topTitle = this.game.add.text(
        this.sliderBack.width/2, 32,
        this.state.getString('sliderTitleTop'), textStyle);
    this.sliderText.topTitle.anchor.x = 0.5;
    this.sliderBack.addChild(this.sliderText.topTitle);

    textStyle =  {
        font: "12px exobolditalic",
        fill: "#ffffff",
        align: "center"
    };
    this.sliderText.topDescription = this.game.add.text(
        this.sliderBack.width/2, 60,
        this.state.getString('sliderDescriptionTop'), textStyle);
    this.sliderText.topDescription.anchor.x = 0.5;
    this.sliderBack.addChild(this.sliderText.topDescription);

    textStyle =  {
        font: "12px exomedium",
        fill: "#ffffff",
        align: "center"
    };
    this.sliderText.bottomTitle = this.game.add.text(
        this.sliderBack.width/2, this.sliderBack.height - 120,
        this.state.getString('sliderTitleBottom'), textStyle);
    this.sliderText.bottomTitle.anchor.x = 0.5;
    this.sliderBack.addChild(this.sliderText.bottomTitle);

    textStyle =  {
        font: "12px exobolditalic",
        fill: "#ffffff",
        align: "center"
    };
    this.sliderText.bottomDescription = this.game.add.text(
        this.sliderBack.width/2, this.sliderBack.height - 92,
        this.state.getString('sliderDescriptionBottom'), textStyle);
    this.sliderText.bottomDescription.anchor.x = 0.5;
    this.sliderBack.addChild(this.sliderText.bottomDescription);

    textStyle =  {
        font: "12px exomedium",
        fill: "#ffffff",
        align: "center"
    };

    this.sliderText.topNumber = this.game.add.text(
        this.sliderBack.width/2, 95,
        this.state.speedLevels.toString(), textStyle);
    this.sliderText.topNumber.anchor.x = 0.5;
    this.sliderBack.addChild(this.sliderText.topNumber);

    this.sliderText.bottomNumber = this.game.add.text(
        this.sliderBack.width/2, this.sliderBack.height - 150,
        '1', textStyle);
    this.sliderText.bottomNumber.anchor.x = 0.5;
    this.sliderBack.addChild(this.sliderText.bottomNumber);

    this.overlay = new Phaser.Sprite(this.game, 0,0,'black');
    this.overlay.scale.x = this.sliderBack.width/2;
    this.overlay.scale.y = this.sliderBack.height/2;
    this.overlay.alpha = 0;
    this.sliderBack.addChild(this.overlay);

};
TGA.Endurance.SliderController.prototype = Object.create(Object.prototype);
TGA.Endurance.SliderController.constructor = TGA.Endurance.SliderController;

TGA.Endurance.SliderController.prototype.setLevel = function(level){
    this.sliderDragButton.y = this.snapSpacing * (this.state.speedLevels - level);
    this.state.checkCurrentSpeed();
};

TGA.Endurance.SliderController.prototype.setState = function(state){
    switch(state){
        case 'disabled':
            this.overlay.alpha = 0.5;
            this.sliderButton.enabled = false;
            this.sliderDragButton.input.draggable = false;
            this.sliderButton.text.setStyle(this.sliderButton.outTextStyle);
            break;
        case 'enabled':
            this.overlay.alpha = 0;
            this.sliderDragButton.input.draggable = true;
            this.sliderButton.enabled = true;
            this.sliderButton.text.setStyle(this.sliderButton.outTextStyle);
            break;
    }
};

TGA.Endurance.SliderController.prototype.sliderButtonOver = function(button){
    if((this.state.canClick && !this.state.isPaused && button.enabled) || button.instructionBool){
        button.animations.play('over');
        this.state.playSound(this.state.sfx.buttonOver);
        button.text.setStyle(button.overTextStyle);
    }
};

TGA.Endurance.SliderController.prototype.sliderButtonOut = function(button){
    if((this.state.canClick && !this.state.isPaused && button.enabled) || button.instructionBool){
        button.animations.play('out');
        button.text.setStyle(button.outTextStyle);
    }
};

TGA.Endurance.SliderController.prototype.sliderButtonPress = function(button){
    if((this.state.canClick && !this.state.isPaused && button.enabled) || button.instructionBool){
        button.text.scale.setTo(0.9);
    }
};

TGA.Endurance.SliderController.prototype.sliderButtonUp = function(button){
    var checkOver = Phaser.Circle.intersectsRectangle(this.game.input.activePointer.circle,button.getBounds());
    if((checkOver && this.state.canClick && !this.state.isPaused && button.enabled) || button.instructionBool){
        this.state.sliderButtonClicked();
        this.state.playSound(this.state.sfx.buttonClick);
    }
    button.text.scale.setTo(1.0);
    button.animations.play('out');
};