var NHLEndurance = NHLEndurance || {};
NHLEndurance.Preloader = function (game) {
};

NHLEndurance.Preloader.prototype = {

    preload: function () {
        //  This is where we create sprites from the assets we loaded in Boot.js
        console.log('preloader preload');
        this.createLoadingScreen();
        //  This sets the preloadBar sprite as a loader sprite.
        //  What that does is automatically crop the sprite from 0 to full-width
        //  as the files below are loaded in.
        //this.load.setPreloadSprite(this.preloadBar);
        this.loadShared();

        //  Here we load the rest of the assets our game needs.
        /*Scripts*/
        this.game.load.script('instructionHandler',TGA.LIB_FP+'tga/instructions.js');
        this.game.load.script('eDisplayBox',TGA.LIB_FP+'tga/e_displaybox.js');
        this.game.load.script('gridController',TGA.LIB_FP+'tga/grid.js');
        this.game.load.script('sliderController',TGA.LIB_FP+'tga/slider.js');
        this.game.load.script('lineController',TGA.LIB_FP+'tga/lines.js');

        /*Images*/
        this.game.load.image('background', TGA.MEDIA_FP+'images/background.jpg');
        this.game.load.image('introBackground', TGA.MEDIA_FP+'images/background.jpg');

        this.game.load.image('breathingRateBox', TGA.MEDIA_FP+'images/boxes/breathingBox.png');
        this.game.load.image('heartRateBox', TGA.MEDIA_FP+'images/boxes/heartrateBox.png');
        this.game.load.image('speedBox', TGA.MEDIA_FP+'images/boxes/speedBox.png');

        this.game.load.image('rink', TGA.MEDIA_FP+'images/rink.png');

        this.game.load.image('grid_inactive', TGA.MEDIA_FP+'images/grid/GridInactive.png');
        this.game.load.image('grid_active', TGA.MEDIA_FP+'images/grid/GridActive.png');
        this.game.load.image('grid_correct', TGA.MEDIA_FP+'images/grid/GridCorrect.png');
        this.game.load.image('grid_incorrect', TGA.MEDIA_FP+'images/grid/GridIncorrect.png');


        this.game.load.image('sliderBack', TGA.MEDIA_FP+'images/slider/sliderMenu.png');
        this.game.load.image('sliderLines', TGA.MEDIA_FP+'images/slider/sliderLines.png');
        this.game.load.image('sliderDragButton', TGA.MEDIA_FP+'images/slider/sliderButton.png');

        this.game.load.image('line_active_active', TGA.MEDIA_FP+'images/lines/active_active.png');
        this.game.load.image('line_active_inactive', TGA.MEDIA_FP+'images/lines/active_inactive.png');
        this.game.load.image('line_correct_active', TGA.MEDIA_FP+'images/lines/correct_active.png');
        this.game.load.image('line_correct_inactive', TGA.MEDIA_FP+'images/lines/correct_inactive.png');
        this.game.load.image('line_inactive_inactive', TGA.MEDIA_FP+'images/lines/inactive_inactive.png');
        this.game.load.image('line_inactive_active', TGA.MEDIA_FP+'images/lines/inactive_active.png');
        this.game.load.image('line_incorrect_active', TGA.MEDIA_FP+'images/lines/incorrect_active.png');
        this.game.load.image('line_incorrect_inactive', TGA.MEDIA_FP+'images/lines/incorrect_inactive.png');


        //  + lots of other required assets here
        /*Spritesheets/Atlases*/
        this.game.load.atlas('skater',TGA.MEDIA_FP+'images/spritesheets/partner.png',TGA.MEDIA_FP+'images/spritesheets/partner.json');
        this.game.load.spritesheet('sliderButton', TGA.MEDIA_FP+'images/spritesheets/start_button_147x50.png', 147, 50, 2);
        this.game.load.spritesheet('hrBox',TGA.MEDIA_FP+'images/spritesheets/hr_box_82x81.png', 82, 81, 4);
        this.game.load.spritesheet('hrBoxHeart', TGA.MEDIA_FP+'images/spritesheets/heart_69x68.png',69,68,4);
        this.game.load.spritesheet('intervalMarker', TGA.MEDIA_FP+'images/spritesheets/interval_circle_32x32.png',32,32,6);

        this.game.load.spritesheet('hrGoalBox', TGA.MEDIA_FP+'images/spritesheets/hr_goal_box_91x48.png', 91, 48, 3);
        this.game.load.spritesheet('gridMarkerLine', TGA.MEDIA_FP+'images/spritesheets/hr_lines_608x6.png', 608, 6, 3);
        this.game.load.spritesheet('gridCheckmark', TGA.MEDIA_FP+'images/spritesheets/line_checkmark_31x31.png', 31, 31, 3);
        /*Sound*/
        /*Sound*/
        this.game.load.audio('music',[TGA.SHARED_MEDIA_FP+'audio/music/Sport.mp3',TGA.SHARED_MEDIA_FP+'audio/music/Sport.ogg']);

        this.game.load.audio('swish1',[TGA.SHARED_MEDIA_FP+'audio/sfx/Swoosh_01.mp3',TGA.SHARED_MEDIA_FP+'audio/sfx/Swoosh_01.ogg']);
        this.game.load.audio('swish2',[TGA.SHARED_MEDIA_FP+'audio/sfx/Swoosh_02.mp3',TGA.SHARED_MEDIA_FP+'audio/sfx/Swoosh_02.ogg']);
        this.game.load.audio('countdown',[TGA.SHARED_MEDIA_FP+'audio/sfx/Countdown.mp3',TGA.SHARED_MEDIA_FP+'audio/sfx/Countdown.ogg']);
        this.game.load.audio('buttonClick',[TGA.SHARED_MEDIA_FP+'audio/sfx/Button_Click_2.mp3',TGA.SHARED_MEDIA_FP+'audio/sfx/Button_Click_2.ogg']);
        this.game.load.audio('buttonOver',[TGA.SHARED_MEDIA_FP+'audio/sfx/Button_Rollover.mp3',TGA.SHARED_MEDIA_FP+'audio/sfx/Button_Rollover.ogg']);
        this.game.load.audio('incorrect',[TGA.SHARED_MEDIA_FP+'audio/sfx/Answer_Incorrect.mp3',TGA.SHARED_MEDIA_FP+'audio/sfx/Answer_Incorrect.ogg']);
        this.game.load.audio('correct',[TGA.SHARED_MEDIA_FP+'audio/sfx/Success_Sound.mp3',TGA.SHARED_MEDIA_FP+'audio/sfx/Success_Sound.ogg']);
        this.game.load.audio('win',[TGA.SHARED_MEDIA_FP+'audio/sfx/Yeah.mp3',TGA.SHARED_MEDIA_FP+'audio/sfx/Yeah.ogg']);

        this.game.load.audio('heartbeatSlow',[TGA.SHARED_MEDIA_FP+'audio/sfx/heartbeat_slow.mp3',TGA.SHARED_MEDIA_FP+'audio/sfx/heartbeat_slow.ogg']);
        this.game.load.audio('heartbeatMedium',[TGA.SHARED_MEDIA_FP+'audio/sfx/heartbeat_medium.mp3',TGA.SHARED_MEDIA_FP+'audio/sfx/heartbeat_medium.ogg']);
        this.game.load.audio('heartbeatFast',[TGA.SHARED_MEDIA_FP+'audio/sfx/heartbeat_fast.mp3',TGA.SHARED_MEDIA_FP+'audio/sfx/heartbeat_fast.ogg']);


    },

    create: function () {
        console.log('preloader create');
    },

    runOnce: false,
    update: function () {
        if(!this.runOnce && TGA.inEverFiApp){
            this.runOnce = true;
            // this.game.HTML5GameService.repositionCanvas();
        }else{
            this.runOnce = true;
            centerCanvas();
        }

        if(!this.ready){
            this.ready = true;
            this.game.time.events.add(Phaser.Timer.SECOND * 0.25, this.prepareForNextScene, this);
        }
    },

    createLoadingScreen: function (){
        this.background = this.add.image(0, 0, 'loadBackground');
        this.gear = this.add.image(this.world.width/2, this.world.height/2, 'spinningGear');
        this.gear.alpha = 0.25;
        this.gear.anchor.setTo(0.5);
        this.gear.update = function(){
            this.angle += 1.0;
        }.bind(this.gear);

    },

    prepareForNextScene: function(){
        var tween = this.game.add.tween(this.gear).to({alpha:0},330,Phaser.Easing.Linear.None, true);
        tween.onComplete.add(function(){this.nextScene();}.bind(this));
    },

    nextScene: function () {
        this.state.start('Game');
    },

    loadShared: function() {
        this.game.load.script('blurX',TGA.SHARED_LIB_FP+'filters/BlurX.js');
        this.game.load.script('blurY',TGA.SHARED_LIB_FP+'filters/BlurY.js');
        this.game.load.script('sharedTextStyleScript',TGA.SHARED_LIB_FP+'tga/textstyles.js');
        this.game.load.script('sharedUIElementsScript',TGA.SHARED_LIB_FP+'tga/ui_elements.js');
        this.game.load.script('sharedMenusScript',TGA.SHARED_LIB_FP+'tga/menus.js');
        this.game.load.script('sharedHelperScript',TGA.SHARED_LIB_FP+'tga/helper.js');

        this.game.load.image('arrow', TGA.SHARED_MEDIA_FP+'images/arrow.png');
        this.game.load.image('black', TGA.SHARED_MEDIA_FP+'images/blackDot.png');
        this.game.load.image('introGameNumber', TGA.SHARED_MEDIA_FP+'images/BlueBarGameNumber.png');

        this.game.load.spritesheet('timer', TGA.SHARED_MEDIA_FP+'images/spritesheets/timer_302x302.png', 302, 302, 4);
        this.game.load.spritesheet('nextButton', TGA.SHARED_MEDIA_FP+'images/spritesheets/next_arrow_60x60.png', 60, 60, 2);
        this.game.load.spritesheet('pauseButtonLight', TGA.SHARED_MEDIA_FP+'images/spritesheets/pauseButtonSpriteSheet_light_46x46.png',46,46,2);
        this.game.load.spritesheet('pauseButtonDark', TGA.SHARED_MEDIA_FP+'images/spritesheets/pauseButtonSpriteSheet_dark_46x46.png',46,46,2);
        this.game.load.spritesheet('bigButton', TGA.SHARED_MEDIA_FP+'images/spritesheets/button_214x215.png',214,215,3);
    }

};
