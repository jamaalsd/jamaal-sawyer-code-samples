/*hook for custom classes & methods */
window.TGA = {};
var NHLEndurance = NHLEndurance || {};
NHLEndurance.Boot = function (game) {

};

NHLEndurance.Boot.prototype = {

    preload: function () {
        console.log('boot preload');
        TGA.inEverFiApp = (this.game.HTML5GameService !== undefined)? true:false;
        if(TGA.inEverFiApp){
            console.log('in the EverFi app');
        }else{
            console.log('being run locally');
        }
        this.initFilePath(TGA.inEverFiApp);
        this.centerCanvas(TGA.inEverFiApp);
        //  Here we load the assets required for our preloader (in this case a background and a loading bar)
        this.load.image('loadBackground',  TGA.MEDIA_FP+'images/introBackground.jpg');
        this.load.image('spinningGear',  TGA.SHARED_MEDIA_FP+'images/spinningGear.png');
    },

    create: function () {
        console.log('boot create');
        //  Unless you specifically know your game needs to support multi-touch I would recommend setting this to 1
        this.input.maxPointers = 1;

        //  Phaser will automatically pause if the browser tab the game is in loses focus. You can disable that here:
        // this.stage.disableVisibilityChange = true;

        //  Same goes for mobile settings.
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        // this.scale.minWidth = 480;
        // this.scale.minHeight = 260;
        // this.scale.maxWidth = 1280;
        // this.scale.maxHeight = 720;
        // this.scale.forceLandscape = true;
        this.game.scale.pageAlignHorizontally = false;
        // this.game.scale.pageAlignVertically = true;
        // this.game.scale.refresh();
        this.scale.setScreenSize(true);

        //  By this point the preloader assets have loaded to the cache, we've set the game settings
        //  So now let's start the real preloader going
        this.state.start('Preloader');
    },

    initFilePath:function (inApp){
        if(!inApp){
            TGA.MEDIA_FP = 'media/';
            TGA.SHARED_MEDIA_FP = '../shared/assets/';
            TGA.LIB_FP = 'lib/';
            TGA.SHARED_LIB_FP = '../shared/assets/javascripts/';
        }else{
            TGA.MEDIA_FP = 'media/';
            TGA.SHARED_MEDIA_FP = '../shared/assets/';
            TGA.LIB_FP = 'lib/';
            TGA.SHARED_LIB_FP = '../shared/assets/javascripts/';
        }

    },

    centerCanvas:function(inApp){
        if(!inApp){
            centerCanvas();
        }else{
            // this.game.HTML5GameService.centerCanvas();
        }
    },

};
