var NHLEndurance = NHLEndurance || {};
NHLEndurance.Game = function (game) {

    /*When a State is added to Phaser it automatically has the following properties set on it, even if they already exist:

    game;       //  a reference to the currently running game
    this.add;       //  used to add sprites, text, groups, etc
    this.camera;    //  a reference to the game camera
    this.cache;     //  the game cache
    this.input;     //  the global input manager (you can access this.input.keyboard, this.input.mouse, as well from it)
    this.load;      //  for preloading assets
    this.math;      //  lots of useful common math operations
    this.sound;     //  the sound manager - add a sound, play one, set-up markers, etc
    this.stage;     //  the game stage
    this.time;      //  the clock
    this.tweens;    //  the tween manager
    this.state;     //  the state manager
    this.world;     //  the game world
    this.particles; //  the particle manager
    this.physics;   //  the physics manager
    this.rnd;       //  the repeatable random number generator

    //  You can use any of these from any function within this State.
    //  But do consider them as being 'reserved words', i.e. don't create a property for your own game called "world" or you'll over-write the world reference.*/
};

NHLEndurance.Game.prototype = {
    /*Debug Variables*/

    debugSkipIntro: false,
    debugSkipInstructions: false,
    debugDrawSkatePath: false,


    DEBUG_MUTE_MUSIC : false,
    DEBUG_MUTE_SFX: false,
    /*VARIABLE DECLARATIONS (names)*/
    backgroundLayer : Phaser.Group,
    baseLayer       : Phaser.Group,
    upperLayer      : Phaser.Group,
    transitionLayer : Phaser.Group,

    /*Game Flags*/
    isPaused: true,
    canClick: true,
    firstTime: true,
    /*Array to stuff tweens so I can stop them later by name*/
    tweenArray : [],

    /*Sound*/
    GAME_MUSIC_VOL : 0.5,
    GAME_SFX_VOL : 0.5,

    /*Game Variables*/

    currentRound: 1,
    totalRounds: 3,
    roundResults:[],

    currentInterval: 1,
    totalIntervals: 6,
    intervalCorrect: [
        true,
        true,
        true,
        true,
        true,
        true
    ],
    gridsPerSpeedUnit: 2,
    currentHeartRate:0,
    currentGoalHeartRate:0,
    currentBreathingRate:0,
    currentSpeed:0,
    speedLevels: 12,
    lastLevel:0,
    runningHeartRate: 60,
    runningBreathingRate: 12,
    currentHeartRateDisplay:[
        60,
        100,
        108,
        116,
        124,
        132,
        140,
        148,
        156,
        164,
        168,
        170,
        172
    ],
    currentBreathingRateDisplay:[
        12,
        16,
        20,
        24,
        28,
        32,
        36,
        40,
        44,
        48,
        52,
        56,
        60
    ],

    skaterCanMove: false,
    skaterAnimationBaseSpeed: 24,
    skaterAnimationAcceleration: 4,
    skatePathPoints : {
        x: [ 468, 438, 282, 252, 282, 438, 468 ],
        y: [ 430, 223, 223, 430, 635, 635, 430 ]
    },

    pathIter: 0,
    skatePath: [],

    speedTime:[
        5000,
        4750,
        4500,
        4250,
        4000,
        3750,
        3500,
        3250,
        3000,
        2750,
        2500,
        2250
    ],

    correctIntervalValues:[
        // Hill
        [4,8,11,8,4,1],
        // Interval
        [10,3,10,3,10,3],
        // Varied
        [5,3,8,12,6,2]
    ],


    /*create loop called first to build the scene*/
    create: function () {
        console.log('game create');
        this.game.stage.smoothed = false;
        // Need to hold a pointer to the currentActiveState for extended class functions
        // Not necessary in the strictest sense but if we don't do this we have to keep
        // calling the getCurrentState function.
        this.game.currentActiveState = this.game.state.getCurrentState();
        // Kick off the Physics system here
        this.game.physics.startSystem(Phaser.Physics.ARCADE);

        // Initialize things, these are placeholder functions for things that I always tend to write.
        TGA.Shared.initHelperFunctions(this);
        this.initSound();
        this.initLayers();
        this.initUI();

        // Sorting our base layer according to a zI (zIndex) value we've given things
        this.baseLayer.sort('zI');
        this.createSkatePath();

        this.checkCurrentSpeed();
        this.updateHeartBreathingRates(0);
        // Start the transition into the game
        this.initTransition();
    },

    /*this is our main game update loop*/
    update: function () {
        if(!this.isPaused){
            if(this.skaterCanMove){
                if(this.skater.animations.paused){
                    this.skater.animations.paused = false;
                }
                this.skaterMovement();
            }else{
                if(!this.skater.animations.paused){
                    this.skater.animations.paused = true;
                }
            }
        }
        this.checkCurrentSpeed();
        this.checkRunningHeartRate();
    },

    /*render called after update*/
    render: function () {

    },

    /*custom pause handler*/
    pauseGame: function () {
        if(!this.isPaused){
            this.game.tweens.pauseAll();
            this.game.sound.pauseAll();
            this.isPaused = true;
            var pauseMenu = new TGA.Shared.PauseMenu(this.game,this.upperLayer, this.pauseButton.key);
            this.pauseButton.alpha = 0.0;
            if(this.game.device.webGL){
                this.blurXBase.blur = 6;
                this.blurYBase.blur = 6;
                this.blurXBack.blur = 6;
                this.blurYBack.blur = 6;
                this.baseLayer.filters = [this.blurXBase, this.blurYBase];
            }
            if(this.timer){
                this.timer.animations.paused = true;
            }
            if(this.skaterCanMove){
                this.skater.animations.paused = true;
            }

        }
    },
    /*custom resume handler*/
    resumeGame: function (menu) {
        this.game.tweens.resumeAll();
        this.game.sound.resumeAll();
        this.pauseButton.alpha = 1.0;
        this.isPaused = false;
        if(this.game.device.webGL){
            this.blurXBase.blur = 0;
            this.blurYBase.blur = 0;
            this.blurXBack.blur = 0;
            this.blurYBack.blur = 0;
            this.baseLayer.filters = null;
        }
        if(this.timer){
            this.timer.animations.paused = false;
        }
        if(this.skaterCanMove){
            this.skater.animations.paused = false;
        }

    },

    /*GAME*/
    checkCurrentSpeed: function() {
        var speedSetting = Math.abs(this.sliderController.sliderDragButton.y/this.sliderController.snapSpacing -  this.speedLevels);
        if(this.currentSpeed !== speedSetting){
            this.currentSpeed = speedSetting;
        }
    },

    checkRunningHeartRate: function(){
        if(parseInt(this.gridController.heartRateBox.text.text, 10) !== Math.floor(this.runningHeartRate)){
            this.gridController.heartRateBox.text.setText(Math.floor(this.runningHeartRate)+' ');
            this.heartRateBox.textBody.setText(Math.floor(this.runningHeartRate));
            this.breathingRateBox.textBody.setText(Math.floor(this.runningBreathingRate));
        }

    },

    sliderButtonClicked: function () {
        if(this.currentInterval <= this.totalIntervals){
            this.sliderController.setState('disabled');
            var time = this.gridController.drawLine(this.currentInterval, this.currentSpeed);
            this.tweenSkaterMovement(time);
            this.tweenRunningHeartRate(time);
            if(this.currentSpeed < 5){
                this.playSound(this.sfx.heartbeatSlow);
            }else if(this.currentSpeed < 9){
                this.playSound(this.sfx.heartbeatMedium);
            }else{
                this.playSound(this.sfx.heartbeatFast);
            }
        }
    },

    // LINE CALLBACK
    lineDrawnCallback: function(){
        this.updateHeartBreathingRates(this.currentSpeed);
        this.game.time.events.add(Phaser.Timer.SECOND * 0.0, function (){this.game.currentActiveState.checkCorrect();}.bind(this));
    },

    updateHeartBreathingRates: function (speed){
        this.currentHeartRate = this.currentHeartRateDisplay[speed];
        this.currentBreathingRate = this.currentBreathingRateDisplay[speed];
    },

    setNextInterval: function(){
        this.currentInterval++;
        this.gridController.setIntervalMarker(this.currentInterval);
        this.lastLevel = this.currentSpeed;
    },

    prepareRound: function () {
        this.resetRound();
        this.startRound();

    },

    resetRound: function (){
        this.gridController.gridLine.alpha = 0;
        this.gridController.setIntervalMarker(1);
        this.gridController.setState('inactive');
        this.sliderController.setLevel(1);
        this.sliderController.setState('disabled');
        this.updateHeartBreathingRates(0);
        this.gridController.killLines();
        this.currentInterval = 1;
        this.lastLevel = 0;
    },

    checkCorrect: function () {
        var correctAnswer = this.correctIntervalValues[this.currentRound-1][this.currentInterval-1];
        var instructions,instructionsHeader;
        var roundComplete = (this.currentInterval >= this.totalIntervals)?true:false;

        if(this.currentSpeed === correctAnswer){
            this.intervalCorrect[this.currentInterval-1] = true;
            this.gridController.setState('correct');
            this.playSound(this.sfx.correct);
            instructions = [{
                text : this.getString("intervalCorrectBody"),
                textPos : {x : 425, y: 350},
                wordWrap: 450,
                showHeader: true,
                headerPos: {x: 425, y: 270}
            }];
            instructionsHeader = this.getString("intervalCorrectHeader");
        }else{
            this.intervalCorrect[this.currentInterval-1] = false;
            this.playSound(this.sfx.incorrect);
            this.gridController.setState('incorrect');
            var text;
            if(roundComplete){
                text = this.getString("intervalIncorrectBodyLast");
            }else{
                text = this.getString("intervalIncorrectBody");
            }
            instructions = [{
                text : text,
                textPos : {x : 425, y: 350},
                wordWrap: 450,
                showHeader: true,
                headerPos: {x: 425, y: 270}
            }];
            instructionsHeader = this.getString("intervalIncorrectHeader");

        }

        var completeFunction;

        if(!roundComplete){
            completeFunction = function (){
                this.currentInterval++;
                this.gridController.setIntervalMarker(this.currentInterval);
                this.gridController.setGoal(this.correctIntervalValues[this.currentRound-1][this.currentInterval-1]);
                this.gridController.setState('active');
                this.sliderController.setState('enabled');
                this.lastLevel = this.currentSpeed;
            }.bind(this);
        }else{
            completeFunction = function (){
                this.roundComplete();
            }.bind(this);
        }
        var eventFunction = function (){
            this.completeInstructions = new TGA.Shared.InstructionMenu(this.game, this.upperLayer, {
                instructions: instructions,
                callback: completeFunction,
                header: instructionsHeader
            });
        }.bind(this);

        this.game.time.events.add(Phaser.Timer.SECOND * 1, eventFunction, this);

    },

    startRound: function () {
        var timerCompeleteFunction = function () {
            this.canClick = true;
            this.gridController.setState('active');
            this.sliderController.setState('enabled');
            this.gridController.setGoal(this.correctIntervalValues[this.currentRound-1][this.currentInterval-1]);
            this.gridController.gridLine.alpha = 1;
        }.bind(this);
        this.timer.countdown(timerCompeleteFunction);
    },

    roundComplete: function () {
        console.log('ROUND COMPLETE');
        this.currentRound++;
        if(this.currentRound <= this.totalRounds){
            var instructions = [{
                text : this.getString("roundCompleteBody"),
                textPos : {x : 425, y: 350},
                wordWrap: 450,
                showHeader: true,
                headerPos: {x: 425, y: 270}
            }];
            var instructionsHeader = this.getString("roundCompleteHeader");
            var completeFunction = function (){

                this.updateRoundCounter();
                this.prepareRound();
            }.bind(this);
            this.completeInstructions = new TGA.Shared.InstructionMenu(this.game, this.upperLayer, {
                instructions: instructions,
                callback: completeFunction,
                header: instructionsHeader
            });

        }else{
            this.gameComplete();
        }

    },

    gameComplete: function (){
        this.endGame();
        this.playSound(this.sfx.gameWin);
        console.log('GAME COMPLETE');
        var instructions = [{
            text : this.getString("gameCompleteBody"),
            textPos : {x : 425, y: 350},
            wordWrap: 450,
            showHeader: true,
            headerPos: {x: 425, y: 270},
            noNextButton:true
        }];
        var instructionsHeader = this.getString("gameCompleteHeader");
        this.completeInstructions = new TGA.Shared.InstructionMenu(this.game, this.upperLayer, {
            instructions: instructions,
            header: instructionsHeader
        });
    },

    updateRoundCounter: function(){
        this.roundCounter.setText(this.getString('roundCounter')+' '+this.currentRound+'/'+this.totalRounds);
    },

    createSkatePath: function(){
        if(this.debugDrawSkatePath) this.bmd.clear();

        var x = (1 / (this.game.width * 2) );
        var xi = 0;

        for (var i = 0; i <= 1; i += x) {
            // var px = this.math.linearInterpolation(this.points.x, i);
            // var py = this.math.linearInterpolation(this.points.y, i);

            var px = this.math.catmullRomInterpolation(this.skatePathPoints.x, i);
            var py = this.math.catmullRomInterpolation(this.skatePathPoints.y, i);

            var node = { x: px, y: py, angle: 0 };

            if(xi > 0 ){
                node.angle = this.math.angleBetweenPoints(this.skatePath[xi-1], node) + Math.PI/2;
            }
            xi++;
            this.skatePath.push(node);


            if(this.debugDrawSkatePath) this.bmd.rect(px, py, 1, 1, 'rgba(255, 255, 255, 1)');
        }

        if(this.debugDrawSkatePath){
            for (var p = 0; p < this.skatePathPoints.x.length; p++){
                this.bmd.rect(this.skatePathPoints.x[p]-3, this.skatePathPoints.y[p]-3, 6, 6, 'rgba(255, 0, 0, 1)');
            }
        }

        this.skatePath[0].angle = this.math.angleBetweenPoints(this.skatePath[this.skatePath.length-1],this.skatePath[0]) + Math.PI/2;
    },

    skaterMovement: function(){
        var pI = Math.floor(this.pathIter);
        this.skater.x = this.skatePath[pI].x;
        this.skater.y = this.skatePath[pI].y;
        this.skater.rotation = this.skatePath[pI].angle;
        if(this.pathIter >= this.skatePath.length){
            this.pathIter = 0;
        }
    },

    tweenSkaterMovement: function (time){
        this.pathIter = 0;
        var tween = this.game.add.tween(this).to({pathIter:this.skatePath.length}, time, Phaser.Easing.Linear.None, false);

        tween.onStart.add(function(){
            this.skater.animations.currentAnim.speed = this.skaterAnimationBaseSpeed + this.currentSpeed * this.skaterAnimationAcceleration;
            this.skater.animations.paused = false;
            this.skaterCanMove = true;
        }.bind(this));

        tween.onComplete.add(function(){
            this.skater.animations.currentAnim.restart();
            this.skater.animations.paused = true;
            this.skaterCanMove = false;
            this.sfx.heartbeatSlow.stop();
            this.sfx.heartbeatMedium.stop();
            this.sfx.heartbeatFast.stop();
        }.bind(this));

        tween.start();
    },

    tweenRunningHeartRate: function (time){
        this.game.add.tween(this).to({runningHeartRate:this.currentHeartRateDisplay[this.currentSpeed]}, time, Phaser.Easing.Linear.None, true);
        this.game.add.tween(this).to({runningBreathingRate:this.currentBreathingRateDisplay[this.currentSpeed]}, time, Phaser.Easing.Linear.None, true);
    },

    /*INIT*/
    initUI: function () {

        this.background = new Phaser.Image(this.game, 0, 0, 'background');
        this.backgroundLayer.add(this.background);
        // Blur Filters
        if(this.game.device.webGL){
            this.blurXBack =this.game.add.filter('BlurX');
            this.blurYBack =this.game.add.filter('BlurY');
            this.blurXBase =this.game.add.filter('BlurX');
            this.blurYBase =this.game.add.filter('BlurY');
            this.backgroundLayer.filters = [this.blurXBack, this.blurYBack];
            this.blurXBase.blur = 0;
            this.blurYBase.blur = 0;
            this.blurXBack.blur = 0;
            this.blurYBack.blur = 0;
        }

        this.underlay = new Phaser.Image(this.game, 0, 0, 'black');
        this.underlay.zI = -10;
        this.baseLayer.add(this.underlay);
        this.underlay.scale.x = this.game.world.width/2;
        this.underlay.scale.y = this.game.world.height/2;
        this.underlay.alpha = 0;


        // Pause Button
        this.pauseButton = new Phaser.Button(
            this.game,
            20,
            20,
            'pauseButtonLight',
            this.pauseGame,
            this,
            0, 1, 1, 0
        );
        this.pauseButton.events.onInputOver.add(function(){
            var state = this.game.currentActiveState;
            if(state.sfx && state.sfx.buttonOver){
                state.playSound(state.sfx.buttonOver);
            }
        }.bind(this));
        this.upperLayer.add(this.pauseButton);
        this.pauseButton.input.useHandCursor = true;

        //Round Counter
        var textStyle =  this.getTextStyle('roundCounter');

        this.roundCounter = this.game.add.text(85, 30, 'ROUND '+this.currentRound+'/'+this.totalRounds, textStyle);
        this.roundCounter.zI = 10;
        this.baseLayer.add(this.roundCounter);
        this.updateRoundCounter();

        // Timer
        this.timer = new TGA.Shared.Timer(this.game, this.game.world.width/2, this.game.world.height/2, this.baseLayer);

        // Display Boxes
        this.speedBox = new TGA.Endurance.DisplayBox(this.game, {
            x:640, y:28, key:'speedBox', scaleX:170, layer: this.baseLayer,
            textOffset:0, yOffset:24,
            title:this.getString('boxTextSpeedTitle'),
            header:this.getString('boxTextSpeed'),
            body:'0'
        });
        this.speedBox.update = function(){
            if(parseInt(this.speedBox.textBody.text, 10) !== this.currentSpeed){
                this.speedBox.textBody.setText(this.currentSpeed);
            }
        }.bind(this);


        this.heartRateBox = new TGA.Endurance.DisplayBox(this.game, {
            x:830, y:28, key:'heartRateBox', scaleX:210, layer: this.baseLayer,
            textOffset:0, yOffset:24,
            title:this.getString('boxTextHeartRateTitle'),
            header:this.getString('boxTextHeartRate'),
            body:'0'
        });
        // this.heartRateBox.update = function(){
        //     if(parseInt(this.heartRateBox.textBody.text, 10) !== this.currentHeartRate){
        //         this.heartRateBox.textBody.setText(this.currentHeartRate);
        //     }
        // }.bind(this);

        this.breathingRateBox = new TGA.Endurance.DisplayBox(this.game, {
            x:1060, y:28, key:'breathingRateBox', scaleX:190, layer: this.baseLayer,
            textOffset:0, yOffset:24,
            title:this.getString('boxTextBreathingRateTitle'),
            header:this.getString('boxTextBreathingRate'),
            body:'0'
        });
        // this.breathingRateBox.update = function(){
        //     if(parseFloat(this.breathingRateBox.textBody.text) !== this.currentBreathingRate){
        //         this.breathingRateBox.textBody.setText(this.currentBreathingRate);
        //     }
        // }.bind(this);

        // Rink
        this.rink = new Phaser.Image(this.game, 160, 150, 'rink');
        this.rink.zI = 0;
        this.baseLayer.add(this.rink);

        if(this.debugDrawSkatePath){
            this.bmd = this.add.bitmapData(this.game.width, this.game.height);
            this.bmd.addToWorld();
        }

        // Skater
        this.skater = new Phaser.Sprite(this.game, this.skatePathPoints.x[0], this.skatePathPoints.y[0], 'skater');
        this.skater.zI = 1;
        this.skater.anchor.setTo(0.5);
        this.skater.scale.setTo(0.5);
        this.baseLayer.add(this.skater);
        this.skater.animations.add('skating', [ 10,11,12,13,14,15,16,17,18,19,
                                                20,21,22,23,24,25,26,27,28,29,
                                                30,31,32,33,34,35,36,37,38,39,
                                                40,41,42,43,44,45,46,47,48,49,
                                                50,51,52,53,54,55,56,57,58,59,
                                                60,61,61,61,61,61,61,61,61,61,
                                                61,61,61,61,61,61,61,61,61,61,
                                                61,61,61,61,61,61,61,61,61,61,
                                                61,61,61,61,61,61,61,61,61,61],this.skaterAnimationBaseSpeed,true);
        this.skater.animations.add('idle',[10]);
        this.skater.animations.play('skating');
        this.skater.animations.paused = true;

        this.gridController = new TGA.Endurance.GridController(this.game,{
            x:640, y:160, layer:this.baseLayer
        });
        this.gridController.setIntervalMarker(1);
        this.gridController.setGoal(6);
        this.gridController.setState('inactive');
        this.gridController.gridLine.alpha = 0;

        this.sliderController = new TGA.Endurance.SliderController(this.game,{
            x:35, y:160, layer:this.baseLayer
        });
        this.sliderController.setLevel(1);
        this.sliderController.setState('enabled');



    },

    initLayers: function () {
        this.backgroundLayer    =this.game.add.group();
        this.baseLayer          =this.game.add.group();
        this.upperLayer         =this.game.add.group();
        this.transitionLayer    =this.game.add.group();
    },

    initSound: function(){
        if(this.DEBUG_MUTE_MUSIC) this.GAME_MUSIC_VOL = 0;
        if(this.DEBUG_MUTE_SFX) this.GAME_SFX_VOL = 0;
        var music = this.game.sound.add('music',this.GAME_MUSIC_VOL,true);
        this.playSound(music);

        this.sfx = {};
        this.sfx.correct = this.game.sound.add('correct', this.GAME_SFX_VOL);
        this.sfx.incorrect = this.game.sound.add('incorrect', this.GAME_SFX_VOL);
        this.sfx.swish1 = this.game.sound.add('swish1', this.GAME_SFX_VOL);
        this.sfx.swish2 = this.game.sound.add('swish2', this.GAME_SFX_VOL);
        this.sfx.countdown = this.game.sound.add('countdown', this.GAME_SFX_VOL);
        this.sfx.buttonClick = this.game.sound.add('buttonClick', this.GAME_SFX_VOL);
        this.sfx.buttonOver = this.game.sound.add('buttonOver', this.GAME_SFX_VOL);
        this.sfx.gameWin = this.game.sound.add('win',this.GAME_SFX_VOL);

        this.sfx.heartbeatSlow = this.game.sound.add('heartbeatSlow',this.GAME_SFX_VOL);
        this.sfx.heartbeatMedium = this.game.sound.add('heartbeatMedium',this.GAME_SFX_VOL);
        this.sfx.heartbeatFast = this.game.sound.add('heartbeatFast',this.GAME_SFX_VOL);


    },

    initTransition: function () {
        this.initGame();
    },

    initGame: function() {
        if(!this.debugSkipIntro){
            this.startIntro();
        }else{
            this.startInstructions();
        }
    },

    startIntro: function(){
        this.game.currentActiveState.canClick = false;
        // var viewBlurredBack  = this.transitionLayer.create(0,0, "blurredBackground");
        var viewBack  = this.transitionLayer.create(0,0, "background");
        var viewFront = this.transitionLayer.create(this.game.world.centerX, this.game.world.centerY, "introBackground");
        viewFront.anchor.setTo(0.5);
        var overlay = this.transitionLayer.create(0,0, "black");
        overlay.scale.x = this.game.world.width/2;
        overlay.scale.y = this.game.world.height/2;
        var gameNumber = this.transitionLayer.create(this.game.world.centerX, this.game.world.centerY-110, "introGameNumber");
        gameNumber.anchor.setTo(0.5);

        var textStyle =  TGA.TextStyles.getStyle('introGameNumber');
        var gameText = this.game.add.text(0, 0, this.getString('gameNumber').toUpperCase(), textStyle);
        gameText.anchor.setTo(0.5);
        gameNumber.addChild(gameText);

        textStyle =  TGA.TextStyles.getStyle('introGameTitle');
        var gameTitle = this.game.add.text(this.game.world.centerX, this.game.world.centerY, this.getString('gameTitle').toUpperCase(), textStyle);
        gameTitle.anchor.setTo(0.5);

        this.transitionLayer.add(gameTitle);

        overlay.alpha = 0.0;
        gameNumber.alpha = 0.0;
        gameTitle.alpha = 0.0;

        var tween1 = this.game.add.tween(overlay).to({alpha:0.5}, 1000, Phaser.Easing.Default, false, 500);

        var tDist = 200;
        gameNumber.x += tDist;
        gameTitle.x -=  tDist;
        var tween2 = this.game.add.tween(gameNumber).to({x:gameNumber.x - tDist, alpha: 1}, 750, Phaser.Easing.Exponential.Out, false, 1000);
        var tween2_1 = this.game.add.tween(gameTitle).to({x:gameTitle.x + tDist, alpha: 1}, 750, Phaser.Easing.Exponential.Out, false, 1000);
        tween2.onStart.add(
            function(){
                tween2_1.start();
                this.playSound(this.sfx.swish1,{
                    delay: 1000
                });
            }.bind(this));

        var tween3 = this.game.add.tween(gameNumber).to({x:gameNumber.x - tDist*2, alpha: 0}, 750, Phaser.Easing.Exponential.Out, false, 500);
        var tween3_1 = this.game.add.tween(gameTitle).to({x:gameTitle.x + tDist*2, alpha: 0}, 750, Phaser.Easing.Exponential.Out, false, 500);
        tween3.onStart.add(function(){
            tween3_1.start();
            this.playSound(this.sfx.swish2,{
                delay: 500
            });
        }.bind(this));
        tween3.onComplete.add(function(){
            gameNumber.destroy(true);
        }.bind(this));

        var tween4 = this.game.add.tween(overlay).to({alpha:0}, 500, Phaser.Easing.Default, false, 0);

        var tween5 = this.game.add.tween(viewFront).to({alpha:0}, 10, Phaser.Easing.Linear.None, false, 0);
        tween5.onStart.add(function(){
            viewFront.bringToTop();
        }.bind(this));

        var tween6 = this.game.add.tween(viewBack).to({alpha:0}, 500, Phaser.Easing.Linear.None, false, 500);

        tween6.onComplete.add(function(){
            this.baseLayer.sort('zI');
            overlay.destroy(true);
            viewFront.destroy(true);
            viewBack.destroy(true);
            this.game.time.events.add(Phaser.Timer.SECOND * 1, function(){this.game.currentActiveState.startInstructions();}.bind(this));
        }.bind(this));

        tween1.chain(tween2,tween3,tween4,tween5,tween6);
        tween1.start();
    },

    startInstructions: function (){
        console.log('START INSTRUCTIONS');
        if(!this.debugSkipInstructions){
            this.game.currentActiveState.isPaused = true;
            this.firstTimeInstructions = new TGA.Shared.InstructionMenu(this.game, this.upperLayer, {
                header: this.getString('instructionHeader'),
                firstTime: true,
                firstTimeHandler: TGA.Endurance.firstTimeInstructionHandler,
                instructions: [
                    {
                        text : this.getString("i1"),
                        textPos : {x : 425, y: 350},
                        wordWrap: 450,
                        showHeader: true,
                        headerPos: {x: 425, y: 270}
                    },
                    {
                        text : this.getString("i2"),
                        textPos : {x : 90, y: 370},
                        wordWrap: 425,
                        showArrow: true,
                        arrowPos:{x: 492, y:340},
                        // arrowRot:-1.57
                    },
                    {
                        text : this.getString("i3"),
                        textPos : {x : 100, y: 370},
                        wordWrap: 425,
                        showArrow: false,
                        // arrowPos:{x: 492, y:340},
                        // arrowRot:0
                    },
                    {
                        text : this.getString("i4"),
                        textPos : {x : 200, y: 150},
                        wordWrap: 400,
                        showArrow: true,
                        arrowPos:{x: 200, y:115},
                        arrowScale:{x:-1, y:1}
                    },
                    {
                        text : this.getString("i5"),
                        textPos : {x : 200, y: 525},
                        wordWrap: 425,
                        // showArrow: true,
                        // arrowPos:{x: 445, y:400},
                        arrowScale:{x:-1,y:1},
                    },
                    {
                        text : this.getString("i6"),
                        textPos : {x : 180, y: 150},
                        wordWrap: 425,
                        showArrow: true,
                        arrowPos:{x: 590, y:115},
                        arrowScale:{x:1,y:1},
                    },
                    {
                        text : this.getString("i7"),
                        textPos : {x : 425, y: 350},
                        wordWrap: 450,
                    }
                ],
                callback: function(){
                    this.isPaused = false;
                    this.canClick = false;
                    this.prepareRound();
                }.bind(this)
            });
        }else{
            this.canClick = false;
            this.prepareRound();
        }
    },


    /*MISC*/

    gameText:{
        "gameNumber":"Experiment",
        "gameTitle":"Endurance ",
        "roundCounter":"ROUND",
        "boxTextSpeedTitle":"SPEED ",
        "boxTextSpeed":"LEVEL OF \nINTENSITY",
        "boxTextHeartRateTitle":"CURRENT HEART RATE ",
        "boxTextHeartRate":"BEATS PER \nMINUTE",
        "boxTextBreathingRateTitle":"BREATHING RATE ",
        "boxTextBreathingRate":" BREATHS PER \n MINUTE",
        "sliderTitleTop":"SPRINT ",
        "sliderTitleBottom":"WARM UP ",
        "sliderDescriptionTop":"HIGH INTENSITY ",
        "sliderDescriptionBottom":"LOW INTENSITY ",
        "sliderButtonText":"START",
        "instructionHeader":"INSTRUCTIONS",
        "i1":"You will be helping the players hit their target heart rate during their shift.",
        "i2":"The player's training plan for the entire shift is shown here. The higher the bar, the higher the target heart rate.",
        "i3":"The player's shifts on the ice last about 45 seconds. In this training plan, each shift has been divided into smaller intervals.",
        "i4":"Set the player's speed before each interval. Be sure to set the speed just right to hit the target heart rate zone. You don't want their heart rates to spike too high or stay too low.\n",
        "i5":"When you've set the speed for the interval, press the \"START\" button.",
        "i6":"Your player's current heart rate appears here. If it turns green, you're in the target heart rate zone. If it turns red, you have picked a speed that is too high or too low for the target heart rate.\n",
        "i7":"Try to hit the target heart rate in as many of the intervals of the shift as possible. Let's get started!",
        "intervalCorrectHeader":"GOOD JOB",
        "intervalCorrectBody":"You selected the correct interval for the target heart rate zone.",
        "intervalIncorrectHeader":"ALMOST",
        "intervalIncorrectBody":"You missed the target heart rate zone for that interval.  Be sure to adjust the player's speed for the next section.",
        "intervalIncorrectBodyLast":"You missed the target heart rate zone for that interval.",
        "roundCompleteHeader":"GREAT WORKOUT",
        "roundCompleteBody":"You completed the workout.  Let's start a new one.",
        "gameCompleteHeader":"WELL DONE",
        "gameCompleteBody":"You gave the team a fantastic workout. It’s time to review your data and see if there are any patterns!"
    }
};
