ig.module(
   'plugins.server'
)
.requires(
)
.defines(function(){

  Server = ig.Class.extend({ 
    
    socket:         undefined,
    connectionID:   0,
    isConnected:    false,
    
    register: function(callback) {
      
      this.socket = io.connect(sjp.config.serveruri,{'force new connection':true});
      
      this.socket.on('accept', (function(data) {
        callback(data);
        this.isConnected = true;
      }).bind(this));
     
    },
    
    sendData: function(cmd,data) {
      
      this.socket.emit(cmd,data);
      
    },
    
    setHandler: function(cmd,callback) {
      
      this.socket.on(cmd,callback);
      
    },
    
    removeHandlers: function(cmd) {
      
      this.socket.removeAllListeners(cmd);
      
    },
    
    removeHandler: function(cmd,func) {
      
      this.socket.removeListener(cmd,func);
      
    }
    
  });

});