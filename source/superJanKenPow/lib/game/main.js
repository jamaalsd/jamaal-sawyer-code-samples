ig.module(
    'game.main'
)
.requires(
    'impact.game',
    'plugins.crossfade',
    'plugins.impact-storage',
    'plugins.scale',
    'plugins.impact-splash-loader',
    'plugins.impact-tween.tween',
    'plugins.cocoonjs_touchevent_fix',

    'game.adapters.logic',

    'game.entities.fx.particle-generator',
    'game.entities.timer.timer',
    'game.entities.ui.clickable',
    'game.entities.ui.impactimage',
    'game.entities.ui.impactfont',
    'game.entities.controllers.openingcontroller',
    'game.entities.controllers.mainmenucontroller',
    'game.entities.controllers.selectionscreencontroller',
    'game.entities.controllers.prebattlecontroller',
    'game.entities.controllers.arenacontroller',
    'game.levels.opening',
    'game.levels.mainmenu',
    'game.levels.selectionscreen',
    'game.levels.prebattle',
    'game.levels.arena'
    //,'impact.debug.debug'
)
.defines(function(){

SuperJanKenPowGame = ig.Game.extend({
    /*GLOBAL VARIABLES*/
    canClick:false,
    adsOn:true,
    soundOn:true,
    musicOn:true,
    localStorage: undefined,

    logic:new LogicController(),
    mode:'single', // single | multi

    player1Variables:{
        name:'PLYR',
        type:'rock',
        isSet:false,
        health: 9,
        attackArr:[]
    },
    player2Variables:{
        name:'CPU',
        type:'paper',
        isSet:false,
        health: 9,
        attackArr:[]
    },

    MEDIA_PATH: 'media/img/16_9/',
    MUSIC_VOL:0.5,
    SFX_VOL: 1,
    VOICE_VOL: 1,

    HEALTH_PER_ROUND: 9,

    /*SOUND LOADING*/
    introMusic: new ig.Sound('media/sound/music/arpanauts.*',false),
    selectMusic: new ig.Sound('media/sound/music/resistors.*',false),
    preBattleMusic: new ig.Sound('media/sound/music/anightofdizzyspells.*',false),
    battleMusic: new ig.Sound('media/sound/music/comeandfindme.*',false),

    s_textAppear: new ig.Sound('media/sound/fx/textAppear.*'),
    s_textDisappear: new ig.Sound('media/sound/fx/textDisappear.*'),
    s_logoAppear: new ig.Sound('media/sound/fx/logoAppear.*'),
    s_iyou: new ig.Sound('media/sound/fx/iyouMixed.*'),
    s_subboom: new ig.Sound('media/sound/fx/subboom.*'),

    s_playerHurt: new ig.Sound('media/sound/fx/battle/hurt.*'),
    s_playerHurtDeath: new ig.Sound('media/sound/fx/battle/hurt_death.*'),
    s_playerClash: new ig.Sound('media/sound/fx/battle/clash.*'),
    s_playerWhoosh: new ig.Sound('media/sound/fx/battle/whoosh.*'),
    s_playerWhoosh2: new ig.Sound('media/sound/fx/battle/whoosh2.*'),
    s_playerWhoosh3: new ig.Sound('media/sound/fx/battle/whoosh3.*'),
    s_playerWhoosh3rev: new ig.Sound('media/sound/fx/battle/whoosh3rev.*'),
    s_playerSelect: new ig.Sound('media/sound/fx/battle/select.*'),
    s_playerSelectLast: new ig.Sound('media/sound/fx/battle/select.*'),
    s_playerSelectQuit: new ig.Sound('media/sound/fx/battle/select_quit.*'),

    s_dying: {
        player1:[
            new ig.Sound('media/sound/voices/dying/L/1.*'),
            new ig.Sound('media/sound/voices/dying/L/2.*'),
            new ig.Sound('media/sound/voices/dying/L/3.*'),
            new ig.Sound('media/sound/voices/dying/L/4.*'),
            new ig.Sound('media/sound/voices/dying/L/5.*'),
            new ig.Sound('media/sound/voices/dying/L/6.*'),
            new ig.Sound('media/sound/voices/dying/L/7.*'),
            new ig.Sound('media/sound/voices/dying/L/8.*'),
            new ig.Sound('media/sound/voices/dying/L/9.*')
        ],
        player2:[
            new ig.Sound('media/sound/voices/dying/R/1.*'),
            new ig.Sound('media/sound/voices/dying/R/2.*'),
            new ig.Sound('media/sound/voices/dying/R/3.*'),
            new ig.Sound('media/sound/voices/dying/R/4.*'),
            new ig.Sound('media/sound/voices/dying/R/5.*'),
            new ig.Sound('media/sound/voices/dying/R/6.*'),
            new ig.Sound('media/sound/voices/dying/R/7.*'),
            new ig.Sound('media/sound/voices/dying/R/8.*'),
            new ig.Sound('media/sound/voices/dying/R/9.*')
        ]
    },

    s_getting_hit: {
        player1:[
            new ig.Sound('media/sound/voices/getting_hit/1_high_health/L/1.*'),
            new ig.Sound('media/sound/voices/getting_hit/1_high_health/L/2.*'),
            new ig.Sound('media/sound/voices/getting_hit/2_mid_health/L/1.*'),
            new ig.Sound('media/sound/voices/getting_hit/2_mid_health/L/2.*'),
            new ig.Sound('media/sound/voices/getting_hit/3_low_health/L/1.*'),
            new ig.Sound('media/sound/voices/getting_hit/3_low_health/L/2.*')
        ],
        player2:[
            new ig.Sound('media/sound/voices/getting_hit/1_high_health/R/1.*'),
            new ig.Sound('media/sound/voices/getting_hit/1_high_health/R/2.*'),
            new ig.Sound('media/sound/voices/getting_hit/2_mid_health/R/1.*'),
            new ig.Sound('media/sound/voices/getting_hit/2_mid_health/R/2.*'),
            new ig.Sound('media/sound/voices/getting_hit/3_low_health/R/1.*'),
            new ig.Sound('media/sound/voices/getting_hit/3_low_health/R/2.*')
        ]
    },

    s_names: {
        player1:{
            paper: new ig.Sound('media/sound/voices/names/L/paper.*'),
            rock: new ig.Sound('media/sound/voices/names/L/rock.*'),
            scissors: new ig.Sound('media/sound/voices/names/L/scissors.*')
        },
        player2:{
            paper: new ig.Sound('media/sound/voices/names/R/paper.*'),
            rock: new ig.Sound('media/sound/voices/names/R/rock.*'),
            scissors: new ig.Sound('media/sound/voices/names/R/scissors.*')
        }
    },

    s_trash_talk:{
        rock: new ig.Sound('media/sound/voices/trash_talk/rock.*'),
        paper: new ig.Sound('media/sound/voices/trash_talk/paper.*'),
        scissors: new ig.Sound('media/sound/voices/trash_talk/scissors.*'),
    },

    s_victory:{
        player1:[
            new ig.Sound('media/sound/voices/victory/L/1.*'),
            new ig.Sound('media/sound/voices/victory/L/2.*'),
            new ig.Sound('media/sound/voices/victory/L/3.*'),
            new ig.Sound('media/sound/voices/victory/L/4.*'),
        ],
        player2:[
            new ig.Sound('media/sound/voices/victory/R/1.*'),
            new ig.Sound('media/sound/voices/victory/R/2.*'),
            new ig.Sound('media/sound/voices/victory/R/3.*'),
            new ig.Sound('media/sound/voices/victory/R/4.*'),
        ]
    },

    init: function() {
        // Initialize your game here; bind keys etc.
        this.initLocalStorage();
        this.initMusic();
        this.initUI();
        this.preloadAnims();
        this.loadLevelDeferred(LevelOpening);
        //this.loadLevelDeferred(LevelMainmenu);
        //this.loadLevelDeferred(LevelSelectionscreen);
        //this.loadLevelDeferred(LevelPrebattle);
        //this.loadLevelDeferred(LevelArena);
    },

    update: function() {
        // Update all entities and backgroundMaps
        this.parent();
        // Add your own, additional update code here
    },

    canDraw:true,
    draw : function() {
        if(this.canDraw){
            this.parent();
            ig.game.sortEntitiesDeferred();
        }

    },

    /*INIT FUNCTIONS*/
    initMusic: function(){

        ig.music.loop = true;
        if(this.musicOn){
            ig.music.volume = this.MUSIC_VOL;
        }else{
            ig.music.volume = 0;
        }
        ig.music.add(this.introMusic, 'intro');
        ig.music.add(this.selectMusic, 'select');
        ig.music.add(this.battleMusic, 'battle');
        ig.music.add(this.preBattleMusic, 'prebattle');
        ig.music.play('intro');
    },

    initUI: function(){
        ig.input.initMouse();
        ig.input.bind( ig.KEY.MOUSE1, 'leftButton' );
    },

    preloadAnims: function(){
        var anim = new ig.AnimationSheet(ig.game.MEDIA_PATH+'character_selection/fighters/combined_animated.png', 136, 118);
        anim = new ig.AnimationSheet(ig.game.MEDIA_PATH+'character_selection/fighters/combined_chosen.png', 127, 126);
        anim = new ig.AnimationSheet(ig.game.MEDIA_PATH+'shared/battle_anim/combined.png', ig.system.width, ig.system.height);
        anim = new ig.AnimationSheet(ig.game.MEDIA_PATH+'character_selection/text/combined_title.png', 175, 45);
        anim = new ig.AnimationSheet(ig.game.MEDIA_PATH+'character_selection/text/combined_stats.png', 405, 100);
        anim = new ig.AnimationSheet(ig.game.MEDIA_PATH+'character_selection/fighters/combined_side.png', 130, 114);
        anim = new ig.AnimationSheet(ig.game.MEDIA_PATH+'character_selection/text/select_fighter_alt.png',362,45);
        anim = new ig.AnimationSheet(ig.game.MEDIA_PATH+'shared/hands/player1/combined.png', 270, 105);
        anim = new ig.AnimationSheet(ig.game.MEDIA_PATH+'character_selection/buttons/back.png', 52, 46);
        anim = new ig.AnimationSheet(ig.game.MEDIA_PATH+'shared/title_screens/1_left.png',160,568);
        anim = new ig.AnimationSheet(ig.game.MEDIA_PATH+'shared/title_screens/1_right.png',160,568);
    },

    initLocalStorage: function(){
        this.localStorage = new ig.Storage();
        this.localStorage.initUnset('soundOn', true);
        this.localStorage.initUnset('musicOn', true);
        this.localStorage.initUnset('adsOn', true);
        this.localStorage.initUnset('name', 'PLYR');
        this.soundOn = this.localStorage.getBool('soundOn');
        this.musicOn = this.localStorage.getBool('musicOn');
        this.adsOn = this.localStorage.getBool('adsOn');
        this.player1Variables.name = this.localStorage.get('name');
    },

    /*DISPLAY FUNCTIONS*/
    setCenterX: function(sprite){
        sprite.pos.x = ig.system.width/2 - sprite.size.x/2;
    },

    setCenterY: function(sprite){
        sprite.pos.y = ig.system.height/2 - sprite.size.y/2;
    },

    screenShakeOn: function(intensityX, intensityY){
        this.shakeScreenBool = true;
        this.shakeAmplitude = {x:intensityX, y:intensityY};
    },

    screenShakeOff: function(){
        this.shakeScreenBool = false;
        this.shakeAmplitude = {x:0,y:0};
    },

    takeScreenShot: function(){
        if(!this.screenshot)
        this.screenshot = new Image();
        // console.log(this.screenshot);
        var canvas = document.getElementById('canvas');
        this.screenshot.data = canvas.toDataURL();
        /*why did I do this??? to load the image into memory*/
        this.screenshot.animSheet = new ig.AnimationSheet(this.screenshot.data, ig.system.width, ig.system.height);
    },

    /*SOUND FUNCTIONS*/
    playSFX: function(sound){
        if(ig.game.soundOn){
            if(sound.volume !== this.SFX_VOL){
                sound.volume = this.SFX_VOL;
            }
            sound.play();
        }
    },

    playVoice: function(sound){
        if(ig.game.soundOn){
            if(sound.volume !== this.VOICE_VOL){
                sound.volume = this.VOICE_VOL;
            }
            sound.play();
        }
    },

    /*UTILITIES*/
    getRandomInt: function(min, max) {
        if(min === max){
            return min;
        }
        return Math.floor(Math.random() * (max - min + 1)) + min;
    },

    getRandomReal: function(min, max) {
        if(min === max){
            return min;
        }
        return Math.random() * (max - min) + min;
    },

    degreesToRadians: function(degrees){
        return degrees * (Math.PI/180);
    }
});
ig.System.scaleMode=ig.System.SCALE.CRISP;

ig.main( '#canvas', SuperJanKenPowGame, 60, 320, 568, 1, ig.ImpactSplashLoader);

window.addEventListener('load', resizeCanvas,false);
window.addEventListener('resize', resizeCanvas,false);
function resizeCanvas() {
    if (!navigator.isCocoonJS){
        var canvas = document.getElementById('canvas');
        var height = window.innerHeight;
        var ratio = canvas.width/canvas.height;
        var width = height * ratio;
        canvas.style.width = width+'px';
        canvas.style.height = height+'px';
    }else{
        Cocoon.Utils.setAntialias(false);
    }
}
resizeCanvas();
});
