ig.module( 'game.levels.selectionscreen' )
.requires( 'impact.image','game.entities.controllers.selectionscreencontroller' )
.defines(function(){
LevelSelectionscreen=/*JSON[*/{
	"entities": [
		{
			"type": "EntitySelectionscreencontroller",
			"x": 0,
			"y": 0
		}
	],
	"layer": []
}/*]JSON*/;
});