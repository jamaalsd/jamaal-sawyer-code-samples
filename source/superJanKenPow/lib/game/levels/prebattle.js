ig.module( 'game.levels.prebattle' )
.requires( 'impact.image','game.entities.controllers.prebattlecontroller' )
.defines(function(){
LevelPrebattle=/*JSON[*/{
	"entities": [
		{
			"type": "EntityPrebattlecontroller",
			"x": 0,
			"y": 0,
			"settings": {
				"size": {
					"x": 20,
					"y": 36
				}
			}
		}
	],
	"layer": []
}/*]JSON*/;
});