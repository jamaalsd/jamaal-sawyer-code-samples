ig.module( 'game.levels.opening' )
.requires( 'impact.image','game.entities.controllers.openingcontroller' )
.defines(function(){
LevelOpening=/*JSON[*/{
	"entities": [
		{
			"type": "EntityOpeningcontroller",
			"x": 0,
			"y": 0
		}
	],
	"layer": []
}/*]JSON*/;
});