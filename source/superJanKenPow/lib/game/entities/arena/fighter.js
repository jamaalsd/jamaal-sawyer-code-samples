ig.module(
    'game.entities.arena.fighter'
)
.requires(
    'impact.entity'
)
.defines(function(){
    EntityArenafighter = ig.Entity.extend({
      junkTweenVar:0,
      zIndex: 7,
      isWeakAgainst: false,
      anchor:{x:0,y:0},
      scale: {x:1.25,y:1.25},
      size:{x:72,y:67},
      init: function(x,y,settings) {
        this.parent(x,y,settings);
        if(ig.game.player1Variables.type === ig.game.player2Variables.type && !settings.isPlayer1){
          this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'arena/fighters/fighters_alt.png', 72, 67);
        }else{
          this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'arena/fighters/fighters.png', 72, 67);
        }
        var animSpeed = 0.15;
        switch(settings.type){
          case 'paper':
            this.addAnim('idle',    0,   [0],true);
            this.addAnim('slide',   0,   [1],true);
            this.addAnim('low',     0,   [2],true);
            this.addAnim('mid',     0,   [3],true);
            this.addAnim('high',    0,   [4],true);
            this.addAnim('hit',     0,   [5],true);
            this.addAnim('flash',   animSpeed,   [6,5,0],true);
            this.addAnim('dead',    animSpeed,   [6,5,7],true);
            this.addAnim('victory', 0.5,   [0,8],true);
            this.addAnim('null',    0,   [0],true);
            break;
          case 'scissors':
            this.addAnim('idle',    0,   [9],true);
            this.addAnim('slide',   0,   [10],true);
            this.addAnim('low',     0,   [11],true);
            this.addAnim('mid',     0,   [12],true);
            this.addAnim('high',    0,   [13],true);
            this.addAnim('hit',     0,   [14],true);
            this.addAnim('flash',   animSpeed,   [15,14,9],true);
            this.addAnim('dead',    animSpeed,   [15,14,16],true);
            this.addAnim('victory', 0.5,   [9,17],true);
            this.addAnim('null',    0,   [9],true);
            break;
          case 'rock':
            this.addAnim('idle',    0,   [18],true);
            this.addAnim('slide',   0,   [19],true);
            this.addAnim('low',     0,   [20],true);
            this.addAnim('mid',     0,   [21],true);
            this.addAnim('high',    0,   [22],true);
            this.addAnim('hit',     0,   [23],true);
            this.addAnim('flash',   animSpeed,   [24,23,18],true);
            this.addAnim('dead',    animSpeed,   [24,23,25],true);
            this.addAnim('victory', 0.5,   [18,26],true);
            this.addAnim('null',    0,   [18],true);
            break;
        }
        if(!settings.isPlayer1){
          this.anims.idle.flip.x    = true;
          this.anims.slide.flip.x   = true;
          this.anims.low.flip.x     = true;
          this.anims.mid.flip.x     = true;
          this.anims.high.flip.x    = true;
          this.anims.hit.flip.x     = true;
          this.anims.flash.flip.x   = true;
          this.anims.dead.flip.x    = true;
          this.anims.victory.flip.x = true;
        }
        this.currentAnim = this.anims.idle;
        this.origPos = {x:x,y:y};

        this.isWeakAgainst = this.determineWeak();
      },

      fadeVars:{
        dist: 45,
        time: 0.3,
      },
      fadeIn: function(delay){
        this.reset();
        delay = delay || 0;

        this.currentAnim = this.anims.slide;
        this.currentAnim.alpha = 0.0;

        var tTime   = this.fadeVars.time;
        var tEasing = ig.Tween.Easing.Exponential.EaseOut;

        this.tween({currentAnim:{alpha:1.0}},tTime,{
          //easing:tEasing,
          delay: delay,
          onStart: function(){
            if(this.isPlayer1|| (!this.isPlayer1 && this.controller.player1Fighter.isDead) ){
              ig.game.playSFX(ig.game.s_playerWhoosh);
            }
          }.bind(this),
          onComplete:function(){
            if(this.isPlayer1 && this.controller.player2Fighter.isDead){
              this.anims.victory.rewind();
              this.currentAnim = this.anims.victory;
              this.playVictoryVoice(1.1);
            }else if (!this.isPlayer1 && this.controller.player1Fighter.isDead){
              this.anims.victory.rewind();
              this.currentAnim = this.anims.victory;
              this.playVictoryVoice(1.1);
            }else{
              this.currentAnim = this.anims.idle;
            }
          }.bind(this)
        }).start();

        var dist = this.fadeVars.dist;

        if(this.currentAnim.flip.x){
          dist *= -1;
        }
        this.pos.x -= dist;
        this.tween({pos:{x:this.origPos.x}},tTime,{
            delay: delay,
            easing: tEasing
        }).start();

        return delay + tTime;
      },

      fadeOut: function(delay, pos){
        if(pos){
          this.reset(false);
        }else{
          this.reset();
        }

        delay = delay || 0;
        pos = pos || this.origPos.x;

        var tTime   = this.fadeVars.time;
        var tEasing = ig.Tween.Easing.Exponential.EaseOut;

        this.tween({currentAnim:{alpha:0.0}},tTime,{
          easing: tEasing,
          delay: delay,
          onStart:function(){
            if(this.isPlayer1){
              ig.game.playSFX(ig.game.s_playerWhoosh2);
            }
            this.currentAnim = this.anims.slide;
            this.currentAnim.alpha = 1.0;
          }.bind(this),
          onComplete:function(){
            //this.currentAnim = this.anims.idle;
          }.bind(this)
        }).start();

        var dist = this.fadeVars.dist;

        if(this.currentAnim.flip.x){
          dist *= -1;
        }
        this.tween({pos:{x:pos - dist}},tTime,{
            delay: delay,
            //easing: tEasing
        }).start();

        return delay + tTime;
      },

      attackVars:{
        dist:65,
        time: 0.15
      },
      attack: function(delay, attack){
        this.reset();
        delay = delay || 0;
        if(this.isPlayer1){
          attack = ig.game.player1Variables.attackArr.shift();
        }else{
          attack = ig.game.player2Variables.attackArr.shift();
        }


        this.currentAttack = attack;
        this.currentAnim = this.anims[this.currentAttack];
        this.currentAnim.alpha = 0;

        var tTime = this.attackVars.time;
        var tEasing = ig.Tween.Easing.Exponential.EaseIn;
        var dist = this.attackVars.dist;

        this.tween({currentAnim:{alpha:1.0}},tTime,{
          easing:tEasing,
          delay: delay,
          onStart:function(){
            this.spawnHitType();
          }.bind(this),
          onComplete:function(){
            this.attackResolution();
          }.bind(this)
        }).start();

        if(this.currentAnim.flip.x){
          dist *= -1;
        }

        this.tween({pos:{x:this.origPos.x+dist}},tTime,{
            delay: delay,
            //easing: tEasing
        }).start();
      },

      attackResolution: function(){
        var action = this.determineAction();
        switch(action){
          case 'win':
            this.attackSuccess();
            break;
          case 'lose':
            this.attackFailure();
            break;
          case 'draw':
            this.attackDraw();
            break;
        }
      },

      attackSVars:{
        time: 0.5,
        dist: 20,
        fadeTime: 0.0,
      },
      attackSuccess: function(delay){
        this.reset(false);
        delay = delay || 0;

        var dist = this.attackSVars.dist;
        if(this.currentAnim.flip.x){
          dist *= -1;
        }

        var tTime   = this.attackSVars.time;
        var tEasing = ig.Tween.Easing.Exponential.EaseOut;

        this.tween({pos:{x:this.pos.x-dist}}, tTime,{
          easing: tEasing,
          delay: delay,
          onComplete:function(){
            if(this.isPlayer1){
              if(this.controller.player2Fighter.isDead){
                this.controller.playerDeath(2);
              }else{
                this.controller.roundReady = true;
              }
            }
          }.bind(this)
        }).start();

      },

      isDead: false,
      attackFailure: function(delay){
        this.reset(false);
        delay = delay || 0;

        var dist = this.attackSVars.dist;
        if(this.currentAnim.flip.x){
          dist *= -1;
        }



        var tTime   = this.attackSVars.time;
        var tEasing = ig.Tween.Easing.Exponential.EaseOut;

        this.isDead = this.subtractHealth();

        if(!this.isDead){
          ig.game.playSFX(ig.game.s_playerHurt);
          this.anims.flash.rewind();
          this.currentAnim = this.anims.flash;
          this.playHitSound();
        }else{
          ig.game.playSFX(ig.game.s_playerHurtDeath);
          if(this.isPlayer1){
            ig.game.playVoice(ig.game.s_dying.player1[ig.game.getRandomInt(0,ig.game.s_dying.player1.length-1)]);
          }else{
            ig.game.playVoice(ig.game.s_dying.player2[ig.game.getRandomInt(0,ig.game.s_dying.player1.length-1)]);
          }

          this.anims.dead.rewind();
          this.currentAnim = this.anims.dead;
        }


        this.tween({pos:{x:this.pos.x-dist}}, tTime,{
          easing: tEasing,
          delay: delay,
          onStart:function(){
            //this.currentAnim = this.anims.slide;
            //this.currentAnim.alpha = 1.0;
          }.bind(this),
          onComplete:function(){
            if(this.isPlayer1){
              if(this.isDead){
                this.controller.playerDeath(1);
              }else{
                this.controller.roundReady = true;
              }
            }
          }.bind(this)
        }).start();

        this.spawnHitNumber();

      },

      attackDraw: function (delay){
        this.reset(false);
        delay = delay || 0;

        var dist = this.attackSVars.dist;
        if(this.currentAnim.flip.x){
          dist *= -1;
        }

        var tTime   = this.attackSVars.time;
        var tEasing = ig.Tween.Easing.Exponential.EaseOut;

        if(this.isPlayer1){
          ig.game.playSFX(ig.game.s_playerClash);
          //this.controller.generateSparks();
        }

        this.tween({pos:{x:this.pos.x-dist}}, tTime,{
          easing: tEasing,
          delay: delay,
          onStart:function(){
            //this.currentAnim = this.anims.slide;
            //this.currentAnim.alpha = 1.0;
          }.bind(this),
          onComplete:function(){
            if(this.isPlayer1)
            this.controller.roundReady = true;
          }.bind(this)
        }).start();
      },

      subtractHealth: function(){
        var isDead = false;
        if(this.isWeakAgainst){
          if(this.isPlayer1){
            ig.game.player1Variables.health -= 2;
          }else{
            ig.game.player2Variables.health -= 2;
          }
        }else{
          if(this.isPlayer1){
            ig.game.player1Variables.health --;
          }else{
            ig.game.player2Variables.health --;
          }
        }

        if(this.isPlayer1){
          if(ig.game.player1Variables.health <= 0){
            isDead = true;
            ig.game.player1Variables.health = 0;
          }
          this.controller.player1Health.setHealth(ig.game.player1Variables.health);
          this.controller.player1Portrait.setHealth(ig.game.player1Variables.health);
        }else{
          if(ig.game.player2Variables.health <= 0){
            isDead = true;
            ig.game.player2Variables.health = 0;
          }
          this.controller.player2Health.setHealth(ig.game.player2Variables.health);
          this.controller.player2Portrait.setHealth(ig.game.player2Variables.health);
        }
        return isDead;
      },

      hitFlags:{
        high:false,
        mid:false,
        low:false,
      },

      playHitSound: function (){
        var health;
        if(this.isPlayer1){
          health = ig.game.player1Variables.health;
        }else{
          health = ig.game.player2Variables.health;
        }

        if(health  <= 9 && health > 6 && !this.hitFlags.high){
          if(this.isPlayer1){
            if(this.controller.hitSoundSelection[0] === 0){
              ig.game.playVoice(ig.game.s_getting_hit.player1[0]);
            }else{
              ig.game.playVoice(ig.game.s_getting_hit.player1[1]);
            }
          }else{
            if(this.controller.hitSoundSelection[0] === 1){
              ig.game.playVoice(ig.game.s_getting_hit.player2[0]);
            }else{
              ig.game.playVoice(ig.game.s_getting_hit.player2[1]);
            }
          }
          this.hitFlags.high = true;
        }else if(health <= 6 && health > 3 && !this.hitFlags.mid){
          if(this.isPlayer1){
            if(this.controller.hitSoundSelection[1] === 0){
              ig.game.playVoice(ig.game.s_getting_hit.player1[2]);
            }else{
              ig.game.playVoice(ig.game.s_getting_hit.player1[3]);
            }
          }else{
            if(this.controller.hitSoundSelection[1] === 1){
              ig.game.playVoice(ig.game.s_getting_hit.player2[2]);
            }else{
              ig.game.playVoice(ig.game.s_getting_hit.player2[3]);
            }
          }
          this.hitFlags.mid = true;
        }else if(health <= 3 && !this.hitFlags.low){
          if(this.isPlayer1){
            if(this.controller.hitSoundSelection[1] === 0){
              ig.game.playVoice(ig.game.s_getting_hit.player1[4]);
            }else{
              ig.game.playVoice(ig.game.s_getting_hit.player1[5]);
            }
          }else{
            if(this.controller.hitSoundSelection[1] === 1){
              ig.game.playVoice(ig.game.s_getting_hit.player2[4]);
            }else{
              ig.game.playVoice(ig.game.s_getting_hit.player2[5]);
            }
          }
          this.hitFlags.low = true;
        }
      },

      playVictoryVoice:function(delay){
        this.tween({junkTweenVar:0.0},delay,{
          onComplete: function(){
            if(this.isPlayer1){
              ig.game.playVoice(ig.game.s_victory.player1[ig.game.getRandomInt(0,ig.game.s_victory.player1.length-1)]);
            }else{
              ig.game.playVoice(ig.game.s_victory.player2[ig.game.getRandomInt(0,ig.game.s_victory.player2.length-1)]);
            }
          }.bind(this)
        }).start();
      },

      spawnHitType: function(){
        var hitTypePos;
        if(this.isPlayer1){
          hitTypePos = {
            x: 30,
            y: 325
          };
        }else{
          hitTypePos = {
            x: 245,
            y: 325
          };
        }
        var hitType = ig.game.spawnEntity('EntityArenahittype', hitTypePos.x, hitTypePos.y, {
          controller: this
        });

        var tTime = 1.25;
        var tEasing = ig.Tween.Easing.Exponential.EaseIn;
        hitType.tween({currentAnim:{alpha:0.0}},tTime,{
          easing: tEasing
        }).start();
        hitType.tween({pos:{y:this.pos.y - 140}},tTime,{
          onComplete: function(){
            this.kill();
          }.bind(hitType)
        }).start();
      },

      spawnHitNumber: function(){
        var hitNumberPos;
        if(this.isPlayer1){
          hitNumberPos = {
            x: 80,
            y: 290
          };
        }else{
          hitNumberPos = {
            x: 175,
            y: 290
          };
        }
        var hitNumber = ig.game.spawnEntity('EntityArenahitnumber', hitNumberPos.x, hitNumberPos.y, {
          controller: this
        });

        var tTime = 0.4;
        var tEasing = ig.Tween.Easing.Exponential.EaseIn;
        hitNumber.tween({currentAnim:{alpha:0.0}},tTime,{
          easing: tEasing
        }).start();
        hitNumber.tween({pos:{y:this.pos.y - 50}},tTime,{
          onComplete: function(){
            this.kill();
          }.bind(hitNumber)
        }).start();
      },

      /*
      low beats high
      mid beats low
      high beats mid
      */

      determineAction: function(){
        var thisAtk = this.currentAttack;
        var otherAtk;

        if(this.isPlayer1){
          otherAtk = this.controller.player2Fighter.currentAttack;
        }else{
          otherAtk = this.controller.player1Fighter.currentAttack;
        }

        if(thisAtk === otherAtk){
          return 'draw';
        }else if(otherAtk === 'null'){
          return 'win';
        }else if(thisAtk === 'null'){
          return 'lose';
        }

        switch(thisAtk){
          case 'low':
            if(otherAtk === 'mid'){
              return 'lose';
            }else{
              return 'win';
            }
            break;
          case 'mid':
            if(otherAtk === 'high'){
              return 'lose';
            }else{
              return 'win';
            }
            break;
          case 'high':
            if(otherAtk === 'low'){
              return 'lose';
            }else{
              return 'win';
            }
            break;
        }
      },

      determineWeak: function(){
        var thisType;
        var otherType;
        if(this.isPlayer1){
          thisType  = ig.game.player1Variables.type;
          otherType = ig.game.player2Variables.type;
        }else{
          thisType  = ig.game.player2Variables.type;
          otherType = ig.game.player1Variables.type;
        }

        switch(thisType){
          case 'rock':
            if(otherType === 'paper'){
              return true;
            }
            break;
          case 'paper':
            if(otherType === 'scissors'){
              return true;
            }
            break;
          case 'scissors':
            if(otherType === 'rock'){
              return true;
            }
            break;
        }
        return false;
      },

      reset: function(resetP){
        if(resetP === undefined){
          resetP = true;
        }
        this.stopTweens(false);
        this.resetAnimAlpha();
        if(resetP){
          this.resetPos();
        }
      },

      resetAnimAlpha: function(){
        this.anims.idle.alpha    = 1.0;
        this.anims.slide.alpha   = 1.0;
        this.anims.low.alpha     = 1.0;
        this.anims.mid.alpha     = 1.0;
        this.anims.high.alpha    = 1.0;
        this.anims.hit.alpha     = 1.0;
        this.anims.flash.alpha   = 1.0;
        this.anims.dead.alpha    = 1.0;
        this.anims.victory.alpha = 1.0;
      },

      resetPos: function(){
        this.pos.x = this.origPos.x;
        this.pos.y = this.origPos.y;
      }
  });
});