ig.module(
    'game.entities.arena.health'
)
.requires(
    'impact.entity'
)
.defines(function(){
    EntityArenahealth = ig.Entity.extend({
      zIndex: 7,
      scale: {x:1,y:1},
      anchor:{x:0,y:0},
      numberOffset:{x:32,y:2},

      init: function(x,y,settings) {
        this.parent(x,y,settings);
        this.size = {x:40, y:40};
        var animSpeed = 0;
        this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'arena/heart.png', 40, 40);
        this.addAnim('idle', animSpeed, [0], true);
        this.currentAnim = this.anims.idle;
        this.number = ig.game.spawnEntity('EntityArenanumber', this.pos.x + this.numberOffset.x, this.pos.y + this.numberOffset.y,{
          //scale:{x:0.8, y:0.8}
        });
        this.setHealth(9);

      },

      update: function(){
        this.parent();
        this.number.currentAnim.alpha = this.currentAnim.alpha;
        this.number.pos.x = this.pos.x + this.numberOffset.x;
        this.number.pos.y = this.pos.y + this.numberOffset.y;

      },

      draw: function (){
        this.parent();
      },

      setHealth: function(health){
        this.number.currentAnim = this.number.anims[health.toString()];
      }
    });

    EntityArenanumber = ig.Entity.extend({
      zIndex: 7,
      anchor:{x:0,y:0},
      //scale: {x:0.51,y:0.51},
      init: function(x,y,settings) {
        this.parent(x,y,settings);
        this.size = {x:35, y:38};
        var animSpeed = 0;
        this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'arena/text/numbers_alt.png', 35, 38);
        this.addAnim('0', animSpeed, [0], true);
        this.addAnim('1', animSpeed, [1], true);
        this.addAnim('2', animSpeed, [2], true);
        this.addAnim('3', animSpeed, [3], true);
        this.addAnim('4', animSpeed, [4], true);
        this.addAnim('5', animSpeed, [5], true);
        this.addAnim('6', animSpeed, [7], true);
        this.addAnim('7', animSpeed, [6], true);
        this.addAnim('8', animSpeed, [8], true);
        this.addAnim('9', animSpeed, [9], true);
        this.currentAnim = this.anims['0'];
      }
    });
});