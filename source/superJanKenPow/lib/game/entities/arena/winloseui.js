ig.module(
    'game.entities.arena.winloseui'
)
.requires(
    'impact.entity'
)
.defines(function(){
    EntityArenawinloseui = ig.Entity.extend({
        zIndex: 10,
        size:{x:1,y:1},
        alpha:1,
        childArr:[],

        init: function(x,y,settings){
          this.parent(x,y,settings);
          var player = (this.isPlayer1? 'player1.png':'player2.png');
          // this.playerText = ig.game.spawnEntity('EntityImpactimage', 100,180, {
          //   img: new ig.Image(ig.game.MEDIA_PATH+'arena/text/select_attack_'+player),
          //   size: {x: 131, y: 30},
          //   clickable: false,
          //   zIndex: 10,
          // });
          // this.addChild(this.playerText);

          var winlose = (this.playerWin? 'arena/text/arena_win_':'arena/text/arena_loss_');

          this.winLoseText = ig.game.spawnEntity('EntityImpactimage', 0,220, {
            img: new ig.Image(ig.game.MEDIA_PATH+winlose+player),
            size: {x: 147, y: 33},
            clickable: false,
            zIndex: 10,
          });
          ig.game.setCenterX(this.winLoseText);
          this.addChild(this.winLoseText);

          this.rematchButton = ig.game.spawnEntity('EntityImpactimage', 0,275, {
            img: new ig.Image(ig.game.MEDIA_PATH+'arena/text/win_lose_rematch_button_'+player),
            size: {x: 143, y: 50},
            clickable: true,
            zIndex: 10,
            name:'rematch',
            controller:this
          });
          ig.game.setCenterX(this.rematchButton);
          this.rematchButton.onClick = function (){
            if(ig.game.canClick)
            this.controller.buttonClicked(this.name);
          }.bind(this.rematchButton);
          this.addChild(this.rematchButton);

          this.quitButton = ig.game.spawnEntity('EntityImpactimage', 0,355, {
            img: new ig.Image(ig.game.MEDIA_PATH+'arena/text/win_lose_quit_button_'+player),
            size: {x: 80, y: 50},
            clickable: true,
            zIndex: 10,
            name:'quit',
            controller:this
          });
          ig.game.setCenterX(this.quitButton);
          this.quitButton.onClick = function (){
            if(ig.game.canClick)
            this.controller.buttonClicked(this.name);
          }.bind(this.quitButton);
          this.addChild(this.quitButton);

        },

        update: function(){
          this.parent();
          for(var i = 0; i < this.childArr.length; i++){
            if(this.childArr[i].child.currentAnim){
              this.childArr[i].child.currentAnim.alpha= this.alpha;
            }
            this.childArr[i].child.pos.x = this.pos.x + this.childArr[i].origPos.x;
            this.childArr[i].child.pos.y = this.pos.y + this.childArr[i].origPos.y;
          }
        },

        buttonClicked:function(button){
          var fadeOut;
          if(ig.game.mode === 'single'){
            ig.game.takeScreenShot();
            switch(button){
              case 'rematch':
                ig.game.playSFX(ig.game.s_textDisappear);
                ig.game.loadLevelDeferred(LevelSelectionscreen);
                break;
              case 'quit':
                ig.game.playSFX(ig.game.s_textDisappear);
                ig.game.loadLevelDeferred(LevelMainmenu);
                break;
              }
          }
        },

        selectFadeVars: {
          dist: 50,
          time: 0.25
        },
        fadeIn: function(delay){
          delay = delay || 0;
          this.alpha = 0;
          var tTime = this.selectFadeVars.time;
          var tEasing = ig.Tween.Easing.Exponential.EaseOut;

          var dist = this.selectFadeVars.dist;
          this.pos.x += dist;

          this.tween({alpha:1.0},tTime,{
            delay:delay,
            onStart:function(){
              ig.game.playSFX(ig.game.s_playerWhoosh3);
            }.bind(this)
          }).start();
          this.tween({pos:{x:this.pos.x-dist}},tTime,{
            easing: tEasing,
            delay: delay
          }).start();

          return delay+tTime;
        },

        fadeOut: function(delay){
          delay = delay || 0;
          this.alpha = 1;

          var tTime = this.selectFadeVars.time;
          var tEasing = ig.Tween.Easing.Exponential.EaseIn;

          var dist = this.selectFadeVars.dist;

          this.tween({alpha:0.0},tTime,{
            delay:delay
          }).start();
          this.tween({pos:{x:this.pos.x-dist}},tTime,{
            delay:delay,
            easing: tEasing,
            onComplete: function(){
              this.pos.x += this.selectFadeVars.dist;
            }.bind(this)
          }).start();

          return delay+tTime;
        },

        addChild: function(child){
          this.childArr.push({
            origPos:{
              x:child.pos.x - this.pos.x,
              y:child.pos.y - this.pos.y
            },
            child: child
          });
        },

        kill: function(){
          this.childArr = [];
          this.parent();
        }
      });
});