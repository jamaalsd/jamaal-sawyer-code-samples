ig.module(
    'game.entities.arena.portrait'
)
.requires(
    'impact.entity'
)
.defines(function(){
    EntityArenaportrait = ig.Entity.extend({
        _wmDrawBox:true,
        _wmScalable:true,
        _wmBoxColor:'#FF0000',
        zIndex:5,
        size:{x:60,y:66},
        anchor:{x:0, y:0},
        collides: ig.Entity.COLLIDES.NEVER,
        scale: {x:1.45,y:1.45},
        titleOffset: {x:0,y:98},
        playerTitleOffset:{x:0,y:0},
        playerNameOffset:{x:3, y:17},

        init: function(x,y,settings) {
          this.parent(x,y,settings);
          if(!settings.isAlternate){
            this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'shared/portraits/combined_portraits.png', 60, 66);
          }else{
            this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'shared/portraits/combined_portraits_alt.png', 60,66);
          }

          switch(settings.type){
            case 'rock':
              this.addAnim('full',    1, [1], true);
              this.addAnim('per75',   1, [4], true);
              this.addAnim('per50',   1, [7], true);
              this.addAnim('per25',   1, [10], true);
              this.addAnim('beaten',  1, [13], true);
              break;
            case 'paper':
              this.addAnim('full',    1, [0], true);
              this.addAnim('per75',   1, [3], true);
              this.addAnim('per50',   1, [6], true);
              this.addAnim('per25',   1, [9], true);
              this.addAnim('beaten',  1, [12], true);
              break;
            case 'scissors':
              this.addAnim('full',    1, [2], true);
              this.addAnim('per75',   1, [5], true);
              this.addAnim('per50',   1, [8], true);
              this.addAnim('per25',   1, [11], true);
              this.addAnim('beaten',  1, [14], true);
              break;
          }
          this.currentAnim = this.anims.full;
          this.title = ig.game.spawnEntity('EntityArenaportraittitle', x+this.titleOffset.x, y+this.titleOffset.y, {
            zIndex:this.zIndex,
            type:settings.type
          });

          this.playerTitle = ig.game.spawnEntity('EntityArenaportraitplayerbadge',x + this.playerTitleOffset.x, y + this.playerTitleOffset.y, {
              isPlayer1:settings.isPlayer1,
              zIndex:this.zIndex+1
          });

          var text;
          if(settings.isPlayer1){
            text = ig.game.player1Variables.name;
          }else{
            text = ig.game.player2Variables.name;
          }
          this.playerName = ig.game.spawnEntity('EntityImpactfont',x+this.playerNameOffset.x, y+this.playerNameOffset.y, {
              font: new ig.Font('media/fonts/minecraftia14.png'),
              text: text,
              align: ig.Font.ALIGN.LEFT,
              zIndex:this.zIndex+1
          });
        },

        update: function(){
          this.parent();
          this.title.pos.x = this.pos.x + this.titleOffset.x;
          this.title.pos.y = this.pos.y + this.titleOffset.y;
          this.title.currentAnim.alpha = this.currentAnim.alpha;

          this.playerTitle.pos.x = this.pos.x + this.playerTitleOffset.x;
          this.playerTitle.pos.y = this.pos.y + this.playerTitleOffset.y;
          this.playerTitle.currentAnim.alpha = this.currentAnim.alpha;

          this.playerName.pos.x = this.pos.x + this.playerNameOffset.x;
          this.playerName.pos.y = this.pos.y + this.playerNameOffset.y;
          this.playerName.alpha = this.currentAnim.alpha;
        },

        draw: function() {
          this.parent();
        },

        setHealth: function (health){
          if(health < 1){
            this.currentAnim = this.anims.beaten;
          }else if(health < 3){
            this.currentAnim = this.anims.per25;
          }else if(health < 6){
            this.currentAnim = this.anims.per50;
          }else if(health < 8){
            this.currentAnim = this.anims.per75;
          }else{
            this.currentAnim = this.anims.full;
          }
        }
    });

  EntityArenaportraittitle = ig.Entity.extend({
      zIndex: 5,
      scale: {x:0.51,y:0.51},
      init: function(x,y,settings) {
        this.parent(x,y,settings);
        this.size = {x:175, y:45};
        var animSpeed = 0;
        this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'character_selection/text/combined_title.png', 175, 45);
        this.addAnim('rock', animSpeed, [1], true);
        this.addAnim('paper', animSpeed, [0], true);
        this.addAnim('scissors', animSpeed, [2], true);
        this.currentAnim = this.anims[settings.type];
      },
      update:function(){
        this.parent();
      },
      draw:function(){
        this.parent();
      }
  });



  EntityArenaportraitplayerbadge = ig.Entity.extend({
      zIndex: 6,
      anchor:{x:0,y:0},
      scale: {x:0.51,y:0.51},
      init: function(x,y,settings) {
        this.parent(x,y,settings);
        this.size = {x:55, y:45};
        var animSpeed = 0;
        this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'arena/text/players_alt_2.png', 76, 45);
        if(settings.isPlayer1){
          this.addAnim('idle', animSpeed, [0], true);
        }else{
          this.addAnim('idle', animSpeed, [1], true);
        }
        this.currentAnim = this.anims.idle;
      }
  });
});