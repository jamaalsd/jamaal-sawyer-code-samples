ig.module(
    'game.entities.arena.hit'
)
.requires(
    'impact.entity',
    'impact.entity-pool'
)
.defines(function(){
    EntityArenahitnumber = ig.Entity.extend({
      zIndex: 7.5,
      size:{x:60,y:44},
      scale: {x:1,y:1},
      init: function(x,y,settings) {
        this.parent(x,y,settings);
        this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'arena/text/hit_numbers.png', 60, 44);
        if(this.controller.isPlayer1){
          if(!this.controller.isWeakAgainst){
            this.addAnim('play', 0, [0], true);
          }else{
            this.addAnim('play', 0, [1], true);
          }
        }else{
          if(!this.controller.isWeakAgainst){
            this.addAnim('play', 0, [2], true);
          }else{
            this.addAnim('play', 0, [3], true);
          }
        }
        this.currentAnim = this.anims.play;
      },
      reset: function(x,y,settings){
        this.parent(x,y, settings);
        if(this.tweens.length > 0){
            this.stopTweens(false);
        }
        this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'arena/text/hit_numbers.png', 60, 44);
        if(this.controller.isPlayer1){
          if(!this.controller.isWeakAgainst){
            this.addAnim('play', 0, [0], true);
          }else{
            this.addAnim('play', 0, [1], true);
          }
        }else{
          if(!this.controller.isWeakAgainst){
            this.addAnim('play', 0, [2], true);
          }else{
            this.addAnim('play', 0, [3], true);
          }
        }
        this.currentAnim = this.anims.play;
        this.currentAnim.alpha = 1;
      },
  });

  EntityArenahittype = ig.Entity.extend({
      zIndex: 7.5,
      size:{x:102,y:44},
      anchor:{x:0,y:0},
      scale: {x:0.5,y:0.5},
      init: function(x,y,settings) {
        this.parent(x,y,settings);
        this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'arena/text/hit_type_alt.png', 102, 44);

        if(this.controller.currentAttack === 'high'){
          this.addAnim('play', 0, [0], true);
        }else if(this.controller.currentAttack === 'mid'){
          this.addAnim('play', 0, [1], true);
        }else if (this.controller.currentAttack === 'low'){
          this.addAnim('play', 0, [2], true);
        }else if (this.controller.currentAttack === 'null'){
          this.addAnim('play', 0, [3], true);
        }else{
          console.log(this.controller.currentAttack);
        }

        this.currentAnim = this.anims.play;
      },
      reset: function(x,y,settings){
        this.parent(x,y, settings);
        if(this.tweens.length > 0){
            this.stopTweens(false);
        }
        if(this.controller.currentAttack === 'high'){
          this.addAnim('play', 0, [0], true);
        }else if(this.controller.currentAttack === 'mid'){
          this.addAnim('play', 0, [1], true);
        }else if(this.controller.currentAttack === 'low'){
          this.addAnim('play', 0, [2], true);
        }else if (this.controller.currentAttack === 'null'){
          this.addAnim('play', 0, [3], true);
        }else{
          console.log(this.controller.currentAttack);
        }
        this.currentAnim = this.anims.play;
        this.currentAnim.alpha = 1;
      },
  });

  ig.EntityPool.enableFor( EntityArenahitnumber );
  ig.EntityPool.enableFor( EntityArenahittype );
});