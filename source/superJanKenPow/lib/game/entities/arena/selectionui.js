ig.module(
    'game.entities.arena.selectionui'
)
.requires(
    'impact.entity',
    'game.entities.ui.clickable'
)
.defines(function(){
    EntityArenaselectionui = ig.Entity.extend({
    zIndex: 10,
    size:{x:1,y:1},
    alpha:1,
    childArr:[],
    timerTime:7,

    init: function(x,y,settings){
      this.parent(x,y,settings);
      var player = (this.isPlayer1? 'player1.png':'player2.png');

      this.selectionTimer = ig.game.spawnEntity('Entitytimerhorizontal', 0, 0,{
        zIndex: 10,
        anchor: {x:0.0,y:0.0},
        controller:this
      });
      this.selectionTimer.onTimerComplete = function (){
        this.commitAttack();
      }.bind(this);
      this.selectionTimer.currentAnim.alpha = 0.0;

      this.quitButton = ig.game.spawnEntity('EntityArenaquitbutton', 0,14, {
        isPlayer1: this.isPlayer1,
        controller:this
      });
      ig.game.setCenterX(this.quitButton);
      this.addChild(this.quitButton);


      // this.playerText = ig.game.spawnEntity('EntityImpactimage', 100,180, {
      //   img: new ig.Image(ig.game.MEDIA_PATH+'arena/text/select_attack_'+player),
      //   size: {x: 131, y: 30},
      //   clickable: false,
      //   zIndex: 10,
      // });
      // this.addChild(this.playerText);


      this.selectText = ig.game.spawnEntity('EntityImpactimage', 65,220, {
        img: new ig.Image(ig.game.MEDIA_PATH+'arena/text/select_attack_text_'+player),
        size: {x: 200, y: 30},
        clickable: false,
        zIndex: 10,
      });
      this.addChild(this.selectText);

      this.high = ig.game.spawnEntity('EntityImpactimage', 15,260, {
        img: new ig.Image(ig.game.MEDIA_PATH+'arena/text/select_attack_high_'+player),
        size: {x: 80, y: 50},
        clickable: true,
        zIndex: 10,
        name:'high',
        controller:this
      });
      this.high.onClick = function (){
        if(ig.game.canClick)
        this.controller.buttonClicked(this.name);
      }.bind(this.high);
      this.addChild(this.high);

      this.mid = ig.game.spawnEntity('EntityImpactimage', 120,260, {
        img: new ig.Image(ig.game.MEDIA_PATH+'arena/text/select_attack_mid_'+player),
        size: {x: 80, y: 50},
        clickable: true,
        zIndex: 10,
        name:'mid',
        controller:this
      });
      this.mid.onClick = function (){
        if(ig.game.canClick)
        this.controller.buttonClicked(this.name);
      }.bind(this.mid);
      this.addChild(this.mid);

      this.low = ig.game.spawnEntity('EntityImpactimage', 225,260, {
        img: new ig.Image(ig.game.MEDIA_PATH+'arena/text/select_attack_low_'+player),
        size: {x: 80, y: 50},
        clickable: true,
        zIndex: 10,
        name:'low',
        controller:this
      });
      this.low.onClick = function (){
        if(ig.game.canClick)
        this.controller.buttonClicked(this.name);
      }.bind(this.low);

      this.addChild(this.low);

      this.descrip = ig.game.spawnEntity('EntityImpactimage', 0,435, {
        img: new ig.Image(ig.game.MEDIA_PATH+'arena/text/select_attack_descrip_'+player),
        anchor:{x:0,y:0},
        size: {x: 138, y: 55},
        clickable: false,
        zIndex: 10,
      });
      ig.game.setCenterX(this.descrip);
      this.addChild(this.descrip);

      this.selectionBoxes = [];
      var sBox = ig.game.spawnEntity('EntityPlayerselectionbox', 50,495,{
        isPlayer1: this.isPlayer1,
      });
      this.selectionBoxes.push(sBox);
      this.addChild(sBox);

      sBox = ig.game.spawnEntity('EntityPlayerselectionbox', 145,495,{
        isPlayer1: this.isPlayer1,
      });
      this.selectionBoxes.push(sBox);
      this.addChild(sBox);

      sBox = ig.game.spawnEntity('EntityPlayerselectionbox', 245,495,{
        isPlayer1: this.isPlayer1,
      });
      this.selectionBoxes.push(sBox);
      this.addChild(sBox);
    },

    update: function(){
      this.parent();
      for(var i = 0; i < this.childArr.length; i++){
        if(this.childArr[i].child.currentAnim){
          this.childArr[i].child.currentAnim.alpha= this.alpha;
        }
        this.childArr[i].child.pos.x = this.pos.x + this.childArr[i].origPos.x;
        this.childArr[i].child.pos.y = this.pos.y + this.childArr[i].origPos.y;
      }
    },

    buttonClicked:function(button){
      if(this.isPlayer1){
        if(ig.game.player1Variables.attackArr.length >= 3){
          return;
        }
      }else{
        if(ig.game.player2Variables.attackArr.length >= 3){
          return;
        }
      }
      switch(button){
        case 'high':
          if(this.isPlayer1){
            ig.game.player1Variables.attackArr.push('high');
          }else{
            ig.game.player2Variables.attackArr.push('high');
          }
          break;
        case 'mid':
          if(this.isPlayer1){
            ig.game.player1Variables.attackArr.push('mid');
          }else{
            ig.game.player2Variables.attackArr.push('mid');
          }
          break;
        case 'low':
          if(this.isPlayer1){
            ig.game.player1Variables.attackArr.push('low');
          }else{
            ig.game.player2Variables.attackArr.push('low');
          }
          break;
      }
      this.quitButton.reset();
      var length = (this.isPlayer1? ig.game.player1Variables.attackArr.length : ig.game.player2Variables.attackArr.length);
      this.selectionBoxes[length-1].select();

      if(length === 3){
        ig.game.playSFX(ig.game.s_playerSelectLast);
        this.commitAttack();
      }else{
        ig.game.playSFX(ig.game.s_playerSelect);
      }
    },

    commitAttack: function() {

      if(ig.game.mode === 'single') {
        this.beginSequence();
      } else {
        var attk = ig.game.player1Variables.attackArr;
        for(var i=0; i < 3; i++) {
          if(typeof(attk[i]) === 'undefined') {
            attk[i] = 'null';
          }
        }
        ig.game.logic.submitAttack(attk,this.beginSequence.bind(this));
      }

    },

    beginSequence: function(){
      var time = this.fadeOut(0.2);
        ig.game.canClick = false;
        this.controller.tween({junkTweenVar:0},time,{
          onComplete:function(){
            this.startSequence();
          }.bind(this.controller)
        }).start();
      },

    selectFadeVars: {
      dist: 50,
      time: 0.25
    },
    fadeIn: function(delay){
      delay = delay || 0;
      this.alpha = 0;
      this.resetSBoxes();
      this.quitButton.reset();
      var tTime = this.selectFadeVars.time;
      var tEasing = ig.Tween.Easing.Exponential.EaseOut;

      var dist = this.selectFadeVars.dist;
      this.pos.x += dist;

      this.tween({alpha:1.0},tTime,{
        delay:delay,
        onStart:function(){
          ig.game.playSFX(ig.game.s_playerWhoosh3);
        }.bind(this)
      }).start();
      this.tween({pos:{x:this.pos.x-dist}},tTime,{
        delay:delay,
        easing: tEasing
      }).start();

      this.selectionTimer.currentAnim.alpha = 0;
      this.selectionTimer.resetTimer();
      this.selectionTimer.tween( {currentAnim: {alpha:1} },tTime,{
        delay:delay,
        onComplete:function(){
          this.selectionTimer.startTimer(this.timerTime);
        }.bind(this)
      }).start();

      return delay+tTime;
    },

    fadeOut: function(delay){
      delay = delay || 0;
      this.alpha = 1;

      var tTime = this.selectFadeVars.time;
      var tEasing = ig.Tween.Easing.Exponential.EaseIn;

      var dist = this.selectFadeVars.dist;

      this.tween({alpha:0.0},tTime,{
        delay:delay
      }).start();
      this.tween({pos:{x:this.pos.x-dist}},tTime,{
        delay:delay,
        easing: tEasing,
        onStart:function(){
          ig.game.playSFX(ig.game.s_playerWhoosh3rev);
        }.bind(this),
        onComplete: function(){
          this.pos.x += this.selectFadeVars.dist;
        }.bind(this)
      }).start();

      this.selectionTimer.pauseTimer();
      this.selectionTimer.tween({currentAnim:{alpha:0.0}},tTime,{
        delay:delay
      }).start();

      return delay+tTime;
    },

    resetSBoxes: function(){
      for(var i = 0; i < this.selectionBoxes.length; i++){
        this.selectionBoxes[i].reset();
      }
    },

    addChild: function(child){
      this.childArr.push({
        origPos:{
          x:child.pos.x - this.pos.x,
          y:child.pos.y - this.pos.y
        },
        child: child
      });
    },

    killAll: function(){
      for(var i = 0; i < this.selectionBoxes.length; i++){
        this.selectionBoxes[i].kill();
      }
      this.selectionTimer.kill();
      this.selectText.kill();
      this.high.kill();
      this.mid.kill();
      this.low.kill();
      this.descrip.kill();
      this.quitButton.kill();
      this.childArr = [];
      this.kill();
    }
  });

  EntityPlayerselectionbox = ig.Entity.extend({
      zIndex: 10,
      size:{x:32,y:32},
      scale: {x:1,y:1},
      init: function(x,y,settings) {
        this.parent(x,y,settings);
        var player = (this.isPlayer1? 'player1.png':'player2.png');
        this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'arena/text/select_attack_selection_box_'+player, 32, 32);
        this.addAnim('selected',0.5,[0],true);
        this.addAnim('notSelected',0.5,[1],true);
        this.currentAnim = this.anims.notSelected;
      },
      select: function(){
        this.currentAnim = this.anims.selected;
        this.currentAnim.alpha = 1.0;
      },
      reset: function(){
        this.currentAnim = this.anims.notSelected;
        this.currentAnim.alpha = 1.0;
      }
  });

  EntityArenaquitbutton = EntityClickable.extend({
      zIndex: 10,
      size:{x:64,y:20},
      scale: {x:1,y:1},
      clickable: true,
      init: function(x,y,settings) {
        this.parent(x,y,settings);
        var player = (this.isPlayer1? 'player1.png':'player2.png');
        this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'arena/text/quit_button_'+player, 64, 20);
        this.addAnim('idle',0,[0],true);
        this.addAnim('confirm',0,[1],true);
        this.currentAnim = this.anims.idle;
      },
      reset: function(x,y,settings){
        this.currentAnim = this.anims.idle;
      },

      onClick: function(){
        if(ig.game.canClick){

          if(this.currentAnim === this.anims.idle){
            ig.game.playSFX(ig.game.s_playerSelectQuit);
            this.currentAnim = this.anims.confirm;
          }else if (this.currentAnim === this.anims.confirm){
            this.controller.selectionTimer.pauseTimer();
            ig.game.takeScreenShot();
            ig.game.playSFX(ig.game.s_textDisappear);
            ig.game.loadLevelDeferred(LevelMainmenu);
          }
        }
      }
  });
});