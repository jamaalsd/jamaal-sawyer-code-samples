ig.module(
    'game.entities.ui.impactfont'
).requires(
    'impact.entity'
).defines(function() {

    EntityImpactfont = ig.Entity.extend({
        _wmDrawBox:true,
        _wmScalable:true,
        _wmBoxColor:'#FF0000',
        size:{x:0,y:0},
        type: ig.Entity.TYPE.NEVER,
        alpha:1,
        font:undefined,
        text:'',
        name:'text',
        align: ig.Font.ALIGN.LEFT,
        clickable: false,


        init: function(x,y,settings) {
          this.parent(x,y,settings);
          if(!settings.font){
            this.font = new ig.Font('media/fonts/default.png');
          }else{
            this.font = settings.font;
          }
          if(this.clickable){
            this.hitbox = ig.game.spawnEntity('EntityImpactfonthitbox',this.pos.x,this.pos.y,{
              pText: this
            });
          }
        },

        update: function(){
          this.parent();
        },

        draw: function() {
          this.parent();
          if(this.alpha !== this.font.alpha){
            this.font.alpha = this.alpha;
          }
          this.font.draw(this.text, this.pos.x,this.pos.y, this.align);
        },

        set: function(text){
          this.text = text;
        },

        onClick: function(){
          if(!this.clickable) return;
          console.log('Click');
        }
    });

    EntityImpactfonthitbox = EntityClickable.extend({
      pOffsetX:0,
      name:'fontHitBox',
      init:function(x,y,settings){
        this.parent(x,y,settings);
        this.zIndex = this.pText.zIndex;
      },

      update: function(){
        this.parent();
        this.setSize();
        this.setPos();
      },

      draw: function() {
        this.parent();
      },

      setSize: function(){
        this.size = {
          x: this.pText.font.widthForString(this.pText.text),
          y: this.pText.font.heightForString(this.pText.text)
        };
      },

      setPos: function(){
        switch(this.pText.align){
          case ig.Font.ALIGN.LEFT:
            this.pOffsetX = 0;
            break;
          case ig.Font.ALIGN.RIGHT:
            this.pOffsetX = -this.pText.font.widthForString(this.pText.text);
            break;
          case ig.Font.ALIGN.CENTER:
            this.pOffsetX = -this.pText.font.widthForString(this.pText.text)/2;
            break;
        }
        this.pos.x = this.pText.pos.x + this.pOffsetX;
        this.pos.y = this.pText.pos.y;
      },

      onClick: function(){
        this.pText.onClick();
      }
    });
});