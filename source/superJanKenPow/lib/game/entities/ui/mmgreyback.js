ig.module(
    'game.entities.ui.mmgreyback'
).requires(
    'impact.entity'
).defines(function() {
    EntityMmgreyback= ig.Entity.extend({
        _wmDrawBox:true,
        _wmScalable:true,
        _wmBoxColor:'#FF0000',
        collides: ig.Entity.COLLIDES.NEVER,
        /* settings:
         * {img:ig.Image('imagefilename')}
         * width height
         */
        init: function(x,y,settings) {
          this.parent(x,y,settings);
          this.size = {x: ig.system.width, y: ig.system.height};
          this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'main_menu/greymmattack.png', ig.system.width, ig.system.height);
          this.addAnim('play', 0.1, [2,1,0], false);
          this.currentAnim = this.anims.play;
        },

        update: function(){
          this.parent();
        },

        draw: function() {
          this.parent();
        }

    });
});