ig.module(
    'game.entities.ui.impactimage'
).requires(
    'game.entities.ui.clickable'
).defines(function() {

    EntityImpactimage = EntityClickable.extend({
        _wmDrawBox:true,
        _wmScalable:true,
        _wmBoxColor:'#FF0000',
        size:{x:20,y:20},
        collides: ig.Entity.COLLIDES.NEVER,
        junkTweenVar:0,
        /* settings:
         * {img:ig.Image('imagefilename')}
         * width height
         */
        init: function(x,y,settings) {
          this.parent(x,y,settings);
          if(settings.img instanceof ig.AnimationSheet){
            this.animSheet = settings.img;
          }else{
            this.animSheet = new ig.AnimationSheet(settings.img.path, settings.size.x, settings.size.y);
          }
          this.addAnim('play', 1, [0], true);
          this.currentAnim = this.anims.play;
          if(settings.clickable !== undefined){
            this.clickable = settings.clickable;
          }
        },

        update: function(){
          this.parent();
        },

        draw: function() {
          this.parent();
        },

    });
});