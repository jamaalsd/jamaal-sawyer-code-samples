ig.module(
    'game.entities.ui.transition'
).requires(
    'game.entities.ui.impactimage'
).defines(function() {

    EntityTransition = EntityImpactimage.extend({
        _wmDrawBox:true,
        _wmScalable:true,
        _wmBoxColor:'#FF0000',
        size:{x:20,y:20},
        collides: ig.Entity.COLLIDES.NEVER,
        clickable: false,
        /* settings:
         * {img:ig.Image('imagefilename')}
         * width height
         */
        init: function(x,y,settings) {
          this.parent(x,y,settings);
          var animSheet = new ig.AnimationSheet(settings.img.path, settings.size.x, settings.size.y);
          this.currentAnim = new ig.Animation(animSheet,1,[0],false);
          if(settings.scale !== undefined){
            this.setScale(settings.scale.x, settings.scale.y);
          }
          if(settings.clickable !== undefined){
            this.clickable = settings.clickable;
          }
        },

        update: function(){
          this.parent();
        },

        draw: function() {
          this.parent();
        }

    });
});