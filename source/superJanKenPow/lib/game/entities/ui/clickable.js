ig.module(
    'game.entities.ui.clickable'
).requires(
    'impact.entity'
).defines(function(){

    EntityClickable = ig.Entity.extend({

        size: { x : 1, y : 1 },
        type: ig.Entity.TYPE.B,
        isOver : false,
        name: 'clickable',
        mouseIn : 0,
        clickable : true,
        debug: false,

        update: function() {
            this.parent();
            if(this.clickable){
                if (this.inFocus()) {
                    if(ig.input.released('leftButton') && this.isOver){
                        this.onRelease();
                    }else if(ig.input.pressed('leftButton')){
                        this.onClick();
                    }else if(!this.isOver){
                        this.onOver();
                        this.isOver = true;
                    }
                }else if(this.isOver){
                    this.onOut();
                    this.isOver = false;
                }
            }
        },

        onClick: function () {
            if(ig.game.canClick && this.debug)
            console.log(this.name + ': onClick');
        },

        onOver: function () {
            if(ig.game.canClick && this.debug)
            console.log(this.name + ': onOver');
        },

        onOut: function () {
            if(ig.game.canClick && this.debug)
            console.log(this.name + ': onOut');
        },

        onRelease: function () {
            if(ig.game.canClick && this.debug)
            console.log(this.name + ': onRelease');
        },

        inFocus: function() {
            return (
               (this.pos.x <= (ig.input.mouse.x + ig.game.screen.x)) &&
               ((ig.input.mouse.x + ig.game.screen.x) <= this.pos.x + this.size.x) &&
               (this.pos.y <= (ig.input.mouse.y + ig.game.screen.y)) &&
               ((ig.input.mouse.y + ig.game.screen.y) <= this.pos.y + this.size.y)
            );
         }
        
    });
});