ig.module(
    'game.entities.ui.mmtile'
).requires(
    'impact.entity'
).defines(function() {
    EntityMmtile = ig.Entity.extend({
        _wmDrawBox:true,
        _wmScalable:true,
        _wmBoxColor:'#FF0000',
        size:{x:119,y:1},
        collides: ig.Entity.COLLIDES.NEVER,
        /* settings:
         * {img:ig.Image('imagefilename')}
         * width height
         */
        init: function(x,y,settings) {
          this.parent(x,y,settings);
          this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'main_menu/mm_tile.png', 119, 1);
          var anims = [];
          this.addAnim('idle', 1, [0], true);
          this.addAnim('out', 1, [0,5,4,3,2,1], false);
          this.addAnim('in', 1, [0,1,2,3,4,5], false);
          this.currentAnim = this.anims.idle;
          if(settings.flip){
            this.anims.idle.flip.x = true;
            this.anims.out.flip.x = true;
            this.anims.in.flip.x = true;
          }
          this.setScale(1,568);
        },

        update: function(){
          this.parent();
        },

        draw: function() {
          this.parent();
        }

    });
});