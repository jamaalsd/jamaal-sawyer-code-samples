ig.module(
    'game.entities.ui.mmlogo'
).requires(
    'impact.entity'
).defines(function() {
    EntityMmlogo = ig.Entity.extend({
        _wmDrawBox:true,
        _wmScalable:true,
        _wmBoxColor:'#FF0000',
        size:{x:272,y:86},
        collides: ig.Entity.COLLIDES.NEVER,
        /* settings:
         * {img:ig.Image('imagefilename')}
         * width height
         */
        init: function(x,y,settings) {
          this.parent(x,y,settings);
          this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'main_menu/logo.png', 272, 86);
          this.addAnim('idle', 1, [0], true);
          this.currentAnim = this.anims.idle;
          this.highlight = ig.game.spawnEntity('EntityImpactimage',this.pos.x,this.pos.y, {
              img: new ig.Image(ig.game.MEDIA_PATH+'main_menu/logo_highlight.png'),
              size: {x:272, y:86},
              clickable: false,
              zIndex: this.zIndex + 1
          });
          this.highlight.currentAnim.alpha = 0;
        },

        update: function(){
          this.parent();
          this.highlight.pos.x = this.pos.x;
          this.highlight.pos.y = this.pos.y;
        },

        draw: function() {
          this.parent();
        }

    });
});