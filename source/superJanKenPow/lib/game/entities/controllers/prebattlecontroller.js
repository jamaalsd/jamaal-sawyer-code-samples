ig.module(
    'game.entities.controllers.prebattlecontroller'
).requires(
    'impact.entity',
    'game.entities.prebattle.portrait',
    'game.entities.prebattle.hand',
    'game.entities.prebattle.fighter'
).defines(function() {

    EntityPrebattlecontroller = ig.Entity.extend({
        _wmDrawBox:true,
        _wmScalable:true,
        _wmBoxColor:'#FF0000',
        size:{x:20,y:20},
        collides: ig.Entity.COLLIDES.NEVER,
        debugSetPlayers: false,
        shakeHands: true,
        tweenJunkVar: 0,
        player1HandPos: {x:-310,y:245},
        player2HandPos: {x:250,y:175},
        init: function(x,y,settings) {
          this.parent(x,y,settings);
          if(!ig.global.wm){
            ig.music.crossFade(0.01, 'prebattle');
            this.initPrebattle();
          }
        },

        player1ShakeVars: {x:{min:-0.5,max:0.5},y:{min:-0.5,max:0.5}},
        player2ShakeVars: {x:{min:0,max:0.5},y:{min:0,max:0.5}},
        update: function(){
          this.parent();
          if(this.shakeHands){
            this.player1Hand.pos.x = this.player1HandPos.x + ig.game.getRandomInt(this.player1ShakeVars.x.min,this.player1ShakeVars.x.max);
            this.player1Hand.pos.y = this.player1HandPos.y + ig.game.getRandomInt(this.player1ShakeVars.y.min,this.player1ShakeVars.y.max);

            this.player2Hand.pos.x = this.player2HandPos.x + ig.game.getRandomInt(this.player2ShakeVars.x.min,this.player2ShakeVars.x.max);
            this.player2Hand.pos.y = this.player2HandPos.y + ig.game.getRandomInt(this.player2ShakeVars.y.min,this.player2ShakeVars.y.max);
          }
        },

        draw: function() {
          this.parent();
        },

        initPrebattle: function(){
          this.createParticleGenerator();
          //this.particleGen.visible = false;

          if(ig.game.mode === 'single'){
            if(this.debugSetPlayers || !ig.game.player1Variables.isSet){
              this.setPlayerRandom(ig.game.player1Variables);
            }
            this.setPlayerRandom(ig.game.player2Variables);
            ig.game.player2Variables.name = 'CPU';
          }


          var overlay = ig.game.spawnEntity('EntityImpactimage', 0, 0, {
              img: new ig.Image(ig.game.MEDIA_PATH+'overlay.png'),
              size: {x: 320, y: 568},
              clickable: false,
              zIndex: 1000
            });

          this.flash = ig.game.spawnEntity('EntityImpactimage', ig.system.width/2 - 1, ig.system.height/2 - 1, {
              img: new ig.Image(ig.game.MEDIA_PATH+'white_transition.png'),
              scale: {x: ig.system.width, y: ig.system.height},
              size: {x: 1, y: 1},
              clickable: false,
              zIndex: overlay.zIndex-1,
            });
          this.flash.currentAnim.alpha = 0;

          this.black = ig.game.spawnEntity('EntityImpactimage', ig.system.width/2 - 1, ig.system.height/2 - 1, {
              img: new ig.Image(ig.game.MEDIA_PATH+'black_transition.png'),
              scale: {x: ig.system.width, y: ig.system.height},
              size: {x: 1, y: 1},
              clickable: false,
              zIndex: overlay.zIndex-2,
            });
          this.black.currentAnim.alpha = 0;

          this.player1Fighter = ig.game.spawnEntity('EntityPrebattleplayerfighter',-350,175,{
            zIndex: 2.6,
            isPlayer1:true,
            controller:this,
            scale:{x:2.0, y: 2.0},
          });
          this.player2Fighter = ig.game.spawnEntity('EntityPrebattleplayerfighter',350,175,{
            zIndex: 2.55,
            isPlayer1:false,
            controller:this,
            scale:{x:2.0, y: 2.0},
          });

          this.player1Portrait = ig.game.spawnEntity('EntityPrebattleportrait',20,15,{
            isPlayer1: true,
            zIndex: 1.1,
          });

          this.player2Portrait = ig.game.spawnEntity('EntityPrebattleportrait',215,15,{
            isPlayer1: false,
            zIndex: 1.1,
          });

          this.versusText = ig.game.spawnEntity('EntityImpactimage',140,50,{
            img: new ig.Image(ig.game.MEDIA_PATH+'prebattle/text/vs.png'),
            size: {x: 37, y: 27},
            scale: {x:1.45, y:1.45},
            clickable: false,
            zIndex: 1.1,
          });

          this.player1Portrait.pos.y -= 150;
          this.player2Portrait.pos.y -= 150;
          this.versusText.pos.y -= 150;


          this.background1 = ig.game.spawnEntity('EntityImpactimage', 0, 0, {
              img: new ig.Image(ig.game.MEDIA_PATH+'prebattle/background.png'),
              size: {x: 320, y: 568},
              clickable: false,
              zIndex: 0
            });

          this.background2 = ig.game.spawnEntity('EntityImpactimage', 0, 0, {
              img: new ig.Image(ig.game.MEDIA_PATH+'prebattle/background2.png'),
              size: {x: 320, y: 568},
              clickable: false,
              zIndex: 0.5
            });
          this.background2.currentAnim.alpha = 0.0;

          this.leftCurtain = ig.game.spawnEntity('EntityImpactimage', 0, 0, {
              img: new ig.Image(ig.game.MEDIA_PATH+'shared/title_screens/1_left.png'),
              size: {x: 160, y: 568},
              clickable: false,
              zIndex: 999
            });
          this.rightCurtain = ig.game.spawnEntity('EntityImpactimage', 160, 0, {
              img: new ig.Image(ig.game.MEDIA_PATH+'shared/title_screens/1_right.png'),
              size: {x: 160, y: 568},
              clickable: false,
              zIndex: 999
            });

          this.player1Hand = ig.game.spawnEntity('EntityPrebattlehand', -145, 230, {
            isPlayer1:true,
            zIndex: 4,
            scale: {x:1.5, y: 1.5},
            anchor: {x:0.0, y: 0.0},
          });

          this.player2Hand = ig.game.spawnEntity('EntityPrebattlehand', 120, 175, {
            isPlayer1:false,
          });

          this.player1Hand.currentAnim.angle = ig.game.degreesToRadians(-20);
          this.player2Hand.currentAnim.angle = ig.game.degreesToRadians(20);

          var tDelay = 0.5;
          var tTime = 0.15;
          var easing = ig.Tween.Easing.Linear.EaseNone;
          this.leftCurtain.tween({pos:{x:-160}},tTime,{
            easing:easing,
            delay: tDelay,
            onComplete: function (){
              this.kill();
            }.bind(this.leftCurtain)
          }).start();

          this.rightCurtain.tween({pos:{x:320}},tTime,{
            easing:easing,
            delay: tDelay,
            onStart: function(){
              ig.game.playSFX(ig.game.s_textDisappear);
            }.bind(this.rightCurtain),
            onComplete: function (){
              this.kill();
            }.bind(this.rightCurtain)
          }).start();


          this.tween({player1HandPos:{x:-145}},tTime,{
            easing:easing,
            delay: tDelay,
            onComplete: function (){
            }.bind(this)
          }).start();

          this.tween({player2HandPos:{x:120}},tTime,{
            easing:easing,
            delay: tDelay,
            onComplete: function (){
            }.bind(this)
          }).start();

          this.words = {};
          this.words.ready = ig.game.spawnEntity('EntityImpactimage', 160 - 145/2, 400, {
              img: new ig.Image(ig.game.MEDIA_PATH+'prebattle/text/ready.png'),
              size: {x: 145, y: 38},
              clickable: false,
              zIndex: 5,
            });
          this.words.ready.tweenInPos = this.words.ready.pos.x;
          this.words.ready.tweenOutPos = this.words.ready.pos.x - 50;
          this.words.ready.pos.x += 50;
          this.words.ready.currentAnim.alpha = 0;


          this.words.jan = ig.game.spawnEntity('EntityImpactimage', 160 - 107/2, 400, {
              img: new ig.Image(ig.game.MEDIA_PATH+'prebattle/text/jan.png'),
              size: {x: 107, y: 38},
              clickable: false,
              zIndex: 5,
            });
          this.words.jan.tweenInPos = this.words.jan.pos.x;
          this.words.jan.tweenOutPos = this.words.jan.pos.x - 50;
          this.words.jan.pos.x += 50;
          this.words.jan.currentAnim.alpha = 0;

          this.words.ken = ig.game.spawnEntity('EntityImpactimage', 160 - 107/2, 400, {
              img: new ig.Image(ig.game.MEDIA_PATH+'prebattle/text/ken.png'),
              size: {x: 107, y: 38},
              clickable: false,
              zIndex: 5,
            });
          this.words.ken.tweenInPos = this.words.ken.pos.x;
          this.words.ken.tweenOutPos = this.words.ken.pos.x - 50;
          this.words.ken.pos.x += 50;
          this.words.ken.currentAnim.alpha = 0;


          this.words.pow = ig.game.spawnEntity('EntityImpactimage', 160 - 91/2, 230, {
              img: new ig.Image(ig.game.MEDIA_PATH+'prebattle/text/pow.png'),
              size: {x: 91, y: 38},
              clickable: false,
              zIndex: 3,
            });
          this.words.pow.currentAnim.alpha = 0;

          tDelay += 1.0;
          tTime = 0.5;

          easing = ig.Tween.Easing.Exponential.EaseOut;
          var ta1 = this.words.ready.tween({currentAnim:{alpha:1.0}}, tTime, {
            easing:easing,
            delay:tDelay
          });
          var tp1 = this.words.ready.tween({pos:{x:this.words.ready.tweenInPos}}, tTime, {
            easing:easing,
            delay:tDelay,
            onComplete: function(){
              this.sequence1();
            }.bind(this)
          });

          tDelay = 0.2;
          easing = ig.Tween.Easing.Exponential.EaseOut;
          var ta2 = this.words.ready.tween({currentAnim:{alpha:0.0}}, tTime, {
            delay: tDelay,
            easing:easing
          });
          var tp2 = this.words.ready.tween({pos:{x:this.words.ready.tweenOutPos}}, tTime+0.01, {
            delay: tDelay,
            easing:easing,
            onComplete: function (){
              this.kill();
            }.bind(this.words.ready)
          });

          ta1.chain(ta2);
          ta1.start();

          tp1.chain(tp2);
          tp1.start();

          var skip = ig.game.spawnEntity('EntityImpactimage', 0, 4, {
            img: new ig.Image(ig.game.MEDIA_PATH+'prebattle/text/skip.png'),
            size: {x: 92, y: 46},
            anchor:{x:0.5,y:0},
            scale: {x:0.5, y: 0.5},
            clickable: true,
            zIndex: 10,
            onClick: function(){
              this.loadArena();
            }.bind(this)
          });
          ig.game.setCenterX(skip);
          skip.tween({currentAnim:{alpha:0.0}}, 5.0).start();

        },

        sequence1: function(){
          var tDelay  = 1.0;
          var tLDelay = 0.33;
          var tTime   = 1.0;
          var easing  = ig.Tween.Easing.Exponential.EaseOut;

          this.tween({player1ShakeVars: {x:{min:-3, max: 3}, y:{min:-3, max:3}}}, tTime/2, {
            delay: tDelay
          }).start();

          this.tween({player2ShakeVars: {x:{min:0, max: 3}, y:{min:0, max:3}}}, tTime/2, {
            delay: tDelay
          }).start();

          this.player1Hand.tween({currentAnim:{angle:ig.game.degreesToRadians(0)}},tTime,{
            loop: ig.Tween.Loop.Reverse,
            loopCount: 1,
            loopDelay: tLDelay,
            easing:easing,
            delay:tDelay,
          }).start();
          this.player2Hand.tween({currentAnim:{angle:ig.game.degreesToRadians(0)}},tTime,{
            loop: ig.Tween.Loop.Reverse,
            loopCount: 1,
            loopDelay: tLDelay,
            easing:easing,
            delay:tDelay
          }).start();

          this.background2.tween({currentAnim:{alpha:0.75}},tTime,{
            loop: ig.Tween.Loop.Reverse,
            loopCount: 1,
            loopDelay: tLDelay,
            easing:easing,
            delay:tDelay
          }).start();

          //tTime = 0.5;
          //easing = ig.Tween.Easing.Exponential.EaseOut;
          var ta1 = this.words.jan.tween({currentAnim:{alpha:1.0}}, tTime, {
            easing:easing,
            delay:tDelay
          });
          var tp1 = this.words.jan.tween({pos:{x:this.words.jan.tweenInPos}}, tTime, {
            easing:easing,
            delay:tDelay,
            onComplete: function(){
              this.sequence2();
            }.bind(this)
          });

          tDelay = 0.5;
          easing = ig.Tween.Easing.Exponential.EaseOut;
          var ta2 = this.words.jan.tween({currentAnim:{alpha:0.0}}, tTime, {
            delay: tDelay,
            easing:easing,
          });
          var tp2 = this.words.jan.tween({pos:{x:this.words.jan.tweenOutPos}}, tTime+0.01, {
            delay: tDelay,
            easing:easing,
            onComplete: function (){
              this.kill();
            }.bind(this.words.jan)
          });

          ta1.chain(ta2);
          ta1.start();

          tp1.chain(tp2);
          tp1.start();
        },

        sequence2: function(){
          var tDelay  = 1.5;
          var tLDelay = 0.33;
          var tTime   = 1.0;
          var easing  = ig.Tween.Easing.Exponential.EaseOut;



          this.tween({player1ShakeVars: {x:{min:-6, max: 6}, y:{min:-6, max:6}}}, tTime/2, {
            delay: tDelay
          }).start();

          this.tween({player2ShakeVars: {x:{min:0, max: 6}, y:{min:0, max:6}}}, tTime/2, {
            delay: tDelay
          }).start();

          this.player1Hand.currentAnim.angle = ig.game.degreesToRadians(-20);
          this.player1Hand.tween({currentAnim:{angle:ig.game.degreesToRadians(0)}},tTime,{
            loop: ig.Tween.Loop.Reverse,
            loopCount: 1,
            loopDelay: tLDelay,
            easing:easing,
            delay:tDelay
          }).start();
          this.player1Hand.currentAnim.angle = ig.game.degreesToRadians(0);

          this.player2Hand.currentAnim.angle = ig.game.degreesToRadians(20);
          this.player2Hand.tween({currentAnim:{angle:ig.game.degreesToRadians(0)}},tTime,{
            loop: ig.Tween.Loop.Reverse,
            loopCount: 1,
            loopDelay: tLDelay,
            easing:easing,
            delay:tDelay
          }).start();
          this.player2Hand.currentAnim.angle = ig.game.degreesToRadians(0);

          this.background2.tween({currentAnim:{alpha:0.75}},tTime,{
            loop: ig.Tween.Loop.Reverse,
            loopCount: 1,
            loopDelay: tLDelay,
            easing:easing,
            delay:tTime+tDelay/3
          }).start();

          //tTime = 0.5;
          //easing = ig.Tween.Easing.Exponential.EaseOut;
          var ta1 = this.words.ken.tween({currentAnim:{alpha:1.0}}, tTime, {
            easing:easing,
            delay:tDelay
          });
          var tp1 = this.words.ken.tween({pos:{x:this.words.ken.tweenInPos}}, tTime, {
            easing:easing,
            delay:tDelay,
            onComplete: function(){
              this.sequence3();
            }.bind(this)
          });

          tDelay = 0.5;
          easing = ig.Tween.Easing.Exponential.EaseOut;
          var ta2 = this.words.ken.tween({currentAnim:{alpha:0.0}}, tTime, {
            delay: tDelay,
            easing:easing,
          });
          var tp2 = this.words.ken.tween({pos:{x:this.words.ken.tweenOutPos}}, tTime+0.01, {
            delay: tDelay,
            easing:easing,
            onComplete: function (){
              this.kill();
            }.bind(this.words.ken)
          });

          ta1.chain(ta2);
          ta1.start();

          tp1.chain(tp2);
          tp1.start();
        },

        sequence3: function(){
          var tDelay  = 1.5;
          var tTime   = 0.1;
          var easing  = ig.Tween.Easing.Linear.EaseNone;

          this.tween({player1ShakeVars: {x:{min:-9, max: 9}, y:{min:-9, max:9}}}, tTime/2, {
            delay: tDelay
          }).start();

          this.tween({player2ShakeVars: {x:{min:0, max: 9}, y:{min:0, max:9}}}, tTime/2, {
            delay: tDelay
          }).start();

          this.player1Hand.currentAnim.angle = ig.game.degreesToRadians(-20);
          this.player1Hand.tween({currentAnim:{angle:ig.game.degreesToRadians(0)}},tTime,{
            easing:easing,
            delay:tDelay,
            onStart: function(){
              this.shakeHands = false;
            }.bind(this),
            onComplete: function(){
              this.sequence4();
            }.bind(this)
          }).start();
          this.player1Hand.currentAnim.angle = ig.game.degreesToRadians(0);

          this.player2Hand.currentAnim.angle = ig.game.degreesToRadians(20);
          this.player2Hand.tween({currentAnim:{angle:ig.game.degreesToRadians(0)}},tTime,{
            easing:easing,
            delay:tDelay
          }).start();
          this.player2Hand.currentAnim.angle = ig.game.degreesToRadians(0);

          this.background2.tween({currentAnim:{alpha:1}},tTime,{
            easing:easing,
            delay:tDelay,
            onComplete:function(){
              this.kill();
            }.bind(this.background1)
          }).start();

          this.flash.tween({currentAnim:{alpha:1.0}}, tTime, {
            delay: tDelay,
            loop:ig.Tween.Loop.Reverse,
            loopCount:1,
            loopDelay:0.1,
            onComplete: function(){
            }.bind(this.flash)
          }).start();

        },

        sequence4: function (){
          this.player1Hand.setState(ig.game.player1Variables.type);
          this.player2Hand.setState(ig.game.player2Variables.type);
          ig.game.playSFX(ig.game.s_logoAppear);
          ig.game.playSFX(ig.game.s_subboom);
          ig.music.fadeOut(3.5);
          this.player1Hand.vel = {x:-6, y: 12};
          this.player2Hand.vel = {x:6, y: -12};
          this.particleGen.visible = true;
          this.words.pow.currentAnim.alpha = 1.0;
          var tTime = 1.0;
          this.words.pow.tween({scale:{x:4, y: 4}}, tTime).start();
          this.words.pow.tween({currentAnim:{alpha:0}}, tTime,{
            onComplete: function (){
              this.kill();
            }.bind(this.words.pow)
          }).start();

          this.tween({junkTweenVar:0},tTime,{
            onComplete: function (){
              this.sequence5();
            }.bind(this)
          }).start();
        },

        sequence5: function(){
          var tTime = 0.5;
          var tDelay = 0.33;
          var easing = ig.Tween.Easing.Exponential.EaseOut;
          this.player1Fighter.currentAnim.alpha = 0.0;
          this.player2Fighter.currentAnim.alpha = 0.0;

          this.player1Fighter.pos.x = -100;
          this.player1Fighter.tween({currentAnim:{alpha:1.0}},tTime,{
            onStart: function (){
              this.player1Hand.currentAnim.alpha = 0.75;
            }.bind(this),
            easing: easing,
            delay: tDelay,
          }).start();
          this.player1Fighter.tween({pos:{x:-80}},tTime,{
            delay: tDelay,
            easing: easing,
            onStart:function(){
              ig.game.playVoice(ig.game.s_names.player1[ig.game.player1Variables.type]);
            }.bind(this.player1Fighter),
            onComplete:function (){
            }.bind(this)
          }).start();

          this.player2Fighter.pos.x = 185;
          this.player2Fighter.tween({currentAnim:{alpha:1.0}},tTime,{
            easing: easing,
            delay: tDelay+0.5,
          }).start();
          this.player2Fighter.tween({pos:{x:140}},tTime,{
            delay: tDelay+0.5,
            easing: easing,
            onStart:function(){
              this.controller.sequence6();
              ig.game.playVoice(ig.game.s_names.player2[ig.game.player2Variables.type]);
            }.bind(this.player2Fighter)
          }).start();

        },

        sequence6: function(){
          var tTime = 0.25;
          var tDelay = 0.0;
          var easing = ig.Tween.Easing.Exponential.EaseOut;
          this.player1Portrait.currentAnim.alpha = 0;
          this.player2Portrait.currentAnim.alpha = 0;
          this.versusText.currentAnim.alpha = 0;

          this.player1Portrait.tween({currentAnim:{alpha:1.0}},tTime,{
            //easing: easing,
            delay: tDelay,
          }).start();
          this.player1Portrait.tween({pos:{y: this.player1Portrait.pos.y + 150}},tTime,{
            delay: tDelay,
            easing: easing,
          }).start();

          this.player2Portrait.tween({currentAnim:{alpha:1.0}},tTime,{
            //easing: easing,
            delay: tDelay,
          }).start();
          this.player2Portrait.tween({pos:{y: this.player2Portrait.pos.y + 150}},tTime,{
            delay: tDelay,
            easing: easing,
          }).start();

          this.versusText.tween({currentAnim:{alpha:1.0}},tTime,{
            //easing: easing,
            delay: tDelay,
          }).start();
          this.versusText.tween({pos:{y: this.versusText.pos.y + 150}},tTime,{
            delay: tDelay,
            easing: easing,
            onComplete:function (){
              this.sequence7();
            }.bind(this)
          }).start();
        },

        sequence7: function (){
          var tTime = 1.0;
          var tDelay = 0.5;
          var easing = ig.Tween.Easing.Exponential.EaseOut;
          this.flash.tween({currentAnim:{alpha:1.0}}, tTime, {
            delay: tDelay,
            onComplete: function(){
              this.loadArena();
            }.bind(this)
          }).start();
        },

        createParticleGenerator: function(){

          this.particleGen = ig.game.spawnEntity('EntityParticleGenerator', 0, 575,{
            visible : false,
            particleClass: 'EntityFireParticle',
            size: {
              x: 250,
              y: 1
            },
            spawnRate : 0.2,
            particles : 1,
            pVel:{
              x:{
                min: 0,
                max: 0
              },
              y:{
                min:-500,
                max:-300
              }
            },
            pScale : {
              proportional : true,
              min : 1.5,
              max : 3
            },
            pIndex : {
              min : 2.5,
              max : 3.5
            },
            pAlpha : {
              min : 0.33,
              max : 0.85
            },
            // pScaleOut : {
            //     enabled : true,
            //     proportional : false,
            //     min : 1,
            //     max : 6,
            //     time : {
            //         min : 1,
            //         max : 5
            //     }
            // },
            pAngle:{
              min:45,
              max:45
            },
            pFadeOut : {
              enabled: true,
              time: {
                    min : 0.5,
                    max : 2.5
                },
            },
            // pRotate : {
            //   enabled: true,
            //   direction : 2,
            //   time : {
            //       min : 0.50,
            //       max : 1.0
            //   }
            // },
            pLifetime : 2.5
          });
          ig.game.setCenterX(this.particleGen);

        },

        loadArena: function (){
          //if(ig.game.mode === 'single'){
            ig.game.loadLevelDeferred(LevelArena);
          //}
        },

        setPlayerRandom: function(player){
          switch(ig.game.getRandomInt(1,3)){
            case 1:
              player.type = 'rock';
              break;
            case 2:
              player.type = 'paper';
              break;
            case 3:
              player.type = 'scissors';
              break;
          }
          player.isSet = true;
        }
    });
});