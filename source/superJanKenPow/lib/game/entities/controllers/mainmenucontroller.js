ig.module(
    'game.entities.controllers.mainmenucontroller'
).requires(
    'impact.entity',
    'game.entities.controllers.mainmenubuttoncontroller',
    'game.entities.ui.mmgreyback',
    'game.entities.ui.mmtile',
    'game.entities.ui.mmlogo'
).defines(function() {

    EntityMainmenucontroller = ig.Entity.extend({
        _wmDrawBox:true,
        _wmScalable:true,
        _wmBoxColor:'#FF0000',
        size:{x:20,y:20},
        collides: ig.Entity.COLLIDES.NEVER,
        bgTiles: [],
        zIndex: 10000,
        init: function(x,y,settings) {
          this.parent(x,y,settings);
          if(!ig.global.wm){
            ig.music.crossFade(0.01, 'intro');
            this.initMainMenu();
          }
        },

        update: function(){
          this.parent();

        },

        draw: function() {
          this.parent();
        },

        initMainMenu: function(){
          ig.game.player1Variables.isSet = false;
          ig.game.player2Variables.isSet = false;
          this.createParticleGenerator();
          var overlay = ig.game.spawnEntity('EntityImpactimage', 0, 0, {
              img: new ig.Image(ig.game.MEDIA_PATH+'overlay.png'),
              size: {x: ig.system.width, y: ig.system.height},
              clickable: false,
              zIndex: 1000
            });

          var underlay = ig.game.spawnEntity('EntityImpactimage', 0, 0, {
              img: new ig.Image(ig.game.MEDIA_PATH+'black_transition.png'),
              anchor: {x: 0, y: 0},
              scale: {x: ig.system.width, y: ig.system.height},
              size: {x: 1, y: 1},
              clickable: false,
              zIndex: -1000
            });

          var tile = ig.game.spawnEntity('EntityMmtile',0,ig.system.height/2-1, {flip:false, zIndex: 0});
          //tile.currentAnim = tile.anims.out;
          this.bgTiles.push(tile);
          ig.game.spawnEntity('EntityImpactimage',119,ig.system.height/2-1, {
              img: new ig.Image(ig.game.MEDIA_PATH+'main_menu/menu_center.png'),
              scale: {x:1, y:568},
              size: {x:82, y:1},
              clickable: false,
              zIndex: 0
          });
          tile = ig.game.spawnEntity('EntityMmtile',201,ig.system.height/2-1,{flip:true, zIndex: 0});
          //tile.currentAnim = tile.anims.out;
          this.bgTiles.push(tile);

          var back = ig.game.spawnEntity('EntityMmgreyback',0,0,{zIndex:2});
          back.currentAnim.alpha = 0.08;

          var logo = ig.game.spawnEntity('EntityMmlogo', 0, 40, {zIndex:6});
          logo.currentAnim.alpha = 0.0;
          ig.game.setCenterX(logo);

          var buttonController = ig.game.spawnEntity('EntityMainmenubuttoncontroller',0,0,{controller:this});
          buttonController.setMenuItems({
            items: ['vs_cpu','online','options','instructions','remove_ads'],
            startY: 150,
            offsetY: 75
          });

          this.nameText = ig.game.spawnEntity('EntityImpactfont',8,5,{
            zIndex: 5,
            alpha : 1,
            text:'Name: '+ig.game.player1Variables.name,
            align: ig.Font.ALIGN.LEFT,
            font: new ig.Font('media/fonts/minecraftia14.png')
          });

          if(ig.game.screenshot){
            logo.currentAnim.alpha = 1.0;
            var tTime = 0.5;
            var tDelay = 0.05;
            ig.game.canDraw = false;
            sprite = ig.game.spawnEntity('EntityImpactimage', 0, 0, {
              img: ig.game.screenshot.animSheet,
              size: {x: ig.system.width, y: ig.system.height},
              clickable: false,
              zIndex: overlay.zIndex+1,
              controller:this
            });
            sprite.tween({currentAnim:{alpha:0.0}},tTime,{
              delay: tDelay,
              easing: ig.Tween.Easing.Exponential.EaseOut,
              onStart: function(){
                ig.game.canDraw = true;
              }.bind(this),
              onComplete: function (){
                this.kill();
              }.bind(sprite)
            }).start();
            sprite.tween({scale:{x:1.25,y:1.25}},tTime,{
              easing: ig.Tween.Easing.Exponential.EaseOut,
              delay: tDelay
            }).start();
            buttonController.tweenButtonsIntro({
              moveStartDelay: tDelay,
              offsetMoveDelay: 0,
              alphaDelay: tDelay,
              moveDuration: 0,
              moveDistance: 0,
              moveEase: ig.Tween.Easing.Exponential.EaseOut
            });
            this.particleGen.visible = true;
          }else{
            this.nameText.alpha = 0;
            this.createIntroTransition(logo, buttonController);
          }

        },

        createIntroTransition: function(logo, buttonController) {
          var i, sprite;
          var layers = 16;
          var offset = ig.system.height/layers;
          var transSpriteArr = [];
          var tDuration = 0.12;
          var tDelay = tDuration * 0.5;
          var tWait = 0.7;
          var tEase = ig.Tween.Easing.Linear.EaseNone;
          for(i = 0; i < layers; i++){
            sprite = ig.game.spawnEntity('EntityImpactimage', ig.system.width/2 - 1, i * offset + offset/2 - 1,{
              img: new ig.Image(ig.game.MEDIA_PATH+'black_transition.png'),
              scale: {x: ig.system.width, y: offset+1},
              size: {x: 1, y: 1},
              clickable: false,
              zIndex: 100
            });

            var toX;
            if(i%2 === 0){
              toX = sprite.pos.x - sprite.size.x - 2;
            }else{
              toX = sprite.pos.x + sprite.size.x + 2;
            }
            sprite.tween({pos:{x : toX}}, tDuration,{
              onStart: function (){
                //ig.game.playSFX(ig.game.s_textAppear);
              }.bind(this),
              onComplete: function (){
                this.kill();
              }.bind(sprite),
              delay: i * tDelay + tWait,
              easing: tEase,
            }).start();
          }

          buttonController.tweenButtonsIntro({
            moveStartDelay: (i * tDelay)/6 + tWait + tDelay,
            offsetMoveDelay: 0.15,
            alphaDelay: i * tDelay + tWait+tDelay,
            moveDuration: 0.3,
            moveDistance: 100,
            moveEase: ig.Tween.Easing.Exponential.EaseOut
          });


          sprite = ig.game.spawnEntity('EntityImpactimage', 0, 0, {
              img: new ig.Image(ig.game.MEDIA_PATH+'black_transition.png'),
              anchor: {x:0,y:0},
              scale: {x:ig.system.width/2, y: ig.system.height/2},
              size: {x: 2, y: 2},
              clickable: false,
              zIndex: 100,
            });
          sprite.currentAnim.alpha = 1.0;

          sprite.tween({currentAnim:{alpha:0.0}}, (tDuration-tDelay)*layers+tDelay, {
            onComplete: function (){this.kill();}.bind(sprite),
            delay: tWait,
            easing: tEase
          }).start();


          sprite = ig.game.spawnEntity('EntityImpactimage', 0, 0, {
              img: new ig.Image(ig.game.MEDIA_PATH+'white_transition.png'),
              anchor:{x:0,y:0},
              scale: {x: ig.system.width/2, y: ig.system.height/2},
              size: {x: 2, y: 2},
              clickable: false,
              zIndex: 10,
              controller: this,
            });
          sprite.currentAnim.alpha = 0.0;

          var lDuration = 0.1;
          var lDuration2 = 1;
          var lDelay = i * tDelay + tWait+tDelay;

          var tween1 = logo.highlight.tween({currentAnim:{alpha:1.0}}, lDuration, {
            onStart: function (){
                ig.game.playSFX(ig.game.s_logoAppear);
              }.bind(this),
            delay: lDelay,
            easing: tEase
          });
          var tween2 = logo.highlight.tween({currentAnim:{alpha:0.0}}, lDuration2, {
            onStart: function (){this.currentAnim.alpha = 1;}.bind(logo),
            // delay: i * tDelay + tWait,
            easing: tEase
          });
          tween1.chain(tween2);
          tween1.start();

          tween1 = sprite.tween({currentAnim:{alpha:0.25}}, lDuration, {
            delay: lDelay,
            easing: tEase
          });
          tween2 = sprite.tween({currentAnim:{alpha:0.0}}, lDuration2, {
            onStart: function(){
              this.controller.particleGen.visible = true;
            }.bind(sprite),
            onComplete: function (){
              this.controller.displayText();
              this.kill();
            }.bind(sprite),
            // delay: i * tDelay + tWait,
            easing: tEase
          });
          tween1.chain(tween2);
          tween1.start();
        },

        createParticleGenerator: function(){
          this.particleGen = ig.game.spawnEntity('EntityParticleGenerator', 0, 575,{
            visible : false,
            size: {
              x: 70,
              y: 1
            },
            spawnRate : 0.2,
            particles : 1,
            pVel:{
              x:{
                min: -0,
                max: 0
              },
              y:{
                min:-500,
                max:-200
              }
            },
            pScale : {
              proportional : true,
              min : 1,
              max : 4
            },
            pIndex : {
              min : 1.5,
              max : 1.5
            },
            pAlpha : {
              min : 0.1,
              max : 0.5
            },
            // pScaleOut : {
            //     enabled : true,
            //     proportional : false,
            //     min : 1,
            //     max : 6,
            //     time : {
            //         min : 1,
            //         max : 5
            //     }
            // },
            // pAngle:{
            //   min:45,
            //   max:45
            // },
            pFadeOut : {
              enabled: true,
              time: {
                    min : 0.5,
                    max : 3.0
                },
            },
            pRotate : {
              enabled: true,
              direction : 2,
              time : {
                  min : 0.50,
                  max : 1.0
              }
            },
            pLifetime : 3
          });
          ig.game.setCenterX(this.particleGen);
        },

        displayText: function(){
          var xPosFrom = 10;
          var tTime = 0.66;
          var tDelay = 0;
          var tEase = ig.Tween.Easing.Exponential.EaseOut;
          this.nameText.tween({alpha:1},tTime,{
            easing:tEase,
            delay: tDelay
          }).start();
          this.nameText.pos.x -= xPosFrom;
          this.nameText.tween({pos:{x:this.nameText.pos.x+xPosFrom}},tTime,{
            easing:tEase,
            delay: tDelay
          }).start();
        }

    });
});