ig.module(
    'game.entities.controllers.arenacontroller'
).requires(
    'impact.entity',
    'game.entities.arena.winloseui',
    'game.entities.arena.hit',
    'game.entities.arena.fighter',
    'game.entities.arena.selectionui',
    'game.entities.arena.health',
    'game.entities.arena.portrait'
).defines(function() {

    EntityArenacontroller = ig.Entity.extend({
        _wmDrawBox:true,
        _wmScalable:true,
        _wmBoxColor:'#FF0000',
        size:{x:20,y:20},
        collides: ig.Entity.COLLIDES.NEVER,
        bgTiles: [],
        sequenceStarted: false,
        roundsStartVal: 3,
        roundsLeft: 0,
        roundReady: true,
        tweenJunkVar: 0,
        init: function(x,y,settings) {
          this.parent(x,y,settings);
          ig.game.canClick = false;
          if(!ig.global.wm){
            ig.music.crossFade(0.01, 'battle');
            this.selectHitSounds();
            this.initDebugInput();
            this.initArena();
            this.startIntro();
          }
        },

        update: function(){
          this.parent();
          this.debugInput();
          if(this.sequenceStarted && this.roundReady){
            if(this.roundsLeft > 0){
              this.sequenceRound();
            }else{
              this.sequenceEnd();
            }
          }
        },

        draw: function() {
          this.parent();
        },

        introVars:{
          flashTime: 1.0,
          portraitInTime: 0.2,
          scrollTime: 1.5,
          fighterDelay: 0.2,
          selectionDelay: 0.5
        },
        startIntro:function(){
          this.player1Fighter.currentAnim.alpha = 0;
          this.player2Fighter.currentAnim.alpha = 0;
          this.arenaBackground.pos.y += 200;
          this.player1Portrait.currentAnim.alpha = 0;
          this.player2Portrait.currentAnim.alpha = 0;
          this.player1Health.currentAnim.alpha = 0;
          this.player2Health.currentAnim.alpha = 0;
          var flash = ig.game.spawnEntity('EntityImpactimage', 0, 0, {
            img: new ig.Image(ig.game.MEDIA_PATH+'white_transition.png'),
            scale: {x: ig.system.width, y: ig.system.height},
            anchor: {x:0,y:0},
            size: {x: 1, y: 1},
            clickable: false,
            zIndex: 100,
          });
          flash.currentAnim.alpha = 1.0;
          flash.tween({currentAnim:{alpha:0.0}},this.introVars.flashTime,{
            onComplete: function(){
              this.kill();
            }.bind(flash)
          }).start();

          this.arenaBackground.tween({pos:{y:-200}}, this.introVars.scrollTime, {
            onComplete:function(){
              this.player1Portrait.tween({currentAnim:{alpha:1.0}},this.introVars.portraitInTime,{
              }).start();

              this.player2Portrait.tween({currentAnim:{alpha:1.0}},this.introVars.portraitInTime,{
              }).start();

              this.player1Health.tween({currentAnim:{alpha:1.0}},this.introVars.portraitInTime,{
              }).start();

              this.player2Health.tween({currentAnim:{alpha:1.0}},this.introVars.portraitInTime,{
                onComplete:function(){
                  var time = this.player1Fighter.fadeIn(this.introVars.fighterDelay);
                  this.player2Fighter.fadeIn(this.introVars.fighterDelay);
                  this.startSelectionUI(this.introVars.selectionDelay + time);
                }.bind(this)
              }).start();
            }.bind(this)
          }).start();
          ig.game.playSFX(ig.game.s_iyou);

        },

        startSelectionUI:function(delay){
          delay = delay || 0;
          var time = this.selectionUI.fadeIn(delay);
          this.tween({junkTweenVar:0}, time, {
            onComplete:function (){
              ig.game.canClick = true;
            }.bind(this)
          }).start();
        },

        startSequence: function(){
          /*debugonly*/

          if(ig.game.mode === 'single') {
            this.fillAttackArrayNull(ig.game.player1Variables.attackArr);
            this.fillAttackArrayRandom(ig.game.player2Variables.attackArr);
          }
          this.roundsLeft = this.roundsStartVal;
          this.roundReady = true;
          this.sequenceStarted = true;

        },

        sequenceRound: function(){
          var firstRound;
          if(this.roundsLeft === this.roundsStartVal){
            firstRound = true;
          }else{
            firstRound = false;
          }
          if(firstRound){
            this.underlay.tween({currentAnim:{alpha:0.5}},0.15,{
              onComplete: function(){
                var delay = this.player1Fighter.fadeOut(0.15);
                this.player2Fighter.fadeOut(0.15);
                this.tween({junkTweenVar:0},delay,{
                  onComplete:function(){
                    this.player1Fighter.attack();
                    this.player2Fighter.attack();
                  }.bind(this)
                }).start();
              }.bind(this)
            }).start();
          }else{
            var tTime = 0.15;
            this.tween({junkTweenVar:0},tTime,{
              onComplete:function(){
                this.player1Fighter.attack();
                this.player2Fighter.attack();
              }.bind(this)
            }).start();
          }
          this.roundReady = false;
          this.roundsLeft--;
        },

        sequenceEnd: function(){
          this.roundReady = false;
          var delay = this.player1Fighter.fadeIn();
          this.player2Fighter.fadeIn();
          this.underlay.tween({currentAnim:{alpha:0.0}},0.15,{
            delay: delay,
            onComplete:function(){
              this.sequenceStarted = false;
              if(!this.player1Fighter.isDead && !this.player2Fighter.isDead){
                this.startSelectionUI();
              }
            }.bind(this)
          }).start();
          ig.game.player1Variables.attackArr = [];
          ig.game.player2Variables.attackArr = [];
        },

        playerDeath: function(player){
          var delay;
          switch(player){
            case 1:
              delay = this.player2Fighter.fadeIn();
              break;
            case 2:
              delay = this.player1Fighter.fadeIn();
              break;
          }

          this.underlay.tween({currentAnim:{alpha:0.0}},0.15,{
            delay: delay,
            onComplete:function(){
                var playerWin = (this.player2Fighter.isDead? true:false);
                this.winLoseUI = ig.game.spawnEntity('EntityArenawinloseui',0,0,{
                  isPlayer1:true,
                  playerWin: playerWin
                });
                this.winLoseUI.alpha = 0;
                var time = this.winLoseUI.fadeIn(0.5);
                this.tween({junkTweenVar:0},time,{
                  onComplete:function(){
                    ig.game.canClick = true;
                  }.bind(this)
                }).start();
              }.bind(this)
          }).start();

          this.selectionUI.killAll();


        },

        initArena: function(){
          ig.game.player1Variables.attackArr = [];
          ig.game.player2Variables.attackArr = [];
          if(!ig.game.player1Variables.isSet){
            this.setPlayerRandom(ig.game.player1Variables);
            this.setPlayerRandom(ig.game.player2Variables);
            ig.game.player2Variables.name = 'CPU';
          }
          this.setPlayerHealth(ig.game.HEALTH_PER_ROUND);
          var overlay = ig.game.spawnEntity('EntityImpactimage', 0, 0, {
              img: new ig.Image(ig.game.MEDIA_PATH+'overlay.png'),
              size: {x: 320, y: 568},
              clickable: false,
              zIndex: 1000
          });
          this.initArenaBackground();
          this.initArenaUI();
        },

        initArenaBackground: function(){
          var arenaSelect;
          switch(ig.game.getRandomInt(1,3)){
            case 1:
              arenaSelect = ig.game.MEDIA_PATH+'arena/backgrounds/1.png';
              break;
            case 2:
              arenaSelect = ig.game.MEDIA_PATH+'arena/backgrounds/2.png';
              break;
            case 3:
              arenaSelect = ig.game.MEDIA_PATH+'arena/backgrounds/3.png';
              break;
          }

          this.arenaBackground = ig.game.spawnEntity('EntityImpactimage',0,-200, {
              img: new ig.Image(arenaSelect),
              size: {x:480, y:852},
              clickable: false,
              zIndex: 0
          });
          ig.game.setCenterX(this.arenaBackground);

          this.underlay = ig.game.spawnEntity('EntityImpactimage', 0,0, {
            img: new ig.Image(ig.game.MEDIA_PATH+'black_transition.png'),
            scale: {x: ig.system.width, y: ig.system.height},
            size: {x: 1, y: 1},
            anchor: {x:0,y:0},
            clickable: false,
            zIndex: 1,
          });
          this.underlay.currentAnim.alpha = 0.0;
        },

        initArenaUI: function(){
          this.player1Portrait = ig.game.spawnEntity('EntityArenaportrait', 20, 15, {
            isAlternate:false,
            isPlayer1:true,
            type:ig.game.player1Variables.type
          });

          var isAlternate = ((ig.game.player1Variables.type === ig.game.player2Variables.type)?true:false);
          this.player2Portrait = ig.game.spawnEntity('EntityArenaportrait', 215, 15, {
            isAlternate:isAlternate,
            isPlayer1: false,
            type:ig.game.player2Variables.type
          });

          // this.versusText = ig.game.spawnEntity('EntityImpactimage',140,50,{
          //   img: new ig.Image(ig.game.MEDIA_PATH+'prebattle/text/vs.png'),
          //   size: {x: 37, y: 27},
          //   scale: {x:1.45, y:1.45},
          //   clickable: false,
          //   zIndex: 5,
          // });

          this.player1Health = ig.game.spawnEntity('EntityArenahealth', 32, 136);
          this.player2Health = ig.game.spawnEntity('EntityArenahealth', 226, 136);

          this.player1Health.setHealth(ig.game.player1Variables.health);
          this.player1Portrait.setHealth(ig.game.player1Variables.health);

          this.player2Health.setHealth(ig.game.player2Variables.health);
          this.player2Portrait.setHealth(ig.game.player2Variables.health);

          this.player1Fighter = ig.game.spawnEntity('EntityArenafighter', 28, 320,{
            type:ig.game.player1Variables.type,
            isPlayer1:true,
            controller:this,
            zIndex:7.5
          });

          this.player2Fighter = ig.game.spawnEntity('EntityArenafighter', 204, 320,{
            type:ig.game.player2Variables.type,
            isPlayer1:false,
            controller:this
          });

          this.selectionUI = ig.game.spawnEntity('EntityArenaselectionui',0,0,{
            controller: this,
            isPlayer1: true
          });
          this.selectionUI.alpha = 0;

        },

        generateSparks: function(){
          this.particleGen = ig.game.spawnEntity('EntityParticleGenerator', 161, 355,{
            visible : true,
            repeat: false,
            size: {
              x: 4,
              y: 20
            },
            //spawnRate : 0.2,
            particles : 5,
            pVel:{
              x:{
                min: -200,
                max: 200
              },
              y:{
                min:-200,
                max:200
              }
            },
            pScale : {
              proportional : true,
              min : 1,
              max : 2
            },
            pIndex : {
              min : 7.1,
              max : 7.5
            },
            pAlpha : {
              min : 1,
              max : 1
            },
            // pScaleOut : {
            //     enabled : true,
            //     proportional : false,
            //     min : 1,
            //     max : 6,
            //     time : {
            //         min : 1,
            //         max : 5
            //     }
            // },
            // pAngle:{
            //   min:45,
            //   max:45
            // },
            pFadeOut : {
              enabled: true,
              time: {
                    min : 0.25,
                    max : 0.45
                },
            },
            // pRotate : {
            //   enabled: true,
            //   direction : 2,
            //   time : {
            //       min : 0.50,
            //       max : 1.0
            //   }
            // },
            //pLifetime : 3
          });
        },

        initDebugInput: function (){
          ig.input.bind(ig.KEY._1, 'debug1');
          ig.input.bind(ig.KEY._2, 'debug2');
          ig.input.bind(ig.KEY._3, 'debug3');
          ig.input.bind(ig.KEY._4, 'debug4');
        },

        debugInput: function(){
          if(ig.input.pressed('debug1')){
            this.player1Fighter.fadeIn(0.1);
            this.player2Fighter.fadeIn(0.1);
          }else if(ig.input.pressed('debug2')){
            this.player1Fighter.fadeOut();
            this.player2Fighter.fadeOut();
          }else if (ig.input.pressed('debug3') && !this.sequenceStarted && !this.player1Fighter.isDead && !this.player2Fighter.isDead){
            this.startSequence();
          }else if (ig.input.pressed('debug4') && !this.sequenceStarted && !this.player1Fighter.isDead && !this.player2Fighter.isDead){
            this.startSelectionUI();
          }
        },

        setPlayerRandom: function(player){
          switch(ig.game.getRandomInt(1,3)){
            case 1:
              player.type = 'rock';
              break;
            case 2:
              player.type = 'paper';
              break;
            case 3:
              player.type = 'scissors';
              break;
          }
          player.isSet = true;
        },

        setPlayerHealth: function(health){
          ig.game.player1Variables.health = health;
          ig.game.player2Variables.health = health;

        },

        fillAttackArrayRandom: function(arr){
          var atk;
          for(var i = 0; i < 3; i++){
            switch(ig.game.getRandomInt(1,3)){
              case 1:
                atk = 'high';
                break;
              case 2:
                atk = 'mid';
                break;
              case 3:
                atk = 'low';
                break;
            }
            arr.push(atk);
          }
        },

        fillAttackArrayNull: function(arr){
          var i = 3-arr.length;
          for(i; i > 0; i--){
            arr.push('null');
          }
        },

        hitSoundSelection:[],
        selectHitSounds: function (){
          for(var i = 0; i < 3; i++){
            this.hitSoundSelection.push(ig.game.getRandomInt(0,1));
          }
        },

    });
});