ig.module(
    'game.entities.controllers.mainmenubuttoncontroller'
).requires(
    'impact.entity',
    'game.entities.ui.clickable',
    'game.entities.ui.impactimage'
).defines(function() {

    EntityMainmenubuttoncontroller = ig.Entity.extend({
        _wmDrawBox:true,
        _wmScalable:true,
        _wmBoxColor:'#FF0000',
        size:{x:20,y:20},
        collides: ig.Entity.COLLIDES.NEVER,
        tweenJunkVar: 0,
        menuItems: [],
        init: function(x,y,settings) {
          this.parent(x,y,settings);
          this.initMenuItems(6);
        },

        update: function(){
          this.parent();
        },

        draw: function() {
          this.parent();
        },

        buttonPressed:function(name,title){
          var tweenButtons = false;
          switch(name){
            case 'sound_on':
            case 'sound_off':
              if(ig.game.soundOn){
                ig.game.soundOn = false;
                ig.game.localStorage.set('soundOn',false);
                title.currentAnim = title.anims['sound_off'];
              }else{
                ig.game.soundOn = true;
                ig.game.localStorage.set('soundOn',true);
                title.currentAnim = title.anims['sound_on'];
              }
              ig.game.playSFX(ig.game.s_textAppear);
              break;

            case 'music_on':
            case 'music_off':
              if(ig.game.musicOn){
                ig.game.musicOn = false;
                ig.music.volume = 0;
                ig.game.localStorage.set('musicOn',false);
                title.currentAnim = title.anims['music_off'];
              }else{
                ig.game.musicOn = true;
                ig.music.volume = ig.game.MUSIC_VOL;
                ig.game.localStorage.set('musicOn',true);
                title.currentAnim = title.anims['music_on'];
              }
              break;

            case 'options':
              tweenButtons = true;
              var sound,music;
              if(ig.game.localStorage.getBool('soundOn')){
                sound = 'sound_on';
              }else{
                sound = 'sound_off';
              }
              if(ig.game.localStorage.getBool('musicOn')){
                music = 'music_on';
              }else{
                music = 'music_off';
              }
              this.setMenuItems({
                items: [sound,music,'back'],
                startY: 225,
                offsetY: 75
              });
              break;

            case 'back':
              tweenButtons = true;
              this.setMenuItems({
                items: ['vs_cpu','online','options','instructions','remove_ads'],
                startY: 150,
                offsetY: 75
              });
              break;

            case 'instructions':
              break;

            case 'remove_ads':
              tweenButtons = true;
              var items;
              if(ig.game.localStorage.getBool('adsOn')){
                items = ['remove_ads_enabled','restore_purchases','back'];
              }else{
                items = ['remove_ads_disabled','restore_purchases','back'];
              }
              this.setMenuItems({
                items: items,
                startY: 225,
                offsetY: 75
              });
              break;

            case 'vs_cpu':
            case 'online':
              this.controller.particleGen.enabled = false;
              ig.game.canClick = false;
              var sprite = ig.game.spawnEntity('EntityImpactimage', ig.system.width/2 - 1, ig.system.height/2 - 1, {
                img: new ig.Image(ig.game.MEDIA_PATH+'black_transition.png'),
                scale: {x: ig.system.width, y: ig.system.height},
                size: {x: 1, y: 1},
                clickable: false,
                zIndex: 1000
                });
              sprite.currentAnim.alpha = 0.0;
              var tTime = 0.50;

              ig.game.takeScreenShot();
              ig.game.playSFX(ig.game.s_textDisappear);
              if(ig.game.mode === 'single') {
                ig.game.player2Variables.name = 'CPU';
                this.loadSelectionScreen();
              } else {
                this.lookForOpponent.bind(this)();
              }
              break;
            }

          switch(name) {
            case 'vs_cpu':
              ig.game.mode = 'single';
              break;
            case 'online':
              ig.game.mode = 'multi';
              ig.game.logic.getServerConnection();
              break;
            default:
              break;
          }

          if(tweenButtons){
            ig.game.canClick = false;
            this.tweenButtons({
              offsetMoveDelay: 0.125,
              moveDuration: 0.25,
              moveDistance: 100,
              moveEase: ig.Tween.Easing.Exponential.EaseOut,
            });
          }

        },

        loadSelectionScreen: function() {

          ig.game.loadLevelDeferred(LevelSelectionscreen);
        },


        lookForOpponent: function() {

          // ONLINEMODETODO
          // add some fancy graphics here for waiting for opponent
          ig.game.logic.requestOpponent(this.opponentFound.bind(this));

        },

        opponentFound: function() {

          // ONLINEMODETODO
          // add some fancy "opponent found!" animation here before loading selection screen

          this.loadSelectionScreen();

        },

        initMenuItems: function(count){
          for(var i = 0; i < count; i++){
            this.menuItems.push(
              ig.game.spawnEntity('EntityMainmenubutton', -1000, -1000,{zIndex:3, controller:this})
            );
            ig.game.setCenterX(this.menuItems[i]);
          }
        },

        setAlphaMenuButton:function(button, value){
          button.currentAnim.alpha = value;
          button.title.currentAnim.alpha = value;
        },

        setMenuItems: function(settings){
          var items = settings.items;
          var startY = settings.startY;
          var offsetY = settings.offsetY;
          var i, menuButton;
          for(i = 0; i < items.length; i++){
            menuButton = this.menuItems[i];
            menuButton.name = items[i];
            menuButton.title.currentAnim = menuButton.title.anims[items[i]];
            menuButton.pos.y = startY + (offsetY * i);
            menuButton.enabled = true;
            menuButton.title.hitbox.setHitBoxSizePos(items[i]);
            menuButton.title.hitbox.name = items[i];
            this.setAlphaMenuButton(menuButton, 0);
          }
          for(i; i < this.menuItems.length; i++){
            menuButton = this.menuItems[i];
            menuButton.enabled = false;
            menuButton.pos.y = -1000;
          }
        },

        tweenButtonsIntro: function(settings){
          var i, dCount, menuButton, origPosY;
          for(i = 0; i < this.menuItems.length; i++){
            menuButton = this.menuItems[i];
            if(menuButton.enabled){
              this.setAlphaMenuButton(menuButton, 0);
              origPosY = menuButton.pos.y;
              menuButton.pos.y += settings.moveDistance;
              menuButton.tween({pos:{y:origPosY}},settings.moveDuration,{
                easing: settings.moveEase,
                delay: settings.moveStartDelay + (i*settings.offsetMoveDelay)
              }).start();
              menuButton.tween({currentAnim:{alpha:1}},settings.moveDuration,{
                delay: settings.moveStartDelay + (i*settings.offsetMoveDelay)
              }).start();
            }
          }
          this.tween({tweenJunkVar: 1},0,{
            delay: settings.alphaDelay,
            onStart: function (){
              for(i = 0; i < this.menuItems.length; i++){
                menuButton = this.menuItems[i];
                if(menuButton.enabled){
                  menuButton.title.currentAnim.alpha = 1.0;
                }
                ig.game.canClick = true;
              }
            }.bind(this)
          }).start();
        },

        tweenButtons: function(settings){
          var i, dCount, menuButton, origPosY;
          for(i = 0; i < this.menuItems.length; i++){
            menuButton = this.menuItems[i];
            if(menuButton.enabled){
              this.setAlphaMenuButton(menuButton, 0);
              origPosY = menuButton.pos.y;
              menuButton.pos.y += settings.moveDistance;
              menuButton.tween({pos:{y:origPosY}},settings.moveDuration,{
                easing: settings.moveEase,
                delay: (i*settings.offsetMoveDelay)
              }).start();
              menuButton.tween({currentAnim:{alpha:1}},settings.moveDuration,{
                delay: (i*settings.offsetMoveDelay)
              }).start();
              menuButton.tween({title:{currentAnim:{alpha:1}}},settings.moveDuration,{
                onStart: function () {ig.game.playSFX(ig.game.s_textAppear);}.bind(this),
                delay: (i*settings.offsetMoveDelay)
              }).start();
            }
          }
          this.tween({tweenJunkVar: 1},0,{
            delay: (i*settings.offsetMoveDelay),
            onStart: function (){
              ig.game.canClick = true;
            }.bind(this)
          }).start();
        }

    });

    EntityMainmenubutton = ig.Entity.extend({
      size:{x:320,y:27},
      name:'background',
      enabled:false,
      init: function(x,y,settings) {
        this.parent(x,y,settings);
        this.settings.controller = this.controller;
        this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'main_menu/buttons/button_background.png', 320, 27);
        this.addAnim('play',1,[0],true);
        this.currentAnim = this.anims.play;
        this.title = ig.game.spawnEntity('EntityImpactimage', this.x, this.y,{
          zIndex: this.zIndex+1,
          size: {x:320, y:27},
          clickable: false,
          img: new ig.Image(ig.game.MEDIA_PATH+'main_menu/buttons/button_spritesheet.png')
        });
        this.title.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'main_menu/buttons/button_spritesheet_alt.png', 320,27);
        this.title.addAnim('back',1,[0],true);
        this.title.addAnim('instructions',1,[1],true);
        this.title.addAnim('music_off',1,[2],true);
        this.title.addAnim('music_on',1,[3],true);
        this.title.addAnim('online',1,[4],true);
        this.title.addAnim('options',1,[5],true);
        this.title.addAnim('remove_ads',1,[6],true);
        this.title.addAnim('remove_ads_disabled',1,[7],true);
        this.title.addAnim('remove_ads_enabled',1,[8],true);
        this.title.addAnim('restore_purchases',1,[9],true);
        this.title.addAnim('sound_off',1,[10],true);
        this.title.addAnim('sound_on',1,[11],true);
        this.title.addAnim('vs_cpu',1,[12],true);
        this.title.currentAnim = this.anims.back;
        this.title.hitbox = ig.game.spawnEntity('EntityMainmenubuttonhitbox',this.title.pos.x,this.title.pos.y,{
          size:{x:10,y:10},
          zIndex: this.title.zIndex+1,
          name:'blank',
          title: this.title,
          controller: this.controller
        });
      },

      update: function(){
        this.parent();
        this.title.pos.x = this.pos.x;
        this.title.pos.y = this.pos.y;
        this.title.hitbox.pos.x = this.title.pos.x + this.title.hitbox.posOffset.x;
        this.title.hitbox.pos.y = this.title.pos.y + this.title.hitbox.posOffset.y;
      },

      draw: function() {
        this.parent();
      }
    });

    EntityMainmenubuttonhitbox = EntityClickable.extend({
      size:{x:320,y:27},
      posOffset:{x:0,y:0},
      init: function(x,y,settings) {
        this.parent(x,y,settings);
        this.title = settings.title;
        this.controller = settings.controller;
      },

      update: function(){
        this.parent();
      },

      draw: function() {
        this.parent();
      },

      setHitBoxSizePos: function(type){
        this.name = type;
        switch(type){
          case 'vs_cpu':
            this.size.x = 88;
            this.size.y = 27;
            this.posOffset.x = 117;
            break;
          case 'online':
            this.size.x = 96;
            this.size.y = 27;
            this.posOffset.x = 113;
            break;
          case 'options':
            this.size.x = 117;
            this.size.y = 27;
            this.posOffset.x = 102;
            break;
          case 'remove_ads':
            this.size.x = 168;
            this.size.y = 27;
            this.posOffset.x = 76;
            break;
          case 'instructions':
            this.size.x = 168;
            this.size.y = 27;
            this.posOffset.x = 76;
            break;
          case 'sound_on':
            this.size.x = 102;
            this.size.y = 27;
            this.posOffset.x = 172;
            break;
          case 'sound_off':
            this.size.x = 102;
            this.size.y = 27;
            this.posOffset.x = 172;
            break;
          case 'music_on':
            this.size.x = 102;
            this.size.y = 27;
            this.posOffset.x = 172;
            break;
          case 'music_off':
            this.size.x = 102;
            this.size.y = 27;
            this.posOffset.x = 172;
            break;
          case 'back':
            this.size.x = 70;
            this.size.y = 27;
            this.posOffset.x = 127;
            break;
        }
      },

      onClick: function () {
        this.controller.buttonPressed(this.name,this.title);
      }
    });
});