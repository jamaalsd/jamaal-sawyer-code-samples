ig.module(
    'game.entities.controllers.openingcontroller'
).requires(
    'impact.entity'
).defines(function() {

    EntityOpeningcontroller = ig.Entity.extend({
        _wmDrawBox:true,
        _wmScalable:true,
        _wmBoxColor:'#FF0000',
        size:{x:20,y:20},
        collides: ig.Entity.COLLIDES.NEVER,
        runOnce: true,
        init: function(x,y,settings) {
          this.parent(x,y,settings);
          if(!ig.global.wm){
            ig.music.crossFade(0.01, 'intro');
            this.initOpening();
          }
        },

        update: function(){
          this.parent();
        },

        draw: function() {
          this.parent();
        },

        initOpening: function() {
          var overlay = ig.game.spawnEntity('EntityImpactimage', 0, 0, {
              img: new ig.Image(ig.game.MEDIA_PATH+'overlay.png'),
              size: {x: ig.system.width, y: ig.system.height},
              clickable: false,
              zIndex: 1000
            });

          var creditArray = [];
          var sprite = ig.game.spawnEntity('EntityImpactimage',0,0, {
                img: new ig.Image(ig.game.MEDIA_PATH+'opening/1.png'),
                size: {x:202, y:32},
                clickable: false
            });

          ig.game.setCenterX(sprite);
          sprite.pos.y = 75;
          sprite.currentAnim.alpha = 0;
          creditArray.push(sprite);

          sprite = ig.game.spawnEntity('EntityImpactimage',0,0, {
                img: new ig.Image(ig.game.MEDIA_PATH+'opening/2.png'),
                size: {x:123, y:29},
                clickable: false
            });

          ig.game.setCenterX(sprite);
          sprite.pos.y = 150;
          sprite.currentAnim.alpha = 0;
          creditArray.push(sprite);

          sprite = ig.game.spawnEntity('EntityImpactimage',0,0, {
                img: new ig.Image(ig.game.MEDIA_PATH+'opening/3.png'),
                size: {x:228, y:26},
                clickable: false
            });

          ig.game.setCenterX(sprite);
          sprite.pos.y = 225;
          sprite.currentAnim.alpha = 0;
          creditArray.push(sprite);

          sprite = ig.game.spawnEntity('EntityImpactimage',0,0, {
                img: new ig.Image(ig.game.MEDIA_PATH+'opening/4.png'),
                size: {x:177, y:21},
                clickable: false
            });

          ig.game.setCenterX(sprite);
          sprite.pos.y = 300;
          sprite.currentAnim.alpha = 0;
          creditArray.push(sprite);

          sprite = ig.game.spawnEntity('EntityImpactimage',0,0, {
                img: new ig.Image(ig.game.MEDIA_PATH+'opening/last.png'),
                size: {x:250, y:227},
                clickable: false,
                name:'openingLast'
            });
          ig.game.setCenterX(sprite);
          sprite.pos.y = 275;
          sprite.currentAnim.alpha = 0;
          this.revealCredits(creditArray, sprite);
        },

        revealCredits: function(credits, last){
          var i,sprite,tween,origPosX,origPosY;
          var duration = 0.15;
          var duration2 = 0.75;
          var tWait = 1;
          var tWait2 = 1;
          var tWait3 = 1.2;
          var tDelay = duration;
          var xFrom = 100;
          var yFrom = 100;
          var tEase = ig.Tween.Easing.Bounce.EaseOut;
          var tEase2 = ig.Tween.Easing.Exponential.EaseOut;
          for(i = 0; i < credits.length; i++){
            sprite = credits[i];
            origPosX = sprite.pos.x;
            sprite.pos.x -= xFrom;

            tween = sprite.tween({pos:{x:origPosX}}, duration, {
              delay: tWait + i * tDelay,
              easing: tEase,
              onStart: function (){
                //ig.game.playSFX(ig.game.s_textAppear);
              }.bind(this)
            });
            tween.start();

            tween = sprite.tween({currentAnim:{alpha:1.0}}, duration, {
              delay: tWait + i * tDelay,
              easing: tEase,
            });
            tween.start();
          }

          origPosY = last.pos.y;
          last.pos.y += yFrom;

          tween = last.tween({currentAnim:{alpha:1.0}},duration2,{
            onStart: function (){
              //ig.game.playSFX(ig.game.s_textDisappear);
            }.bind(this),
            delay: tWait + tWait2 + credits.length * tDelay,
            easing: tEase2
          });
          tween.start();

          tween = last.tween({pos:{y:origPosY}},duration2,{
            delay: tWait + tWait2 + credits.length * tDelay,
            easing: tEase2
          }).start();

          for(i = 0; i < credits.length; i++){
            sprite = credits[i];

            tween = sprite.tween({pos:{y: sprite.pos.y - yFrom}}, duration2, {
              delay: tWait + tWait2 + credits.length * tDelay,
              easing: tEase2
            });
            tween.start();

            sprite.currentAnim.alpha = 1.0;
            tween = sprite.tween({currentAnim:{alpha: 0.0}}, duration2, {
              delay: tWait + tWait2 + credits.length * tDelay,
              easing: tEase2
            });
            tween.start();
            sprite.currentAnim.alpha = 0.0;
          }

          last.currentAnim.alpha  = 1.0;
          tween = last.tween({currentAnim:{alpha:0.0}},duration2,{
            onStart: function (){
              //ig.game.playSFX(ig.game.s_textDisappear);
            }.bind(this),
            delay: tWait + tWait2 + tWait3 + credits.length * tDelay,
            easing: tEase2,
            onComplete: function(){
              ig.game.loadLevelDeferred(LevelMainmenu);
            }.bind(this)
          });
          tween.start();
          last.currentAnim.alpha = 0.0;

          last.pos.y = origPosY;
          tween = last.tween({pos:{y:origPosY-yFrom}},duration2,{
            delay: tWait + tWait2 + tWait3 + credits.length * tDelay,
            easing: tEase2
          }).start();
          last.pos.y += yFrom;





        }
    });
});