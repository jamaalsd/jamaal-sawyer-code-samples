ig.module(
    'game.entities.controllers.selectionscreencontroller'
).requires(
    'impact.entity',
    'game.entities.selection.background',
    'game.entities.selection.hand'
).defines(function() {

    EntitySelectionscreencontroller = ig.Entity.extend({
        _wmDrawBox:true,
        _wmScalable:true,
        _wmBoxColor:'#FF0000',
        size:{x:20,y:20},
        collides: ig.Entity.COLLIDES.NEVER,
        selectionBackground: undefined,
        selectionArray: [],
        buttonArray: [],
        timerTime : 10,
        flash: undefined,
        tweenJunkVar: 0,
        fighterSelect:0,
        fighterSelectArr:['rock','paper','scissors'],
        init: function(x,y,settings) {
          this.parent(x,y,settings);
          if(!ig.global.wm){
            ig.music.crossFade(0.01, 'select');
            this.initSelectionScreen();
          }
        },

        update: function(){
          this.parent();
        },

        draw: function() {
          this.parent();
        },

        buttonClicked: function (name){
          if(ig.game.canClick){
            switch(name){
              case 'left':
                this.moveSelection('left');
                break;
              case 'right':
                this.moveSelection('right');
                break;
              case 'select':
                this.selectFighter(this.fighterSelectArr[this.fighterSelect]);
                break;
              case 'back':
              this.gotoMainMenu();
                break;
            }
          }
        },

        initSelectionScreen: function(){
          var sprite, i, spriteHeight, tTime, tDelay;
          var overlay = ig.game.spawnEntity('EntityImpactimage', 0, 0, {
              img: new ig.Image(ig.game.MEDIA_PATH+'overlay.png'),
              size: {x: 320, y: 568},
              clickable: false,
              zIndex: 1000
            });
          ig.game.canClick = false;

          if(!ig.game.screenshot){
            console.log('here');
            tTime = 0.50;
            tDelay = 0.0;
            sprite = ig.game.spawnEntity('EntityImpactimage', ig.system.width/2 - 1, ig.system.height/2 - 1, {
              img: new ig.Image(ig.game.MEDIA_PATH+'black_transition.png'),
              scale: {x: ig.system.width, y: ig.system.height},
              size: {x: 1, y: 1},
              clickable: false,
              zIndex: overlay.zIndex-1,
              controller:this
            });
            sprite.tween({currentAnim:{alpha:0.0}},tTime,{
              delay: tDelay,
              onComplete: function (){
                ig.game.canClick = true;
                this.controller.selectionTimer.startTimer(this.controller.timerTime);
                this.kill();
              }.bind(sprite)
            }).start();
          }else{
            ig.game.ssFlag = false;
            ig.game.canDraw =false;
            tTime = 0.5;
            tDelay = 0.05;
            sprite = ig.game.spawnEntity('EntityImpactimage', 0, 0, {
              img: ig.game.screenshot.animSheet,
              size: {x: ig.system.width, y: ig.system.height},
              clickable: false,
              zIndex: overlay.zIndex+1,
              controller:this
            });
            sprite.tween({currentAnim:{alpha:0.0}},tTime,{
              delay: tDelay,
              easing: ig.Tween.Easing.Exponential.EaseOut,
              onStart: function(){
                ig.game.canDraw = true;
              }.bind(this),
              onComplete: function (){
                ig.game.canClick = true;
                this.controller.selectionTimer.startTimer(this.controller.timerTime);
                this.kill();
              }.bind(sprite)
            }).start();
            sprite.tween({scale:{x:1.25,y:1.25}},tTime,{
              easing: ig.Tween.Easing.Exponential.EaseOut,
              delay: tDelay
            }).start();

          }

          this.leftCurtain = ig.game.spawnEntity('EntityImpactimage', -160, 0, {
              img: new ig.Image(ig.game.MEDIA_PATH+'shared/title_screens/1_left.png'),
              size: {x: 160, y: 568},
              clickable: false,
              zIndex: 999
            });
          this.rightCurtain = ig.game.spawnEntity('EntityImpactimage', 321, 0, {
              img: new ig.Image(ig.game.MEDIA_PATH+'shared/title_screens/1_right.png'),
              size: {x: 160, y: 568},
              clickable: false,
              zIndex: 999
            });

          this.selectionFighter = ig.game.spawnEntity('EntitySelectionplayerfighter', -250,62, {
              zIndex: 0.5,
              controller: this
          });

          this.selectionFighter.anims.rock.alpha = 0.0;
          this.selectionFighter.anims.paper.alpha = 0.0;
          this.selectionFighter.anims.scissors.alpha = 0.0;



          this.selectionBackground = ig.game.spawnEntity('EntitySelectionbackground', 0,0,{controller:this});
          this.selectionBackground.setState('rock');
          this.selectionArray.push(this.selectionBackground);

          this.selectionBackground = ig.game.spawnEntity('EntitySelectionbackground', ig.system.width,0,{controller:this});
          this.selectionBackground.setState('paper');
          this.selectionArray.push(this.selectionBackground);

          this.selectionBackground = ig.game.spawnEntity('EntitySelectionbackground', -ig.system.width,0,{controller:this});
          this.selectionBackground.setState('scissors');
          this.selectionArray.push(this.selectionBackground);

          sprite = ig.game.spawnEntity('EntityImpactimage', 0, 2,{
              img: new ig.Image(ig.game.MEDIA_PATH+'character_selection/buttons/back.png'),
              size: {x: 52, y: 46},
              scale: {x:0.5, y:0.5},
              clickable: true,
              zIndex: 5,
              name:'back',
            });
          sprite.controller = this;
          this.buttonArray.push(sprite);
          sprite.onClick = function () {
            if(ig.game.canClick)
            this.controller.buttonClicked(this.name);
          }.bind(sprite);

          sprite = ig.game.spawnEntity('EntityImpactimage', 16, 255,{
              img: new ig.Image(ig.game.MEDIA_PATH+'character_selection/buttons/arrow_left.png'),
              size: {x: 36, y: 45},
              clickable: true,
              zIndex: 6,
              name:'left',
            });
          sprite.controller = this;
          sprite.onClick = function () {
            if(ig.game.canClick)
            this.controller.buttonClicked(this.name);
          }.bind(sprite);
          this.buttonArray.push(sprite);

          sprite = ig.game.spawnEntity('EntityImpactimage', 268, 255,{
              img: new ig.Image(ig.game.MEDIA_PATH+'character_selection/buttons/arrow_right.png'),
              size: {x: 36, y: 45},
              clickable: true,
              zIndex: 5,
              name:'right',
            });
          sprite.controller = this;
          sprite.onClick = function () {
            if(ig.game.canClick)
            this.controller.buttonClicked(this.name);
          }.bind(sprite);
          this.buttonArray.push(sprite);

          this.selectionTitle = ig.game.spawnEntity('EntitySelectionplayertext', 55, 10);
          // tTime = 2;
          // tDelay = 1;
          // sprite.tween( {currentAnim:{alpha:0.75}},tTime,{
          //   loop: ig.Tween.Loop.Reverse,
          //   delay: tDelay
          //   // onComplete: function (){
          //   //   if(this.currentAnim.alpha === 1){
          //   //     this.currentAnim.alpha = 0;
          //   //   }else{
          //   //     this.currentAnim.alpha = 1;
          //   //   }
          //   //}.bind(sprite)
          // }).start();

          this.selectionTimer = ig.game.spawnEntity('Entitytimerhorizontal', 0, 517,{
            zIndex: 3.9,
            controller: this,
          });
          this.selectionTimer.onTimerComplete = function (){
              this.controller.buttonClicked('select');
            }.bind(this.selectionTimer);

          spriteHeight = 50;
          sprite = ig.game.spawnEntity('EntityImpactimage', ig.system.width/2 - 1, spriteHeight/2 - 1,{
              img: new ig.Image(ig.game.MEDIA_PATH+'black_transition.png'),
              scale: {x: ig.system.width, y: spriteHeight},
              size: {x: 1, y: 1},
              clickable: false,
              zIndex: 4
            });

          spriteHeight = 50;
          sprite = ig.game.spawnEntity('EntityImpactimage', ig.system.width/2 - 1, ig.system.height - (spriteHeight/2) - 1,{
              img: new ig.Image(ig.game.MEDIA_PATH+'black_transition.png'),
              scale: {x: ig.system.width, y: spriteHeight},
              size: {x: 1, y: 1},
              clickable: false,
              zIndex: 4
            });

          this.flash = ig.game.spawnEntity('EntityImpactimage', ig.system.width/2 - 1, ig.system.height/2 - 1, {
              img: new ig.Image(ig.game.MEDIA_PATH+'white_transition.png'),
              scale: {x: ig.system.width, y: ig.system.height},
              size: {x: 1, y: 1},
              clickable: false,
              zIndex: 100,
          });
          this.flash.currentAnim.alpha = 0.0;


        },

        moveSelection: function(direction){
          var xMove;
          if(direction === 'left'){
            xMove = ig.system.width;
            this.fighterSelect -= 1;
            if(this.fighterSelect < 0){
              this.fighterSelect = 2;
            }
          }else{
            xMove = -ig.system.width;
            this.fighterSelect += 1;
            if(this.fighterSelect > 2){
              this.fighterSelect = 0;
            }
          }
          var moveTime = 0.15;

          ig.game.canClick = false;
          var sprite;
          for(var i = 0; i < this.selectionArray.length; i++){
            sprite = this.selectionArray[i];
            sprite.tween({pos:{x:sprite.pos.x + xMove}},moveTime,{
              onComplete: function (){
                if(this.pos.x >= ig.system.width*2){
                  this.pos.x = -ig.system.width;
                }else if(this.pos.x <= ig.system.width * -2){
                  this.pos.x = ig.system.width;
                }
              }.bind(sprite)
            }).start();
          }
          this.tween({tweenJunkVar:1},moveTime+0.01,{
            onComplete: function (){
              ig.game.canClick = true;
            }.bind(this)
          }).start();
          ig.game.playSFX(ig.game.s_textDisappear);
        },

        selectFighter: function(type){
          ig.game.canClick = false;
          ig.game.playSFX(ig.game.s_logoAppear);
          var i;
          for(i = 0; i < this.selectionArray.length; i++){
            if(this.selectionArray[i].state === type){
              this.selectionArray[i].fighter.currentAnim.alpha = 0;
              this.selectionArray[i].fighterSelected.currentAnim.alpha = 1;
              this.selectionArray[i].portrait.currentAnim.alpha = 0.0;
              this.selectionArray[i].portrait.title.currentAnim.alpha = 0.0;
              this.selectionArray[i].portrait.stats.currentAnim.alpha = 0.0;
              //this.selectionArray[i].hand.currentAnim.alpha = 0.0;
              this.selectionArray[i].currentAnim.alpha = 0.2;
              this.selectionArray[i].currentAnim.frameTime *= 2;
              this.selectionArray[i].moveHand = false;
              this.selectionArray[i].chosen = true;
              //this.selectionArray[i].fighterSelected.setScale(2, 2);
              // if(type === 'paper'){
              //   this.selectionArray[i].fighterSelectedOffset.x -= 15;
              // }else{
                //this.selectionArray[i].fighterSelectedOffset.x -= 39;
              //}
              //this.selectionArray[i].fighterSelected.pos.y -= 90;
              break;
            }
          }
          if(ig.game.mode === 'single'){

            ig.game.player1Variables.isSet = true;
            ig.game.player1Variables.type = type;

            var tTime = 0.5;
            this.flash.currentAnim.alpha = 1.0;
            this.flash.tween({currentAnim:{alpha:0}},tTime,{
              onComplete: function (){
                // this.kill();
                //this.controller.loadPrebattle(1);
                this.selectFighter2();
              }.bind(this)
            }).start();

          } else {

            ig.game.player1Variables.isSet = true;
            ig.game.player1Variables.type = type;

            this.waitForOpponentChoice(type);

          }
          for(i = 0; i < this.buttonArray.length; i++){
            this.buttonArray[i].currentAnim.alpha = 0.0;
          }
          this.selectionTitle.currentAnim.alpha = 0.0;
          this.selectionTimer.killTimer();

        },

        waitForOpponentChoice:function(type) {

          ig.game.logic.chooseWarrior(type,this.onOpponentChoice.bind(this));

          // ONLINEMODETODO fancy "waiting graphics" here
        },

        onOpponentChoice: function() {

          var tTime = 0.5;
            this.flash.currentAnim.alpha = 1.0;
            this.flash.tween({currentAnim:{alpha:0}},tTime,{
              onComplete: function (){
                this.selectFighter2();
              }.bind(this)
            }).start();
        },

        selectFighter2: function (){
          var select;
          //if(ig.game.mode === 'single'){
            for(i = 0; i < this.selectionArray.length; i++){
              if(this.selectionArray[i].state === ig.game.player1Variables.type){
                select = this.selectionArray[i];
              }
            }
            this.selectionFighter.setState(ig.game.player1Variables.type);
          //}
          var tTime = 0.75;
          var tDelay = 0.25;
          var dist = 100;
          var easing = ig.Tween.Easing.Exponential.EaseOut;
          select.portrait.tween({currentAnim:{alpha:0.0}},tTime,{
            delay:tDelay,
            easing:easing
          }).start();
          select.portrait.tween({pos:{x:select.portrait.pos.x + dist}},tTime,{
            delay:tDelay,
            easing:easing
          }).start();

          select.hand.tween({currentAnim:{alpha:0.0}},tTime,{
            delay:tDelay,
            easing:easing
          }).start();
          select.hand.tween({pos:{x:select.hand.pos.x + dist}},tTime,{
            delay:tDelay,
            easing:easing
          }).start();

          select.fighterSelected.tween({currentAnim:{alpha:0.0}},tTime,{
            delay:tDelay,
            easing:easing,
            onComplete: function(){
              ig.game.playVoice(ig.game.s_trash_talk[this.type]);
            }.bind(select.fighterSelected)
          }).start();
          select.fighterSelected.tween({pos:{x:select.fighterSelected.pos.x - dist}},tTime,{
            delay:tDelay,
            easing:easing
          }).start();


          this.selectionFighter.pos.x += dist;
          this.selectionFighter.tween({currentAnim:{alpha:1.0}},tTime,{
            delay:tDelay+0.1,
            easing:easing
          }).start();
          this.selectionFighter.tween({pos:{x:this.selectionFighter.pos.x - dist}},tTime,{
            delay:tDelay+0.1,
            easing:easing,
            onComplete:function(){
              this.controller.loadPrebattle(1.75);
            }.bind(this.selectionFighter)
          }).start();
        },

        loadPrebattle: function (delay){
          var tTime = 0.15;
          var easing = ig.Tween.Easing.Linear.EaseNone;

          this.leftCurtain.tween({pos:{x:0}},tTime,{
            onStart: function (){
              ig.game.playSFX(ig.game.s_textDisappear);
            }.bind(this.leftCurtain),
            delay: delay,
            easing:easing,
            onComplete: function (){
              ig.game.loadLevelDeferred(LevelPrebattle);
            }.bind(this.leftCurtain)
          }).start();


          this.rightCurtain.tween({pos:{x:160}},tTime,{
            delay: delay,
            easing:easing,
            onComplete: function (){
            }.bind(this.rightCurtain)
          }).start();

        },

        gotoMainMenu: function(){
          ig.game.canClick = false;
          this.selectionTimer.pauseTimer();
          ig.game.takeScreenShot(true);
          ig.game.loadLevelDeferred(LevelMainmenu);
          ig.game.playSFX(ig.game.s_textDisappear);
        }
    });
});