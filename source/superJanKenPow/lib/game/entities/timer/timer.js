ig.module(
    'game.entities.timer.timer'
).requires(
    'impact.entity'
).defines(function() {
    Entitytimerhorizontal = ig.Entity.extend({
        _wmDrawBox:true,
        _wmScalable:true,
        _wmBoxColor:'#FF0000',
        size:{x:1,y:1},
        collides: ig.Entity.COLLIDES.NEVER,
        lastPercent: 1,
        percent: 1,
        anchor:{x:0.0,y:1.0},

        init: function(x,y,settings) {
          this.parent(x,y,settings);
          if(settings.controller){
            this.controller = settings.controller;
          }
          this.controller = settings.controller;
          if(settings.onTimerComplete){
            this.onTimerComplete = settings.onTimerComplete;
          }
          var scale = {x:ig.system.width,y:4};
          if(settings.scale){
            scale = {x: settings.scale.x, y: settings.scale.y};
          }
          this.origHorSize = scale.x;
          this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'timer/white.png', 1, 1);
          this.addAnim('idle', 1, [0], true);
          this.currentAnim = this.anims.idle;
          this.setScale(scale.x, scale.y);
          if(settings.startNow){
            this.startTimer(settings.duration);
          }
        },

        startTimer: function (duration){
          this.timerTween = this.tween({percent:0}, duration,{
            onComplete: function (){
              this.onTimerComplete();
            }.bind(this)
          });
          this.timerTween.start();
        },

        pauseTimer: function (){
          this.pauseTweens();
        },

        resumeTimer: function (){
          this.resumeTweens();
        },

        killTimer: function (){
          this.stopTweens(false);
          this.percent = 0;
          this.kill();
        },

        resetTimer: function (){
          this.stopTweens(false);
          this.percent = 1;
        },

        update: function(){
          this.parent();
          if(this.percent !== this.lastPercent){
            this.lastPercent = this.percent;
            this.setScale(this.origHorSize * this.percent, this.size.y);
          }
        },

        draw: function() {
          this.parent();
        },

        onTimerComplete: function (){
          this.currentAnim.alpha = 0;
          console.log('Timer Complete!');
        }

    });
});