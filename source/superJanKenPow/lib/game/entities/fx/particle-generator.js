ig.module(
    'game.entities.fx.particle-generator'
)
.requires(
    'impact.entity',
    'game.entities.fx.particles.particles',
    'plugins.impact-tween.tween'
)
.defines(function(){
    EntityParticleGenerator = ig.Entity.extend({
        /*Width and height of particle distribution*/
        size: { x: 50, y: 50 },

        enabled: true,

        visible: true,

        /*Type of particle to create*/
        particleClass: 'EntityBaseParticle',

        /*How many seconds between spawning particles*/
        spawnRate: 0.25,

        /*Make this particle generator run infinitely*/
        repeat: true,

        /*How many particles to spawn at once*/
        particles: 1,

        /*Particle Velocity*/
        pVel: {
            x: {
                min: -10,
                max: 10
            },
            y: {
                min: -10,
                max: 10,
            }
        },
        /*Particle fade out (optional)*/
        pFadeOut : {
            enabled: false,
            time: {
                min : 1,
                max : 1
            },
        },

        /*Particle scale out (optional)*/
        pScaleOut : {
            enabled : false,
            proportional: true,
            min : 1,
            max : 1,
            time : {
                min : 1,
                max : 1
            }
        },

        /*Particle starting angle (in degrees)*/
        pAngle : {
            min : 0,
            max : 0
        },

        /*particle starting scale*/
        pScale : {
          min : 1,
          max : 1
        },

        /*Particle rotation over time*/
        pRotate : {
            enabled: false,
            direction : 1,
            time : {
                min : 1,
                max : 1
            }
        },

        /*Particle zIndex*/
        pIndex : {
            min : 1,
            max : 1
        },

        /*Particle starting alpha*/
        pAlpha : {
            min : 1,
            max : 1
        },

        /*How many seconds before particle dies*/
        pLifetime: 1,

        init: function( x, y, settings ) {
            this.parent( x, y, settings );

            /*if it is instant we fire particles and kill generator*/
            if (!this.repeat) {
                for (var i = 0; i < this.particles; i++) {
                    this.spawnParticle();
                }
                this.kill();
                return;
            }

            // Create a timer that generates a particle every time it expires
            this.timerGenerate = new ig.Timer();
            this.timerGenerate.set(this.spawnRate / this.particles);
        },

        update: function () {
            this.parent();

            if (this.timerGenerate.delta() > 0) {
                for(var i = 0; i < this.particles; i++){
                    if(this.enabled){
                        this.spawnParticle();
                    }
                }
                this.timerGenerate.reset();
            }
        },

        // Generates a particle within the particle generator's bounds
        spawnParticle: function () {
            var repeat = this.repeat;
            var scaleX,scaleY;
            if(this.pScale.proportional){
                scaleX = scaleY = ig.game.getRandomReal(this.pScale.min, this.pScale.max);
            }else{
                scaleX = ig.game.getRandomReal(this.pScale.min, this.pScale.max);
                scaleY = ig.game.getRandomReal(this.pScale.min, this.pScale.max);
            }
            var particle = ig.game.spawnEntity(this.particleClass, this.pos.x + ig.game.getRandomInt(0, this.size.x-1), this.pos.y + ig.game.getRandomInt(0, this.size.y-1),{
                vel : {
                    x : ig.game.getRandomReal(this.pVel.x.min, this.pVel.x.max),
                    y : ig.game.getRandomReal(this.pVel.y.min, this.pVel.y.max)
                },
                scale : {
                    x : scaleX,
                    y : scaleY
                },
                repeat : repeat,
                controller: this,
                zIndex : ig.game.getRandomReal(this.pIndex.min, this.pIndex.max)
            });

            particle.initAlpha = particle.currentAnim.alpha = ig.game.getRandomReal(this.pAlpha.min, this.pAlpha.max);
            
            particle.currentAnim.angle = ig.game.getRandomReal(ig.game.degreesToRadians(this.pAngle.min), ig.game.degreesToRadians(this.pAngle.max));
            
            if(this.pRotate.enabled){
                var spinDirection;
                switch(this.pRotate.direction){
                    case -1:
                        spinDirection = ig.game.degreesToRadians(-360);
                        break;
                    case 1:
                        spinDirection = ig.game.degreesToRadians(360);
                        break;
                    case 2:
                        if(ig.game.getRandomInt(0,1) === 1){
                            spinDirection = ig.game.degreesToRadians(360);
                        }else{
                            spinDirection = ig.game.degreesToRadians(-360);
                        }
                        break;
                }

                particle.tween({currentAnim:{angle: spinDirection}}, ig.game.getRandomReal(this.pRotate.time.min, this.pRotate.time.max), {
                    loop: ig.Tween.Loop.Revert,
                }).start();
            }

            if(this.pScaleOut.enabled){
                var scaleToX, scaleToY;
                if(this.pScaleOut.proportional){
                    scaleToX = scaleToY = ig.game.getRandomReal(this.pScaleOut.min, this.pScaleOut.max);
                }else{
                    scaleToX = ig.game.getRandomReal(this.pScaleOut.min, this.pScaleOut.max);
                    scaleToY = ig.game.getRandomReal(this.pScaleOut.min, this.pScaleOut.max);
                }
                
                particle.tween(
                    { scale : { x : scaleToX, y : scaleToY}},
                    ig.game.getRandomReal(this.pScaleOut.time.min, this.pScaleOut.time.max),{
                }).start();
            }

            if(this.pFadeOut.enabled){
                particle.alphaTween = particle.tween({currentAnim:{alpha:0.0}}, ig.game.getRandomReal(this.pFadeOut.time.min, this.pFadeOut.time.max),{
                    onComplete: function (){
                        this.kill();
                    }.bind(particle)
                }).start();
            }else{
                particle.tween({junkTweenValue: 0},this.pLifetime,{
                    onComplete: function (){
                        this.kill();
                    }.bind(particle)
                }).start();
            }
        }
    });

});