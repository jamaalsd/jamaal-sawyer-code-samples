ig.module(
    'game.entities.fx.particles.particles'
)
.requires(
    'impact.entity',
    'impact.entity-pool'
)
.defines(function(){
    EntityBaseParticle = ig.Entity.extend({
        size: { x: 2, y: 2 },
        maxVel: {x:10000,y:10000},
        junkTweenValue: 0.0,
        zIndex:0,
        name:'particle',
        repeat : true,
        initAlpha:1,
        controller:undefined,
        init: function( x, y, settings ) {
            this.parent( x, y, settings );
            this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'particles/white.png', 2, 2);
            this.addAnim('play', 1, [0], false);
            this.currentAnim = this.anims.play;
        },

        reset: function(x,y,settings){
            this.parent(x,y, settings);
            if(this.tweens.length > 0){
                this.stopTweens(false);
            }
        },

        update: function () {
            this.parent();
            
        },

        draw: function () {
            if(this.repeat){
                if(this.controller.visible){
                    this.parent();
                }
            }else{
                this.parent();
            }
        }
    });
    EntityFireParticle = EntityBaseParticle.extend({
        size: { x: 2, y: 2 },
        maxVel: {x:10000,y:10000},
        junkTweenValue: 0.0,
        zIndex:0,
        repeat : true,
        initAlpha:1,
        controller:undefined,
        init: function( x, y, settings ) {
            this.parent( x, y, settings );
            this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'particles/fire.png', 2, 2);
            this.addAnim('play', 1, [ig.game.getRandomInt(0,2)], false);
            this.currentAnim = this.anims.play;
        },

        reset: function(x,y,settings){
            this.parent(x,y, settings);
            if(this.tweens.length > 0){
                this.stopTweens(false);
            }
        },

        update: function () {
            this.parent();
            
        },

        draw: function () {
            if(this.repeat){
                if(this.controller.visible){
                    this.parent();
                }
            }else{
                this.parent();
            }
        }
    });
    ig.EntityPool.enableFor( EntityBaseParticle );
    ig.EntityPool.enableFor( EntityBaseParticle );

});