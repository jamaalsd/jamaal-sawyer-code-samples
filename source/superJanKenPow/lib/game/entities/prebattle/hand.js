ig.module(
    'game.entities.prebattle.hand'
)
.requires(
    'impact.entity'
)
.defines(function(){
    EntityPrebattlehand = ig.Entity.extend({
      zIndex: 2,
      isPlayer1: true,
      init: function(x,y,settings) {
        this.parent(x,y,settings);
        this.size = {x:270, y:105};
        this.origPos = {x:x,y:y};
        this.isPlayer1 = settings.isPlayer1;
        if(this.isPlayer1){
          this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'shared/hands/player1/combined.png', 270, 105);
          this.addAnim('rock', 0, [2], true);
          this.addAnim('paper', 0, [0], true);
          this.addAnim('scissors', 0, [4], true);
        }else{
          this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'shared/hands/player2/combined.png', 270, 105);
          this.addAnim('rock', 0, [3], true);
          this.addAnim('paper', 0, [1], true);
          this.addAnim('scissors', 0, [5], true);
        }
        this.setState('rock');
        
      },

      update: function(){
        this.parent();
        if(this.isPlayer1){
          if(this.currentAnim.pivot.x !== this.pos.x){
            this.currentAnim.pivot.x = this.pos.x;
          }
        }else{
          if(this.currentAnim.pivot.x !== this.pos.x + this.size.x){
            this.currentAnim.pivot.x = this.pos.x + this.size.x;
          }
        }
      },

      setState: function(state){
        this.currentAnim = this.anims[state];
      }
    });
});