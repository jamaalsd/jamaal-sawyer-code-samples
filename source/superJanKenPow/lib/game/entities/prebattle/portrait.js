ig.module(
    'game.entities.prebattle.portrait'
)
.requires(
    'impact.entity'
)
.defines(function(){
    EntityPrebattleportrait = ig.Entity.extend({
      zIndex: 6,
      anchor:{x:0, y:0},
      scale: {x:1.45,y:1.45},
      isPlayer1: true,
      title: undefined,
      playerTitleOffset:{x:0,y:0},
      titleOffset: {x:0,y:98},
      playerNameOffset:{x:3, y:17},
      init: function(x,y,settings) {
        this.parent(x,y,settings);
        this.size = {x:60, y:66};
        var animSpeed = 0;
        if(!settings.isPlayer1 && ig.game.player1Variables.type === ig.game.player2Variables.type){
          this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'shared/portraits/combined_portraits_alt.png', 60, 66);
        }else{
          this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'shared/portraits/combined_portraits.png', 60, 66);
        }
        this.addAnim('rock', animSpeed, [1], true);
        this.addAnim('paper', animSpeed, [0], true);
        this.addAnim('scissors', animSpeed, [2], true);
        this.title = ig.game.spawnEntity('EntityPrebattleportraittitle', x+this.titleOffset.x, y+this.titleOffset.y, {zIndex:this.zIndex});
        if(settings.isPlayer1){
          this.setState(ig.game.player1Variables.type);
        }else{
          this.setState(ig.game.player2Variables.type);
        }

        this.playerTitle = ig.game.spawnEntity('EntityPrebattleportraitplayerbadge',x + this.playerTitleOffset.x, y + this.playerTitleOffset.y, {
          isPlayer1:settings.isPlayer1,
          zIndex:this.zIndex+1
        });

        var text;
        if(settings.isPlayer1){
          text = ig.game.player1Variables.name;
        }else{
          text = ig.game.player2Variables.name;
        }
        this.playerName = ig.game.spawnEntity('EntityImpactfont',x+this.playerNameOffset.x, y+this.playerNameOffset.y, {
            font: new ig.Font('media/fonts/minecraftia14.png'),
            text: text,
            align: ig.Font.ALIGN.LEFT,
            zIndex:this.zIndex+1
        });
      },

      update: function(){
        this.parent();
        this.title.pos.x = this.pos.x + this.titleOffset.x;
        this.title.pos.y = this.pos.y + this.titleOffset.y;
        this.title.currentAnim.alpha = this.currentAnim.alpha;

        this.playerTitle.pos.x = this.pos.x + this.playerTitleOffset.x;
        this.playerTitle.pos.y = this.pos.y + this.playerTitleOffset.y;
        this.playerTitle.currentAnim.alpha = this.currentAnim.alpha;

        this.playerName.pos.x = this.pos.x + this.playerNameOffset.x;
        this.playerName.pos.y = this.pos.y + this.playerNameOffset.y;
        this.playerName.alpha = this.currentAnim.alpha;
      },

      setState: function(state){
        this.currentAnim = this.anims[state];
        this.title.setState(state);
      }
    });

    EntityPrebattleportraittitle = ig.Entity.extend({
      zIndex: 6,
      scale: {x:0.51,y:0.51},
      init: function(x,y,settings) {
        this.parent(x,y,settings);
        this.size = {x:175, y:45};
        var animSpeed = 0;
        //if(ig.game.mode === 'single'){
          this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'character_selection/text/combined_title.png', 175, 45);
          this.addAnim('rock', animSpeed, [1], true);
          this.addAnim('paper', animSpeed, [0], true);
          this.addAnim('scissors', animSpeed, [2], true);
          this.setState('rock');
        //}
      },

      setState: function(state){
        this.currentAnim = this.anims[state];
      }
    });

    EntityPrebattleportraitplayerbadge = ig.Entity.extend({
      zIndex: 6,
      anchor:{x:0,y:0},
      scale: {x:0.51,y:0.51},
      init: function(x,y,settings) {
        this.parent(x,y,settings);
        this.size = {x:55, y:45};
        var animSpeed = 0;
        this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'arena/text/players_alt_2.png', 76, 45);
        if(settings.isPlayer1){
          this.addAnim('idle', animSpeed, [0], true);
        }else{
          this.addAnim('idle', animSpeed, [1], true);
        }
        this.currentAnim = this.anims.idle;
      }
  });
});