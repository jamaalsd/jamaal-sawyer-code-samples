ig.module(
    'game.entities.prebattle.fighter'
)
.requires(
    'impact.entity'
)
.defines(function(){
    EntityPrebattleplayerfighter = ig.Entity.extend({
      zIndex: 2,
      isPlayer1: true,
      init: function(x,y,settings) {
        this.parent(x,y,settings);
        this.size = {x:130, y:114};
        this.origPos = {x:x,y:y};
        this.isPlayer1 = settings.isPlayer1;
        if(!this.isPlayer1 && ig.game.player1Variables.type === ig.game.player2Variables.type){
          this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'prebattle/fighters/combined_alt.png', 130, 114);
        }else{
          this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'prebattle/fighters/combined.png', 130, 114);
        }
        this.addAnim('rock', 0, [1], true);
        this.addAnim('paper', 0, [0], true);
        this.addAnim('scissors', 0, [2], true);
        if(this.isPlayer1){
          this.setState(ig.game.player1Variables.type);
        }else{
          this.setState(ig.game.player2Variables.type);
        }
      },

      update: function(){
        this.parent();
        if(!this.isPlayer1 && !this.currentAnim.flip.x){
          this.currentAnim.flip.x = true;
        }
      },

      setState: function(state){
        this.currentAnim = this.anims[state];
      }
    });
});