ig.module(
    'game.entities.selection.hand'
)
.requires(
    'impact.entity'
)
.defines(function(){
    EntitySelectionbackgroundhand = ig.Entity.extend({
      zIndex: 2,
      init: function(x,y,settings) {
        this.parent(x,y,settings);
        this.size = {x:270, y:105};
        //if(ig.game.mode === 'single'){
          this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'shared/hands/player1/combined.png', 270, 105);
          this.addAnim('rock', 0, [2], true);
          this.addAnim('paper', 0, [0], true);
          this.addAnim('scissors', 0, [4], true);
          this.setState('rock');
        //}
      },

      setState: function(state){
        this.currentAnim = this.anims[state];
        this.currentAnim.angle = -0.785398163;
      }
    });
});