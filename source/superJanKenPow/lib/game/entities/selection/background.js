ig.module(
    'game.entities.selection.background'
)
.requires(
    'impact.entity',
    'game.entities.ui.clickable'
)
.defines(function(){
    EntitySelectionbackground = ig.Entity.extend({
      zIndex: 0,
      moveHand:true,
      chosen: false,
      hand: undefined,
      title: undefined,
      fighter: undefined,
      fighterSelected: undefined,
      portrait: undefined,
      state: undefined,
      handOffset: {x:-55,y:450},
      fighterOffset: {x:130, y:210},
      fighterSelectedOffset: {x:46, y:178},
      portraitOffset: {x:16, y:54},
      controller: undefined,
      init: function(x,y,settings) {
        this.parent(x,y,settings);
        this.controller = settings.controller;
        this.size = {x:ig.system.width, y:ig.system.height};
        var backAnimSpeed = 0.1;
        this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'shared/battle_anim/combined.png', ig.system.width, ig.system.height);
        this.addAnim('rock', backAnimSpeed, [5,4,3], false);
        this.addAnim('paper', backAnimSpeed, [2,1,0], false);
        this.addAnim('scissors', backAnimSpeed, [8,7,6], false);
        this.currentAnim = this.anims.rock;
        this.hand = ig.game.spawnEntity('EntitySelectionbackgroundhand', x+this.handOffset.x, y+this.handOffset.y);
        this.fighter = ig.game.spawnEntity('EntitySelectionbackgroundfighter', x+this.fighterOffset.x, y+this.fighterOffset.y, {controller:this.controller});
        this.fighterSelected = ig.game.spawnEntity('EntitySelectionbackgroundfighterselected', x+this.fighterSelectedOffset.x, y+this.fighterSelectedOffset.y, {controller:this.controller});
        this.portrait = ig.game.spawnEntity('EntitySelectionbackgroundportrait', x+this.portraitOffset.x, y+this.portraitOffset.y);

      },

      update: function(){
        this.parent();

        if(!this.chosen){
          if(this.moveHand){
            this.hand.pos.x = this.pos.x+this.handOffset.x + ig.game.getRandomInt(-1,1);
            this.hand.pos.y = this.pos.y+this.handOffset.y + ig.game.getRandomInt(-1,1);
          }
          this.fighter.pos.x = this.pos.x+this.fighterOffset.x;
          this.fighterSelected.pos.x = this.pos.x+this.fighterSelectedOffset.x;
          this.portrait.pos.x = this.pos.x+this.portraitOffset.x;
        }

      },

      draw: function() {
        this.parent();
      },

      setState: function(state){
        this.state = state;
        this.currentAnim = this.anims[state];
        this.hand.setState(state);
        this.fighter.setState(state);
        this.fighterSelected.setState(state);
        this.portrait.setState(state);
      }
    });

    EntitySelectionbackgroundfighter = EntityClickable.extend({
      zIndex: 1.5,
      scale: {x:1.5,y:1.5},
      clickable:true,
      controller:undefined,
      type:undefined,
      init: function(x,y,settings) {
        this.parent(x,y,settings);
        this.controller = settings.controller;

        var animSpeed = 0.2;
        this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'character_selection/fighters/combined_animated.png', 136, 118);
        this.addAnim('rock', animSpeed, [6,4,7,4], false);
        this.addAnim('paper', animSpeed, [3,0,1,0], false);
        this.addAnim('scissors', animSpeed, [8,5,2,5], false);
        this.setState('rock');
        this.size = {x:50, y:158};
        this.offset = {x:60,y:18};
      },

      setState: function(state){
        this.currentAnim = this.anims[state];
        this.type = state;
      },

      onClick: function(){
        this.controller.buttonClicked('select');
      }
    });

    EntitySelectionbackgroundfighterselected = ig.Entity.extend({
      zIndex: 1.5,
      scale: {x:1.5,y:1.5},
      controller:undefined,
      type: undefined,
      init: function(x,y,settings) {
        this.parent(x,y,settings);
        this.controller = settings.controller;
        var animSpeed = 1;
        this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'character_selection/fighters/combined_chosen.png', 127, 126);
        this.addAnim('rock', animSpeed, [1], false);
        this.addAnim('paper', animSpeed, [0], false);
        this.addAnim('scissors', animSpeed, [2], false);
        this.setState('rock');
        this.size = {x:127, y:126};
        this.anims.rock.alpha = 0.0;
        this.anims.paper.alpha = 0.0;
        this.anims.scissors.alpha = 0.0;
      },

      setState: function(state){
        this.currentAnim = this.anims[state];
        this.type = state;
      },
    });

    EntitySelectionbackgroundportrait = ig.Entity.extend({
      zIndex: 1,
      scale: {x:1.45,y:1.45},
      title: undefined,
      stats: undefined,
      titleOffset: {x:0,y:98},
      statsOffset: {x:90, y:23},
      init: function(x,y,settings) {
        this.parent(x,y,settings);
        this.size = {x:60, y:66};
        var animSpeed = 0;

        this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'shared/portraits/combined_portraits.png', 60, 66);
        this.addAnim('rock', animSpeed, [1], true);
        this.addAnim('paper', animSpeed, [0], true);
        this.addAnim('scissors', animSpeed, [2], true);
        this.title = ig.game.spawnEntity('EntitySelectionbackgroundportraittitle', x+this.titleOffset.x, y+this.titleOffset.y);
        this.stats = ig.game.spawnEntity('EntitySelectionbackgroundportraitstats', x+this.statsOffset.x, y+this.statsOffset.y);
        this.setState('rock');
      },

      update: function(){
        this.parent();
        this.title.pos.x = this.pos.x + this.titleOffset.x;
        this.title.pos.y = this.pos.y + this.titleOffset.y;
        this.stats.pos.x = this.pos.x + this.statsOffset.x;
        this.stats.pos.y = this.pos.y + this.statsOffset.y;
        this.title.currentAnim.alpha = this.currentAnim.alpha;
      },

      setState: function(state){
        this.currentAnim = this.anims[state];
        this.title.setState(state);
        this.stats.setState(state);
      }
    });

    EntitySelectionbackgroundportraittitle = ig.Entity.extend({
      zIndex: 2,
      scale: {x:0.51,y:0.51},
      init: function(x,y,settings) {
        this.parent(x,y,settings);
        this.size = {x:175, y:45};
        var animSpeed = 0;
        //if(ig.game.mode === 'single'){
          this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'character_selection/text/combined_title.png', 175, 45);
          this.addAnim('rock', animSpeed, [1], true);
          this.addAnim('paper', animSpeed, [0], true);
          this.addAnim('scissors', animSpeed, [2], true);
          this.setState('rock');
        //}
      },

      setState: function(state){
        this.currentAnim = this.anims[state];
      }
    });

    EntitySelectionbackgroundportraitstats = ig.Entity.extend({
      zIndex: 2,
      scale: {x:0.5,y:0.5},
      init: function(x,y,settings) {
        this.parent(x,y,settings);
        this.size = {x:405, y:100};
        var animSpeed = 0;
        this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'character_selection/text/combined_stats.png', 405, 100);
        this.addAnim('rock', animSpeed, [1], true);
        this.addAnim('paper', animSpeed, [0], true);
        this.addAnim('scissors', animSpeed, [2], true);
        this.setState('rock');

      },

      setState: function(state){
        this.currentAnim = this.anims[state];
      }
    });

    EntitySelectionplayerfighter = ig.Entity.extend({
      zIndex: 1.5,
      scale:{x:10.0, y:10.0},
      init: function(x,y,settings) {
        this.parent(x,y,settings);
        this.size = {x:130, y:114};
        this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'character_selection/fighters/combined_side.png', 130, 114);
        this.addAnim('rock', 0, [1], true);
        this.addAnim('paper', 0, [0], true);
        this.addAnim('scissors', 0, [2], true);
        this.setState('rock');
      },

      update: function(){
        this.parent();
        if(!this.currentAnim.flip.x){
          this.currentAnim.flip.x = true;
        }
      },

      setState: function(state){
        this.currentAnim = this.anims[state];
      }
    });

    EntitySelectionplayertext = ig.Entity.extend({
      zIndex:6,
      scale: {x:0.5,y:0.5},
      init: function(x,y,settings){
        this.parent(x,y,settings);
        this.size = {x:362,y:45};
        var animSpeed = 0;
        this.animSheet = new ig.AnimationSheet(ig.game.MEDIA_PATH+'character_selection/text/select_fighter_alt.png',362,45);
        this.addAnim('p1', animSpeed, [0],true);
        this.addAnim('p2', animSpeed, [1],true);
        //if(ig.game.mode === 'single'){
          this.currentAnim = this.anims.p1;
        //}
      }
    });
});