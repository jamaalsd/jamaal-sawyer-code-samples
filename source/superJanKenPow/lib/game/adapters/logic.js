/**
 * A logic adapter that takes care of gameplay logic.  Adapts to support networked or single player mode.
 */
ig.module(
  'game.adapters.logic'
  )
.requires(
  'plugins.server'
  )
.defines(function () {

  
  LogicController = ig.Class.extend({  
    
    playerID:0,
    opponentID:0,
    
    server:undefined,
    
    staticInstantiate: function() {
        if( LogicController.instance == null ) {
            return null;
        }
        else {
            return LogicController.instance;
        }
    },
    
    init: function() {
      this.server = new Server();
      LogicController.instance = this;
    },
    
    getServerConnection: function() {
    
      if(!this.server.isConnected) {
        
        this.server.register(this.prepareHandlers.bind(this));
        
      }
      
    },
    
    prepareHandlers:function(data) {
      
      this.playerID = data.player_id;
      this.server.setHandler('warriors_chosen',this.setupPlayers.bind(this));
      this.server.setHandler('round_results',this.roundUpdate.bind(this));
        
    },
    
    setPlayerID:function(data) {
      
      this.playerID = data.player_id;
      
    },
    
    roundUpdate:function(data) {
      
      var result = data.result[0];
      
      for(var p in result) {
        
        if(typeof(result[p].player_id) === 'undefined') continue;
        
        if(result[p].player_id !== this.playerID) {
          
          ig.game.player2Variables = result[p].playerVars;
          
        } else {
          
          ig.game.player1Variables = result[p].playerVars;
          
        }
        
      }
      
    },
    
    submitAttack:function(attackArr,callback) {
      
      this.server.removeHandlers('round_results');
      this.server.setHandler('round_results',this.roundUpdate.bind(this));
      this.server.setHandler('round_results',callback);
      this.server.sendData('submit_attack_choices',{
        attackChoices:attackArr
      });
      
    },
    
    
    setupPlayers:function(data) {
      
      for(var i=0; i < data.result.length; i++) {
        
        if(data.result[i].player_id !== this.playerID) {
          
          this.opponentID = data.result[i].player_id;
          ig.game.player2Variables = data.result[i].playerVars;
          
        } else {
          
          ig.game.player1Variables = data.result[i].playerVars;
          
        }
        
      }
      
    },
    
    
    /**
     * Here we look for an opponent
     */
    requestOpponent:function(callback) {

      if(ig.game.mode === 'multi') {
        
        this.getChallenger(callback);
        
      }
          
    },
    
    /**
     * 
     * @param {type} callback
     * @returns {array}
     */
    chooseWarrior:function(type,callback) {
      
      this.server.removeHandler('warriors_chosen',callback);
      this.server.setHandler('warriors_chosen',callback);
      this.server.sendData('submit_warrior_choice',{choice:type});
      
    },
    
    
    /**
     * 
     * @returns {array}
     */
    getChallenger:function(callback) {
      
      this.server.removeHandlers('player_found'); // clear any existing
      this.server.setHandler('player_found',callback);
      this.server.sendData('request_opponent');
      
    }
    
  });
  
});