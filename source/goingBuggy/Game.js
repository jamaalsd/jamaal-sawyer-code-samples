GoingBuggy.Game = function (game) {
    /*When a State is added to Phaser it automatically has the following properties set on it, even if they already exist:

    game;       //  a reference to the currently running game
    this.add;       //  used to add sprites, text, groups, etc
    this.camera;    //  a reference to the game camera
    this.cache;     //  the game cache
    this.input;     //  the global input manager (you can access this.input.keyboard, this.input.mouse, as well from it)
    this.load;      //  for preloading assets
    this.math;      //  lots of useful common math operations
    this.sound;     //  the sound manager - add a sound, play one, set-up markers, etc
    this.stage;     //  the game stage
    this.time;      //  the clock
    this.tweens;    //  the tween manager
    this.state;     //  the state manager
    this.world;     //  the game world
    this.particles; //  the particle manager
    this.physics;   //  the physics manager
    this.rnd;       //  the repeatable random number generator

    //  You can use any of these from any function within this State.
    //  But do consider them as being 'reserved words', i.e. don't create a property for your own game called "world" or you'll over-write the world reference.*/
};

GoingBuggy.Game.prototype = {
    /*Debug Variables*/
    debug        : true,
    DEBUG_MUTE   : false,
    DEBUG_FPS    : false,
    fpsCounter   : Phaser.Text,
    /*VARIABLE DECLARATIONS (names)*/

    //possible states levelMenu,howToMenu,roundPlaying
    gameState : 'levelMenu',

    /*layers*/
    backgroundLayer : Phaser.Group,
    baseLayer       : Phaser.Group,
    upperLayer      : Phaser.Group,
    topLayer        : Phaser.Group,
    transitionLayer : Phaser.Group,

    /*game variables*/
    coinsEarned     : 0,
    coinsEarnedLast : 0,
    TOTAL_SCENES_PER_LEVEL : 3,
    scenesCorrect   : 0,
    wrongAnswers    : 0,
    currentLevel    : 1,
    currentScene    : 1,
    levelSettings   : [],
    currentLevelSettings : {level:'',id:1},
    roundStart      : false,
    dayNightState   : 'neutral',
    clouds              : [],
    placementHotBoxes   : [],
    tweenArray          : [],
    cursorSprite    : Phaser.Sprite,
    scene1Items : [],
    scene2Items : [],
    scene3Items : [],
    scene1Sprites : {},
    scene2Sprites : {},
    scene3Sprites : {},

    sceneSelect : [],
    starArray: [],

    /*Sound*/
    GAME_MUSIC_VOL : 0.25,
    GAME_SFX_VOL : 1.0,
    GAME_INSTRUCTION_VOL : 0.66,

    successSFX  : [],
    failureSFX  : [],
    gameMusic   : undefined,

    scene1instructions : [],
    scene2instructions : [],
    scene3instructions : [],
    /*create loop called first*/
    create: function () {
        game.currentActiveState = game.state.getCurrentState();
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.input.activePointer.circle.diameter = 15;
        this.initSound();
        this.initLayers();
        this.initUI();
        this.initLevel();
        this.setScene();
        //this.debugSetAllHotBoxes();
        this.initTransition();
    },

    /*this is our main game update loop*/
    update: function () {
    },

    /*render called after update, mostly used to display debug info*/
    render: function () {

    },

    /*GAME*/
    checkCursor:function(){
        if(game.currentActiveState.cursorSprite.alpha === 1.0){
            game.currentActiveState.placementHotBoxInputHandler();
            game.currentActiveState.clearCursor();
            game.currentActiveState.clearAllHotBoxes();
        }
    },

    cloudWrap: function (cloud) {
        cloud.x = 900;
    },

    positionMenuButtons: function (scene){
        //console.log('positioning');
        var i, list, positionIndex, button;
        list = RampsToReading.UI.buttonList;
        switch(scene){
            case 1:
                positionIndex = RampsToReading.UI.buttonPlacement.scene1;
                break;
            case 2:
                positionIndex = RampsToReading.UI.buttonPlacement.scene2;
                break;
            case 3:
                positionIndex = RampsToReading.UI.buttonPlacement.scene3;
                break;
        }
        for(i = 0; i < list.length; i++){
            if(positionIndex[list[i]]){
                button = this.returnUIButton(list[i]);
                button.scale.setTo(positionIndex[list[i]].scale);
                button.x = positionIndex[list[i]].x;
                button.y = positionIndex[list[i]].y;
            }else{
                button = this.returnUIButton(list[i]);
                button.x = -100;
                button.y = -100;
            }
        }
    },

    returnUIButton: function (name){
        var i, button;
        for(i = 0; i < this.UIItems.length; i++){
            if(this.UIItems[i].name === name){
                button = this.UIItems[i];
                break;
            }
        }
        return button;
    },

    setCursor: function(selection){
        if(this.cursorSprite.contains !== selection){
            this.cursorSprite.loadTexture(selection+'Cursor');
            this.cursorSprite.contains = selection;
            this.cursorSprite.alpha = 1.0;
            this.cursorSprite.name = selection;
            //console.log(selection +'Cursor');
            
            //if(selection.indexOf('Static')=== -1)
            //this.setPlacementBoxes(this.currentScene, selection);
        }else{
            //console.log(selection +'Cursor');
            this.cursorSprite.contains = '';
            this.cursorSprite.alpha = 0.0;
            //this.clearPlacementBoxes();
        }
    },

    clearAllScenes: function(){
        var i;
        this.clearScene(11);
        this.clearScene(12);
        this.clearScene(13);
        this.clearScene(14);
        this.clearScene(21);
        this.clearScene(22);
        this.clearScene(23);
        this.clearScene(24);
        this.clearScene(31);
        this.clearScene(32);
        this.clearScene(33);
        this.clearScene(34);
    },

    clearScene: function(scene){
        var i, itemArr;
        itemArr = this.returnSceneItemArr(scene);
        for(i = 0; i < itemArr.length; i++){
            itemArr[i].alpha = 0.0;
            if(itemArr[i].animations){
                itemArr[i].animations.stop();
            }
            if(itemArr[i].contains){
                itemArr[i].contains = '';
            }

        }
        if(this.dayNightState !== 'neutral') this.setSky('neutral');
    },

    setSky: function(state){
        var i,tween,settings;
        
        switch(state){
            case 'day':
                this.background.loadTexture('day');
                this.moonSprite.alpha = 0.0;
                this.sunSprite.alpha = 1.0;
                this.sunSprite.animations.play('play');
                for(i = 0; i < this.clouds.length; i++){
                    tween = game.add.tween(this.clouds[i]).to({alpha: 1.0}, 500, Phaser.Easing.Linear.None, true);
                    tween.name = 'clouds';
                    this.tweenArray.push(tween);
                }
                break;
            case 'night':
                this.background.loadTexture('night');
                this.sunSprite.alpha = 0.0;
                this.moonSprite.alpha = 1.0;
                this.moonSprite.animations.play('play');
                this.tweenClear('clouds');
                for(i = 0; i < this.clouds.length; i++){
                    this.clouds[i].alpha = 0.0;
                }
                break;
            case 'neutral':
                this.background.y = 0;
                this.background.loadTexture('neutral');
                this.moonSprite.alpha = 0.0;
                this.sunSprite.alpha = 0.0;
                this.tweenClear('clouds');
                for(i = 0; i < this.clouds.length; i++){
                    this.clouds[i].alpha = 0.0;
                }
                break;
        }
        
        this.dayNightState = state;
    },

    returnSceneItemArr: function(scene){
        switch(scene){
            case 11:
                return this.scene11Items;
            case 12:
                return this.scene12Items;
            case 13:
                return this.scene13Items;
            case 14:
                return this.scene14Items;
            case 21:
                return this.scene21Items;
            case 22:
                return this.scene22Items;
            case 23:
                return this.scene23Items;
            case 24:
                return this.scene24Items;
            case 31:
                return this.scene31Items;
            case 32:
                return this.scene32Items;
            case 33:
                return this.scene33Items;
            case 34:
                return this.scene34Items;
        }
    },

    returnSpriteFormSceneItemArr:function(arr,name){
        for(var i = 0; i < arr.length; i++){
            if(arr[i].name === name){
                return arr[i];
            }
        }
    },

    setScene: function(){
        var level = this.levelSettings[this.scenesCorrect].level;
        var id = this.levelSettings[this.scenesCorrect].id;
        var i, itemArr, sSettings, settings, scene, sceneNum, scenery;
        settings = sceneData[level]['id'+id];
        scene = settings.scene;
        this.sceneText.text = settings.description;

        this.currentScene = scene;
        this.clearAllScenes();
        
        sceneNum = parseInt(scene.toString()[0],10);
        this.positionMenuButtons(sceneNum);
        itemArr = this.returnSceneItemArr(scene);

        scenery = RampsToReading.UI.sceneryPlacement['scene'+scene];
        for(i = 0; i < itemArr.length; i++){
            if(itemArr[i].loadOnInit){
                if(scenery[itemArr[i].name]){
                    itemArr[i].x = scenery[itemArr[i].name].x;
                    itemArr[i].y = scenery[itemArr[i].name].y;
                    itemArr[i].zI = scenery[itemArr[i].name].zI;
                    itemArr[i].scale.x = scenery[itemArr[i].name].scale;
                    itemArr[i].animations.add('play',null,31,true);
                    itemArr[i].animations.play('play');
                }
                itemArr[i].alpha = 1.0;
            }
        }
        if(sceneNum === 1){
            this.clock.x = scenery['clock'].x;
            this.clock.y = scenery['clock'].y;
            this.clock.alpha = 0.0;
            this.flower.x = scenery['flower'].x;
            this.flower.y = scenery['flower'].y;
            this.flower.alpha = 0.0;
            this.gumball.x = scenery['gumball'].x;
            this.gumball.y = scenery['gumball'].y;
            this.gumball.animations.stop();
            this.gumball.alpha = 0.0;
        }else if(sceneNum === 2){
            this.boat.x = scenery['boat'].x;
            this.boat.y = scenery['boat'].y;
            this.boat.animations.stop();
            this.boat.alpha = 0.0;
            this.mushroom.scale.x = scenery['mushroom'].scale;
            this.mushroom.x = scenery['mushroom'].x;
            this.mushroom.y = scenery['mushroom'].y;
            this.mushroom.alpha = 0.0;
        }else{
            this.lights.scale.x = scenery['lights'].scale;
            this.lights.x = scenery['lights'].x;
            this.lights.y = scenery['lights'].y;
            this.lights.alpha = 0.0;

            this.crayon.x = scenery['crayon'].x;
            this.crayon.y = scenery['crayon'].y;
            this.crayon.alpha = 0.0;
        }

        sSettings = RampsToReading.UI.skySettings['scene'+this.currentScene];
        this.sunSprite.x = sSettings.sun.x;
        this.sunSprite.y = sSettings.sun.y;
        this.moonSprite.x = sSettings.moon.x;
        this.moonSprite.y = sSettings.moon.y;
        this.background.y = sSettings.background.y;

        for(i=0; i < this.clouds.length; i++){
            this.clouds[i].y = sSettings.clouds.y;
        }

        switch(sceneNum){
            case 1:
                if(settings.init.redCap){
                    this.setInitSprite(settings, 'redCap', this.scene1Sprites['redCap'], sceneNum);
                }
                if(settings.init.greenCap){
                    this.setInitSprite(settings, 'greenCap', this.scene1Sprites['greenCap'], sceneNum);
                }
                if(settings.init.blueCap){
                    this.setInitSprite(settings, 'blueCap', this.scene1Sprites['blueCap'], sceneNum);
                }
                if(settings.init.door){
                    this.setInitSprite(settings, 'door', this.scene1Sprites['door'], sceneNum);
                }
                if(settings.init.gumball){
                    this.setGumball();
                }
                if(settings.init.clock){
                    this.setClock();
                }
                if(settings.init.flower){
                    this.setFlowers(settings.init.flower);
                }
                break;
            case 2:
                if(settings.init.ground0){
                    this.setInitSprite(settings, 'ground0', this.scene2Sprites['ground0'], sceneNum);
                }
                if(settings.init.ground1){
                    this.setInitSprite(settings, 'ground1', this.scene2Sprites['ground1'], sceneNum);
                }
                if(settings.init.ground2){
                    this.setInitSprite(settings, 'ground2', this.scene2Sprites['ground2'], sceneNum);
                }
                if(settings.init.redLeaf){
                    this.setInitSprite(settings, 'redLeaf', this.scene2Sprites['redLeaf'], sceneNum);
                }
                if(settings.init.yellowLeaf){
                    this.setInitSprite(settings, 'yellowLeaf', this.scene2Sprites['yellowLeaf'], sceneNum);
                }
                if(settings.init.greenLeaf){
                    this.setInitSprite(settings, 'greenLeaf', this.scene2Sprites['greenLeaf'], sceneNum);
                }
                if(settings.init.float0){
                    this.setInitSprite(settings, 'float0', this.scene2Sprites['float0'], sceneNum);
                }
                if(settings.init.float1){
                    this.setInitSprite(settings, 'float1', this.scene2Sprites['float1'], sceneNum);
                }
                if(settings.init.float2){
                    this.setInitSprite(settings, 'float2', this.scene2Sprites['float2'], sceneNum);
                }
                if(settings.init.float3){
                    this.setInitSprite(settings, 'float3', this.scene2Sprites['float3'], sceneNum);
                }
                if(settings.init.boat){
                    this.setBoat();
                }
                if(settings.init.mushroom){
                    this.setMushrooms(settings.init.mushroom);
                }
                break;
            case 3:
                if(settings.init.redBlock){
                    this.setInitSprite(settings, 'redBlock', this.scene3Sprites['redBlock'], sceneNum);
                }
                if(settings.init.blueBlock){
                    this.setInitSprite(settings, 'blueBlock', this.scene3Sprites['blueBlock'], sceneNum);
                }
                if(settings.init.greenBlock){
                    this.setInitSprite(settings, 'greenBlock', this.scene3Sprites['greenBlock'], sceneNum);
                }
                if(settings.init.character0){
                    this.setInitSprite(settings, 'character0', this.scene3Sprites['character0'], sceneNum);
                }
                if(settings.init.lights){
                    this.setLights();
                }
                if(settings.init.crayon){
                    this.setCrayon(settings.init.crayon);
                }
                break;
        }
        this.backgroundLayer.sort('zI');
        if(settings.init.sky){
            this.setSky(settings.init.sky);
        }
    },

    setBoat: function(){
        if(this.boat.alpha !== 1){
            this.boat.animations.play('play');
            this.boat.alpha = 1;
        }
    },

    setClock: function(){
        if(this.clock.alpha !== 1){
            this.clock.alpha = 1;
        }
    },

    setLights: function(){
        if(this.lights.alpha !== 1){
            this.lights.alpha = 1;
        }
    },

    setGumball: function(){
        if(this.gumball.alpha !== 1){
            this.gumball.animations.play('play');
            this.gumball.alpha = 1;
        }
    },

    checkInitBug: function(text){
        switch(text){
            case 'bee':
            case 'spider':
            case 'ladybug':
            case 'worm':
                return true;
            default:
                return false;
        }
    },

    setCrayon: function(color){
        switch(color){
            case 'purple':
                this.crayon.loadTexture('purpleCrayon_3');
                break;
            case 'orange':
                this.crayon.loadTexture('orangeCrayon_3');
                break;
            case 'green':
                this.crayon.loadTexture('greenCrayon_3');
                break;
        }
        this.crayon.alpha = 1;
    },

    setFlowers: function(color){
        switch(color){
            case 'red':
                this.flower.loadTexture('redFlower_1');
                break;
            case 'blue':
                this.flower.loadTexture('blueFlower_1');
                break;
            case 'purple':
                this.flower.loadTexture('purpleFlower_1');
                break;
        }
        this.flower.alpha = 1;
    },

    setMushrooms: function(color){
        switch(color){
            case 'red':
                this.mushroom.loadTexture('mushroomRed_2');
                break;
            case 'blue':
                this.mushroom.loadTexture('mushroomBlue_2');
                break;
            case 'orange':
                this.mushroom.loadTexture('mushroomOrange_2');
                break;
        }
        this.mushroom.alpha = 1;
    },

    clearCursor:function(){
        if(this.cursorSprite.alpha !== 0.0){
            this.setCursor(this.cursorSprite.contains);
        }
    },

    /*INIT*/
    initLayers: function () {
        this.backgroundLayer    = game.add.group();
        this.baseLayer          = game.add.group();
        this.upperLayer         = game.add.group();
        this.topLayer           = game.add.group();
        this.transitionLayer    = game.add.group();
    },

    initLevel: function(){
        var levelSet;
        this.wrongAnswers = 0;
        this.scenesCorrect = 0;
        switch(this.currentLevel){
            case 1:
            case 2:
                levelSet = 'level12';
                break;
            default:
                levelSet = 'level'+this.currentLevel;
        }
        var idSelect = Phaser.Math.numberArrayStep(1,10);
        //var idSelect = [7,8,9];
        //console.log(idSelect);
        Phaser.Utils.shuffle(idSelect);
        this.levelSettings = [];
        for(var i = 0; i < this.TOTAL_SCENES_PER_LEVEL; i++){
            this.levelSettings.push({level:levelSet, id:idSelect[i]});
        }
    },

    initSound: function (){
        var i;

        this.buttonOverFX = game.add.audio('buttonOver');
        this.buttonOverFX.volume = this.GAME_SFX_VOL;

        this.altButtonOverFX = game.add.audio('altButtonOver');
        this.altButtonOverFX.volume = this.GAME_SFX_VOL;
        for(i = 0; i <= 15; i++){
            this.successSFX.push(game.add.audio('success'+(i+1)));
        }
        this.failureSFX.push(game.add.audio('failure1'));
        this.failureSFX.push(game.add.audio('failure2'));

        this.wrongFX = game.add.audio('wrongAnswer');
        this.yeahCrowd = game.add.audio('yeahCrowd');
        this.ohCrowd = game.add.audio('ohCrowd');

        this.gameMusic = game.add.audio('gameMusic');
        this.gameMusic.volume = this.GAME_MUSIC_VOL;
        this.gameMusic.loop = true;
        this.gameMusic.play();
    },

    initUI: function () {
        var i, sprite, anim, vel, offset;

        /*day/night*/
        this.background = game.add.sprite(800,100,'neutral');
        this.background.anchor.x = 1.0;
        this.background.zI = -10;
        this.backgroundLayer.add(this.background);

        sprite = game.add.sprite(570, 94, 'sun');
        sprite.animations.add('play',null, 31);
        sprite.alpha = 0.0;
        sprite.zI = -9;
        this.backgroundLayer.add(sprite);
        this.sunSprite = sprite;

        sprite = game.add.sprite(580, -15, 'moon');
        sprite.animations.add('play',null, 31);
        sprite.alpha = 0.0;
        sprite.zI = -9;
        this.backgroundLayer.add(sprite);
        this.moonSprite = sprite;

        /*clouds*/
        vel = -25;
        offset = game.rnd.integerInRange(100,300);
        sprite = game.add.sprite(0, 0, 'cloud1');
        game.physics.arcade.enable(sprite);
        sprite.animations.add('play',null,50,true);
        //sprite.animations.play('play');
        //sprite.animations.getAnimation('play').frame = Math.floor(Math.random() * sprite.animations.getAnimation('play').frameTotal);
        sprite.zI = -8;
        sprite.x = 900 - offset;
        sprite.y = 20;
        sprite.origX = sprite.x;
        sprite.body.velocity.x = vel;
        sprite.update = function(){
            if(this.x < -300){
                game.currentActiveState.cloudWrap(this);
            }
        }.bind(sprite);
        this.backgroundLayer.add(sprite);
        this.clouds.push(sprite);

        sprite = game.add.sprite(0, 0, 'cloud2');
        game.physics.arcade.enable(sprite);
        anim = sprite.animations.add('play',null,50,true);
        //sprite.animations.play('play');
        //sprite.animations.getAnimation('play').frame = Math.floor(Math.random() * sprite.animations.getAnimation('play').frameTotal);
        sprite.zI = -8;
        sprite.x = 1650 - offset;
        sprite.y = 20;
        sprite.origX = sprite.x;
        sprite.body.velocity.x = vel;
        sprite.update = function(){
            if(this.x < -300){
                game.currentActiveState.cloudWrap(this);
            }
        }.bind(sprite);
        this.backgroundLayer.add(sprite);
        this.clouds.push(sprite);

        sprite = game.add.sprite(0, 0, 'cloud3');
        game.physics.arcade.enable(sprite);
        anim = sprite.animations.add('play',null,50,true);
        //sprite.animations.play('play');
        //sprite.animations.getAnimation('play').frame = Math.floor(Math.random() * sprite.animations.getAnimation('play').frameTotal);
        sprite.zI = -8;
        sprite.x = 1450 - offset;
        sprite.y = 20;
        sprite.origX = sprite.x;
        sprite.body.velocity.x = vel;
        sprite.update = function(){
            if(this.x < -300){
                game.currentActiveState.cloudWrap(this);
            }
        }.bind(sprite);
        this.backgroundLayer.add(sprite);
        this.clouds.push(sprite);

        for(i = 0; i < this.clouds.length; i++){
            this.clouds[i].alpha = 0.0;
        }

        /*toolbar/UI*/
        this.toolbar = game.add.sprite(0,0, 'toolbar');
        this.upperLayer.addChild(this.toolbar);

        var style = { font: "17px arial_narrowbold, sans-serif", fill: "#000000"};
        this.sceneText = game.add.text(18,12, '', style);
        this.sceneText.wordWrap = true;
        this.sceneText.wordWrapWidth = 195;
        this.upperLayer.add(this.sceneText);

        // this.replayButton = new RampsToReading.UI.Button({
        //     x : 115,
        //     y : 520,
        //     z : 0,
        //     name : 'replayButton',
        //     group : this.upperLayer,
        //     scale : 1.0,
        //     anchor : { x: 0.0, y: 0.0},
        //     sprite : 'replayButton'
        // });

        this.clearButton = new RampsToReading.UI.Button({
            x : 705,
            y : 393,
            z : 0,
            name : 'clearButton',
            group : this.upperLayer,
            scale : 1.0,
            anchor : { x: 0.0, y: 0.0},
            sprite : 'clearButton'
        });

        this.doneButton = new RampsToReading.UI.Button({
            x : 705,
            y : 493,
            z : 0,            name : 'doneButton',
            group : this.upperLayer,
            scale : 1.0,
            anchor : { x: 0.0, y: 0.0},
            sprite : 'doneButton'
        });

        this.UIItems = [];

        var list = RampsToReading.UI.buttonList;


        for(i = 0; i < list.length; i++){
            this.UIItems.push(
                new RampsToReading.UI.Button({
                    x : -1000,
                    y : -1000,
                    zI : 0,
                    name : list[i],
                    group : this.upperLayer,
                    scale : 1.0,
                    anchor : { x: 0.0, y: 0.0},
                    sprite : list[i]
                })
            );
        }

        /*coin*/
        this.spinningCoin = game.add.sprite(240, 15, 'coin');
        //this.spinningCoin.scale.setTo(0.66);
        this.spinningCoin.animations.add('spin');
        this.spinningCoin.play('spin', 20, true, false);
        this.spinningCoin.zI = 10;
        this.backgroundLayer.addChild(this.spinningCoin);

        this.coinProgressBase = game.add.sprite(this.spinningCoin.x, 0, 'coinProgressBase');
        this.coinProgressBase.x += this.spinningCoin.width + 12;
        this.coinProgressBase.y +=4;
        this.coinProgressBase.alpha = 0.0;
        this.spinningCoin.addChild(this.coinProgressBase);

        this.coinProgressFill = game.add.sprite(0, 0, 'coinProgressFill');
        this.coinProgressFill.cropEnabled = true;
        this.coinProgressFill.cropRect = new Phaser.Rectangle(0, 0, 1, this.coinProgressFill.height);
        this.coinProgressFill.updateCrop();
        this.coinProgressFill.x = this.coinProgressBase.x;
        this.coinProgressFill.y = this.spinningCoin.y+15;
        this.coinProgressFill.anchor.y = 0.5;
        this.coinProgressFill.scale.y = 1;
        this.backgroundLayer.addChild(this.coinProgressFill);

        this.coinProgressTab = game.add.sprite(this.coinProgressFill.x + this.coinProgressBase.width + 35, this.spinningCoin.y - 15, 'coinProgressTab');
        this.coinProgressTab.anchor.x = 0.5;
        this.coinProgressTab.zI = 10;
        this.coinProgressTab.scale.setTo(1);
        this.backgroundLayer.addChild(this.coinProgressTab);

        var style1 = { font: "32px vag_roundedbold", fill: "#ffffff", align:"center", stroke:"#000000", strokeThickness: 4};
        this.coinProgressTab.text = game.add.text(12, 8, ' 0 ', style1);
        
        this.coinProgressTab.text.anchor.x = 0.5;
        this.coinProgressTab.addChild(this.coinProgressTab.text);
        this.coinProgressTab.pic = game.add.sprite(0,0,'coinMenu');
        this.coinProgressTab.pic.x -= 38;
        this.coinProgressTab.pic.y += 12;
        this.coinProgressTab.pic.scale.setTo(0.5);
        this.coinProgressTab.text.addChild(this.coinProgressTab.pic);
        game.currentActiveState.coinProgressTab.x = game.currentActiveState.coinProgressFill.x+game.currentActiveState.coinProgressFill.width+35;
        this.coinProgressTab.update = function (){
                this.text.text = ' '+game.currentActiveState.coinsEarned+' ';
                //this.x = game.currentActiveState.coinProgressFill.x+game.currentActiveState.coinProgressFill.width+35;
        }.bind(this.coinProgressTab);

        this.coinProgressTab.text.y += 5;
        this.coinProgressTab.pic.y -= 4;

        /*health stars*/
        var height = 35;
        sprite = game.add.sprite(620, height,'star');
        sprite.zI = 10;
        this.backgroundLayer.add(sprite);
        this.starArray.push(sprite);
        sprite.alpha = 1;

        sprite = game.add.sprite(680, height,'star');
        sprite.zI = 10;
        this.backgroundLayer.add(sprite);
        this.starArray.push(sprite);
        sprite.alpha = 1;

        sprite = game.add.sprite(740, height,'star');
        sprite.zI = 10;
        this.backgroundLayer.add(sprite);
        this.starArray.push(sprite);
        sprite.alpha = 1;

        /*SCENE 1 SPRITE INIT*/
        this.scene11Items = [];
        this.scene12Items = [];
        this.scene13Items = [];
        this.scene14Items = [];

        sprite = game.add.sprite(804,378,'scene1_1');
        sprite.name = 'scene1_1';
        sprite.loadOnInit = true;
        sprite.anchor.setTo(1.0);
        sprite.zI = 1.0;
        this.backgroundLayer.add(sprite);
        this.scene11Items.push(sprite);

        sprite = game.add.sprite(804,378,'scene1_2');
        sprite.name = 'scene1_2';
        sprite.loadOnInit = true;
        sprite.anchor.setTo(1.0);
        sprite.zI = 1.0;
        this.backgroundLayer.add(sprite);
        this.scene12Items.push(sprite);

        sprite = game.add.sprite(804,378,'scene1_3');
        sprite.name = 'scene1_3';
        sprite.loadOnInit = true;
        sprite.anchor.setTo(1.0);
        sprite.zI = 1.0;
        this.backgroundLayer.add(sprite);
        this.scene13Items.push(sprite);

        sprite = game.add.sprite(804,378,'scene1_4');
        sprite.name = 'scene1_4';
        sprite.loadOnInit = true;
        sprite.anchor.setTo(1.0);
        sprite.zI = 1.0;
        this.backgroundLayer.add(sprite);
        this.scene14Items.push(sprite);

        sprite = game.add.sprite(240,238,'redCap');
        sprite.name = sprite.key;
        sprite.loadOnInit = true;
        sprite.zI = 3.0;
        this.backgroundLayer.add(sprite);
        this.scene11Items.push(sprite);
        this.scene12Items.push(sprite);
        this.scene13Items.push(sprite);
        this.scene14Items.push(sprite);

        sprite = game.add.sprite(455,238,'blueCap');
        sprite.name = sprite.key;
        sprite.loadOnInit = true;
        sprite.zI = 3.0;
        this.backgroundLayer.add(sprite);
        this.scene11Items.push(sprite);
        this.scene12Items.push(sprite);
        this.scene13Items.push(sprite);
        this.scene14Items.push(sprite);

        sprite = game.add.sprite(348,303,'greenCap');
        sprite.name = sprite.key;
        sprite.loadOnInit = true;
        sprite.zI = 5.0;
        this.backgroundLayer.add(sprite);
        this.scene11Items.push(sprite);
        this.scene12Items.push(sprite);
        this.scene13Items.push(sprite);
        this.scene14Items.push(sprite);

        this.clock = game.add.sprite(308,10,'clock_1');
        this.clock.name = 'clock';
        this.clock.scale.setTo(0.75);
        this.clock.zI = 1.0;
        this.backgroundLayer.add(this.clock);
        this.scene11Items.push(this.clock);
        this.scene12Items.push(this.clock);
        this.scene13Items.push(this.clock);
        this.scene14Items.push(this.clock);

        this.gumball = game.add.sprite(382,-68, 'gumball_1');
        this.gumball.name = 'gumball';
        this.gumball.zI = 1.0;
        this.backgroundLayer.add(this.gumball);
        this.scene11Items.push(this.gumball);
        this.scene12Items.push(this.gumball);
        this.scene13Items.push(this.gumball);
        this.scene14Items.push(this.gumball);
        this.gumball.animations.add('play',null,31,true);

        this.flower = game.add.sprite(435,115,'blueFlower_1');
        this.flower.name = 'flower';
        this.flower.zI = 1.0;
        this.backgroundLayer.add(this.flower);
        this.scene11Items.push(this.flower);
        this.scene12Items.push(this.flower);
        this.scene13Items.push(this.flower);
        this.scene14Items.push(this.flower);

        sprite = game.add.sprite(-1000,-1000,'placeholder');
        sprite.zI = 1;
        sprite.alpha = 0;
        this.backgroundLayer.add(sprite);
        this.scene11Items.push(sprite);
        this.scene12Items.push(sprite);
        this.scene13Items.push(sprite);
        this.scene14Items.push(sprite);
        this.scene1Sprites['redCap'] = sprite;

        sprite = game.add.sprite(-1000,-1000,'placeholder');
        sprite.zI = 1;
        sprite.alpha = 0;
        this.backgroundLayer.add(sprite);
        this.scene11Items.push(sprite);
        this.scene12Items.push(sprite);
        this.scene13Items.push(sprite);
        this.scene14Items.push(sprite);
        this.scene1Sprites['blueCap'] = sprite;

        sprite = game.add.sprite(-1000,-1000,'placeholder');
        sprite.zI = 1;
        sprite.alpha = 0;
        this.backgroundLayer.add(sprite);
        this.scene11Items.push(sprite);
        this.scene12Items.push(sprite);
        this.scene13Items.push(sprite);
        this.scene14Items.push(sprite);
        this.scene1Sprites['greenCap'] = sprite;

        sprite = game.add.sprite(-1000,-1000,'placeholder');
        sprite.zI = 1;
        sprite.alpha = 0;
        this.backgroundLayer.add(sprite);
        this.scene11Items.push(sprite);
        this.scene12Items.push(sprite);
        this.scene13Items.push(sprite);
        this.scene14Items.push(sprite);
        this.scene1Sprites['door'] = sprite;



        /*END SCENE 1*/
        
        /*SCENE 2 SPRITE INIT*/
        this.scene21Items = [];
        this.scene22Items = [];
        this.scene23Items = [];
        this.scene24Items = [];

        sprite = game.add.sprite(804,378,'scene2_1');
        sprite.name = sprite.key;
        sprite.loadOnInit = true;
        sprite.anchor.setTo(1.0);
        sprite.zI = 1.0;
        this.backgroundLayer.add(sprite);
        this.scene21Items.push(sprite);

        sprite = game.add.sprite(804,378,'scene2_2');
        sprite.name = sprite.key;
        sprite.loadOnInit = true;
        sprite.anchor.setTo(1.0);
        sprite.zI = 1.0;
        this.backgroundLayer.add(sprite);
        this.scene22Items.push(sprite);

        sprite = game.add.sprite(804,378,'scene2_3');
        sprite.name = sprite.key;
        sprite.loadOnInit = true;
        sprite.anchor.setTo(1.0);
        sprite.zI = 1.0;
        this.backgroundLayer.add(sprite);
        this.scene23Items.push(sprite);

        sprite = game.add.sprite(804,378,'scene2_4');
        sprite.name = sprite.key;
        sprite.loadOnInit = true;
        sprite.anchor.setTo(1.0);
        sprite.zI = 1.0;
        this.backgroundLayer.add(sprite);
        this.scene24Items.push(sprite);

        sprite = game.add.sprite(240,238,'pond_2');
        sprite.name = sprite.key;
        sprite.loadOnInit = true;
        sprite.zI = 3.0;
        this.backgroundLayer.add(sprite);
        this.scene21Items.push(sprite);
        this.scene22Items.push(sprite);
        this.scene23Items.push(sprite);
        this.scene24Items.push(sprite);

        sprite = game.add.sprite(240,238,'rocks_2');
        sprite.name = sprite.key;
        sprite.loadOnInit = true;
        sprite.zI = 3.0;
        this.backgroundLayer.add(sprite);
        this.scene21Items.push(sprite);
        this.scene22Items.push(sprite);
        this.scene23Items.push(sprite);
        this.scene24Items.push(sprite);

        sprite = game.add.sprite(240,238,'sign_2');
        sprite.name = sprite.key;
        sprite.loadOnInit = true;
        sprite.zI = 3.0;
        this.backgroundLayer.add(sprite);
        this.scene21Items.push(sprite);
        this.scene22Items.push(sprite);
        this.scene23Items.push(sprite);
        this.scene24Items.push(sprite);

        sprite = game.add.sprite(240,238,'signAlt_2');
        sprite.name = sprite.key;
        sprite.loadOnInit = true;
        sprite.zI = 3.0;
        this.backgroundLayer.add(sprite);
        this.scene21Items.push(sprite);
        this.scene22Items.push(sprite);
        this.scene23Items.push(sprite);
        this.scene24Items.push(sprite);

        sprite = game.add.sprite(240,238,'grass_2');
        sprite.name = sprite.key;
        sprite.loadOnInit = true;
        sprite.zI = 3.0;
        this.backgroundLayer.add(sprite);
        this.scene21Items.push(sprite);
        this.scene22Items.push(sprite);
        this.scene23Items.push(sprite);
        this.scene24Items.push(sprite);

        this.boat = game.add.sprite(705,235,'boat_2');
        this.boat.name = 'boat';
        this.boat.zI = 3.0;
        this.backgroundLayer.add(this.boat);
        this.scene21Items.push(this.boat);
        this.scene22Items.push(this.boat);
        this.scene23Items.push(this.boat);
        this.scene24Items.push(this.boat);
        this.boat.animations.add('play',null,31,true);


        this.mushroom = game.add.sprite(490,30,'mushroomBlue_2');
        this.mushroom.name = 'mushroom';
        this.mushroom.zI = 1.1;
        this.backgroundLayer.add(this.mushroom);
        this.scene21Items.push(this.mushroom);
        this.scene22Items.push(this.mushroom);
        this.scene23Items.push(this.mushroom);
        this.scene24Items.push(this.mushroom);

        sprite = game.add.sprite(-1000,-1000,'placeholder');
        sprite.zI = 1;
        sprite.alpha = 0;
        this.backgroundLayer.add(sprite);
        this.scene21Items.push(sprite);
        this.scene22Items.push(sprite);
        this.scene23Items.push(sprite);
        this.scene24Items.push(sprite);
        this.scene2Sprites['ground0'] = sprite;

        sprite = game.add.sprite(-1000,-1000,'placeholder');
        sprite.zI = 1;
        sprite.alpha = 0;
        this.backgroundLayer.add(sprite);
        this.scene21Items.push(sprite);
        this.scene22Items.push(sprite);
        this.scene23Items.push(sprite);
        this.scene24Items.push(sprite);
        this.scene2Sprites['ground1'] = sprite;

        sprite = game.add.sprite(-1000,-1000,'placeholder');
        sprite.zI = 1;
        sprite.alpha = 0;
        this.backgroundLayer.add(sprite);
        this.scene21Items.push(sprite);
        this.scene22Items.push(sprite);
        this.scene23Items.push(sprite);
        this.scene24Items.push(sprite);
        this.scene2Sprites['ground2'] = sprite;

        sprite = game.add.sprite(-1000,-1000,'placeholder');
        sprite.zI = 1;
        sprite.alpha = 0;
        this.backgroundLayer.add(sprite);
        this.scene21Items.push(sprite);
        this.scene22Items.push(sprite);
        this.scene23Items.push(sprite);
        this.scene24Items.push(sprite);
        this.scene2Sprites['redLeaf'] = sprite;

        sprite = game.add.sprite(-1000,-1000,'placeholder');
        sprite.zI = 1;
        sprite.alpha = 0;
        this.backgroundLayer.add(sprite);
        this.scene21Items.push(sprite);
        this.scene22Items.push(sprite);
        this.scene23Items.push(sprite);
        this.scene24Items.push(sprite);
        this.scene2Sprites['yellowLeaf'] = sprite;
        
        sprite = game.add.sprite(-1000,-1000,'placeholder');
        sprite.zI = 1;
        sprite.alpha = 0;
        this.backgroundLayer.add(sprite);
        this.scene21Items.push(sprite);
        this.scene22Items.push(sprite);
        this.scene23Items.push(sprite);
        this.scene24Items.push(sprite);
        this.scene2Sprites['greenLeaf'] = sprite;

        sprite = game.add.sprite(-1000,-1000,'placeholder');
        sprite.zI = 1;
        sprite.alpha = 0;
        this.backgroundLayer.add(sprite);
        this.scene21Items.push(sprite);
        this.scene22Items.push(sprite);
        this.scene23Items.push(sprite);
        this.scene24Items.push(sprite);
        this.scene2Sprites['float0'] = sprite;

        sprite = game.add.sprite(-1000,-1000,'placeholder');
        sprite.zI = 1;
        sprite.alpha = 0;
        this.backgroundLayer.add(sprite);
        this.scene21Items.push(sprite);
        this.scene22Items.push(sprite);
        this.scene23Items.push(sprite);
        this.scene24Items.push(sprite);
        this.scene2Sprites['float1'] = sprite;

        sprite = game.add.sprite(-1000,-1000,'placeholder');
        sprite.zI = 1;
        sprite.alpha = 0;
        this.backgroundLayer.add(sprite);
        this.scene21Items.push(sprite);
        this.scene22Items.push(sprite);
        this.scene23Items.push(sprite);
        this.scene24Items.push(sprite);
        this.scene2Sprites['float2'] = sprite;

        sprite = game.add.sprite(-1000,-1000,'placeholder');
        sprite.zI = 1;
        sprite.alpha = 0;
        this.backgroundLayer.add(sprite);
        this.scene21Items.push(sprite);
        this.scene22Items.push(sprite);
        this.scene23Items.push(sprite);
        this.scene24Items.push(sprite);
        this.scene2Sprites['float3'] = sprite;

        /*END SCENE 2*/

        /*SCENE 3*/
        this.scene31Items = [];
        this.scene32Items = [];
        this.scene33Items = [];
        this.scene34Items = [];

        sprite = game.add.sprite(804,378,'scene3_1');
        sprite.name = sprite.key;
        sprite.loadOnInit = true;
        sprite.anchor.setTo(1.0);
        sprite.zI = 1.0;
        this.backgroundLayer.add(sprite);
        this.scene31Items.push(sprite);

        sprite = game.add.sprite(804,378,'scene3_2');
        sprite.name = sprite.key;
        sprite.loadOnInit = true;
        sprite.anchor.setTo(1.0);
        sprite.zI = 1.0;
        this.backgroundLayer.add(sprite);
        this.scene32Items.push(sprite);

        sprite = game.add.sprite(804,378,'scene3_3');
        sprite.name = sprite.key;
        sprite.loadOnInit = true;
        sprite.anchor.setTo(1.0);
        sprite.zI = 1.0;
        this.backgroundLayer.add(sprite);
        this.scene33Items.push(sprite);

        sprite = game.add.sprite(804,378,'scene3_4');
        sprite.name = sprite.key;
        sprite.loadOnInit = true;
        sprite.anchor.setTo(1.0);
        sprite.zI = 1.0;
        this.backgroundLayer.add(sprite);
        this.scene34Items.push(sprite);

        sprite = game.add.sprite(240,238,'redBlock_3');
        sprite.name = sprite.key;
        sprite.loadOnInit = true;
        sprite.zI = 3.0;
        this.backgroundLayer.add(sprite);
        this.scene31Items.push(sprite);
        this.scene32Items.push(sprite);
        this.scene33Items.push(sprite);
        this.scene34Items.push(sprite);

        sprite = game.add.sprite(240,238,'greenBlock_3');
        sprite.name = sprite.key;
        sprite.loadOnInit = true;
        sprite.zI = 3.0;
        this.backgroundLayer.add(sprite);
        this.scene31Items.push(sprite);
        this.scene32Items.push(sprite);
        this.scene33Items.push(sprite);
        this.scene34Items.push(sprite);

        sprite = game.add.sprite(240,238,'blueBlock_3');
        sprite.name = sprite.key;
        sprite.loadOnInit = true;
        sprite.zI = 3.0;
        this.backgroundLayer.add(sprite);
        this.scene31Items.push(sprite);
        this.scene32Items.push(sprite);
        this.scene33Items.push(sprite);
        this.scene34Items.push(sprite);

        this.lights = game.add.sprite(834,0,'lights_3');
        this.lights.name = this.lights.key;
        this.lights.zI = 1.1;
        this.lights.scale.x = 1;
        this.backgroundLayer.add(this.lights);
        this.scene31Items.push(this.lights);
        this.scene32Items.push(this.lights);
        this.scene33Items.push(this.lights);
        this.scene34Items.push(this.lights);

        sprite = game.add.sprite(253,1,'crushPin1_3');
        sprite.name = sprite.key;
        sprite.loadOnInit = true;
        sprite.zI = 1.2;
        sprite.scale.x = 1;
        this.backgroundLayer.add(sprite);
        this.scene31Items.push(sprite);
        this.scene32Items.push(sprite);
        this.scene33Items.push(sprite);
        this.scene34Items.push(sprite);

        var sprite2 = game.add.sprite(206,1,'crushPin2_3');
        sprite2.name = sprite.key;
        sprite2.zI = 1.2;
        this.backgroundLayer.add(sprite2);
        sprite.addChild(sprite2);

        this.crayon = game.add.sprite(460,315,'purpleCrayon_3');
        this.crayon.name = 'crayon';
        //this.crayon.loadOnInit = true;
        this.crayon.zI = 1.2;
        this.backgroundLayer.add(this.crayon);
        this.scene31Items.push(this.crayon);
        this.scene32Items.push(this.crayon);
        this.scene33Items.push(this.crayon);
        this.scene34Items.push(this.crayon);

        sprite = game.add.sprite(-1000,-1000,'placeholder');
        sprite.zI = 1;
        sprite.alpha = 0;
        this.backgroundLayer.add(sprite);
        this.scene31Items.push(sprite);
        this.scene32Items.push(sprite);
        this.scene33Items.push(sprite);
        this.scene34Items.push(sprite);
        this.scene3Sprites['redBlock'] = sprite;

        sprite = game.add.sprite(-1000,-1000,'placeholder');
        sprite.zI = 1;
        sprite.alpha = 0;
        this.backgroundLayer.add(sprite);
        this.scene31Items.push(sprite);
        this.scene32Items.push(sprite);
        this.scene33Items.push(sprite);
        this.scene34Items.push(sprite);
        this.scene3Sprites['greenBlock'] = sprite;

        sprite = game.add.sprite(-1000,-1000,'placeholder');
        sprite.zI = 1;
        sprite.alpha = 0;
        this.backgroundLayer.add(sprite);
        this.scene31Items.push(sprite);
        this.scene32Items.push(sprite);
        this.scene33Items.push(sprite);
        this.scene34Items.push(sprite);
        this.scene3Sprites['blueBlock'] = sprite;

        sprite = game.add.sprite(-1000,-1000,'placeholder');
        sprite.zI = 1;
        sprite.alpha = 0;
        this.backgroundLayer.add(sprite);
        this.scene31Items.push(sprite);
        this.scene32Items.push(sprite);
        this.scene33Items.push(sprite);
        this.scene34Items.push(sprite);
        this.scene3Sprites['character0'] = sprite;
        // /*END SCENE 3*/

        for(i = 0; i < this.placementHotBoxes.length;i++){
            this.placementHotBoxes[i].destroy();
        }
        this.placementHotBoxes = [];

        /*HOTBOX INIT/PLACEMENT*/
        for(i = 0; i < 10; i++){
            sprite = game.add.sprite(-100, 0, 'placementHotBox');
            sprite.animations.add('play', null, 31, true);
            sprite.animations.play('play');
            sprite.zI = 10.0;
            sprite.inputEnabled = true;
            this.backgroundLayer.add(sprite);
            this.placementHotBoxes.push(sprite);
        }
        this.backgroundLayer.sort('zI');

        /*button cursor, setting to 'worm' arbitrarily*/
        sprite = game.add.sprite(0,0,'wormCursor');
        sprite.anchor.setTo(0.5);
        sprite.alpha = 0.0;
        sprite.contains = '';
        sprite.update = function () {
            this.x = game.input.activePointer.x;
            this.y = game.input.activePointer.y;
        }.bind(sprite);
        this.cursorSprite = sprite;
        this.upperLayer.add(sprite);
        /*Menu stuff*/

        this.overlay = game.add.sprite(0,0,'overlay');
        this.overlay.alpha = 0.0;
        this.upperLayer.add(this.overlay);

        this.successBanner = game.add.sprite(200,-360,'successBanner');
        this.successBanner.animations.add('play',null,31,false);
        this.upperLayer.add(this.successBanner);

        this.menuScreen = game.add.sprite(0,0,'menuLevel1');
        this.menuScreen.inputEnabled = true;
        this.menuScreen.events.onInputUp.add(game.currentActiveState.menuScreenClick, this);
        this.upperLayer.addChild(this.menuScreen);
        this.loadMenu(this.currentLevel, false);

        this.menuButton = game.add.sprite(60,595,'menuButton', this);
        this.menuButton.anchor.y = 1.0;
        this.menuButton.zI = 3.0;
        this.menuButton.inputEnabled = true;
        this.menuButton.animations.add('nextIdle',[0]);
        this.menuButton.animations.add('helpHighlight',[2]);
        this.menuButton.animations.add('helpIdle',[1]);
        this.menuButton.animations.add('nextHighlight',[3]);
        this.menuButton.animations.add('playHighlight',[4]);
        this.menuButton.events.onInputOver.add(game.currentActiveState.menuButtonOver, this);
        this.menuButton.events.onInputOut.add(game.currentActiveState.menuButtonOut, this);
        this.menuButton.events.onInputUp.add(game.currentActiveState.menuButtonClick, this);
        this.menuButton.animations.play('nextIdle');
        this.topLayer.addChild(this.menuButton);

        var style2 = { font: "32px vag_roundedbold", fill: "#ffffff", stroke:"#000000", strokeThickness: 4};
        this.levelText = game.add.text(800,0, ' Level '+this.currentLevel+' ', style2);
        this.levelText.anchor.x = 1.0;
        this.levelText.zI = 10;
        this.backgroundLayer.add(this.levelText);

    },

    menuScreenClick: function (){
        //console.log('here');
        if(!this.roundStart && this.menuScreen.alpha !== 0){
            this.menuButtonClick();
        }
    },

    initTransition: function () {
        this.transition = new Phaser.Sprite(game, 0,0, 'transition');
        this.transitionLayer.add(this.transition);
        var tween = game.add.tween(this.transition).to({ alpha: 0.0}, 1000, Phaser.Easing.Linear.None,true, 250, 0, false);
    },

    debugSetAllHotBoxes: function(){
        console.log(RampsToReading.UI.hotBoxPlacement);
        console.log('scene'+this.currentScene);
        var placementArr = RampsToReading.UI.hotBoxPlacement['scene'+this.currentScene];
        var i;
        for(i = 0; i < placementArr.length; i++){
            this.placementHotBoxes[i].x = placementArr[i].x;
            this.placementHotBoxes[i].y = placementArr[i].y;
            this.placementHotBoxes[i].name = placementArr[i].name;
        }
        for(i; i < this.placementHotBoxes.length; i++){
            this.placementHotBoxes[i].x = -100;
            this.placementHotBoxes[i].y = 0;
            this.placementHotBoxes[i].name = undefined;
        }
    },

    clearAllHotBoxes: function(){
        for(var i = 0; i < this.placementHotBoxes.length; i++){
            this.placementHotBoxes[i].x = -100;
            this.placementHotBoxes[i].y = 0;
            this.placementHotBoxes[i].name = undefined;
        }
    },

    setHotBoxes: function(sprite){
        var hotBoxSelectionArr = RampsToReading.UI.hotBoxSelection['scene'+this.currentScene][sprite];
        var placementArr = RampsToReading.UI.hotBoxPlacement['scene'+this.currentScene];
        var i,j, currentSelect;
        console.log(this.placementHotBoxes);

        for(i = 0; i < hotBoxSelectionArr.length; i++){
            currentSelect = hotBoxSelectionArr[i];
            for(j = 0; j < placementArr.length; j++){
                if(placementArr[j].name === currentSelect){
                    this.placementHotBoxes[i].x = placementArr[j].x;
                    this.placementHotBoxes[i].y = placementArr[j].y;
                    this.placementHotBoxes[i].name = placementArr[j].name;
                }
            }
        }
        for(i;i < placementArr.length; i++){
            this.placementHotBoxes[i].x = -100;
            this.placementHotBoxes[i].y = 0;
            this.placementHotBoxes[i].name = undefined;
        }
    },

    placementHotBoxInputHandler: function (){
        var box;
        for(var i = 0; i < this.placementHotBoxes.length; i++){
            if(this.placementHotBoxes[i].input.checkPointerOver(game.input.activePointer,true)){
                box = this.placementHotBoxes[i];
                break;
            }
        }

        if(box !== undefined){
            this.chooseAndSetSprite(box.name, this.cursorSprite.name);
        }
    },

    chooseAndSetSprite: function(location, cSprite){
        var sceneSprites;
        var scene = parseInt(this.currentScene.toString()[0],10);
        switch(parseInt(this.currentScene.toString()[0],10)){
            case 1:
                sceneSprites = this.scene1Sprites;
                break;
            case 2:
                sceneSprites = this.scene2Sprites;
                break;
            case 3:
                sceneSprites = this.scene3Sprites;
                break;
        }
        var sprite = sceneSprites[location];
        switch(cSprite){
            case 'ladybug':
            case 'spider':
            case 'bee':
            case 'worm':
                this.setBug(sprite,cSprite, scene, location);
                break;
            case 'cupcake':
            case 'cookie':
            case 'cocoa':
            case 'beachball':
            case 'radio':
            case 'sunglasses':
            case 'fire':
            case 'keyboard':
            case 'guitar':
            case 'horn':
            case 'drums':
                this.setItem(sprite,cSprite, scene, location);
                break;
        }
    },

    checkCorrect: function(){
        var correct = this.checkScene();
        if(correct){
            this.correctAnswer();
        }else{
            this.wrongAnswer();

        }
    },

    correctAnswer: function(){
        this.coinsEarned+=5;
        this.yeahCrowd.play();
        this.roundStart = false;
        this.scenesCorrect++;
        this.wrongAnswers = 0;
        var s = game.rnd.integerInRange(0,15);
        this.successSFX[s].play();
        this.starArray[0].alpha = 1;
        this.starArray[1].alpha = 1;
        this.starArray[2].alpha = 1;
        if(this.scenesCorrect >= this.TOTAL_SCENES_PER_LEVEL){
            this.scenesCorrect = 0;
            this.menuScreen.alpha = 1;
            APP.user.addPoints(this.coinsEarned - this.coinsEarnedLast, 'Completed level ' + this.currentLevel + ' of ' + document.title);
            this.coinsEarnedLast = this.coinsEarned;
            this.menuScreen.loadTexture('menuLevelComplete');
            this.menuScreen.inputEnabled = true;
            this.gameState = 'nextLevel';
            this.menuButton.animations.play('nextIdle');
        }else{
            this.overlay.alpha = 1.0;
            this.successBanner.animations.play('play');
            var time = (96 * (1000/31))/2;
            //console.log(time);
            game.time.events.add(time, game.currentActiveState.loadNextScene, this);
            game.time.events.add(time*2.5, game.currentActiveState.finishLoadNextScene, this);
        }
    },

    wrongAnswer: function(){
        this.roundStart = false;
        this.wrongFX.play();
        this.wrongAnswers++;
        switch(this.wrongAnswers){
            case 1:
                this.starArray[0].alpha = 0;
                this.overlay.alpha = 1.0;
                this.failureSFX[0].play();
                game.time.events.add(Phaser.Timer.SECOND * this.failureSFX[0].totalDuration + Phaser.Timer.SECOND * 0.25, this.returnToGame, this);
                break;
            case 2:
                this.starArray[1].alpha = 0;
                this.overlay.alpha = 1.0;
                this.failureSFX[1].play();
                game.time.events.add(Phaser.Timer.SECOND * this.failureSFX[1].totalDuration + Phaser.Timer.SECOND * 0.25, this.returnToGame, this);
                break;
            case 3:
                this.starArray[2].alpha = 0;
                this.gameState = 'playerLose';
                this.coinsEarned = 0;
                this.ohCrowd.play();
                this.loadLoseMenu();
                break;
        }
    },

    loadNextScene : function(){
        this.setScene();
        this.roundStart = false;
    },

    finishLoadNextScene : function(){
        this.overlay.alpha = 0;
        this.roundStart = true;
    },

    returnToGame: function(){
        this.setScene();
        this.roundStart = true;
        this.overlay.alpha = 0.0;
    },

    checkScene: function(){
        var scene = parseInt(this.currentScene.toString()[0],10);
        var settings = sceneData[this.levelSettings[this.scenesCorrect].level]['id'+this.levelSettings[this.scenesCorrect].id]['required'];
        var currentSetting;
        var isCorrect;
        for(var i = 0; i < settings.length; i++){
            currentSetting = settings[i];
            isCorrect = true;
            switch(scene){
                case 1:
                    if(currentSetting.redCap){
                        if(this.checkSprite(currentSetting,this.scene1Sprites, 'redCap', scene) === false){
                            isCorrect = false;
                        }
                    }
                    if(currentSetting.greenCap){
                        if(this.checkSprite(currentSetting,this.scene1Sprites, 'greenCap', scene) === false){
                            isCorrect = false;
                        }
                    }
                    if(currentSetting.blueCap){
                        if(this.checkSprite(currentSetting,this.scene1Sprites, 'blueCap', scene) === false){
                            isCorrect = false;
                        }
                    }
                    if(currentSetting.door){
                        if(this.checkSprite(currentSetting,this.scene1Sprites, 'door', scene) === false){
                            isCorrect = false;
                        }
                    }
                    if(currentSetting.clock){
                        if(this.clock.alpha === 0){
                            isCorrect = false;
                        }
                    }
                    if(currentSetting.flower){
                        if(this.flower.alpha === 0 || this.flower.key.indexOf(currentSetting.flower) === -1){
                            isCorrect = false;
                        }
                    }
                    break;
                case 2:
                    if(currentSetting.redLeaf){
                        if(this.checkSprite(currentSetting,this.scene2Sprites, 'redLeaf', scene) === false){
                            isCorrect = false;
                        }
                    }
                    if(currentSetting.greenLeaf){
                        if(this.checkSprite(currentSetting,this.scene2Sprites, 'greenLeaf', scene) === false){
                            isCorrect = false;
                        }
                    }
                    if(currentSetting.yellowLeaf){
                        if(this.checkSprite(currentSetting,this.scene2Sprites, 'yellowLeaf', scene) === false){
                            isCorrect = false;
                        }
                    }
                    if(currentSetting.ground0){
                        if(this.checkSprite(currentSetting,this.scene2Sprites, 'ground0', scene) === false){
                            isCorrect = false;
                        }
                    }
                    if(currentSetting.ground1){
                        if(this.checkSprite(currentSetting,this.scene2Sprites, 'ground1', scene) === false){
                            isCorrect = false;
                        }
                    }
                    if(currentSetting.ground2){
                        if(this.checkSprite(currentSetting,this.scene2Sprites, 'ground2', scene) === false){
                            isCorrect = false;
                        }
                    }
                    if(currentSetting.float0){
                        if(this.checkSprite(currentSetting,this.scene2Sprites, 'float0', scene) === false){
                            isCorrect = false;
                        }
                    }
                    if(currentSetting.float1){
                        if(this.checkSprite(currentSetting,this.scene2Sprites, 'float1', scene) === false){
                            isCorrect = false;
                        }
                    }
                    if(currentSetting.float2){
                        if(this.checkSprite(currentSetting,this.scene2Sprites, 'float2', scene) === false){
                            isCorrect = false;
                        }
                    }
                    if(currentSetting.float3){
                        if(this.checkSprite(currentSetting,this.scene2Sprites, 'float3', scene) === false){
                            isCorrect = false;
                        }
                    }
                    if(currentSetting.boat){
                        if(this.boat.alpha === 0){
                            isCorrect = false;
                        }
                    }
                    if(currentSetting.mushroom){
                        if(this.mushroom.alpha === 0 || this.mushroom.key.indexOf(this.capitaliseFirstLetter(currentSetting.mushroom)) === -1){
                            isCorrect = false;
                        }
                    }
                    break;
                case 3:
                    if(currentSetting.redBlock){
                        if(this.checkSprite(currentSetting,this.scene3Sprites, 'redBlock', scene) === false){
                            isCorrect = false;
                        }
                    }
                    if(currentSetting.blueBlock){
                        if(this.checkSprite(currentSetting,this.scene3Sprites, 'blueBlock', scene) === false){
                            isCorrect = false;
                        }
                    }
                    if(currentSetting.greenBlock){
                        if(this.checkSprite(currentSetting,this.scene3Sprites, 'greenBlock', scene) === false){
                            isCorrect = false;
                        }
                    }
                    if(currentSetting.character0){
                        if(this.checkSprite(currentSetting,this.scene3Sprites, 'character0', scene) === false){
                            isCorrect = false;
                        }
                    }
                    if(currentSetting.lights){
                        if(this.lights.alpha === 0){
                            isCorrect = false;
                        }
                    }
                    if(currentSetting.crayon){
                        if(this.crayon.alpha === 0 || this.crayon.key.indexOf(currentSetting.crayon) === -1){
                            isCorrect = false;
                        }
                    }
                    break;
            }
            if(currentSetting.sky){
                if(currentSetting.sky !== this.dayNightState){
                    isCorrect = false;
                }
            }
            if(isCorrect){
                break;
            }
        }
        if(isCorrect){
            return true;
        }else{
            return false;
        }
        
    },

    checkSprite: function(settings, sprites, location, scene){
        var textureData, sprite;
        sprite = sprites[location];
        if(settings[location+'Holding']){
            textureData = settings[location] + this.capitaliseFirstLetter(settings[location+'Holding']+'_'+scene);
        }else{
            textureData = settings[location] + 'Empty_'+scene;
        }
        if(textureData === sprite.key){
            return true;
        }else{
            return false;
        }
    },

    setInitSprite: function(settings, sName, sprite, scene){
        var textureData, sData;
        if(settings.init[sName+'Holding']){
            textureData = settings.init[sName]+this.capitaliseFirstLetter(settings.init[sName+'Holding'])+'_'+scene;
        }else{
            textureData = settings.init[sName]+'Empty_'+scene;
        }
        console.log('scene'+this.currentScene);
        console.log(sName);
        sData = RampsToReading.UI.spritePlacement['scene'+this.currentScene][sName][textureData];
        sprite.loadTexture(textureData);
        if(this.checkInitBug(settings.init[sName])){
            sprite.contains = settings.init[sName];
        }else{
            sprite.contains = 'item';
        }
        sprite.alpha = 1;
        sprite.x = sData.x;
        sprite.y = sData.y;
        sprite.zI = sData.zI;
        sprite.scale.x = sData.scale;
        if(sData.angle){
            sprite.angle = sData.angle;
        }else{
            sprite.angle = 0;
        }
        sprite.animations.add('play',null,31,true);
        sprite.animations.play('play');
    },

    setBug: function(sprite, bug, scene, location){
        if(sprite.key.indexOf(bug) === -1||sprite.alpha === 0){
            var textureName = bug + 'Empty_'+scene;
            var sData = RampsToReading.UI.spritePlacement['scene'+this.currentScene][location][textureName];
            sprite.loadTexture(textureName);
            sprite.alpha = 1;
            sprite.x = sData.x;
            sprite.y = sData.y;
            sprite.zI = sData.zI;
            sprite.contains = bug;
            sprite.scale.x = sData.scale;
            if(sData.angle){
                sprite.angle = sData.angle;
            }else{
                sprite.angle = 0;
            }
            sprite.animations.add('play',null,31,true);
            sprite.animations.play('play');
            this.backgroundLayer.sort('zI');
        }
    },

    setItem: function(sprite, item, scene, location){
        var textureName,sData, setItem;
        
        if(sprite.alpha !== 0 && (sprite.contains !== 'item') ){
            textureName =  sprite.contains + this.capitaliseFirstLetter(item)+'_'+scene;
            sData = RampsToReading.UI.spritePlacement['scene'+this.currentScene][location][textureName];
            setItem = true;
        }else if (sprite.alpha === 0 || sprite.key.indexOf(item) === -1){
            textureName = item + 'Empty_'+scene;
            sData = RampsToReading.UI.spritePlacement['scene'+this.currentScene][location][textureName];
            sprite.contains = 'item';
            setItem = true;
        }
        if(setItem === true){
            sprite.loadTexture(textureName);
            sprite.alpha = 1;
            sprite.x = sData.x;
            sprite.y = sData.y;
            sprite.zI = sData.zI;
            sprite.scale.x = sData.scale;
            if(sData.angle){
                sprite.angle = sData.angle;
            }else{
                sprite.angle = 0;
            }
            sprite.animations.add('play',null,31,true);
            sprite.animations.play('play');
            this.backgroundLayer.sort('zI');
        }
        
    },

    /*MENU*/
    menuButtonClick: function (button) {
        //console.log(this.gameState);
        switch(this.gameState){
            case 'levelMenu':
                this.gameState = 'howToMenu';
                this.menuButton.animations.play('nextIdle');
                this.menuScreen.loadTexture('instructions');
                /*temp until we actually get some menu things in*/
                //this.loadMenu(this.currentLevel, true);
                break;
            case 'howToMenu':
                //this.menuScreen.alpha = 0;
                if(!this.roundStart){
                    this.roundStart = true;
                    this.menuScreen.alpha = 0.0;
                    this.menuScreen.inputEnabled = false;
                    //console.log('here');
                    if(this.overlay.alpha !== 1.0){
                        //console.log('here');
                        this.menuButton.animations.play('helpIdle');
                        this.gameState = 'roundPlaying';
                    }
                }
                break;
            case 'roundPlaying':
                if(!this.game.paused){
                    this.game.sound.pauseAll();
                    this.game.time.events.pause();
                    this.game.paused = true;
                    this.loadMenu(this.currentLevel, true);
                    this.menuButton.animations.play('nextIdle');
                    this.game.input.onDown.add(
                        function () {
                            if(this.game.paused){
                                this.game.currentActiveState.unPause();
                            }
                        },this);
                }
                break;
            case 'nextLevel':
                this.game.currentActiveState.nextLevel();
                break;
            case 'playerLose':
                this.game.currentActiveState.levelReset();
                break;
        }
    },

    menuButtonOver: function (button) {
        if(game.device.desktop){
            this.buttonOverFX.play();
        }
        this.buttonOverBool = true;
        switch(this.gameState){
            case 'levelMenu':
                this.menuButton.animations.play('nextHighlight');
                break;
            case 'howToMenu':
                this.menuButton.animations.play('playHighlight');
                break;
            case 'roundPlaying':
                this.menuButton.animations.play('helpHighlight');
                break;
            case 'nextLevel':
                this.menuButton.animations.play('nextHighlight');
                break;
            case 'playerLose':
                this.menuButton.animations.play('playHighlight');
                break;
        }
    },

    menuButtonOut: function (button) {
        this.buttonOverBool = false;
        switch(this.gameState){
            case 'levelMenu':
                this.menuButton.animations.play('nextIdle');
                break;
            case 'howToMenu':
                this.menuButton.animations.play('nextIdle');
                break;
            case 'roundPlaying':
                this.menuButton.animations.play('helpIdle');
                break;
            case 'nextLevel':
                this.menuButton.animations.play('nextIdle');
                break;
            case 'playerLose':
                this.menuButton.animations.play('nextIdle');
        }
    },

    unPause: function (){

        this.game.sound.resumeAll();
        this.game.paused = false;
        this.game.time.events.resume();
        this.game.input.onDown.removeAll();
        this.menuScreen.alpha = 0.0;
        this.menuButton.animations.play('helpIdle');
    },

    loadMenu: function (level, instruction){
        this.menuScreen.alpha = 1.0;

        if(!instruction){
            this.menuScreen.loadTexture('menuLevel'+this.currentLevel);
        }else{
            this.menuScreen.loadTexture('instructions');
        }
    },

    loadLoseMenu: function (){
        this.menuScreen.alpha = 1.0;
        this.menuScreen.loadTexture('menuGameOver');
        this.menuScreen.inputEnabled = true;
        this.menuButton.animations.play('nextIdle');
    },

    levelReset: function(){
        this.starArray[0].alpha = 1;
        this.starArray[1].alpha = 1;
        this.starArray[2].alpha = 1;
        this.wrongAnswers = 0;
        this.initLevel();
        this.setScene();
        this.coinsEarned = this.coinsEarnedLast;
        this.loadMenu(this.currentLevel, false);
        this.menuScreen.inputEnabled = true;
        this.menuScreen.events.onInputUp.add(game.currentActiveState.menuScreenClick, this);
        this.gameState = 'levelMenu';
    },

    nextLevel: function(){
        this.currentLevel++;
        if(this.currentLevel > 6) this.currentLevel = 1;
        this.initLevel();
        this.setScene();
        this.levelText.text = ' Level '+this.currentLevel+' ';
        this.loadMenu(this.currentLevel, false);
        this.menuScreen.inputEnabled = true;
        this.menuScreen.events.onInputUp.add(game.currentActiveState.menuScreenClick, this);
        this.gameState = 'levelMenu';
    },

    /*MISC*/
    calculateTimeFromSpeedAndDistance: function(x1, y1, x2, y2, speed){
        var dist = Math.sqrt(Math.pow(x1-x2,2)+Math.pow(y1-y2,2));
        return (dist/speed)*1000;
    },

    calculateSpeedFromTimeAndDistance: function(x1, y1, x2, y2, time){
        var dist = Math.sqrt(Math.pow(x1-x2,2)+Math.pow(y1-y2,2));
        return (dist/time);
    },

    getInternetExplorerVersion: function () {
        if(game.device.ie){
            return 1;
        }else{
            return -1;
        }
    },

    randomPercentChance: function (percent){
        if(game.rnd.integerInRange(1,100) < percent){
            return true;
        }else{
            return false;
        }
    },

    tweenClear: function (name) {
        var removalArr = [];
        var i;
        for(i = 0; i < this.tweenArray.length; i++){
            if(this.tweenArray[i].name === name){
                this.tweenArray[i].stop();
                removalArr.push(i);
            }
        }
        for(i = 0; i < removalArr.length; i++){
            this.tweenArray.splice(removalArr[i], 1);
        }
    },

    capitaliseFirstLetter: function(string){
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

};
