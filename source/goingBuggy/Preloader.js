
GoingBuggy.Preloader = function (game) {
	this.background = null;
	this.preloadBar = null;
	this.ready = false;
};

GoingBuggy.Preloader.prototype = {

	preload: function () {
	//	These are the assets we loaded in Boot.js
	//	A nice sparkly background and a loading progress bar
	this.background = this.add.sprite(0, 0, 'preloaderBackground');
	this.preloadBar = this.add.sprite(0, 0, 'preloaderBar');
	this.transition = this.add.sprite(0,0, 'transition');
	this.transition.alpha = 0.0;
	this.hand = this.add.sprite(0,0, 'mobileHand');
        this.hand.anchor.setTo(0.5);
        this.hand.x = game.world.width/2;
        this.hand.y = game.world.height/2+150;
        this.hand.alpha = 0.0;
        this.baseLayer  = game.add.group();
        this.upperLayer = game.add.group();
        this.baseLayer.add(this.background);
        this.baseLayer.add(this.preloadBar);
        this.upperLayer.add(this.hand);
        this.upperLayer.add(this.transition);

        game.load.script('UI','games/goingbuggy/game/modules/UI.js');
        game.load.script('sceneData','games/goingbuggy/game/modules/sceneData.js');
	//	This sets the preloadBar sprite as a loader sprite.
	//	What that does is automatically crop the sprite from 0 to full-width
	//	as the files below are loaded in.
	this.load.setPreloadSprite(this.preloadBar);

	//	Here we load the rest of the assets our game needs.
        game.load.image('transition','games/goingbuggy/media/img/bg/transition.png');
        game.load.image('overlay','games/goingbuggy/media/img/menu/overlay.png');
        game.load.image('toolbar','games/goingbuggy/media/img/toolbar/toolbar_back.png');
        game.load.image('star','games/goingbuggy/media/img/menu/star.png');
        game.load.image('placeholder','games/goingbuggy/media/img/sprites/shared/placeholder.png');

        game.load.spritesheet('menuButton','games/goingbuggy/media/img/menu/button_spritesheet.png', 94, 87, 5);
        game.load.spritesheet('replayButton','games/goingbuggy/media/img/toolbar/replay_button_102x51.png',102,51,3);
        game.load.spritesheet('doneButton','games/goingbuggy/media/img/toolbar/done_button_90x93.png',90,93,3);
        game.load.spritesheet('clearButton','games/goingbuggy/media/img/toolbar/clear_button_93x94.png',93,94,3);

        game.load.image('menuLevel1','games/goingbuggy/media/img/menu/menu_text/level1.png');
        game.load.image('menuLevel2','games/goingbuggy/media/img/menu/menu_text/level2.png');
        game.load.image('menuLevel3','games/goingbuggy/media/img/menu/menu_text/level3.png');
        game.load.image('menuLevel4','games/goingbuggy/media/img/menu/menu_text/level4.png');
        game.load.image('menuLevel5','games/goingbuggy/media/img/menu/menu_text/level5.png');
        game.load.image('menuLevel6','games/goingbuggy/media/img/menu/menu_text/level6.png');
        game.load.image('menuLevelComplete', 'games/goingbuggy/media/img/menu/menu_text/levelComplete.png');
        game.load.image('menuGameOver', 'games/goingbuggy/media/img/menu/menu_text/gameOver.png');
        game.load.image('instructions', 'games/goingbuggy/media/img/menu/instructions.png');


        game.load.spritesheet('beachballButton','games/goingbuggy/media/img/toolbar/beachball_43x44.png',43,44,3);
        game.load.spritesheet('beeButton','games/goingbuggy/media/img/toolbar/bee_66x63.png',66,63,3);
        game.load.spritesheet('boatButton','games/goingbuggy/media/img/toolbar/boat_50x55.png',50,55,3);
        game.load.spritesheet('clockButton','games/goingbuggy/media/img/toolbar/clock_55x77.png',55,77,3);
        game.load.spritesheet('cocoaButton','games/goingbuggy/media/img/toolbar/cocoa_48x43.png',48,43,3);
        game.load.spritesheet('cookieButton','games/goingbuggy/media/img/toolbar/cookie_58x55.png',58,55,3);
        game.load.spritesheet('cupcakeButton','games/goingbuggy/media/img/toolbar/cupcake_51x54.png',51,54,3);
        game.load.spritesheet('dayButton','games/goingbuggy/media/img/toolbar/day_button_73x73.png',73,73,3);
        
        game.load.spritesheet('drumsetButton','games/goingbuggy/media/img/toolbar/drums_62x45.png',62,45,3);
        game.load.spritesheet('fireButton','games/goingbuggy/media/img/toolbar/fire_58x44.png',58,44,3);
        game.load.spritesheet('guitarButton','games/goingbuggy/media/img/toolbar/guitar_28x61.png',28,61,3);
        game.load.spritesheet('gumballButton','games/goingbuggy/media/img/toolbar/gumball_42x71.png',42,71,3);
        game.load.spritesheet('keyboardButton','games/goingbuggy/media/img/toolbar/keyboard_65x47.png',65,47,3);
        game.load.spritesheet('ladybugButton','games/goingbuggy/media/img/toolbar/ladybug_69x44.png',69,44,3);
        game.load.spritesheet('lightsButton','games/goingbuggy/media/img/toolbar/lights_40x45.png',40,45,3);
        game.load.spritesheet('nightButton','games/goingbuggy/media/img/toolbar/night_button_73x73.png',73,73,3);
        game.load.spritesheet('radioButton','games/goingbuggy/media/img/toolbar/radio_57x64.png',57,64,3);
        
        game.load.spritesheet('spiderButton','games/goingbuggy/media/img/toolbar/spider_130x54.png',130,54,3);
        game.load.spritesheet('sunglassesButton','games/goingbuggy/media/img/toolbar/sunglasses_71x23.png',71,23,3);
        game.load.spritesheet('hornButton','games/goingbuggy/media/img/toolbar/trumpet_50x29.png',50,29,3);
        game.load.spritesheet('wormButton','games/goingbuggy/media/img/toolbar/worm_90x32.png',90,32,3);

        game.load.spritesheet('greenCrayonButton','games/goingbuggy/media/img/toolbar/crayons/green_55x54.png',55,54,3);
        game.load.spritesheet('orangeCrayonButton','games/goingbuggy/media/img/toolbar/crayons/orange_55x54.png',55,54,3);
        game.load.spritesheet('purpleCrayonButton','games/goingbuggy/media/img/toolbar/crayons/purple_54x54.png',54,54,3);

        game.load.spritesheet('blueFlowersButton','games/goingbuggy/media/img/toolbar/flowers/blue_52x52.png',52,52,3);
        game.load.spritesheet('purpleFlowersButton','games/goingbuggy/media/img/toolbar/flowers/purple_52x52.png',52,52,3);
        game.load.spritesheet('redFlowersButton','games/goingbuggy/media/img/toolbar/flowers/red_52x52.png',52,52,3);

        game.load.spritesheet('blueMushroomButton','games/goingbuggy/media/img/toolbar/mushrooms/blue_56x60.png',56,60,3);
        game.load.spritesheet('orangeMushroomButton','games/goingbuggy/media/img/toolbar/mushrooms/orange_56x60.png',56,60,3);
        game.load.spritesheet('redMushroomButton','games/goingbuggy/media/img/toolbar/mushrooms/red_56x60.png',56,60,3);


        game.load.atlasXML('successBanner','games/goingbuggy/media/img/toolbar/successBanner.png','games/goingbuggy/media/img/toolbar/successBanner.xml');
        game.load.spritesheet('placementHotBox','games/goingbuggy/media/img/sprites/shared/placementHotBox_66x66.png',66,66,21);

        /*put menu/instructions stuff here*/
        game.load.spritesheet('coin', 'games/goingbuggy/media/img/sprites/coin/spinning_coin_gold.png', 32, 32, 8);
        game.load.image('coinProgressBase', 'games/goingbuggy/media/img/sprites/coin/coinProgressBarBase.png');
        game.load.image('coinProgressFill', 'games/goingbuggy/media/img/sprites/coin/coinProgressBarFill.png');
        game.load.image('coinProgressTab', 'games/goingbuggy/media/img/sprites/coin/coinProgressTab.png');
        game.load.image('coinMenu', 'games/goingbuggy/media/img/sprites/coin/coin.png');

        game.load.image('beachballCursor','games/goingbuggy/media/img/menu/cursor/beachball.png');
        game.load.image('beeCursor', 'games/goingbuggy/media/img/menu/cursor/bee.png');
        game.load.image('cocoaCursor', 'games/goingbuggy/media/img/menu/cursor/cocoa.png');
        game.load.image('cookieCursor', 'games/goingbuggy/media/img/menu/cursor/cookie.png');
        game.load.image('cupcakeCursor', 'games/goingbuggy/media/img/menu/cursor/cupcake.png');
        game.load.image('drumsCursor', 'games/goingbuggy/media/img/menu/cursor/drum.png');
        game.load.image('fireCursor', 'games/goingbuggy/media/img/menu/cursor/fire.png');
        game.load.image('guitarCursor', 'games/goingbuggy/media/img/menu/cursor/guitar.png');
        game.load.image('keyboardCursor', 'games/goingbuggy/media/img/menu/cursor/keyboard.png');
        game.load.image('ladybugCursor','games/goingbuggy/media/img/menu/cursor/ladybug.png');
        game.load.image('radioCursor', 'games/goingbuggy/media/img/menu/cursor/radio.png');
        game.load.image('spiderCursor', 'games/goingbuggy/media/img/menu/cursor/spider.png');
        game.load.image('sunglassesCursor', 'games/goingbuggy/media/img/menu/cursor/sunglasses.png');
        game.load.image('hornCursor', 'games/goingbuggy/media/img/menu/cursor/trumpet.png');
        game.load.image('wormCursor', 'games/goingbuggy/media/img/menu/cursor/worm.png');

        game.load.image('boatStaticCursor','games/goingbuggy/media/img/menu/cursor/static/boat.png');
        game.load.image('clockStaticCursor','games/goingbuggy/media/img/menu/cursor/static/clock.png');
        game.load.image('gumballStaticCursor','games/goingbuggy/media/img/menu/cursor/static/gumball.png');
        game.load.image('lightsStaticCursor','games/goingbuggy/media/img/menu/cursor/static/lights.png');

        game.load.image('dayStaticCursor','games/goingbuggy/media/img/menu/cursor/static/day.png');
        game.load.image('nightStaticCursor','games/goingbuggy/media/img/menu/cursor/static/night.png');

        game.load.image('blueFlowersStaticCursor','games/goingbuggy/media/img/menu/cursor/static/blue_flower.png');
        game.load.image('purpleFlowersStaticCursor','games/goingbuggy/media/img/menu/cursor/static/purple_flower.png');
        game.load.image('redFlowersStaticCursor','games/goingbuggy/media/img/menu/cursor/static/red_flower.png');

        game.load.image('blueMushroomStaticCursor','games/goingbuggy/media/img/menu/cursor/static/blue_mushroom.png');
        game.load.image('orangeMushroomStaticCursor','games/goingbuggy/media/img/menu/cursor/static/orange_mushroom.png');
        game.load.image('redMushroomStaticCursor','games/goingbuggy/media/img/menu/cursor/static/red_mushroom.png');
        
        game.load.image('greenCrayonStaticCursor','games/goingbuggy/media/img/menu/cursor/static/green_crayon.png');
        game.load.image('orangeCrayonStaticCursor','games/goingbuggy/media/img/menu/cursor/static/orange_crayon.png');
        game.load.image('purpleCrayonStaticCursor','games/goingbuggy/media/img/menu/cursor/static/purple_crayon.png');

        /*day/night states*/
        game.load.image('neutral', 'games/goingbuggy/media/img/sprites/shared/gray_gradient_570x382.png');
        game.load.image('day','games/goingbuggy/media/img/sprites/shared/day.png');
        game.load.image('night', 'games/goingbuggy/media/img/sprites/shared/night.png');
        game.load.atlasXML('moon', 'games/goingbuggy/media/img/sprites/shared/moon/moon.png','games/goingbuggy/media/img/sprites/shared/moon/moon.xml');
        game.load.atlasXML('sun', 'games/goingbuggy/media/img/sprites/shared/sun/sun.png','games/goingbuggy/media/img/sprites/shared/sun/sun.xml');
        game.load.atlasXML('cloud1', 'games/goingbuggy/media/img/sprites/shared/clouds/cloud1.png','games/goingbuggy/media/img/sprites/shared/clouds/cloud1.xml');
        game.load.atlasXML('cloud2', 'games/goingbuggy/media/img/sprites/shared/clouds/cloud2.png','games/goingbuggy/media/img/sprites/shared/clouds/cloud2.xml');
        game.load.atlasXML('cloud3', 'games/goingbuggy/media/img/sprites/shared/clouds/cloud3.png','games/goingbuggy/media/img/sprites/shared/clouds/cloud3.xml');

        /*scene 1*/
        game.load.image('scene1_1','games/goingbuggy/media/img/sprites/scene1/background1.png');
        game.load.image('scene1_2','games/goingbuggy/media/img/sprites/scene1/background2.png');
        game.load.image('scene1_3','games/goingbuggy/media/img/sprites/scene1/background3.png');
        game.load.image('scene1_4','games/goingbuggy/media/img/sprites/scene1/background4.png');

        game.load.image('blueCap','games/goingbuggy/media/img/sprites/scene1/bottlecaps/blue.png');
        game.load.image('greenCap','games/goingbuggy/media/img/sprites/scene1/bottlecaps/green.png');
        game.load.image('redCap','games/goingbuggy/media/img/sprites/scene1/bottlecaps/red.png');

        game.load.atlasXML('beeEmpty_1','games/goingbuggy/media/img/sprites/scene1/bee/bee_empty.png','games/goingbuggy/media/img/sprites/scene1/bee/bee_empty.xml');
        game.load.atlasXML('beeCocoa_1','games/goingbuggy/media/img/sprites/scene1/bee/bee_cocoa.png','games/goingbuggy/media/img/sprites/scene1/bee/bee_cocoa.xml');
        game.load.atlasXML('beeCookie_1','games/goingbuggy/media/img/sprites/scene1/bee/bee_cookie.png','games/goingbuggy/media/img/sprites/scene1/bee/bee_cookie.xml');
        game.load.atlasXML('beeCupcake_1','games/goingbuggy/media/img/sprites/scene1/bee/bee_cupcake.png','games/goingbuggy/media/img/sprites/scene1/bee/bee_cupcake.xml');
        game.load.atlasXML('clock_1','games/goingbuggy/media/img/sprites/scene1/clock/clock.png','games/goingbuggy/media/img/sprites/scene1/clock/clock.xml');
        game.load.atlasXML('cocoaEmpty_1','games/goingbuggy/media/img/sprites/scene1/cocoa/cocoa.png','games/goingbuggy/media/img/sprites/scene1/cocoa/cocoa.xml');
        game.load.atlasXML('cookieEmpty_1','games/goingbuggy/media/img/sprites/scene1/cookie/cookie.png','games/goingbuggy/media/img/sprites/scene1/cookie/cookie.xml');
        game.load.atlasXML('cupcakeEmpty_1','games/goingbuggy/media/img/sprites/scene1/cupcake/cupcake.png','games/goingbuggy/media/img/sprites/scene1/cupcake/cupcake.xml');
        
        game.load.atlasXML('blueFlower_1','games/goingbuggy/media/img/sprites/scene1/flowers/blue.png','games/goingbuggy/media/img/sprites/scene1/flowers/blue.xml');
        game.load.atlasXML('purpleFlower_1','games/goingbuggy/media/img/sprites/scene1/flowers/purple.png','games/goingbuggy/media/img/sprites/scene1/flowers/purple.xml');
        game.load.atlasXML('redFlower_1','games/goingbuggy/media/img/sprites/scene1/flowers/red.png','games/goingbuggy/media/img/sprites/scene1/flowers/red.xml');
        game.load.atlasXML('gumball_1','games/goingbuggy/media/img/sprites/scene1/gumball/gumball.png','games/goingbuggy/media/img/sprites/scene1/gumball/gumball.xml');

        game.load.atlasXML('ladybugCocoa_1','games/goingbuggy/media/img/sprites/scene1/ladybug/ladybug_cocoa.png','games/goingbuggy/media/img/sprites/scene1/ladybug/ladybug_cocoa.xml');
        game.load.atlasXML('ladybugCookie_1','games/goingbuggy/media/img/sprites/scene1/ladybug/ladybug_cookie.png','games/goingbuggy/media/img/sprites/scene1/ladybug/ladybug_cookie.xml');
        game.load.atlasXML('ladybugCupcake_1','games/goingbuggy/media/img/sprites/scene1/ladybug/ladybug_cupcake.png','games/goingbuggy/media/img/sprites/scene1/ladybug/ladybug_cupcake.xml');
        game.load.atlasXML('ladybugEmpty_1','games/goingbuggy/media/img/sprites/scene1/ladybug/ladybug_empty.png','games/goingbuggy/media/img/sprites/scene1/ladybug/ladybug_empty.xml');

        game.load.atlasXML('spiderCocoa_1','games/goingbuggy/media/img/sprites/scene1/spider/spider_cocoa.png','games/goingbuggy/media/img/sprites/scene1/spider/spider_cocoa.xml');
        game.load.atlasXML('spiderCookie_1','games/goingbuggy/media/img/sprites/scene1/spider/spider_cookie.png','games/goingbuggy/media/img/sprites/scene1/spider/spider_cookie.xml');
        game.load.atlasXML('spiderCupcake_1','games/goingbuggy/media/img/sprites/scene1/spider/spider_cupcake.png','games/goingbuggy/media/img/sprites/scene1/spider/spider_cupcake.xml');
        game.load.atlasXML('spiderEmpty_1','games/goingbuggy/media/img/sprites/scene1/spider/spider_empty.png','games/goingbuggy/media/img/sprites/scene1/spider/spider_empty.xml');

        game.load.atlasXML('wormCocoa_1','games/goingbuggy/media/img/sprites/scene1/worm/worm_cocoa.png','games/goingbuggy/media/img/sprites/scene1/worm/worm_cocoa.xml');
        game.load.atlasXML('wormCookie_1','games/goingbuggy/media/img/sprites/scene1/worm/worm_cookie.png','games/goingbuggy/media/img/sprites/scene1/worm/worm_cookie.xml');
        game.load.atlasXML('wormCupcake_1','games/goingbuggy/media/img/sprites/scene1/worm/worm_cupcake.png','games/goingbuggy/media/img/sprites/scene1/worm/worm_cupcake.xml');
        game.load.atlasXML('wormEmpty_1','games/goingbuggy/media/img/sprites/scene1/worm/worm_empty.png','games/goingbuggy/media/img/sprites/scene1/worm/worm_empty.xml');

        /*scene 2*/
        game.load.image('scene2_1','games/goingbuggy/media/img/sprites/scene2/background1.png');
        game.load.image('scene2_2','games/goingbuggy/media/img/sprites/scene2/background2.png');
        game.load.image('scene2_3','games/goingbuggy/media/img/sprites/scene2/background3.png');
        game.load.image('scene2_4','games/goingbuggy/media/img/sprites/scene2/background4.png');

        game.load.atlasXML('beachballEmpty_2','games/goingbuggy/media/img/sprites/scene2/beachball/beachball.png','games/goingbuggy/media/img/sprites/scene2/beachball/beachball.xml');
        game.load.atlasXML('beeBeachball_2','games/goingbuggy/media/img/sprites/scene2/bee/bee_beachBall.png','games/goingbuggy/media/img/sprites/scene2/bee/bee_beachBall.xml');
        game.load.atlasXML('beeEmpty_2','games/goingbuggy/media/img/sprites/scene2/bee/bee_empty.png','games/goingbuggy/media/img/sprites/scene2/bee/bee_empty.xml');
        game.load.atlasXML('beeFire_2','games/goingbuggy/media/img/sprites/scene2/bee/bee_fire.png','games/goingbuggy/media/img/sprites/scene2/bee/bee_fire.xml');
        game.load.atlasXML('beeRadio_2','games/goingbuggy/media/img/sprites/scene2/bee/bee_radio.png','games/goingbuggy/media/img/sprites/scene2/bee/bee_radio.xml');
        game.load.atlasXML('beeSunglasses_2','games/goingbuggy/media/img/sprites/scene2/bee/bee_sunglasses.png','games/goingbuggy/media/img/sprites/scene2/bee/bee_sunglasses.xml');

        game.load.atlasXML('boat_2','games/goingbuggy/media/img/sprites/scene2/boat/boat.png','games/goingbuggy/media/img/sprites/scene2/boat/boat.xml');

        game.load.atlasXML('fireEmpty_2','games/goingbuggy/media/img/sprites/scene2/fire/fire.png','games/goingbuggy/media/img/sprites/scene2/fire/fire.xml');

        game.load.atlasXML('ladybugBeachball_2','games/goingbuggy/media/img/sprites/scene2/ladybug/ladybug_beachball.png','games/goingbuggy/media/img/sprites/scene2/ladybug/ladybug_beachball.xml');
        game.load.atlasXML('ladybugEmpty_2','games/goingbuggy/media/img/sprites/scene2/ladybug/ladybug_empty.png','games/goingbuggy/media/img/sprites/scene2/ladybug/ladybug_empty.xml');
        game.load.atlasXML('ladybugFire_2','games/goingbuggy/media/img/sprites/scene2/ladybug/ladybug_fire.png','games/goingbuggy/media/img/sprites/scene2/ladybug/ladybug_fire.xml');
        game.load.atlasXML('ladybugRadio_2','games/goingbuggy/media/img/sprites/scene2/ladybug/ladybug_radio.png','games/goingbuggy/media/img/sprites/scene2/ladybug/ladybug_radio.xml');
        game.load.atlasXML('ladybugSunglasses_2','games/goingbuggy/media/img/sprites/scene2/ladybug/ladybug_sunglasses.png','games/goingbuggy/media/img/sprites/scene2/ladybug/ladybug_sunglasses.xml');

        game.load.atlasXML('mushroomBlue_2','games/goingbuggy/media/img/sprites/scene2/mushrooms/blue.png','games/goingbuggy/media/img/sprites/scene2/mushrooms/blue.xml');
        game.load.atlasXML('mushroomOrange_2','games/goingbuggy/media/img/sprites/scene2/mushrooms/orange.png','games/goingbuggy/media/img/sprites/scene2/mushrooms/orange.xml');
        game.load.atlasXML('mushroomRed_2','games/goingbuggy/media/img/sprites/scene2/mushrooms/red.png','games/goingbuggy/media/img/sprites/scene2/mushrooms/red.xml');

        game.load.atlasXML('pond_2','games/goingbuggy/media/img/sprites/scene2/pond/pond.png','games/goingbuggy/media/img/sprites/scene2/pond/pond.xml');
        game.load.atlasXML('radioEmpty_2','games/goingbuggy/media/img/sprites/scene2/radio/radio.png','games/goingbuggy/media/img/sprites/scene2/radio/radio.xml');

        game.load.atlasXML('sign_2','games/goingbuggy/media/img/sprites/scene2/sign/sign.png','games/goingbuggy/media/img/sprites/scene2/sign/sign.xml');
        game.load.atlasXML('signAlt_2','games/goingbuggy/media/img/sprites/scene2/sign/signAlt.png','games/goingbuggy/media/img/sprites/scene2/sign/signAlt.xml');

        game.load.atlasXML('spiderBeachball_2','games/goingbuggy/media/img/sprites/scene2/spider/spider_beachball.png','games/goingbuggy/media/img/sprites/scene2/spider/spider_beachball.xml');
        game.load.atlasXML('spiderEmpty_2','games/goingbuggy/media/img/sprites/scene2/spider/spider_empty.png','games/goingbuggy/media/img/sprites/scene2/spider/spider_empty.xml');
        game.load.atlasXML('spiderFire_2','games/goingbuggy/media/img/sprites/scene2/spider/spider_fire.png','games/goingbuggy/media/img/sprites/scene2/spider/spider_fire.xml');
        game.load.atlasXML('spiderRadio_2','games/goingbuggy/media/img/sprites/scene2/spider/spider_radio.png','games/goingbuggy/media/img/sprites/scene2/spider/spider_radio.xml');
        game.load.atlasXML('spiderSunglasses_2','games/goingbuggy/media/img/sprites/scene2/spider/spider_sunglasses.png','games/goingbuggy/media/img/sprites/scene2/spider/spider_sunglasses.xml');

        game.load.image('sunglassesEmpty_2','games/goingbuggy/media/img/sprites/scene2/sunglasses/sunglasses.png','games/goingbuggy/media/img/sprites/scene2/sunglasses/sunglasses.xml');

        game.load.atlasXML('wormBeachball_2','games/goingbuggy/media/img/sprites/scene2/worm/worm_beachball.png','games/goingbuggy/media/img/sprites/scene2/worm/worm_beachball.xml');
        game.load.atlasXML('wormEmpty_2','games/goingbuggy/media/img/sprites/scene2/worm/worm_empty.png','games/goingbuggy/media/img/sprites/scene2/worm/worm_empty.xml');
        game.load.atlasXML('wormFire_2','games/goingbuggy/media/img/sprites/scene2/worm/worm_fire.png','games/goingbuggy/media/img/sprites/scene2/worm/worm_fire.xml');
        game.load.atlasXML('wormRadio_2','games/goingbuggy/media/img/sprites/scene2/worm/worm_radio.png','games/goingbuggy/media/img/sprites/scene2/worm/worm_radio.xml');
        game.load.atlasXML('wormSunglasses_2','games/goingbuggy/media/img/sprites/scene2/worm/worm_sunglasses.png','games/goingbuggy/media/img/sprites/scene2/worm/worm_sunglasses.xml');

        game.load.image('grass_2','games/goingbuggy/media/img/sprites/scene2/grass/grass.png');
        game.load.image('rocks_2','games/goingbuggy/media/img/sprites/scene2/rocks/rocks.png');
        /*scene 3*/
        game.load.image('scene3_1','games/goingbuggy/media/img/sprites/scene3/background1.png');
        game.load.image('scene3_2','games/goingbuggy/media/img/sprites/scene3/background2.png');
        game.load.image('scene3_3','games/goingbuggy/media/img/sprites/scene3/background3.png');
        game.load.image('scene3_4','games/goingbuggy/media/img/sprites/scene3/background4.png');

        game.load.atlasXML('beeDrums_3','games/goingbuggy/media/img/sprites/scene3/bee/bee_drums.png','games/goingbuggy/media/img/sprites/scene3/bee/bee_drums.xml');
        game.load.atlasXML('beeEmpty_3','games/goingbuggy/media/img/sprites/scene3/bee/bee_empty.png','games/goingbuggy/media/img/sprites/scene3/bee/bee_empty.xml');
        game.load.atlasXML('beeGuitar_3','games/goingbuggy/media/img/sprites/scene3/bee/bee_guitar.png','games/goingbuggy/media/img/sprites/scene3/bee/bee_guitar.xml');
        game.load.atlasXML('beeHorn_3','games/goingbuggy/media/img/sprites/scene3/bee/bee_horn.png','games/goingbuggy/media/img/sprites/scene3/bee/bee_horn.xml');
        game.load.atlasXML('beeKeyboard_3','games/goingbuggy/media/img/sprites/scene3/bee/bee_keyboard.png','games/goingbuggy/media/img/sprites/scene3/bee/bee_keyboard.xml');

        game.load.atlasXML('blueBlock_3','games/goingbuggy/media/img/sprites/scene3/blocks/blue.png','games/goingbuggy/media/img/sprites/scene3/blocks/blue.xml');
        game.load.atlasXML('greenBlock_3','games/goingbuggy/media/img/sprites/scene3/blocks/green.png','games/goingbuggy/media/img/sprites/scene3/blocks/green.xml');
        game.load.atlasXML('redBlock_3','games/goingbuggy/media/img/sprites/scene3/blocks/red.png','games/goingbuggy/media/img/sprites/scene3/blocks/red.xml');

        game.load.atlasXML('greenCrayon_3','games/goingbuggy/media/img/sprites/scene3/crayon/green.png','games/goingbuggy/media/img/sprites/scene3/crayon/green.xml');
        game.load.atlasXML('orangeCrayon_3','games/goingbuggy/media/img/sprites/scene3/crayon/orange.png','games/goingbuggy/media/img/sprites/scene3/crayon/orange.xml');
        game.load.atlasXML('purpleCrayon_3','games/goingbuggy/media/img/sprites/scene3/crayon/purple.png','games/goingbuggy/media/img/sprites/scene3/crayon/purple.xml');

        game.load.atlasXML('drumsEmpty_3','games/goingbuggy/media/img/sprites/scene3/instruments/drum.png','games/goingbuggy/media/img/sprites/scene3/instruments/drum.xml');
        game.load.atlasXML('guitarEmpty_3','games/goingbuggy/media/img/sprites/scene3/instruments/guitar.png','games/goingbuggy/media/img/sprites/scene3/instruments/guitar.xml');
        game.load.atlasXML('hornEmpty_3','games/goingbuggy/media/img/sprites/scene3/instruments/horn.png','games/goingbuggy/media/img/sprites/scene3/instruments/horn.xml');
        game.load.atlasXML('keyboardEmpty_3','games/goingbuggy/media/img/sprites/scene3/instruments/keyboard.png','games/goingbuggy/media/img/sprites/scene3/instruments/keyboard.xml');

        game.load.atlasXML('ladybugDrums_3','games/goingbuggy/media/img/sprites/scene3/ladybug/ladybug_drums.png','games/goingbuggy/media/img/sprites/scene3/ladybug/ladybug_drums.xml');
        game.load.atlasXML('ladybugEmpty_3','games/goingbuggy/media/img/sprites/scene3/ladybug/ladybug_empty.png','games/goingbuggy/media/img/sprites/scene3/ladybug/ladybug_empty.xml');
        game.load.atlasXML('ladybugGuitar_3','games/goingbuggy/media/img/sprites/scene3/ladybug/ladybug_guitar.png','games/goingbuggy/media/img/sprites/scene3/ladybug/ladybug_guitar.xml');
        game.load.atlasXML('ladybugHorn_3','games/goingbuggy/media/img/sprites/scene3/ladybug/ladybug_horn.png','games/goingbuggy/media/img/sprites/scene3/ladybug/ladybug_horn.xml');
        game.load.atlasXML('ladybugKeyboard_3','games/goingbuggy/media/img/sprites/scene3/ladybug/ladybug_keyboard.png','games/goingbuggy/media/img/sprites/scene3/ladybug/ladybug_keyboard.xml');

        game.load.image('lights_3','games/goingbuggy/media/img/sprites/scene3/lights/lights.png');
        game.load.image('crushPin1_3','games/goingbuggy/media/img/sprites/scene3/crushpin/orange.png');
        game.load.image('crushPin2_3','games/goingbuggy/media/img/sprites/scene3/crushpin/blue.png');

        game.load.atlasXML('spiderDrums_3','games/goingbuggy/media/img/sprites/scene3/spider/spider_drums.png','games/goingbuggy/media/img/sprites/scene3/spider/spider_drums.xml');
        game.load.atlasXML('spiderEmpty_3','games/goingbuggy/media/img/sprites/scene3/spider/spider_empty.png','games/goingbuggy/media/img/sprites/scene3/spider/spider_empty.xml');
        game.load.atlasXML('spiderGuitar_3','games/goingbuggy/media/img/sprites/scene3/spider/spider_guitar.png','games/goingbuggy/media/img/sprites/scene3/spider/spider_guitar.xml');
        game.load.atlasXML('spiderHorn_3','games/goingbuggy/media/img/sprites/scene3/spider/spider_horn.png','games/goingbuggy/media/img/sprites/scene3/spider/spider_horn.xml');
        game.load.atlasXML('spiderKeyboard_3','games/goingbuggy/media/img/sprites/scene3/spider/spider_keyboard.png','games/goingbuggy/media/img/sprites/scene3/spider/spider_keyboard.xml');

        game.load.atlasXML('wormDrums_3','games/goingbuggy/media/img/sprites/scene3/worm/worm_drums.png','games/goingbuggy/media/img/sprites/scene3/worm/worm_drums.xml');
        game.load.atlasXML('wormEmpty_3','games/goingbuggy/media/img/sprites/scene3/worm/worm_empty.png','games/goingbuggy/media/img/sprites/scene3/worm/worm_empty.xml');
        game.load.atlasXML('wormGuitar_3','games/goingbuggy/media/img/sprites/scene3/worm/worm_guitar.png','games/goingbuggy/media/img/sprites/scene3/worm/worm_guitar.xml');
        game.load.atlasXML('wormHorn_3','games/goingbuggy/media/img/sprites/scene3/worm/worm_horn.png','games/goingbuggy/media/img/sprites/scene3/worm/worm_horn.xml');
        game.load.atlasXML('wormKeyboard_3','games/goingbuggy/media/img/sprites/scene3/worm/worm_keyboard.png','games/goingbuggy/media/img/sprites/scene3/worm/worm_keyboard.xml');

        game.load.audio('gameMusic', 'games/goingbuggy/media/sound/music/GB_MusicLoop.mp3');
        game.load.audio('buttonOver', 'games/goingbuggy/media/sound/fx/buttonOver.mp3');
        game.load.audio('altButtonOver', 'games/goingbuggy/media/sound/fx/altButtonOver.mp3');
        game.load.audio('failure1', 'games/goingbuggy/media/sound/voice/failure1.mp3');
        game.load.audio('failure2', 'games/goingbuggy/media/sound/voice/failure2.mp3');
        game.load.audio('yeahCrowd', 'games/goingbuggy/media/sound/fx/Yeah.mp3');
        game.load.audio('ohCrowd', 'games/goingbuggy/media/sound/fx/Ohhh.mp3');
        game.load.audio('wrongAnswer','games/goingbuggy/media/sound/fx/Wrong 5.mp3');

        game.load.audio('success1', 'games/goingbuggy/media/sound/voice/congrats0.mp3');
        game.load.audio('success2', 'games/goingbuggy/media/sound/voice/congrats1.mp3');
        game.load.audio('success3', 'games/goingbuggy/media/sound/voice/congrats2.mp3');
        game.load.audio('success4', 'games/goingbuggy/media/sound/voice/congrats3.mp3');
        game.load.audio('success5', 'games/goingbuggy/media/sound/voice/congrats4.mp3');
        game.load.audio('success6', 'games/goingbuggy/media/sound/voice/congrats5.mp3');
        game.load.audio('success7', 'games/goingbuggy/media/sound/voice/congrats6.mp3');
        game.load.audio('success8', 'games/goingbuggy/media/sound/voice/congrats7.mp3');
        game.load.audio('success9', 'games/goingbuggy/media/sound/voice/congrats8.mp3');
        game.load.audio('success10', 'games/goingbuggy/media/sound/voice/congrats9.mp3');
        game.load.audio('success11', 'games/goingbuggy/media/sound/voice/congrats10.mp3');
        game.load.audio('success12', 'games/goingbuggy/media/sound/voice/congrats11.mp3');
        game.load.audio('success13', 'games/goingbuggy/media/sound/voice/congrats12.mp3');
        game.load.audio('success14', 'games/goingbuggy/media/sound/voice/congrats13.mp3');
        game.load.audio('success15', 'games/goingbuggy/media/sound/voice/congrats14.mp3');
        game.load.audio('success16', 'games/goingbuggy/media/sound/voice/congrats15.mp3');
	},

	create: function () {
		//	Once the load has finished we disable the crop because we're going to sit in the update loop for a short while as the music decodes
		this.preloadBar.cropEnabled = false;
	},

	update: function () {

		/*if we have game music uncomment the commented code*/
	//if (this.cache.isSoundDecoded('gameMusic')){
            if(!this.ready){
                if(game.device.desktop){
                    this.ready = true;
                    this.nextScene();
                }else{
                    this.ready = true;
                    this.preloadBar.destroy();
                    this.background.loadTexture('splash');
                    this.background.enableBody = true;
                    this.background.physicsBodyType = Phaser.Physics.ARCADE;
                    this.background.inputEnabled = true;
                    this.background.events.onInputUp.add(this.nextScene, this);
                    game.add.tween(this.hand).to({alpha:1.0},200, Phaser.Easing.Linear.None, true);
                    game.add.tween(this.hand).to({y:this.hand.y-25},300, Phaser.Easing.Linear.None, true, 0, Number.MAX_VALUE, true);
                }
            }
        //}
    },

    nextScene: function () {
        var tween = this.add.tween(this.transition).to({ alpha: 1.0}, 500, Phaser.Easing.Linear.None,true, 0, 0, false);
        tween.onComplete.add(function(){this.state.start('Game');},this);
    }
};
