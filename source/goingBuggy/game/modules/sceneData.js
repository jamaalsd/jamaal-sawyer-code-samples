sceneData =
{
    level12:{
        id1:{
            scene:12,
            description: 'Bee is by the flowers.\n\nSpider hangs at the green table.\n\n',
            init : {
                blueCap:'bee',
                blueCapHolding:'empty',
                flower:'blue'
            },
            required : [
                {
                    blueCap:'bee',
                    blueCapHolding:'empty',
                    greenCap:'spider',
                    greenCapHolding:'empty',
                    flower:'blue'
                },
                {
                    blueCap:'bee',
                    blueCapHolding:'empty',
                    greenCap:'spider',
                    greenCapHolding:'empty',
                    flower:'purple'
                },
                {
                    blueCap:'bee',
                    blueCapHolding:'empty',
                    greenCap:'spider',
                    greenCapHolding:'empty',
                    flower:'red'
                }
            ]
        },
        
        id2:{
            scene:13,
            description: 'This place has a new clock.\n\nWorm is under the new clock.\n\nLady Bug is at the red table.\n\n',
            init : {
                door:'worm',
                doorHolding:'empty',
                clock: true
            },
            required : [
                {
                    redCap:'ladybug',
                    redCapHolding:'empty',
                    door:'worm',
                    doorHolding:'empty',
                    clock:true
                },
                
            ]
        },
        
        id3:{
            scene:21,
            description: 'Worm watches the boat from the yellow leaf.\n\nLady Bug watches the boat from the green leaf.\n\n',
            init : {
                yellowLeaf:'worm',
                yellowLeafHolding:'empty',
                boat:true
            },
            required : [
                {
                    greenLeaf:'ladybug',
                    greenLeafHolding:'empty',
                    yellowLeaf:'worm',
                    yellowLeafHolding:'empty',
                    boat:true
                }
            ]
        },

        id4:{
            scene:22,
            description: 'Someone left a ball at the beach.\n\nBee sees the ball.\n\nBee is over the sign.\n\nSpider sees the ball.\n\nSpider is on the green leaf.\n\n',
            init : {
                float1:'bee',
                float1Holding:'empty',
                ground3:'beachball',
                ground3Holding:'empty'
            },
            required : [
                {
                    greenLeaf:'spider',
                    greenLeafHolding:'empty',
                    float1:'bee',
                    float1Holding:'empty',
                    ground3:'beachball',
                    ground3Holding:'empty'
                },
                {
                    greenLeaf:'spider',
                    greenLeafHolding:'empty',
                    float2:'bee',
                    float2Holding:'empty',
                    ground2:'beachball',
                    ground2Holding:'empty'
                }
            ]
        },

        id5:{
            scene:23,
            description: 'The boat is sailing.\n\nLady Bug is close to the fire.\n\nShe is having fun.\n\n',
            init : {
                ground1:'ladybug',
                ground1Holding:'fire'
            },
            required : [
                {
                    ground1:'ladybug',
                    ground1Holding:'fire',
                    boat:true
                }
            ]
        },

        id6:{
            scene:24,
            description: 'Worm rests on the yellow leaf.\n\nLady Bug has it made in the shade of the red mushroom.\n\n',
            init : {
                ground1:'ladybug',
                ground1Holding:'empty',
                mushroom: 'red'
            },
            required : [
                {
                    yellowLeaf:'worm',
                    yellowLeafHolding:'empty',
                    ground1:'ladybug',
                    ground1Holding:'empty',
                    mushroom: 'red'
                }
            ]
        },

        id7:{
            scene:31,
            description: 'Bee plays drums on the letter B block.\n\nSpider plays horn on the letter G block.\n\n',
            init : {
                redBlock:'bee',
                redBlockHolding:'drums',
                blueBlock:'spider',
                blueBlockHolding:'empty'
            },
            required : [
                {
                    redBlock:'bee',
                    redBlockHolding:'drums',
                    blueBlock:'spider',
                    blueBlockHolding:'horn'
                }
            ]
        },


        id8:{
            scene:32,
            description:'Bee is near the floor.\n\nSpider is above him.\n\n',
            init : {
                greenBlock:'spider',
                greenBlockHolding:'empty'
            },
            required : [
                {
                    greenBlock:'spider',
                    greenBlockHolding:'empty',
                    character0:'bee',
                    character0Holding:'empty'
                }
            ]
        },

        id9:{
            scene:33,
            description:'Spider is in front of the window.\n\nHe plays guitar.\n\nWorm is on the blue block.\n\n',
            init : {
                greenBlock:'spider',
                greenBlockHolding:'guitar'
            },
            required : [
                {
                    greenBlock:'spider',
                    greenBlockHolding:'guitar',
                    blueBlock:'worm',
                    blueBlockHolding:'empty'
                }
            ]
        }
    },

    level3:{
        id1:{
            scene:11,
            description:'Worm is by the gum.\n\nLady Bug is by the flowers.\n\nWhat will they eat?\n\n',
            init : {
                door:'worm',
                doorHolding:'empty',
                gumball:true
            },
            required : [
                {
                    door:'worm',
                    doorHolding:'empty',
                    gumball:true,
                    blueCap:'ladybug',
                    blueCapHolding:'empty',
                    flower:'blue'
                },
                {
                    door:'worm',
                    doorHolding:'empty',
                    gumball:true,
                    blueCap:'ladybug',
                    blueCapHolding:'empty',
                    flower:'purple'
                },
                {
                    door:'worm',
                    doorHolding:'empty',
                    gumball:true,
                    blueCap:'ladybug',
                    blueCapHolding:'empty',
                    flower:'red'
                }
            ]
        },

        id2:{
            scene:31,
            description:'It is dark.\n\nSpider hangs by the window.\n\nHe likes to play piano at night.\n\nBee is above the red block.\n\nShe does not like the dark.\n\nShe keeps the lights on.\n\n',
            init : {
                redBlock:'bee',
                redBlockHolding:'empty',
                greenBlock:'spider',
                greenBlockHolding:'keyboard',
                lights:true
            },
            required : [
                {
                    redBlock:'bee',
                    redBlockHolding:'empty',
                    greenBlock:'spider',
                    greenBlockHolding:'keyboard',
                    lights:true,
                    sky:'night'
                }
            ]
        },

        id3:{
            scene:21,
            description:'Worm sits under the blue striped mushroom.\n\nLady Bug is by the sign.\n\nFire keeps her warm.\n\n',
            init : {
                ground1:'ladybug',
                ground1Holding:'fire',
                mushroom:'blue'
            },
            required : [
                {
                    greenLeaf: 'worm',
                    greenLeafHolding:'empty',
                    ground1:'ladybug',
                    ground1Holding:'fire',
                    mushroom:'blue'
                }
            ]
        },
        
        id4:{
            scene:12,
            description:'Who is by the gum?\nIt is Worm.\n\nDoes he want gum?\nHe just walked in.\n\nLady Bug is also by the gum.\n\nShe has been here a while.\n\n',
            init : {
                greenCap:'ladybug',
                greenCapHolding:'empty',
                gumball:true
            },
            required : [
                {
                    greenCap:'ladybug',
                    greenCapHolding:'empty',
                    gumball:true,
                    door:'worm',
                    doorHolding:'empty'
                }
            ]
        },

        id5:{
            scene:32,
            description:'Can you fix the band room?\n\nThe band needs a green crayon to make a sign.\n\nThey like that color.They also need lights.\n\nThe instruments go on the blocks, from "G" to "B".\n\nHere is the order:\n\nGuitar, drums, piano.\n\nThe bugs will be here soon.\n\nIs the room ready?\n\n',
            init : {
                redBlock:'keyboard',
                redBlockHolding:'empty',
                greenBlock:'drums',
                greenBlockHolding:'empty',
                lights:true
            },
            required : [
                {
                    redBlock:'keyboard',
                    redBlockHolding:'empty',
                    greenBlock:'drums',
                    greenBlockHolding:'empty',
                    lights:true,
                    crayon:'green'
                }
            ]
        },

        id6:{
            scene:22,
            description:'Worm is under the blue striped mushroom.\n\nLady Bug is by the water.\n\nBee watches them from near the green leaf.\n\n',
            init : {
                greenLeaf:'bee',
                greenLeafHolding:'empty',
                ground1:'worm',
                ground1Holding:'empty',
                ground2:'ladybug',
                ground2Holding:'empty'
            },
            required : [
                {
                    greenLeaf:'bee',
                    greenLeafHolding:'empty',
                    ground1:'worm',
                    ground1Holding:'empty',
                    ground2:'ladybug',
                    ground2Holding:'empty',
                    mushroom:'blue'
                }
            ]
        },

        id7:{
            scene:13,
            description:'Worm eats a cookie at the blue table.\n\nBee eats a cupcake at the red table.\n\nSpider is waiting for his cookie at the green table.\n\n',
            init : {
                blueCap:'worm',
                blueCapHolding:'cookie',
                greenCap:'spider',
                greenCapHolding:'empty',
            },
            required : [
                {
                    blueCap:'worm',
                    blueCapHolding:'cookie',
                    greenCap:'spider',
                    greenCapHolding:'empty',
                    redCap:'bee',
                    redCapHolding:'cupcake'
                }
            ]
        },
    
        id8:{
            scene:33,
            description:'Spider is playing drums in front of the red block.\n\nThe other bugs dance.\n\nWorm is on the blue block.\n\nLady Bug dances on the middle block.\n\nBee dances in front of the window.\n\n',
            init : {
                blueBlock:'worm',
                blueBlockHolding:'empty',
                character0:'spider',
                character0Holding:'drums'
            },
            required : [
                {
                    redBlock:'ladybug',
                    redBlockHolding:'empty',
                    greenBlock:'bee',
                    greenBlockHolding:'empty',
                    blueBlock:'worm',
                    blueBlockHolding:'empty',
                    character0:'spider',
                    character0Holding:'drums'
                }
            ]
        },

        id9:{
            scene:23,
            description:'Bee flew in from between the tall grass.\n\nGuess what she saw?\n\nThe other bugs were having a beach party!\n\nLadybug was dancing to music on the green leaf.\n\nWorm toasted marshmallows on the yellow leaf.\n\n',
            init : {
                greenLeaf:'ladybug',
                greenLeafHolding:'empty',
                yellowLeaf:'worm',
                yellowLeafHolding:'empty',
                float3:'bee',
                float3Holding:'empty'
            },
            required : [
                {
                    greenLeaf:'ladybug',
                    greenLeafHolding:'radio',
                    yellowLeaf:'worm',
                    yellowLeafHolding:'fire',
                    float3:'bee',
                    float3Holding:'empty'
                }
            ]
        },
    },

    level4:{
        id1:{
            scene:11,
            description:'Lady Bug is eating a cupcake at the red table.\n\nShe hopes that Worm joins her.\n\nWorm is eating a cookie at the green table.\n\nHe does not like sharing.\n\nLady Bug does not get to taste the cookie.\n\n',
            init : {
                redCap:'ladybug',
                redCapHolding:'cupcake',
            },
            required : [
                {
                    redCap:'ladybug',
                    redCapHolding:'cupcake',
                    greenCap:'worm',
                    greenCapHolding:'cookie'
                }
            ]
        },
        
        id2:{
            scene:22,
            description:'Bee does not feel the cool night air.\n\nShe is flying over the fire close to the water.\n\nWorm is thinking about moving closer to Bee.\n\nHe is cold underneath the orange mushroom.\n\nHe wishes he brought his favorite red blanket.\n\nNext time, Worm will come to the beach better prepared.\n\n',
            init : {
                ground2:'fire',
                ground2Holding:'empty'
            },
            required:[
                {
                    float2:'bee',
                    float2Holding:'empty',
                    ground1:'worm',
                    ground1Holding:'empty',
                    ground2:'fire',
                    ground2Holding:'empty',
                    mushroom:'orange'
                }
            ]
        },

        id3:{
            scene:12,
            description:'Spider can smell the red flowers from his seat at the blue table.\n\nHe wants to move to a different table.\n\nHe is too shy to ask Worm if they can share the green table.\n\nWorm does not see Spider.\n\nWorm is too busy eating his cookie.\n\n',
            init : {
                blueCap: 'spider',
                blueCapHolding:'empty'
            },
            required:[
                {
                    blueCap:'spider',
                    blueCapHolding:'empty',
                    greenCap:'worm',
                    greenCapHolding:'cookie',
                    flower:'red'
                }
            ]
        },

        id4:{
            scene:24,
            description:'Today is Lady Bug\'s birthday.\n\nBee and Spider are setting up for her party on the beach.\n\nBee is so excited about the party that she cannot sit still.\n\nShe is buzzing with excitement near the blue mushrooms. \n\nSpider is much calmer.\n\nHe is playing with the beach ball next to the water.\n\nLady Bug will not arrive to the party until later.\n\n',
            init : {
                ground0:'beachball',
                ground0Holding:'empty',
                mushroom:'blue'
            },
            required:[
                {
                    float0:'spider',
                    float0Holding:'empty',
                    float1:'bee',
                    float1Holding:'empty',
                    ground0:'beachball',
                    ground0Holding:'empty',
                    mushroom:'blue'
                }
            ]
        },
    
        id5:{
            scene:13,
            description:'Buggy Bakery is much busier after dinnertime.\n\nThey have the best desserts in town!\n\nWorm and Spider come every Friday night to eat cupcakes.\n\nTonight worm is at the green table. Spider is at the table in front of the door.\n\nSpider likes to hang around.\n\nHe can look out the door at the stars.\n\n',
            init : {
                greenCap:'worm',
                greenCapHolding:'cupcake'
            },
            required:[
                {
                    redCap:'spider',
                    redCapHolding:'cupcake',
                    greenCap:'worm',
                    greenCapHolding:'cupcake',
                    sky:'night'
                }
            ]
        },

        id6:{
            scene:32,
            description:'Bee and Spider are practicing their latest hit \'Going Buggy\'.\n\nBee just bought a new guitar to play.\n\nBee loves being on the green block because it is in the center.\n\nSpider does not mind being to the right of Bee, on the left of the stage.\n\nHe is happy rocking out on the drums.\n\n',
            init: {
                greenBlock:'bee',
                greenBlockHolding:'guitar'
            },
            required:[
                {
                    greenBlock:'bee',
                    greenBlockHolding:'guitar',
                    blueBlock:'spider',
                    blueBlockHolding:'drums'
                }
            ]
        },
        
        id7:{
            scene:21,
            description:'The bugs love to come to Buggy Beach.\n\nIt is such a beautiful, sunny day!\n\nLady Bug is dancing by the water.\n\nShe always dances when listening to her favorite music.\n\n Worm looks cool wearing his sunglasses.\n\nHe does not need them under the shade of the red mushrooms.\n\nSilly Worm!\n\n',
            init: {
                mushroom:'red',
                sky:'day'
            },
            required:[
                {
                    greenLeaf:'worm',
                    greenLeafHolding:'sunglasses',
                    ground0:'ladybug',
                    ground0Holding:'radio',
                    mushroom:'red',
                    sky:'day'
                }
            ]
        },

        id8:{
            scene:34,
            description:'Worm and Spider have been playing music all day.\n\nWorm\'s fingers hurt from playing the piano too long.\n\nHe wants to try the drums.\n\nWill Spider let him play the drums?\n\nProbably not.\n\nSpider cannot see Worm playing in front of the red block.\n\nSpider is too far away.\n\nSpider is on top of the green block.\n\n',
            init: {
                greenBlock:'spider',
                greenBlockHolding:'drums'
            },
            required:[
                {
                    character0:'worm',
                    character0Holding:'keyboard'
                }
            ]
        },

        id9:{
            scene:14,
            description:'Lady Bug is sitting at the blue table.\n\nShe is in the middle of eating a delicious cookie.\n\nShe is going to buy a gumball before she leaves.\n\nShe needs to wait until Spider is done.\n\nHe is next to the gumball machine.\n\nSpider loves gum.\n\nHe always chews three pieces at once!\n\n',
            init: {
                greenCap:'spider',
                greenCapHolding:'empty'
            },
            required:[
                {
                    blueCap:'ladybug',
                    blueCapHolding:'cookie',
                    greenCap:'spider',
                    greenCapHolding:'empty',
                    gumball:true
                }
            ]
        },
    },

    level5:{
        id1:{
            scene:34,
            description:'The bugs are going to play at a party. They need to practice.  They had better play softly.  The neighbors are asleep.\n\nBee tries to drum softly.  His drum kit rattles the blue block.\n\nWorm has a hard time with his loud horn.  He is closest to the window.  The neighbors will hear him if he is not quiet.\n\nLady Bug can play softly above the green block.  She will not use a pick.  That way, her guitar will not be loud.',
            init : {
                redBlock:'worm',
                redBlockHolding:'horn'
            },
            required : [
                {
                    redBlock:'worm',
                    redBlockHolding:'horn',
                    greenBlock:'ladybug',
                    greenBlockHolding:'guitar',
                    blueBlock:'bee',
                    blueBlockHolding:'drums'
                }
            ]
        },
        
        id2:{
            scene:23,
            description:'The bugs are having fun.  They are resting after a game of beach ball.\n\nLady Bug is listening to music on the yellow leaf.\n\nHe just tuned in his favorite station.  He hopes it will not be too loud.\n\nWorm does not seem to mind.\n\nHe is happy to sit under the shade of a red mushroom.  He likes the same music as Lady Bug.\n\nBee will say something if it gets too loud.  He is flying over the mushroom.',
            init : {
                greenLeaf:'worm',
                greenLeafHolding:'empty'
            },
            required:[
                {
                    greenLeaf:'worm',
                    greenLeafHolding:'empty',
                    yellowLeaf:'ladybug',
                    yellowLeafHolding:'radio',
                    float0:'bee',
                    float0Holding:'empty',
                    mushroom:'red'
                }
            ]
        },

        id3:{
            scene:14,
            description:'Worm had just enough money to buy hot chocolate.  He is enjoying it at the table by the window.\n\nBee bought a cookie.  Bee can smell the hot chocolate at the next table.\n\nSpider was hovering at the green table.  He had just enough money to buy a cup cake.\n\nBee had an idea.  "What if", she thought, "we shared our treats?  Then we could enjoy three things."\n\nBut Bee was a shy Bee.  She was not brave enough to ask.',
            init : {
                greenCap:'spider',
                greenCapHolding:'empty'
            },
            required:[
                {
                    redCap:'bee',
                    redCapHolding:'cookie',
                    blueCap:'worm',
                    blueCapHolding:'cocoa',
                    greenCap:'spider',
                    greenCapHolding:'cupcake'
                }
            ]
        },

        id4:{
            scene:33,
            description:'Time to jam!\n\nWorm is ready to tickle the ivories at his blue block.  Tickle the ivory means play the piano.  Piano keys used to be made of ivory.\n\nLady Bug is set to shred on her guitar.  Shred is what guitar players call playing fast.  She has been waiting from the top of her green stage.\n\nBee has a problem, though.  Her drums are being repaired.  If she can\'t join in, then she may as well unplug the lights from above the red block and go home.',
            init : {
                greenBlock:'ladybug',
                greenBlockHolding:'empty'
            },
            required:[
                {
                    greenBlock:'ladybug',
                    greenBlockHolding:'guitar',
                    redBlock:'bee',
                    redBlockHolding:'empty',
                    blueBlock:'worm',
                    blueBlockHolding:'keyboard',
                    lights:true
                }
            ]
        },
    
        id5:{
            scene:22,
            description:'Spider and Bee love to camp out under the stars.  Bee flies between the green and red leaf and watches the the boat sail into the night.\n\nThe campfire by the shore gives a lot of light.  Bee wonders if Spider can see the boat from where she is hovering at the green leaf.\n\n"Hey, Spider", called Bee.  "Are you watching the sail boat?"  "No, I cannot see it from here", said Spider.  This seemed odd to Bee.\n\nThe campfire is so bright.  Bee looked to Spider.  Of course Spider could not see the boat.  He is wearing sunglasses!',
            init : {
                greenLeaf:'spider',
                greenLeafHolding:'empty'
            },
            required:[
                {
                    greenLeaf:'spider',
                    greenLeafHolding:'sunglasses',
                    float1:'bee',
                    float1Holding:'empty',
                    ground2:'fire',
                    ground2Holding:'empty',
                    boat:true,
                    sky:'night'
                },
                {
                    greenLeaf:'spider',
                    greenLeafHolding:'sunglasses',
                    float1:'bee',
                    float1Holding:'empty',
                    ground2:'fire',
                    ground2Holding:'empty',
                    boat:true,
                    sky:'night'
                },
            ]
        },

        id6:{
            scene:32,
            description:'The Buggy Band is ready for the big show.\n\nWorm\'s mom made a big orange sign that said, "The Buggy Band."\n\nSpider has been drumming since he was a baby.  He does not mind being behind the other players.\n\nWorm is new at the guitar.  That is O.K.  He still sounds good.  Plus, he is close to the lights under the sign, so he is sure to be seen.\n\nBee gets hot and tired playing her horn.  She sets up by the window.  That way, she can catch a breeze if she needs to cool down.',
            init: {
                blueBlock:'bee',
                blueBlockHolding:'empty'
            },
            required:[
                {
                    redBlock:'worm',
                    redBlockHolding:'guitar',
                    greenBlock:'spider',
                    greenBlockHolding:'drums',
                    blueBlock:'bee',
                    blueBlockHolding:'horn',
                    lights:true,
                    crayon:'orange'
                }
            ]
        },
        
        id7:{
            scene:22,
            description:'"Who tossed my ball near the water?" cried Lady Bug.  Lady Bug was napping under the blue mushroom.  When she woke up, she found her ball nearly in the water.\n\n"I was sleeping on this yellow leaf", answered Worm.  "It was not me."\n\n"I was buzzing over the lake", said Bee.  "I just got here."\n\n"What about you Spider?"  asked Lady Bug.  But Spider was no where to be found.\n\nHe did not mean to throw the ball so close to the water.  When he did, he got scared and ran off.',
            init: {
                yellowLeaf:'worm',
                yellowLeafHolding:'empty'
            },
            required:[
                {
                    yellowLeaf:'worm',
                    yellowLeafHolding:'empty',
                    float2:'bee',
                    float2Holding:'empty',
                    ground1:'ladybug',
                    ground1Holding:'empty',
                    ground2:'beachball',
                    ground2Holding:'empty',
                    mushroom:'blue'
                }
            ]
        },

        id8:{
            scene:14,
            description:'Spider and Bee made a bet.  Who ever guessed how many gum balls were in the gum ball machine would win.\n\nBee bet her cookie she could guess. She waited at the red table.\n\nSpider bet his cup cake.  He put his cupcake next to the gum ball machine. He waited at the table closest to the gumball machine.\n\nThey had only a few minutes to guess, so both kept their eyes on the clock.  There was only one problem. Nobody knew exactly how many gum balls were left in the gum ball machine.',
            init: {
                redCap:'bee',
                redCapHolding:'empty'
            },
            required:[
                {
                    redCap:'bee',
                    redCapHolding:'cookie',
                    greenCap:'spider',
                    greenCapHolding:'empty',
                    door:'cupcake',
                    doorHolding:'empty',
                    gumball:true,
                    clock:true
                }
            ]
        },

        id9:{
            scene:34,
            description:'The Buggy Band is ready to play.  Everything needs to be just right.  First, they have an orange sign.  The sign says, "The Buggy Band."\n\nSpider is set up on top of the green block.  He will play the piano.\n\nBee set up in the middle.  She is ready to drum. \n\nThe last block has Worm and his guitar.\n\nThe lights are on. So what is the problem?  Uh oh.  The blocks are supposed to spell a word.  What word to you think they are supposed to spell?',
            init: {
                redBlock:'worm',
                redBlockHolding:'empty'
            },
            required:[
                {
                    redBlock:'worm',
                    redBlockHolding:'guitar',
                    greenBlock:'spider',
                    greenBlockHolding:'keyboard',
                    blueBlock:'bee',
                    blueBlockHolding:'drums',
                    lights:true,
                    crayon:'orange'
                }
            ]
        },
    },

    level6:{
        id1:{
            scene:31,
            description:'The Bugs formed a trio called "The Buggy Band".  They want to be stars some day.\n\nBee plays the drums.  Drums are usually put in the middle of the band.\n\nWorm plays the guitar.  He likes to be at stage "G".\n\nLady Bug is a great keyboard player.  She is on the other stage.\n\nThis band is good.  They like to see their name in lights.  So, they made a big green sign that says "The Buggy Band".  Now, they have seen their name in lights.',
            init : {
                
            },
            required : [
                {
                    redBlock:'ladybug',
                    redBlockHolding:'keyboard',
                    greenBlock:'bee',
                    greenBlockHolding:'drums',
                    blueBlock:'worm',
                    blueBlockHolding:'guitar'
                }
            ]
        },
        
        id2:{
            scene:24,
            description:'Where do bugs go at night?  I will tell you.  They go to a beach party.\n\nLady Bug dances to the music coming out of her radio on the yellow leaf.\n\nBee is flying above the radio.\n\nSpider likes to boogie  near the sign.  The only problem is that it gets so cold at night.\n\nWorm has no fur to keep him warm.  He has to stay warm by the fire he made under the orange mushroom.',
            init : {
                
            },
            required:[
                {
                    redLeaf:'bee',
                    redLeafHolding:'empty',
                    yellowLeaf:'ladybug',
                    yellowLeafHolding:'radio',
                    float0:'spider',
                    float0Holding:'empty',
                    ground1:'worm',
                    ground1Holding:'fire',
                    mushroom:'orange',
                    sky:'night'
                }
            ]
        },

        id3:{
            scene:23,
            description:'Lady Bug and Worm dream of sailing the sea.  For now, they can only dream.\n\nWorm\'s toy boat helps them pretend they are at sea.\n\nWorm pretends that his yellow leaf is the ship deck.\n\nLady Bug pretends her green leaf is the crows nest.\n\n"Can I be at the top of the lighthouse?", asks Spider "The red mushroom makes a good lighthouse."\n\n"Sure," says Worm.  Sailing is a blast.',
            init : {
            },
            required:[
                {
                    greenLeaf:'ladybug',
                    greenLeafHolding:'empty',
                    yellowLeaf:'worm',
                    yellowLeafHolding:'empty',
                    float0:'spider',
                    float0Holding:'empty',
                    mushroom:'red',
                    boat:true
                }
            ]
        },

        id4:{
            scene:22,
            description:'The sand is hot today.  \n\nLady Bug stays in the shade.  The blue mushroom is a very good shade.\n\nSpider does not mind the sun, but the hot sand burns his feelers.  That is why he hangs from the green leaf.  All he needs are his glasses and he is fine.\n\nWorm makes sure he does not touch the hot sand.  He stays on the yellow leaf.',
            init : {
                sky:'night'
            },
            required:[
                {
                    greenLeaf:'spider',
                    greenLeafHolding:'sunglasses',
                    yellowLeaf:'worm',
                    yellowLeafHolding:'empty',
                    ground1:'ladybug',
                    ground1Holding:'empty',
                    mushroom:'blue',
                    sky:'day'
                }
            ]
        },
    
        id5:{
            scene:11,
            description:'What time is it?  The clock above the door tells the time.  It is time for grub.\n\nWhat should Worm eat?  Maybe a cup cake?  Maybe a cookie?  Worm will wait at his favorite spot at the blue table by the door.\n\nBee likes the seat at the middle table.  Bee can see Worm easily, but he can\'t whisper a secret to Worm.  Worm is too far away.  That is O.K.  \n\nHe can whisper his secret to Spider.  Spider hangs near the cup cake painting.',
            init : {
            },
            required:[
                {
                    redCap:'spider',
                    redCapHolding:'empty',
                    blueCap:'worm',
                    blueCapHolding:'empty',
                    greenCap:'bee',
                    greenCapHolding:'empty',
                    clock:true
                }
            ]
        },

        id6:{
            scene:11,
            description:'Who likes hot chocolate?  Lady Bug and Bee love it!\n\nLady bug likes hot chocolate at the front table.\n\nBee likes hot chocolate at the blue table.  She likes to drink as she flies.\n\nWhat shall they have after they finish drinking?\n\nLady Bug likes gum balls.  Perhaps she will have a gum ball.\n\nWe know what Bee likes.  She gets her dessert from the flowers.  Her favorite flavor is red.',
            init: {
            },
            required:[
                {
                    blueCap:'bee',
                    blueCapHolding:'cocoa',
                    greenCap:'ladybug',
                    greenCapHolding:'cocoa',
                    gumball:true,
                    flower: 'red'
                }
            ]
        },
        
        id7:{
            scene:11,
            description:'The Buggy Bakery is very crowded today.\n\nWorm is glad.  Three of his friends are here.\n\nWhat are they doing here this late at night?  Look at the time! Every table has a cupcake on it.\n\nWorm always hangs out by the door.  Spider has joined him.  Spider likes to hang around with Worm.\n\nLady Bug sits in the front.  The moon is pretty tonight.  Maybe Lady Bug will invite Bee to sit with her.  Bee is all alone at the red table.',
            init: {
            },
            required:[
                {
                    redCap:'bee',
                    redCapHolding:'cupcake',
                    blueCap:'spider',
                    blueCapHolding:'cupcake',
                    greenCap:'ladybug',
                    greenCapHolding:'cupcake',
                    door:'worm',
                    doorHolding:'empty',
                    sky:'night'
                }
            ]
        },

        id8:{
            scene:14,
            description:'The Buggy Bakery will close soon.  Bee and Worm have been talking all day.\n\nWorm sees that the moon is in the sky.  That is what is good about sitting at this table.  It has the best view of the sky.  Worm wonders if his friend will be able to fly home in the dark.\n\n"Oh my gosh!", says Bee as he flies close to the door.\n\n"Did you see the time?"  Worm wonders how Bee will get home.\n\nOne thing is for sure.  The two will stay until his cookie has been eaten.',
            init: {
            },
            required:[
                {
                    blueCap:'worm',
                    blueCapHolding:'cookie',
                    door:'bee',
                    doorHolding:'empty',
                    clock:true,
                    sky:'night'
                }
            ]
        },

        id9:{
            scene:13,
            description:'"Hi everyone", said Worm entering the bakery.\n\nSpider is glad to see Worm.  "Sit here, Worm.  Red is your favorite color."  Spider is right.  Red is Worm\'s favorite color.\n\n"Sit here", said Bee.  "You can have my cup cake. I will nibble on these red flowers."\n\nMaybe Worm should sit at Lady Bug\'s table.  That way, Worm can be between both friends.\n\nPlus, Lady Bug can never finish a big cup cake.  Worm can help her eat it.  Worm will have to make up his mind.',
            init: {
            },
            required:[
                {
                    redCap:'spider',
                    redCapHolding:'empty',
                    blueCap:'bee',
                    blueCapHolding:'cupcake',
                    greenCap:'ladybug',
                    greenCapHolding:'cupcake',
                    door:'worm',
                    doorHolding:'empty'
                }
            ]
        },
    }
};

