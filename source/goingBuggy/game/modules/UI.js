if(!RampsToReading) RampsToReading = {};
RampsToReading.UI = {};
//console.log('UI Script Loaded');
RampsToReading.UI.Button = function(settings){
    this.button = new Phaser.Sprite(game, settings.x, settings.y, settings.sprite);
    this.button.name = settings.name;
    this.button.z = settings.z || 0.0;
    this.button.anchor.x = settings.anchor.x || 0.0;
    this.button.anchor.y = settings.anchor.y || 0.0;
    this.button.origPos = {x: settings.x, y: settings.y};
    this.button.scale.x = this.button.scale.y = this.button.origScale = settings.scale;
    this.button.alpha = this.button.origAlpha = settings.alpha || 1.0;
    settings.group.add(this.button);
    this.button.inputEnabled = true;
    this.button.animations.add('over',[1]);
    this.button.animations.add('out',[0]);
    this.button.animations.add('down',[2]);
    this.button.animations.add('up',[0]);
    this.button.events.onInputOver.add(RampsToReading.UI.buttonOver, this);
    this.button.events.onInputOut.add(RampsToReading.UI.buttonOut, this);
    this.button.events.onInputDown.add(RampsToReading.UI.buttonPress, this);
    this.button.events.onInputUp.add(RampsToReading.UI.buttonClick, this);
    return this.button;
};

RampsToReading.UI.Button.prototype = Object.create(Phaser.Button.prototype);
RampsToReading.UI.Button.prototype.constructor = RampsToReading.UI.Button;

RampsToReading.UI.buttonOver = function(button){
    if(game.currentActiveState.roundStart && !game.currentActiveState.input.activePointer.isDown){
        game.currentActiveState.altButtonOverFX.play();
        button.animations.play('over');
    }
};

RampsToReading.UI.buttonOut = function (button) {
    if(game.currentActiveState.roundStart){
        button.animations.play('out');
    }
};

RampsToReading.UI.buttonClick = function (button) {
    if(game.currentActiveState.roundStart){
        button.animations.play('up');
        if(game.currentActiveState.cursorSprite.alpha === 1.0 && game.currentActiveState.cursorSprite.key.indexOf('Static') !== -1){
            switch(button.name){
                case 'dayButton':
                    if(game.currentActiveState.dayNightState !== 'day'){
                        game.currentActiveState.setSky('day');
                    }
                    break;
                case 'nightButton':
                    if(game.currentActiveState.dayNightState !== 'night'){
                        game.currentActiveState.setSky('night');
                    }
                    break;
                case 'clockButton':
                    game.currentActiveState.setClock();
                    break;
                case 'gumballButton':
                    game.currentActiveState.setGumball();
                    break;
                case 'blueFlowersButton':
                    game.currentActiveState.setFlowers('blue');
                    break;
                case 'purpleFlowersButton':
                    game.currentActiveState.setFlowers('purple');
                    break;
                case 'redFlowersButton':
                    game.currentActiveState.setFlowers('red');
                    break;
                case 'boatButton':
                    game.currentActiveState.setBoat();
                    break;
                case 'blueMushroomButton':
                    game.currentActiveState.setMushrooms('blue');
                    break;
                case 'orangeMushroomButton':
                    game.currentActiveState.setMushrooms('orange');
                    break;
                case 'redMushroomButton':
                    game.currentActiveState.setMushrooms('red');
                    break;
                case 'lightsButton':
                    game.currentActiveState.setLights();
                    break;
                case 'purpleCrayonButton':
                    game.currentActiveState.setCrayon('purple');
                    break;
                case 'greenCrayonButton':
                    game.currentActiveState.setCrayon('green');
                    break;
                case 'orangeCrayonButton':
                    game.currentActiveState.setCrayon('orange');
                    break;
            }
        }else{
            switch(button.name){
                case 'clearButton':
                    game.currentActiveState.setScene(game.currentActiveState.currentLevelSettings.level, game.currentActiveState.currentLevelSettings.id);
                    break;
                // case 'replayButton':             
                //     break;
                case 'doneButton':
                    game.currentActiveState.checkCorrect();
                    break;
                
            }
        }
        game.currentActiveState.checkCursor();
    }
};

RampsToReading.UI.buttonPress = function(button) {
    if(game.currentActiveState.roundStart){
        button.animations.play('up');
        switch(button.name){
            case 'beachballButton':
                game.currentActiveState.setCursor('beachball');
                game.currentActiveState.setHotBoxes('beachball');
                break;
            case 'beeButton':
                game.currentActiveState.setCursor('bee');
                game.currentActiveState.setHotBoxes('bee');
                break;
            case 'boatButton':
                game.currentActiveState.setCursor('boatStatic');
                break;
            case 'clockButton':
                game.currentActiveState.setCursor('clockStatic');
                break;
            case 'cocoaButton':
                game.currentActiveState.setCursor('cocoa');
                game.currentActiveState.setHotBoxes('cocoa');
                break;
            case 'cookieButton':
                game.currentActiveState.setCursor('cookie');
                game.currentActiveState.setHotBoxes('cookie');
                break;
            case 'cupcakeButton':
                game.currentActiveState.setCursor('cupcake');
                game.currentActiveState.setHotBoxes('cupcake');
                break;
            case 'dayButton':
                game.currentActiveState.setCursor('dayStatic');
                break;
            case 'drumsetButton':
                game.currentActiveState.setCursor('drums');
                game.currentActiveState.setHotBoxes('drums');
                break;
            case 'fireButton':
                game.currentActiveState.setCursor('fire');
                game.currentActiveState.setHotBoxes('fire');
                break;
            case 'guitarButton':
                game.currentActiveState.setCursor('guitar');
                game.currentActiveState.setHotBoxes('guitar');
                break;
            case 'gumballButton':
                game.currentActiveState.setCursor('gumballStatic');
                break;
            case 'keyboardButton':
                game.currentActiveState.setCursor('keyboard');
                game.currentActiveState.setHotBoxes('keyboard');
                break;
            case 'ladybugButton':
                game.currentActiveState.setCursor('ladybug');
                game.currentActiveState.setHotBoxes('ladybug');
                break;
            case 'lightsButton':
                game.currentActiveState.setCursor('lightsStatic');
                break;
            case 'nightButton':
                game.currentActiveState.setCursor('nightStatic');
                break;
            case 'radioButton':
                game.currentActiveState.setCursor('radio');
                game.currentActiveState.setHotBoxes('radio');
                break;
            case 'spiderButton':
                game.currentActiveState.setCursor('spider');
                game.currentActiveState.setHotBoxes('spider');
                break;
            case 'sunglassesButton':
                game.currentActiveState.setCursor('sunglasses');
                game.currentActiveState.setHotBoxes('sunglasses');
                break;
            case 'hornButton':
                game.currentActiveState.setCursor('horn');
                game.currentActiveState.setHotBoxes('horn');
                break;
            case 'wormButton':
                game.currentActiveState.setCursor('worm');
                game.currentActiveState.setHotBoxes('worm');
                break;
            case 'greenCrayonButton':
                game.currentActiveState.setCursor('greenCrayonStatic');
                break;
            case 'orangeCrayonButton':
                game.currentActiveState.setCursor('orangeCrayonStatic');
                break;
            case 'purpleCrayonButton':
                game.currentActiveState.setCursor('purpleCrayonStatic');
                break;
            case 'blueFlowersButton':
                game.currentActiveState.setCursor('blueFlowersStatic');
                break;
            case 'purpleFlowersButton':
                game.currentActiveState.setCursor('purpleFlowersStatic');
                break;
            case 'redFlowersButton':
                game.currentActiveState.setCursor('redFlowersStatic');
                break;
            case 'blueMushroomButton':
                game.currentActiveState.setCursor('blueMushroomStatic');
                break;
            case 'orangeMushroomButton':
                game.currentActiveState.setCursor('orangeMushroomStatic');
                break;
            case 'redMushroomButton':
                game.currentActiveState.setCursor('redMushroomStatic');
                break;
        }
    }
};
RampsToReading.UI.skySettings = {
    scene11:{
        moon:{x:410,y:-15},
        sun:{x:390,y:94},
        background:{y:100},
        clouds:{y:130}
    },
    scene12:{
        moon:{x:580,y:-15},
        sun:{x:570,y:94},
        background:{y:100},
        clouds:{y:130}
    },
    scene13:{
        moon:{x:410,y:-15},
        sun:{x:390,y:94},
        background:{y:100},
        clouds:{y:130}
    },
    scene14:{
        moon:{x:580,y:-15},
        sun:{x:570,y:94},
        background:{y:100},
        clouds:{y:130}
    },
    scene21:{
        moon:{x:480,y:-122},
        sun:{x:470,y:-8},
        background:{y:0},
        clouds:{y:20}
    },
    scene22:{
        moon:{x:480,y:-122},
        sun:{x:470,y:-8},
        background:{y:0},
        clouds:{y:20}
    },
    scene23:{
        moon:{x:480,y:-122},
        sun:{x:470,y:-8},
        background:{y:0},
        clouds:{y:20}
    },
    scene24:{
        moon:{x:480,y:-122},
        sun:{x:470,y:-8},
        background:{y:0},
        clouds:{y:20}
    },
    scene31:{
        moon:{x:360,y:-125},
        sun:{x:340,y:-8},
        background:{y:0},
        clouds:{y:20}
    },
    scene32:{
        moon:{x:360,y:-125},
        sun:{x:340,y:-8},
        background:{y:0},
        clouds:{y:20}
    },
    scene33:{
        moon:{x:666,y:-120},
        sun:{x:660,y:-8},
        background:{y:0},
        clouds:{y:20}
    },
    scene34:{
        moon:{x:666,y:-120},
        sun:{x:660,y:-8},
        background:{y:0},
        clouds:{y:20}
    }
};

RampsToReading.UI.hotBoxSelection = {
    scene11:{
        ladybug:['redCap','greenCap','blueCap','door'],
        spider:['redCap','greenCap','blueCap','door'],
        bee:['redCap','greenCap','blueCap','door'],
        worm:['redCap','greenCap','blueCap','door'],
        cupcake:['redCap','greenCap','blueCap','door'],
        cookie:['redCap','greenCap','blueCap','door'],
        cocoa:['redCap','greenCap','blueCap','door']
    },
    scene12:{
        ladybug:['redCap','greenCap','blueCap','door'],
        spider:['redCap','greenCap','blueCap','door'],
        bee:['redCap','greenCap','blueCap','door'],
        worm:['redCap','greenCap','blueCap','door'],
        cupcake:['redCap','greenCap','blueCap','door'],
        cookie:['redCap','greenCap','blueCap','door'],
        cocoa:['redCap','greenCap','blueCap','door']
    },
    scene13:{
        ladybug:['redCap','greenCap','blueCap','door'],
        spider:['redCap','greenCap','blueCap','door'],
        bee:['redCap','greenCap','blueCap','door'],
        worm:['redCap','greenCap','blueCap','door'],
        cupcake:['redCap','greenCap','blueCap','door'],
        cookie:['redCap','greenCap','blueCap','door'],
        cocoa:['redCap','greenCap','blueCap','door']
    },
    scene14:{
        ladybug:['redCap','greenCap','blueCap','door'],
        spider:['redCap','greenCap','blueCap','door'],
        bee:['redCap','greenCap','blueCap','door'],
        worm:['redCap','greenCap','blueCap','door'],
        cupcake:['redCap','greenCap','blueCap','door'],
        cookie:['redCap','greenCap','blueCap','door'],
        cocoa:['redCap','greenCap','blueCap','door']
    },
    scene21:{
        ladybug:['redLeaf','yellowLeaf','greenLeaf','ground0','ground1'],
        worm:['redLeaf','yellowLeaf','greenLeaf','ground0','ground1'],
        spider:['redLeaf','yellowLeaf','greenLeaf','float0','float1','float2','float3'],
        bee:['redLeaf','yellowLeaf','greenLeaf','float0','float1','float2','float3'],
        beachball:['yellowLeaf','greenLeaf','ground0','ground1'],
        radio:['yellowLeaf','greenLeaf','ground0','ground1'],
        sunglasses:['yellowLeaf','greenLeaf','ground0','ground1'],
        fire:['yellowLeaf','greenLeaf','ground0','ground1']
    },
    scene22:{
        ladybug:['redLeaf','yellowLeaf','greenLeaf','ground0','ground1','ground2'],
        worm:['redLeaf','yellowLeaf','greenLeaf','ground0','ground1','ground2'],
        spider:['redLeaf','yellowLeaf','greenLeaf','float0','float1','float2'],
        bee:['redLeaf','yellowLeaf','greenLeaf','float0','float1','float2'],
        beachball:['yellowLeaf','greenLeaf','ground0','ground1','ground2'],
        radio:['yellowLeaf','greenLeaf','ground0','ground1','ground2'],
        sunglasses:['yellowLeaf','greenLeaf','ground0','ground1','ground2'],
        fire:['yellowLeaf','greenLeaf','ground0','ground1','ground2']
    },
    scene23:{
        ladybug:['redLeaf','yellowLeaf','greenLeaf','ground0','ground1'],
        worm:['redLeaf','yellowLeaf','greenLeaf','ground0','ground1'],
        spider:['redLeaf','yellowLeaf','greenLeaf','float0','float1','float2','float3'],
        bee:['redLeaf','yellowLeaf','greenLeaf','float0','float1','float2','float3'],
        beachball:['yellowLeaf','greenLeaf','ground0','ground1'],
        radio:['yellowLeaf','greenLeaf','ground0','ground1'],
        sunglasses:['yellowLeaf','greenLeaf','ground0','ground1'],
        fire:['yellowLeaf','greenLeaf','ground0','ground1']
    },
    scene24:{
        ladybug:['redLeaf','yellowLeaf','greenLeaf','ground0','ground1'],
        worm:['redLeaf','yellowLeaf','greenLeaf','ground0','ground1'],
        spider:['redLeaf','yellowLeaf','greenLeaf','float0','float1'],
        bee:['redLeaf','yellowLeaf','greenLeaf','float0','float1'],
        beachball:['yellowLeaf','greenLeaf','ground0','ground1'],
        radio:['yellowLeaf','greenLeaf','ground0','ground1'],
        sunglasses:['yellowLeaf','greenLeaf','ground0','ground1'],
        fire:['yellowLeaf','greenLeaf','ground0','ground1']
    },
    scene31:{
        ladybug:['redBlock','greenBlock','blueBlock'],
        spider:['redBlock','greenBlock','blueBlock'],
        bee:['redBlock','greenBlock','blueBlock'],
        worm:['redBlock','greenBlock','blueBlock'],
        keyboard:['redBlock','greenBlock','blueBlock'],
        guitar:['redBlock','greenBlock','blueBlock'],
        horn:['redBlock','greenBlock','blueBlock'],
        drums:['redBlock','greenBlock','blueBlock']
    },
    scene32:{
        ladybug:['redBlock','greenBlock','blueBlock','character0'],
        spider:['redBlock','greenBlock','blueBlock','character0'],
        bee:['redBlock','greenBlock','blueBlock','character0'],
        worm:['redBlock','greenBlock','blueBlock','character0'],
        keyboard:['redBlock','greenBlock','blueBlock','character0'],
        guitar:['redBlock','greenBlock','blueBlock','character0'],
        horn:['redBlock','greenBlock','blueBlock','character0'],
        drums:['redBlock','greenBlock','blueBlock','character0']
    },
    scene33:{
        ladybug:['redBlock','greenBlock','blueBlock','character0'],
        spider:['redBlock','greenBlock','blueBlock','character0'],
        bee:['redBlock','greenBlock','blueBlock','character0'],
        worm:['redBlock','greenBlock','blueBlock','character0'],
        keyboard:['redBlock','greenBlock','blueBlock','character0'],
        guitar:['redBlock','greenBlock','blueBlock','character0'],
        horn:['redBlock','greenBlock','blueBlock','character0'],
        drums:['redBlock','greenBlock','blueBlock','character0']
    },
    scene34:{
        ladybug:['redBlock','greenBlock','blueBlock','character0'],
        spider:['redBlock','greenBlock','blueBlock','character0'],
        bee:['redBlock','greenBlock','blueBlock','character0'],
        worm:['redBlock','greenBlock','blueBlock','character0'],
        keyboard:['redBlock','greenBlock','blueBlock','character0'],
        guitar:['redBlock','greenBlock','blueBlock','character0'],
        horn:['redBlock','greenBlock','blueBlock','character0'],
        drums:['redBlock','greenBlock','blueBlock','character0']
    },
};

RampsToReading.UI.hotBoxPlacement = {
    scene11:[
        {
            name: 'redCap',
            x: 290,
            y: 200
        },
        {
            name: 'greenCap',
            x: 400,
            y: 255
        },
        {
            name: 'blueCap',
            x: 510,
            y: 200
        },
        {
            name: 'door',
            x: 668,
            y: 155
        }
    ],
    scene12:[
        {
            name: 'redCap',
            x: 575,
            y: 255
        },
        {
            name: 'greenCap',
            x: 480,
            y: 185
        },
        {
            name: 'blueCap',
            x: 675,
            y: 185
        },
        {
            name: 'door',
            x: 309,
            y: 155
        }
    ],
    scene13:[
        {
            name: 'redCap',
            x: 675,
            y: 260
        },
        {
            name: 'greenCap',
            x: 475,
            y: 220
        },
        {
            name: 'blueCap',
            x: 290,
            y: 185
        },
        {
            name: 'door',
            x: 668,
            y: 155
        }
    ],
    scene14:[
        {
            name: 'redCap',
            x: 595,
            y: 260
        },
        {
            name: 'greenCap',
            x: 395,
            y: 260
        },
        {
            name: 'blueCap',
            x: 675,
            y: 185
        },
        {
            name: 'door',
            x: 309,
            y: 155
        }
    ],
    scene21:[
        {
            name:'redLeaf',
            x:300,
            y:40
        },
        {
            name:'yellowLeaf',
            x:390,
            y:140
        },
        {
            name:'greenLeaf',
            x:575,
            y:185
        },
        {
            name:'ground0',
            x:275,
            y:300
        },
        {
            name:'ground1',
            x:520,
            y:255
        },
        {
            name:'float0',
            x:300,
            y:175
        },
        {
            name:'float1',
            x:490,
            y:30
        },
        {
            name:'float2',
            x:550,
            y:110
        },
        {
            name:'float3',
            x:700,
            y:40
        },
    ],
    scene22:[
        {
            name:'redLeaf',
            x:675,
            y:26
        },
        {
            name:'yellowLeaf',
            x:650,
            y:140
        },
        {
            name:'greenLeaf',
            x:270,
            y:40
        },
        {
            name:'ground0',
            x:275,
            y:280
        },
        {
            name:'ground1',
            x:400,
            y:150
        },
        {
            name:'ground2',
            x:630,
            y:290
        },
        {
            name:'float0',
            x:310,
            y:125
        },
        {
            name:'float1',
            x:490,
            y:30
        },
        {
            name:'float2',
            x:610,
            y:210
        }
    ],
    scene23:[
        {
            name:'redLeaf',
            x:675,
            y:26
        },
        {
            name:'yellowLeaf',
            x:610,
            y:170
        },
        {
            name:'greenLeaf',
            x:450,
            y:135
        },
        {
            name:'ground0',
            x:325,
            y:300
        },
        {
            name:'ground1',
            x:530,
            y:275
        },
        {
            name:'float0',
            x:275,
            y:45
        },
        {
            name:'float1',
            x:320,
            y:140
        },
        {
            name:'float2',
            x:490,
            y:30
        },
        {
            name:'float3',
            x:560,
            y:110
        }
    ],
    scene24:[
        {
            name:'redLeaf',
            x:320,
            y:26
        },
        {
            name:'yellowLeaf',
            x:310,
            y:160
        },
        {
            name:'greenLeaf',
            x:685,
            y:115
        },
        {
            name:'ground0',
            x:400,
            y:300
        },
        {
            name:'ground1',
            x:530,
            y:130
        },
        {
            name:'float0',
            x:410,
            y:120
        },
        {
            name:'float1',
            x:490,
            y:30
        },
    ],
    scene31:[
        {
            name:'redBlock',
            x:290,
            y:175
        },
        {
            name:'greenBlock',
            x:475,
            y:110
        },
        {
            name:'blueBlock',
            x:670,
            y:175
        }
    ],
    scene32:[
        {
            name:'redBlock',
            x:670,
            y:175
        },
        {
            name:'greenBlock',
            x:480,
            y:110
        },
        {
            name:'blueBlock',
            x:295,
            y:115
        },
        {
            name:'character0',
            x:480,
            y:225
        }
    ],
    scene33:[
        {
            name:'redBlock',
            x:490,
            y:110
        },
        {
            name:'greenBlock',
            x:670,
            y:180
        },
        {
            name:'blueBlock',
            x:295,
            y:145
        },
        {
            name:'character0',
            x:480,
            y:215
        }
    ],
    scene34:[
        {
            name:'redBlock',
            x:675,
            y:125
        },
        {
            name:'greenBlock',
            x:300,
            y:125
        },
        {
            name:'blueBlock',
            x:485,
            y:125
        },
        {
            name:'character0',
            x:665,
            y:215
        }
    ]
};

RampsToReading.UI.sceneryPlacement = {
    scene11:{
        redCap: {
            x:240,y:238,zI:3,scale:1
        },
        blueCap: {
            x:455,y:238,zI:3,scale:1
        },
        greenCap: {
            x:348,y:303,zI:5,scale:1
        },
        clock:{x:670,y:10},
        gumball:{x:745,y:-68},
        flower:{x:600,y:115}
    },
    scene12:{
        redCap: {
            x:530,y:303,zI:5,scale:1
        },
        blueCap: {
            x:625,y:226,zI:3,scale:1
        },
        greenCap: {
            x:430,y:226,zI:3,scale:1
        },
        clock:{x:308,y:10},
        gumball:{x:382,y:-68},
        flower:{x:740,y:115}
    },
    scene13:{
        redCap: {
            x:626,y:303,zI:7,scale:1
        },
        blueCap: {
            x:242,y:226,zI:3,scale:1
        },
        greenCap: {
            x:428,y:265,zI:5,scale:1
        },
        clock:{x:670,y:10},
        gumball:{x:745,y:-68},
        flower:{x:235,y:115}
    },
    scene14:{
        redCap: {
            x:544,y:303,zI:5,scale:1
        },
        blueCap: {
            x:625,y:230,zI:3,scale:1
        },
        greenCap: {
            x:346,y:303,zI:5,scale:1
        },
        clock:{x:308,y:10},
        gumball:{x:382,y:-68},
        flower:{x:435,y:115}
    },
    scene21:{
        pond_2:{
            x:170,y:207,zI:3,scale:1
        },
        rocks_2:{
            x:470, y:320, zI:5, scale:1
        },
        sign_2:{
            x:670, y:243, zI:5, scale:1
        },
        signAlt_2:{
            x:-1000, y:-1000, zI:1, scale:1
        },
        grass_2:{
            x:430, y:243, zI:3, scale:1
        },
        boat:{x:240,y:180},
        mushroom:{x:570, y:27, scale:1}
    },
    scene22:{
        pond_2:{
            x:810,y:205,zI:3,scale:-1
        },
        rocks_2:{
            x:-1000, y:-1000, zI:1, scale:1
        },
        sign_2:{
            x:-1000, y:-1000, zI:1, scale:1
        },
        signAlt_2:{
            x:465, y:235, zI:5, scale:1
        },
        grass_2:{
            x:-1000, y:-1000, zI:1, scale:1
        },
        boat:{x:550,y:155},
        mushroom:{x:550, y:27, scale:-1}
    },
    scene23:{
        pond_2:{
            x:170,y:207,zI:3,scale:1
        },
        rocks_2:{
            x:-1000, y:-1000, zI:1, scale:1
        },
        sign_2:{
            x:670, y:243, zI:5, scale:1
        },
        signAlt_2:{
            x:-1000, y:-1000, zI:1, scale:1
        },
        grass_2:{
            x:430, y:243, zI:3, scale:1
        },
        boat:{x:240,y:155},
        mushroom:{x:392, y:32, scale:-1}
    },
    scene24:{
        pond_2:{
            x:810,y:205,zI:3,scale:-1
        },
        rocks_2:{
            x:650, y:320, zI:5, scale:1
        },
        sign_2:{
            x:245, y:240, zI:5, scale:1
        },
        signAlt_2:{
            x:-1000, y:-1000, zI:1, scale:1
        },
        grass_2:{
            x:490, y:245, zI:3, scale:1
        },
        boat:{x:705,y:205},
        mushroom:{x:490, y:30, scale:1}
    },
    scene31:{
        redBlock_3:{
            x:228, y:205, zI:5, scale:1
        },
        greenBlock_3:{
            x:410, y:138, zI:3, scale:1
        },
        blueBlock_3:{
            x:605, y:205, zI:5, scale:1
        },
        crushPin1_3:{
            x:557, y:0, zI:1.2, scale:1
        },
        lights:{x:215,y:0, scale:1},
        crayon:{x:460,y:315}
    },
    scene32:{
        redBlock_3:{
            x:610, y:205, zI:5, scale:1
        },
        greenBlock_3:{
            x:422, y:136, zI:3, scale:1
        },
        blueBlock_3:{
            x:237, y:136, zI:3, scale:1
        },
        crushPin1_3:{
            x:557, y:0, zI:1.2, scale:1
        },
        lights:{x:215,y:0, scale:1},
        crayon:{x:265,y:305}
    },
    scene33:{
        redBlock_3:{
            x:431, y:132, zI:3, scale:1
        },
        greenBlock_3:{
            x:612, y:205, zI:5, scale:1
        },
        blueBlock_3:{
            x:235, y:170, zI:5, scale:1
        },
        crushPin1_3:{
            x:253, y:1, zI:1.2, scale:1
        },
        lights:{x:835,y:0, scale:-1},
        crayon:{x:320,y:315}
    },
    scene34:{
        redBlock_3:{
            x:612, y:153, zI:3, scale:1
        },
        greenBlock_3:{
            x:238, y:153, zI:3, scale:1
        },
        blueBlock_3:{
            x:425, y:153, zI:3, scale:1
        },
        crushPin1_3:{
            x:253, y:1, zI:1.2, scale:1
        },
        lights:{x:835,y:0, scale:-1},
        crayon:{x:460,y:315}
    }
};

RampsToReading.UI.buttonPlacement = {
    scene1:{
        ladybugButton : {
            x : 250,
            y : 400,
            scale : 1.0
        },
        spiderButton : {
            x : 285,
            y : 425,
            scale : 1.0
        },
        beeButton : {
            x : 380,
            y : 395,
            scale : 1.0
        },
        wormButton : {
            x : 435,
            y : 445,
            scale : 1.0
        },
        dayButton : {
            x : 542,
            y : 403,
            scale : 1.0
        },
        nightButton : {
            x : 615,
            y : 403,
            scale : 1.0
        },
        cupcakeButton : {
            x : 250,
            y : 510,
            scale : 1.0
        },
        cookieButton : {
            x : 303,
            y : 510,
            scale : 1.0
        },
        cocoaButton : {
            x : 362,
            y : 518,
            scale : 1.0
        },
        gumballButton : {
            x : 412,
            y : 505,
            scale : 1.0
        },
        clockButton : {
            x : 455,
            y : 500,
            scale : 1.0
        },
        blueFlowersButton : {
            x : 530,
            y : 515,
            scale : 1.0
        },
        purpleFlowersButton : {
            x : 585,
            y : 515,
            scale : 1.0
        },
        redFlowersButton : {
            x : 640,
            y : 515,
            scale : 1.0
        },
    },

    scene2:{
        ladybugButton : {
            x : 250,
            y : 400,
            scale : 1.0
        },
        spiderButton : {
            x : 285,
            y : 425,
            scale : 1.0
        },
        beeButton : {
            x : 380,
            y : 395,
            scale : 1.0
        },
        wormButton : {
            x : 435,
            y : 445,
            scale : 1.0
        },
        dayButton : {
            x : 542,
            y : 403,
            scale : 1.0
        },
        nightButton : {
            x : 615,
            y : 403,
            scale : 1.0
        },
        beachballButton : {
            x : 245,
            y : 520,
            scale : 1.0
        },
        radioButton : {
            x : 287,
            y : 500,
            scale : 1.0
        },
        sunglassesButton : {
            x : 340,
            y : 535,
            scale : 1.0
        },
        fireButton : {
            x : 405,
            y : 520,
            scale : 1.0
        },
        boatButton : {
            x : 463,
            y : 510,
            scale : 1.0
        },
        orangeMushroomButton : {
            x : 527,
            y : 510,
            scale : 1.0
        },
        redMushroomButton : {
            x : 582,
            y : 510,
            scale : 1.0
        },
        blueMushroomButton : {
            x : 637,
            y : 510,
            scale : 1.0
        }
    },

    scene3:{
        ladybugButton : {
            x : 250,
            y : 400,
            scale : 1.0
        },
        spiderButton : {
            x : 285,
            y : 425,
            scale : 1.0
        },
        beeButton : {
            x : 380,
            y : 395,
            scale : 1.0
        },
        wormButton : {
            x : 435,
            y : 445,
            scale : 1.0
        },
        dayButton : {
            x : 542,
            y : 403,
            scale : 1.0
        },
        nightButton : {
            x : 615,
            y : 403,
            scale : 1.0
        },
        keyboardButton : {
            x : 250,
            y : 517,
            scale : 1.0
        },
        guitarButton : {
            x : 320,
            y : 510,
            scale : 1.0
        },
        hornButton : {
            x : 352,
            y : 530,
            scale : 1.0
        },
        drumsetButton : {
            x : 407,
            y : 520,
            scale : 1.0
        },
        lightsButton : {
            x : 469,
            y : 520,
            scale : 1.0
        },
        purpleCrayonButton : {
            x : 528,
            y : 515,
            scale : 1.0
        },
        orangeCrayonButton : {
            x : 583,
            y : 515,
            scale : 1.0
        },
        greenCrayonButton : {
            x : 638,
            y : 515,
            scale : 1.0
        }
    }
};

RampsToReading.UI.spritePlacement = {
    scene11:{
        redCap:{
            beeEmpty_1:{x:290,y:125,zI:2,scale:1},
            beeCookie_1:{x:242,y:125,zI:2,scale:1},
            beeCupcake_1:{x:242,y:125,zI:2,scale:1},
            beeCocoa_1:{x:242,y:125,zI:2,scale:1},

            ladybugEmpty_1:{x:305,y:160,zI:2,scale:-1},
            ladybugCookie_1:{x:324,y:160,zI:2,scale:-1},
            ladybugCupcake_1:{x:317,y:160,zI:2,scale:-1},
            ladybugCocoa_1:{x:312,y:160,zI:2,scale:-1},

            spiderEmpty_1:{x:265,y:-340,zI:3.1,scale:1},
            spiderCookie_1:{x:265,y:-340,zI:3.1,scale:1},
            spiderCupcake_1:{x:265,y:-340,zI:3.1,scale:1},
            spiderCocoa_1:{x:265,y:-340,zI:3.1,scale:1},

            wormEmpty_1:{x:324,y:175,zI:2,scale:1},
            wormCookie_1:{x:324,y:175,zI:2,scale:1},
            wormCupcake_1:{x:324,y:175,zI:2,scale:1},
            wormCocoa_1:{x:324,y:175,zI:2,scale:1},

            cookieEmpty_1:{    x:350,   y:235,   zI:3.1,scale:-1},
            cupcakeEmpty_1:{   x:350,   y:200,   zI:3.1,scale:-1},
            cocoaEmpty_1:{     x:310,   y:220,   zI:3.1,scale:1}

        },
        blueCap:{
            beeEmpty_1:{x:500,y:125,zI:2,scale:1},
            beeCookie_1:{x:452,y:125,zI:2,scale:1},
            beeCupcake_1:{x:452,y:125,zI:2,scale:1},
            beeCocoa_1:{x:452,y:125,zI:2,scale:1},

            ladybugEmpty_1:{x:510,y:160,zI:2,scale:-1},
            ladybugCookie_1:{x:529,y:160,zI:2,scale:-1},
            ladybugCupcake_1:{x:522,y:160,zI:2,scale:-1},
            ladybugCocoa_1:{x:517,y:160,zI:2,scale:-1},

            spiderEmpty_1:{x:475,y:-340,zI:3.1,scale:1},
            spiderCookie_1:{x:475,y:-340,zI:3.1,scale:1},
            spiderCupcake_1:{x:475,y:-340,zI:3.1,scale:1},
            spiderCocoa_1:{x:475,y:-340,zI:3.1,scale:1},

            wormEmpty_1:{x:540,y:175,zI:2,scale:1},
            wormCookie_1:{x:540,y:175,zI:2,scale:1},
            wormCupcake_1:{x:540,y:175,zI:2,scale:1},
            wormCocoa_1:{x:540,y:175,zI:2,scale:1},

            cookieEmpty_1:{    x:565,   y:235,   zI:3.1,scale:-1},
            cupcakeEmpty_1:{   x:565,   y:200,   zI:3.1,scale:-1},
            cocoaEmpty_1:{     x:525,   y:220,   zI:3.1,scale:1}

        },
        greenCap:{
            beeEmpty_1:{x:395,y:190,zI:4,scale:1},
            beeCookie_1:{x:347,y:190,zI:4,scale:1},
            beeCupcake_1:{x:347,y:190,zI:4,scale:1},
            beeCocoa_1:{x:347,y:190,zI:4,scale:1},

            ladybugEmpty_1:{x:408,y:225,zI:4,scale:-1},
            ladybugCookie_1:{x:427,y:225,zI:4,scale:-1},
            ladybugCupcake_1:{x:420,y:225,zI:4,scale:-1},
            ladybugCocoa_1:{x:415,y:225,zI:4,scale:-1},

            spiderEmpty_1:{x:376,y:-275,zI:5.1,scale:1},
            spiderCookie_1:{x:376,y:-275,zI:5.1,scale:1},
            spiderCupcake_1:{x:376,y:-275,zI:5.1,scale:1},
            spiderCocoa_1:{x:376,y:-275,zI:5.1,scale:1},

            wormEmpty_1:{x:430,y:235,zI:4,scale:1},
            wormCookie_1:{x:430,y:235,zI:4,scale:1},
            wormCupcake_1:{x:430,y:235,zI:4,scale:1},
            wormCocoa_1:{x:430,y:235,zI:4,scale:1},

            cookieEmpty_1:{    x:460,   y:300,   zI:5.1,scale:-1},
            cupcakeEmpty_1:{   x:460,   y:265,   zI:5.1,scale:-1},
            cocoaEmpty_1:{     x:420,   y:285,   zI:5.1,scale:1}
        },
        door:{
            beeEmpty_1:{x:655,y:125,zI:2,scale:1},
            beeCookie_1:{x:607,y:125,zI:2,scale:1},
            beeCupcake_1:{x:607,y:125,zI:2,scale:1},
            beeCocoa_1:{x:607,y:125,zI:2,scale:1},

            ladybugEmpty_1:{x:660,y:155,zI:2,scale:1},
            ladybugCookie_1:{x:679,y:155,zI:2,scale:1},
            ladybugCupcake_1:{x:672,y:155,zI:2,scale:1},
            ladybugCocoa_1:{x:663,y:155,zI:2,scale:1},

            spiderEmpty_1:{x:640,y:-350,zI:3.1,scale:1},
            spiderCookie_1:{x:640,y:-350,zI:3.1,scale:1},
            spiderCupcake_1:{x:640,y:-350,zI:3.1,scale:1},
            spiderCocoa_1:{x:640,y:-350,zI:3.1,scale:1},

            wormEmpty_1:{x:655,y:160,zI:2,scale:1},
            wormCookie_1:{x:655,y:160,zI:2,scale:1},
            wormCupcake_1:{x:655,y:160,zI:2,scale:1},
            wormCocoa_1:{x:655,y:160,zI:2,scale:1},

            cookieEmpty_1:{    x:670,   y:240,   zI:5.1,scale:1},
            cupcakeEmpty_1:{   x:670,   y:200,   zI:5.1,scale:1},
            cocoaEmpty_1:{     x:680,   y:225,   zI:5.1,scale:1}
        }
    },

    scene12:{
        redCap:{
            beeEmpty_1:{x:575,y:190,zI:4,scale:1},
            beeCookie_1:{x:527,y:190,zI:4,scale:1},
            beeCupcake_1:{x:527,y:190,zI:4,scale:1},
            beeCocoa_1:{x:527,y:190,zI:4,scale:1},

            ladybugEmpty_1:{x:594,y:225,zI:4,scale:-1},
            ladybugCookie_1:{x:575,y:225,zI:4,scale:-1},
            ladybugCupcake_1:{x:568,y:225,zI:4,scale:-1},
            ladybugCocoa_1:{x:563,y:225,zI:4,scale:-1},

            spiderEmpty_1:{x:560,y:-275,zI:5.1,scale:1},
            spiderCookie_1:{x:560,y:-275,zI:5.1,scale:1},
            spiderCupcake_1:{x:560,y:-275,zI:5.1,scale:1},
            spiderCocoa_1:{x:560,y:-275,zI:5.1,scale:1},

            wormEmpty_1:{x:620,y:235,zI:4,scale:1},
            wormCookie_1:{x:620,y:235,zI:4,scale:1},
            wormCupcake_1:{x:620,y:235,zI:4,scale:1},
            wormCocoa_1:{x:620,y:235,zI:4,scale:1},

            cookieEmpty_1:{    x:645,   y:300,   zI:5.1,scale:-1},
            cupcakeEmpty_1:{   x:645,   y:265,   zI:5.1,scale:-1},
            cocoaEmpty_1:{     x:605,   y:285,   zI:5.1,scale:1}
        },
        blueCap:{
            beeEmpty_1:{x:670,y:112,zI:2,scale:1},
            beeCookie_1:{x:622,y:112,zI:2,scale:1},
            beeCupcake_1:{x:622,y:112,zI:2,scale:1},
            beeCocoa_1:{x:622,y:112,zI:2,scale:1},

            ladybugEmpty_1:{x:685,y:152,zI:2,scale:-1},
            ladybugCookie_1:{x:704,y:152,zI:2,scale:-1},
            ladybugCupcake_1:{x:697,y:152,zI:2,scale:-1},
            ladybugCocoa_1:{x:692,y:152,zI:2,scale:-1},

            spiderEmpty_1:{x:650,y:-350,zI:3.1,scale:1},
            spiderCookie_1:{x:650,y:-350,zI:3.1,scale:1},
            spiderCupcake_1:{x:650,y:-350,zI:3.1,scale:1},
            spiderCocoa_1:{x:650,y:-350,zI:3.1,scale:1},

            wormEmpty_1:{x:705,y:160,zI:2,scale:1},
            wormCookie_1:{x:705,y:160,zI:2,scale:1},
            wormCupcake_1:{x:705,y:160,zI:2,scale:1},
            wormCocoa_1:{x:705,y:160,zI:2,scale:1},

            cookieEmpty_1:{    x:735,   y:225,   zI:3.1,scale:-1},
            cupcakeEmpty_1:{   x:735,   y:190,   zI:3.1,scale:-1},
            cocoaEmpty_1:{     x:695,   y:210,   zI:3.1,scale:1}

        },
        greenCap:{
            beeEmpty_1:{x:470,y:112,zI:2,scale:1},
            beeCookie_1:{x:422,y:112,zI:2,scale:1},
            beeCupcake_1:{x:422,y:112,zI:2,scale:1},
            beeCocoa_1:{x:422,y:112,zI:2,scale:1},

            ladybugEmpty_1:{x:490,y:152,zI:2,scale:-1},
            ladybugCookie_1:{x:509,y:152,zI:2,scale:-1},
            ladybugCupcake_1:{x:502,y:152,zI:2,scale:-1},
            ladybugCocoa_1:{x:497,y:152,zI:2,scale:-1},

            spiderEmpty_1:{x:450,y:-350,zI:3.1,scale:1},
            spiderCookie_1:{x:450,y:-350,zI:3.1,scale:1},
            spiderCupcake_1:{x:450,y:-350,zI:3.1,scale:1},
            spiderCocoa_1:{x:450,y:-350,zI:3.1,scale:1},

            wormEmpty_1:{x:520,y:160,zI:2,scale:1},
            wormCookie_1:{x:520,y:160,zI:2,scale:1},
            wormCupcake_1:{x:520,y:160,zI:2,scale:1},
            wormCocoa_1:{x:520,y:160,zI:2,scale:1},

            cookieEmpty_1:{    x:535,   y:225,   zI:3.1,scale:-1},
            cupcakeEmpty_1:{   x:535,   y:190,   zI:3.1,scale:-1},
            cocoaEmpty_1:{     x:495,   y:210,   zI:3.1,scale:1}
        },

        door:{
            beeEmpty_1:{x:295,y:125,zI:2,scale:1},
            beeCookie_1:{x:247,y:125,zI:2,scale:1},
            beeCupcake_1:{x:247,y:125,zI:2,scale:1},
            beeCocoa_1:{x:247,y:125,zI:2,scale:1},

            ladybugEmpty_1:{x:300,y:155,zI:2,scale:1},
            ladybugCookie_1:{x:319,y:155,zI:2,scale:1},
            ladybugCupcake_1:{x:312,y:155,zI:2,scale:1},
            ladybugCocoa_1:{x:303,y:155,zI:2,scale:1},

            spiderEmpty_1:{x:280,y:-350,zI:3.1,scale:1},
            spiderCookie_1:{x:280,y:-350,zI:3.1,scale:1},
            spiderCupcake_1:{x:280,y:-350,zI:3.1,scale:1},
            spiderCocoa_1:{x:280,y:-350,zI:3.1,scale:1},

            wormEmpty_1:{x:295,y:160,zI:2,scale:1},
            wormCookie_1:{x:295,y:160,zI:2,scale:1},
            wormCupcake_1:{x:295,y:160,zI:2,scale:1},
            wormCocoa_1:{x:295,y:160,zI:2,scale:1},

            cookieEmpty_1:{    x:310,   y:240,   zI:5.1,scale:1},
            cupcakeEmpty_1:{   x:310,   y:200,   zI:5.1,scale:1},
            cocoaEmpty_1:{     x:320,   y:225,   zI:5.1,scale:1}
        }
    },

    scene13:{
        redCap:{
            beeEmpty_1:{x:675,y:190,zI:6,scale:1},
            beeCookie_1:{x:627,y:190,zI:6,scale:1},
            beeCupcake_1:{x:627,y:190,zI:6,scale:1},
            beeCocoa_1:{x:627,y:190,zI:6,scale:1},

            ladybugEmpty_1:{x:690,y:225,zI:6,scale:-1},
            ladybugCookie_1:{x:709,y:225,zI:6,scale:-1},
            ladybugCupcake_1:{x:702,y:225,zI:6,scale:-1},
            ladybugCocoa_1:{x:697,y:225,zI:6,scale:-1},

            spiderEmpty_1:{x:650,y:-275,zI:7.1,scale:1},
            spiderCookie_1:{x:650,y:-275,zI:7.1,scale:1},
            spiderCupcake_1:{x:650,y:-275,zI:7.1,scale:1},
            spiderCocoa_1:{x:650,y:-275,zI:7.1,scale:1},

            wormEmpty_1:{x:710,y:230,zI:6,scale:1},
            wormCookie_1:{x:710,y:230,zI:6,scale:1},
            wormCupcake_1:{x:710,y:230,zI:6,scale:1},
            wormCocoa_1:{x:710,y:230,zI:6,scale:1},

            cookieEmpty_1:{    x:740,   y:300,   zI:7.1,scale:-1},
            cupcakeEmpty_1:{   x:740,   y:265,   zI:7.1,scale:-1},
            cocoaEmpty_1:{     x:700,   y:285,   zI:7.1,scale:1}
        },
        blueCap:{
            beeEmpty_1:{x:285,y:112,zI:2,scale:1},
            beeCookie_1:{x:237,y:112,zI:2,scale:1},
            beeCupcake_1:{x:237,y:112,zI:2,scale:1},
            beeCocoa_1:{x:237,y:112,zI:2,scale:1},

            ladybugEmpty_1:{x:305,y:152,zI:2,scale:-1},
            ladybugCookie_1:{x:324,y:152,zI:2,scale:-1},
            ladybugCupcake_1:{x:317,y:152,zI:2,scale:-1},
            ladybugCocoa_1:{x:312,y:152,zI:2,scale:-1},

            spiderEmpty_1:{x:265,y:-350,zI:3.1,scale:1},
            spiderCookie_1:{x:265,y:-350,zI:3.1,scale:1},
            spiderCupcake_1:{x:265,y:-350,zI:3.1,scale:1},
            spiderCocoa_1:{x:265,y:-350,zI:3.1,scale:1},

            wormEmpty_1:{x:330,y:160,zI:2,scale:1},
            wormCookie_1:{x:330,y:160,zI:2,scale:1},
            wormCupcake_1:{x:330,y:160,zI:2,scale:1},
            wormCocoa_1:{x:330,y:160,zI:2,scale:1},

            cookieEmpty_1:{    x:350,   y:225,   zI:3.1,scale:-1},
            cupcakeEmpty_1:{   x:350,   y:190,   zI:3.1,scale:-1},
            cocoaEmpty_1:{     x:310,   y:210,   zI:3.1,scale:1}

        },
        greenCap:{
            beeEmpty_1:{x:470,y:150,zI:4,scale:1},
            beeCookie_1:{x:422,y:150,zI:4,scale:1},
            beeCupcake_1:{x:422,y:150,zI:4,scale:1},
            beeCocoa_1:{x:422,y:150,zI:4,scale:1},

            ladybugEmpty_1:{x:490,y:188,zI:4,scale:-1},
            ladybugCookie_1:{x:509,y:188,zI:4,scale:-1},
            ladybugCupcake_1:{x:502,y:188,zI:4,scale:-1},
            ladybugCocoa_1:{x:497,y:188,zI:4,scale:-1},

            spiderEmpty_1:{x:450,y:-315,zI:5.1,scale:1},
            spiderCookie_1:{x:450,y:-315,zI:5.1,scale:1},
            spiderCupcake_1:{x:450,y:-315,zI:5.1,scale:1},
            spiderCocoa_1:{x:450,y:-315,zI:5.1,scale:1},

            wormEmpty_1:{x:515,y:200,zI:4,scale:1},
            wormCookie_1:{x:515,y:200,zI:4,scale:1},
            wormCupcake_1:{x:515,y:200,zI:4,scale:1},
            wormCocoa_1:{x:515,y:200,zI:4,scale:1},

            cookieEmpty_1:{    x:540,   y:265,   zI:5.1,scale:-1},
            cupcakeEmpty_1:{   x:540,   y:230,   zI:5.1,scale:-1},
            cocoaEmpty_1:{     x:500,   y:250,   zI:5.1,scale:1}
        },

        door:{
            beeEmpty_1:{x:655,y:125,zI:2,scale:1},
            beeCookie_1:{x:607,y:125,zI:2,scale:1},
            beeCupcake_1:{x:607,y:125,zI:2,scale:1},
            beeCocoa_1:{x:607,y:125,zI:2,scale:1},

            ladybugEmpty_1:{x:660,y:155,zI:2,scale:1},
            ladybugCookie_1:{x:679,y:155,zI:2,scale:1},
            ladybugCupcake_1:{x:672,y:155,zI:2,scale:1},
            ladybugCocoa_1:{x:663,y:155,zI:2,scale:1},

            spiderEmpty_1:{x:640,y:-350,zI:3.1,scale:1},
            spiderCookie_1:{x:640,y:-350,zI:3.1,scale:1},
            spiderCupcake_1:{x:640,y:-350,zI:3.1,scale:1},
            spiderCocoa_1:{x:640,y:-350,zI:3.1,scale:1},

            wormEmpty_1:{x:655,y:160,zI:2,scale:1},
            wormCookie_1:{x:655,y:160,zI:2,scale:1},
            wormCupcake_1:{x:655,y:160,zI:2,scale:1},
            wormCocoa_1:{x:655,y:160,zI:2,scale:1},

            cookieEmpty_1:{    x:670,   y:240,   zI:5.1,scale:1},
            cupcakeEmpty_1:{   x:670,   y:200,   zI:5.1,scale:1},
            cocoaEmpty_1:{     x:680,   y:225,   zI:5.1,scale:1}
        }
    },

    scene14:{
        redCap:{
            beeEmpty_1:{x:590,y:185,zI:4,scale:1},
            beeCookie_1:{x:542,y:185,zI:4,scale:1},
            beeCupcake_1:{x:542,y:185,zI:4,scale:1},
            beeCocoa_1:{x:542,y:185,zI:4,scale:1},

            ladybugEmpty_1:{x:605,y:225,zI:4,scale:-1},
            ladybugCookie_1:{x:624,y:225,zI:4,scale:-1},
            ladybugCupcake_1:{x:617,y:225,zI:4,scale:-1},
            ladybugCocoa_1:{x:612,y:225,zI:4,scale:-1},

            spiderEmpty_1:{x:575,y:-275,zI:5.1,scale:1},
            spiderCookie_1:{x:575,y:-275,zI:5.1,scale:1},
            spiderCupcake_1:{x:575,y:-275,zI:5.1,scale:1},
            spiderCocoa_1:{x:575,y:-275,zI:5.1,scale:1},

            wormEmpty_1:{x:630,y:235,zI:4,scale:1},
            wormCookie_1:{x:630,y:235,zI:4,scale:1},
            wormCupcake_1:{x:630,y:235,zI:4,scale:1},
            wormCocoa_1:{x:630,y:235,zI:4,scale:1},

            cookieEmpty_1:{    x:655,   y:300,   zI:5.1,scale:-1},
            cupcakeEmpty_1:{   x:655,   y:265,   zI:5.1,scale:-1},
            cocoaEmpty_1:{     x:615,   y:265,   zI:5.1,scale:1}
        },
        blueCap:{
            beeEmpty_1:{x:670,y:110,zI:2,scale:1},
            beeCookie_1:{x:622,y:110,zI:2,scale:1},
            beeCupcake_1:{x:622,y:110,zI:2,scale:1},
            beeCocoa_1:{x:622,y:110,zI:2,scale:1},

            ladybugEmpty_1:{x:685,y:152,zI:2,scale:-1},
            ladybugCookie_1:{x:704,y:152,zI:2,scale:-1},
            ladybugCupcake_1:{x:693,y:152,zI:2,scale:-1},
            ladybugCocoa_1:{x:688,y:152,zI:2,scale:-1},

            spiderEmpty_1:{x:650,y:-350,zI:3.1,scale:1},
            spiderCookie_1:{x:650,y:-350,zI:3.1,scale:1},
            spiderCupcake_1:{x:650,y:-350,zI:3.1,scale:1},
            spiderCocoa_1:{x:650,y:-350,zI:3.1,scale:1},

            wormEmpty_1:{x:760,y:160,zI:2,scale:1},
            wormCookie_1:{x:760,y:160,zI:2,scale:1},
            wormCupcake_1:{x:760,y:160,zI:2,scale:1},
            wormCocoa_1:{x:760,y:160,zI:2,scale:1},

            cookieEmpty_1:{    x:735,   y:225,   zI:3.1,scale:-1},
            cupcakeEmpty_1:{   x:735,   y:190,   zI:3.1,scale:-1},
            cocoaEmpty_1:{     x:695,   y:190,   zI:3.1,scale:1}

        },
        greenCap:{
            beeEmpty_1:{x:390,y:185,zI:4,scale:1},
            beeCookie_1:{x:342,y:185,zI:4,scale:1},
            beeCupcake_1:{x:342,y:185,zI:4,scale:1},
            beeCocoa_1:{x:342,y:185,zI:4,scale:1},

            ladybugEmpty_1:{x:408,y:225,zI:4,scale:-1},
            ladybugCookie_1:{x:428,y:225,zI:4,scale:-1},
            ladybugCupcake_1:{x:421,y:225,zI:4,scale:-1},
            ladybugCocoa_1:{x:416,y:225,zI:4,scale:-1},

            spiderEmpty_1:{x:375,y:-275,zI:5.1,scale:1},
            spiderCookie_1:{x:375,y:-275,zI:5.1,scale:1},
            spiderCupcake_1:{x:375,y:-275,zI:5.1,scale:1},
            spiderCocoa_1:{x:375,y:-275,zI:5.1,scale:1},

            wormEmpty_1:{x:435,y:235,zI:4,scale:1},
            wormCookie_1:{x:435,y:235,zI:4,scale:1},
            wormCupcake_1:{x:435,y:235,zI:4,scale:1},
            wormCocoa_1:{x:435,y:235,zI:4,scale:1},

            cookieEmpty_1:{    x:455,   y:300,   zI:5.1,scale:-1},
            cupcakeEmpty_1:{   x:455,   y:265,   zI:5.1,scale:-1},
            cocoaEmpty_1:{     x:415,   y:265,   zI:5.1,scale:1}
        },
        door:{
            beeEmpty_1:{x:295,y:125,zI:2,scale:1},
            beeCookie_1:{x:247,y:125,zI:2,scale:1},
            beeCupcake_1:{x:247,y:125,zI:2,scale:1},
            beeCocoa_1:{x:247,y:125,zI:2,scale:1},

            ladybugEmpty_1:{x:300,y:155,zI:2,scale:1},
            ladybugCookie_1:{x:319,y:155,zI:2,scale:1},
            ladybugCupcake_1:{x:312,y:155,zI:2,scale:1},
            ladybugCocoa_1:{x:303,y:155,zI:2,scale:1},

            spiderEmpty_1:{x:280,y:-350,zI:3.1,scale:1},
            spiderCookie_1:{x:280,y:-350,zI:3.1,scale:1},
            spiderCupcake_1:{x:280,y:-350,zI:3.1,scale:1},
            spiderCocoa_1:{x:280,y:-350,zI:3.1,scale:1},

            wormEmpty_1:{x:295,y:160,zI:2,scale:1},
            wormCookie_1:{x:295,y:160,zI:2,scale:1},
            wormCupcake_1:{x:295,y:160,zI:2,scale:1},
            wormCocoa_1:{x:295,y:160,zI:2,scale:1},

            cookieEmpty_1:{    x:310,   y:240,   zI:5.1,scale:1},
            cupcakeEmpty_1:{   x:310,   y:200,   zI:5.1,scale:1},
            cocoaEmpty_1:{     x:320,   y:225,   zI:5.1,scale:1}
        }
    },

    scene21:{
        ground0:{
            ladybugEmpty_2:{        x:250,      y:285,      zI:4,scale:1},
            ladybugSunglasses_2:{   x:250,      y:285,      zI:4,scale:1},
            ladybugBeachball_2:{    x:200,      y:225,      zI:4,scale:1},
            ladybugRadio_2:{        x:235,      y:265,      zI:4, scale:1},
            ladybugFire_2:{         x:255,      y:265,      zI:4, scale:1},

            wormEmpty_2:{           x:265,      y:305,      zI:4,scale:1},
            wormSunglasses_2:{      x:260,      y:298,      zI:4,scale:1},
            wormBeachball_2:{       x:190,      y:239,      zI:4,scale:1},
            wormRadio_2:{           x:235,      y:244,      zI:4,scale:1},
            wormFire_2:{            x:252,      y:268,      zI:4,scale:1},

            sunglassesEmpty_2:{     x:275,      y:340,      zI:4,   scale:1},
            beachballEmpty_2:{      x:275,      y:250,      zI:4,   scale:1},
            radioEmpty_2:{          x:260,      y:270,      zI:4,   scale:1},
            fireEmpty_2:{           x:280,      y:265,      zI:4,   scale:1}

        },
        ground1:{
            ladybugEmpty_2:{        x:510,      y:225,      zI:4,scale:1},
            ladybugSunglasses_2:{   x:510,      y:225,      zI:4,scale:1},
            ladybugBeachball_2:{    x:460,      y:165,      zI:4,scale:1},
            ladybugRadio_2:{        x:495,      y:205,      zI:4, scale:1},
            ladybugFire_2:{         x:515,      y:205,      zI:4, scale:1},

            wormEmpty_2:{           x:500,      y:250,      zI:4,scale:1},
            wormSunglasses_2:{      x:495,      y:243,      zI:4,scale:1},
            wormBeachball_2:{       x:425,      y:184,      zI:4,scale:1},
            wormRadio_2:{           x:470,      y:189,      zI:4,scale:1},
            wormFire_2:{            x:487,      y:213,      zI:4,scale:1},

            sunglassesEmpty_2:{     x:510,      y:280,      zI:2,   scale:1},
            beachballEmpty_2:{      x:510,      y:190,      zI:2,   scale:1},
            radioEmpty_2:{          x:495,      y:210,      zI:2,   scale:1},
            fireEmpty_2:{           x:515,      y:205,      zI:2,   scale:1}
        },

        redLeaf:{
            ladybugEmpty_2:{        x:425,      y:50,      zI:1, scale:-1, angle:35},
            ladybugSunglasses_2:{   x:450,      y:50,      zI:1, scale:-1, angle:35},
            
            spiderEmpty_2:{          x:445,      y: -380,   zI:3.1, scale: -1},
            spiderSunglasses_2:{          x:445,      y: -380,   zI:3.1, scale: -1},

            beeEmpty_2:{          x:360,      y: 10,   zI:2.5, scale: -1},
            beeSunglasses_2:{          x:360,      y: 10,   zI:2.5, scale: -1},


            wormEmpty_2:{               x:260,      y:-5,       zI:1, scale:1,  angle:20},
            wormSunglasses_2:{          x:260,      y:-15,      zI:1, scale:1,  angle:20}
            
        },

        yellowLeaf:{
            ladybugEmpty_2:{             x:370,      y:115,      zI:2, scale:1},
            ladybugSunglasses_2:{        x:370,      y:115,      zI:2, scale:1},
            ladybugBeachball_2:{         x:320,      y:55,      zI:2,scale:1},
            ladybugRadio_2:{             x:355,      y:95,      zI:2, scale:1},
            ladybugFire_2:{              x:375,      y:95,      zI:2, scale:1},

            wormEmpty_2:{           x:350,      y:122,      zI:2,scale:1},
            wormSunglasses_2:{      x:345,      y:115,      zI:2,scale:1},
            wormBeachball_2:{       x:275,      y:56,      zI:2,scale:1},
            wormRadio_2:{           x:320,      y:61,      zI:2,scale:1},
            wormFire_2:{            x:355,      y:100,      zI:2,scale:1},

            spiderEmpty_2:{          x:265,      y: -490,   zI:2, scale: 1},
            spiderSunglasses_2:{     x:265,      y: -490,   zI:2, scale: 1},
            spiderBeachball_2:{      x:300,      y: -490,   zI:2, scale: 1},
            spiderRadio_2:{          x:340,      y: -490,   zI:2, scale: 1},
            spiderFire_2:{           x:355,      y: -515,   zI:2, scale: 1},

            beeEmpty_2:{          x:375,      y: -55,    zI:2, scale: 1},
            beeSunglasses_2:{     x:375,      y: -55,   zI:2, scale: 1},
            beeBeachball_2:{      x:335,      y: -55,   zI:2, scale: 1},
            beeRadio_2:{          x:335,      y: -85,   zI:2, scale: 1},
            beeFire_2:{           x:355,      y:  60,   zI:2, scale: 1},

            sunglassesEmpty_2:{     x:355,      y:170,      zI:2,   scale:1},
            beachballEmpty_2:{      x:355,      y:80,      zI:2,   scale:1},
            radioEmpty_2:{          x:340,      y:100,      zI:2,   scale:1},
            fireEmpty_2:{           x:360,      y:95,      zI:2,   scale:1}

        },

        greenLeaf:{
            ladybugEmpty_2:{             x:565,      y:160,      zI:2.1, scale:1},
            ladybugSunglasses_2:{        x:565,      y:160,      zI:2.1, scale:1},
            ladybugBeachball_2:{         x:515,      y:105,      zI:2.1,scale:1},
            ladybugRadio_2:{             x:540,      y:140,      zI:2.1, scale:1},
            ladybugFire_2:{              x:560,      y:140,      zI:2.1, scale:1},

            wormEmpty_2:{           x:540,      y:170,      zI:2.1,scale:1},
            wormSunglasses_2:{      x:535,      y:165,      zI:2.1,scale:1},
            wormBeachball_2:{       x:465,      y:106,      zI:2.1,scale:1},
            wormRadio_2:{           x:515,      y:111,      zI:2.1,scale:1},
            wormFire_2:{            x:560,      y:140,      zI:2.1,scale:1},

            spiderEmpty_2:{          x:470,      y: -440,   zI:2.1, scale: 1},
            spiderSunglasses_2:{     x:470,      y: -440,   zI:2.1, scale: 1},
            spiderBeachball_2:{      x:505,      y: -440,   zI:2.1, scale: 1},
            spiderRadio_2:{          x:545,      y: -440,   zI:2.1, scale: 1},
            spiderFire_2:{           x:560,      y: -465,   zI:2.1, scale: 1},

            beeEmpty_2:{          x:595,      y: -5,    zI:2.1, scale: 1},
            beeSunglasses_2:{     x:595,      y: -5,   zI:2.1, scale: 1},
            beeBeachball_2:{      x:555,      y: -5,   zI:2.1, scale: 1},
            beeRadio_2:{          x:555,      y: -35,   zI:2.1, scale: 1},
            beeFire_2:{           x:555,      y:  110,   zI:2.1, scale: 1},

            sunglassesEmpty_2:{     x:550,      y:220,      zI:2.1,   scale:1},
            beachballEmpty_2:{      x:550,      y:130,      zI:2.1,   scale:1},
            radioEmpty_2:{          x:535,      y:150,      zI:2.1,   scale:1},
            fireEmpty_2:{           x:555,      y:145,      zI:2.1,   scale:1}
        },

        float0:{
            spiderEmpty_2:{                 x:190,      y: -325,   zI:5.1, scale: 1},
            spiderSunglasses_2:{            x:190,      y: -325,   zI:5.1, scale: 1},
            beeEmpty_2:{                    x:300,      y: 110,    zI:5.1, scale: 1},
            beeSunglasses_2:{               x:300,      y: 110,    zI:5.1, scale: 1},
        },

        float1:{
            spiderEmpty_2:{          x:360,      y: -420,   zI:5.2, scale: 1},
            spiderSunglasses_2:{          x:360,      y: -420,   zI:5.2, scale: 1},
            beeEmpty_2:{             x:465,      y: 10,     zI:5.2, scale: 1},
            beeSunglasses_2:{             x:465,      y: 10,     zI:5.2, scale: 1},
        },

        float2:{
            spiderEmpty_2:{          x:430,      y: -325,   zI:5.3, scale: 1},
            beeEmpty_2:{             x:530,      y: 105,     zI:5.3, scale: 1},
            spiderSunglasses_2:{          x:430,      y: -325,   zI:5.3, scale: 1},
            beeSunglasses_2:{             x:530,      y: 105,     zI:5.3, scale: 1},
        },

        float3:{
            spiderEmpty_2:{          x:490,      y: -430,   zI:5.4, scale: 1},
            beeEmpty_2:{             x:675,      y: 5,     zI:5.4, scale: 1},
            spiderSunglasses_2:{          x:490,      y: -430,   zI:5.4, scale: 1},
            beeSunglasses_2:{             x:675,      y: 5,     zI:5.4, scale: 1},
        },
        
    },

    scene22:{
        ground0:{
            ladybugEmpty_2:{        x:275,      y:260,      zI:4,scale:1},
            ladybugSunglasses_2:{   x:275,      y:260,      zI:4,scale:1},
            ladybugBeachball_2:{    x:225,      y:210,      zI:4,scale:1},
            ladybugRadio_2:{        x:260,      y:250,      zI:4, scale:1},
            ladybugFire_2:{         x:280,      y:250,      zI:4, scale:1},

            wormEmpty_2:{           x:265,      y:280,      zI:4,scale:1},
            wormSunglasses_2:{      x:260,      y:273,      zI:4,scale:1},
            wormBeachball_2:{       x:190,      y:214,      zI:4,scale:1},
            wormRadio_2:{           x:235,      y:219,      zI:4,scale:1},
            wormFire_2:{            x:252,      y:243,      zI:4,scale:1},

            sunglassesEmpty_2:{     x:275,      y:320,      zI:4,   scale:1},
            beachballEmpty_2:{      x:275,      y:230,      zI:4,   scale:1},
            radioEmpty_2:{          x:260,      y:250,      zI:4,   scale:1},
            fireEmpty_2:{           x:280,      y:245,      zI:4,   scale:1}
        },
        ground1:{
            ladybugEmpty_2:{        x:380,      y:135,      zI:2,scale:1},
            ladybugSunglasses_2:{   x:380,      y:135,      zI:2,scale:1},
            ladybugBeachball_2:{    x:275,      y:75,      zI:2,scale:1},
            ladybugRadio_2:{        x:335,      y:115,      zI:2, scale:1},
            ladybugFire_2:{         x:380,      y:115,      zI:2, scale:1},

            wormEmpty_2:{           x:375,      y:160,      zI:2,scale:1},
            wormSunglasses_2:{      x:370,      y:153,     zI:2,scale:1},
            wormBeachball_2:{       x:300,      y:88,      zI:2,scale:1},
            wormRadio_2:{           x:330,      y:85,      zI:2,scale:1},
            wormFire_2:{            x:362,      y:115,      zI:2,scale:1},

            sunglassesEmpty_2:{     x:380,      y:190,      zI:2,   scale:1},
            beachballEmpty_2:{      x:380,      y:100,      zI:2,   scale:1},
            radioEmpty_2:{          x:365,      y:120,      zI:2,   scale:1},
            fireEmpty_2:{           x:385,      y:115,      zI:2,   scale:1}
        },
        ground2:{
            ladybugEmpty_2:{        x:620,      y:275,      zI:4,scale:1},
            ladybugSunglasses_2:{   x:620,      y:275,      zI:4,scale:1},
            ladybugBeachball_2:{    x:515,      y:210,      zI:4,scale:1},
            ladybugRadio_2:{        x:575,      y:255,      zI:4, scale:1},
            ladybugFire_2:{         x:585,      y:255,      zI:4, scale:1},

            wormEmpty_2:{           x:605,      y:298,      zI:4,scale:1},
            wormSunglasses_2:{      x:600,      y:291,     zI:4,scale:1},
            wormBeachball_2:{       x:530,      y:232,      zI:4,scale:1},
            wormRadio_2:{           x:575,      y:227,      zI:4,scale:1},
            wormFire_2:{            x:592,      y:261,      zI:4,scale:1},
        
            sunglassesEmpty_2:{     x:610,      y:335,      zI:4,   scale:1},
            beachballEmpty_2:{      x:610,      y:245,      zI:4,   scale:1},
            radioEmpty_2:{          x:595,      y:265,      zI:4,   scale:1},
            fireEmpty_2:{           x:615,      y:260,      zI:4,   scale:1}
        },
        greenLeaf:{
            ladybugEmpty_2:{        x:390,      y:75,      zI:1, scale:-1, angle:35},
            ladybugSunglasses_2:{   x:390,      y:75,      zI:1, scale:-1, angle:35},

            spiderEmpty_2:{          x:445,      y: -380,   zI:2.5, scale: -1},
            spiderSunglasses_2:{          x:445,      y: -380,   zI:2.5, scale: -1},
            
            beeEmpty_2:{          x:350,      y: 15,   zI:2.5, scale: -1},
            beeSunglasses_2:{          x:350,      y: 15,   zI:2.5, scale: -1},

            wormEmpty_2:{               x:295,      y:20,      zI:1, scale:1,  angle:40},
            wormSunglasses_2:{          x:295,      y:10,      zI:1, scale:1,  angle:40}
        },
        redLeaf:{
            ladybugEmpty_2:{        x:620,      y:40,      zI:1, scale:1, angle:-35},
            ladybugSunglasses_2:{   x:620,      y:40,      zI:1, scale:1, angle:-35},

            spiderEmpty_2:{             x:590,      y: -360,   zI:3.5, scale: 1},
            spiderSunglasses_2:{             x:590,      y: -360,   zI:3.5, scale: 1},

            beeEmpty_2:{          x:675,      y: 15,   zI:2.5, scale: 1},
            beeSunglasses_2:{          x:675,      y: 15,   zI:2.5, scale: 1},
            
            wormEmpty_2:{               x:775,      y:-10,      zI:1, scale:-1,  angle:-20},
            wormSunglasses_2:{          x:775,      y:-20,       zI:1, scale:-1,  angle:-20}
        },
        yellowLeaf:{
            ladybugEmpty_2:{        x:640,      y:110,      zI:2,scale:1},
            ladybugSunglasses_2:{   x:640,      y:110,      zI:2,scale:1},
            ladybugBeachball_2:{    x:535,      y:45,      zI:2,scale:1},
            ladybugRadio_2:{        x:575,      y:90,      zI:2, scale:1},
            ladybugFire_2:{         x:632,      y:90,      zI:2, scale:1},

            wormEmpty_2:{           x:625,      y:115,      zI:2,scale:1},
            wormSunglasses_2:{      x:615,      y:108,      zI:2,scale:1},
            wormBeachball_2:{       x:545,      y:48,       zI:2,scale:1},
            wormRadio_2:{           x:590,      y:60,       zI:2,scale:1},
            wormFire_2:{            x:620,      y:90,       zI:2,scale:1},

            spiderEmpty_2:{          x:540,      y: -495,   zI:2, scale: 1},
            spiderSunglasses_2:{     x:540,      y: -495,   zI:2, scale: 1},
            spiderBeachball_2:{      x:575,      y: -495,   zI:2, scale: 1},
            spiderRadio_2:{          x:615,      y: -495,   zI:2, scale: 1},
            spiderFire_2:{           x:630,      y: -520,   zI:2, scale: 1},

            beeEmpty_2:{          x:665,      y: -60,    zI:2, scale: 1},
            beeSunglasses_2:{     x:665,      y: -60,   zI:2, scale: 1},
            beeBeachball_2:{      x:625,      y: -60,   zI:2, scale: 1},
            beeRadio_2:{          x:625,      y: -90,   zI:2, scale: 1},
            beeFire_2:{           x:625,      y:  55,   zI:2, scale: 1},

            sunglassesEmpty_2:{     x:630,      y:165,      zI:2,   scale:1},
            beachballEmpty_2:{      x:630,      y:75,      zI:2,   scale:1},
            radioEmpty_2:{          x:615,      y:95,      zI:2,   scale:1},
            fireEmpty_2:{           x:635,      y:90,      zI:2,   scale:1}
        },

        float0:{
            spiderEmpty_2:{          x:180,      y: -320,   zI:5.1, scale: 1},
            beeEmpty_2:{             x:295,      y: 100,    zI:5.1, scale: 1},
            spiderSunglasses_2:{          x:180,      y: -320,   zI:5.1, scale: 1},
            beeSunglasses_2:{             x:295,      y: 100,    zI:5.1, scale: 1},
        },

        float1:{
            spiderEmpty_2:{          x:360,      y: -420,   zI:5.2, scale: 1},
            beeEmpty_2:{             x:475,      y: 0,    zI:5.2, scale: 1},
            spiderSunglasses_2:{          x:360,      y: -420,   zI:5.2, scale: 1},
            beeSunglasses_2:{             x:475,      y: 0,    zI:5.2, scale: 1},
        },

        float2:{
            spiderEmpty_2:{          x:820,      y: -325,   zI:5.3, scale: -1},
            beeEmpty_2:{             x:705,      y: 105,    zI:5.3, scale: -1},
            spiderSunglasses_2:{          x:820,      y: -325,   zI:5.3, scale: -1},
            beeSunglasses_2:{             x:705,      y: 105,    zI:5.3, scale: -1},
        }
    },

    scene23:{
        ground0:{
            ladybugEmpty_2:{        x:300,      y:275,      zI:4,scale:1},
            ladybugSunglasses_2:{   x:300,      y:275,      zI:4,scale:1},
            ladybugBeachball_2:{    x:195,      y:210,      zI:4,scale:1},
            ladybugRadio_2:{        x:255,      y:255,      zI:4, scale:1},
            ladybugFire_2:{         x:265,      y:255,      zI:4, scale:1},

            wormEmpty_2:{           x:300,      y:305,      zI:4,scale:1},
            wormSunglasses_2:{      x:295,      y:298,      zI:4,scale:1},
            wormBeachball_2:{       x:225,      y:240,      zI:4,scale:1},
            wormRadio_2:{           x:270,      y:240,      zI:4,scale:1},
            wormFire_2:{            x:287,      y:268,      zI:4,scale:1},

            sunglassesEmpty_2:{     x:320,      y:340,      zI:4,   scale:1},
            beachballEmpty_2:{      x:320,      y:250,      zI:4,   scale:1},
            radioEmpty_2:{          x:305,      y:270,      zI:4,   scale:1},
            fireEmpty_2:{           x:325,      y:265,      zI:4,   scale:1}
        },
        ground1:{
            ladybugEmpty_2:{        x:500,      y:265,      zI:3,scale:1},
            ladybugSunglasses_2:{   x:500,      y:265,      zI:3,scale:1},
            ladybugBeachball_2:{    x:395,      y:200,      zI:3,scale:1},
            ladybugRadio_2:{        x:455,      y:245,      zI:3, scale:1},
            ladybugFire_2:{         x:465,      y:245,      zI:3, scale:1},

            wormEmpty_2:{           x:490,      y:280,      zI:3,scale:1},
            wormSunglasses_2:{      x:485,      y:273,      zI:3,scale:1},
            wormBeachball_2:{       x:465,      y:215,      zI:3,scale:1},
            wormRadio_2:{           x:460,      y:215,      zI:3,scale:1},
            wormFire_2:{            x:477,      y:243,      zI:3,scale:1},

            sunglassesEmpty_2:{     x:505,      y:310,      zI:4,   scale:1},
            beachballEmpty_2:{      x:505,      y:220,      zI:4,   scale:1},
            radioEmpty_2:{          x:490,      y:240,      zI:4,   scale:1},
            fireEmpty_2:{           x:510,      y:235,      zI:4,   scale:1}



        },
        redLeaf:{
            ladybugEmpty_2:{        x:620,      y:40,      zI:1, scale:1, angle:-35},
            ladybugSunglasses_2:{   x:620,      y:40,      zI:1, scale:1, angle:-35},

            spiderEmpty_2:{             x:590,      y: -360,   zI:4.5, scale: 1},
            spiderSunglasses_2:{             x:590,      y: -360,   zI:4.5, scale: 1},

            beeEmpty_2:{             x:685,      y: 10,   zI:4.5, scale: 1},
            beeSunglasses_2:{             x:685,      y: 10,   zI:4.5, scale: 1},
            
            wormEmpty_2:{               x:775,      y:-10,      zI:1, scale:-1,  angle:-20},
            wormSunglasses_2:{          x:775,     y:-20,       zI:1, scale:-1,  angle:-20}
        },
        yellowLeaf:{
            ladybugEmpty_2:{        x:590,      y:135,      zI:2.2,scale:1},
            ladybugSunglasses_2:{   x:590,      y:135,      zI:2.2,scale:1},
            ladybugBeachball_2:{    x:485,      y:70,       zI:2.2,scale:1},
            ladybugRadio_2:{        x:530,      y:115,       zI:2.2, scale:1},
            ladybugFire_2:{         x:582,      y:115,       zI:2.2, scale:1},

            wormEmpty_2:{           x:575,      y:145,      zI:2.2,scale:1},
            wormSunglasses_2:{      x:565,      y:138,      zI:2.2,scale:1},
            wormBeachball_2:{       x:495,      y:90,       zI:2.2,scale:1},
            wormRadio_2:{           x:540,      y:90,       zI:2.2,scale:1},
            wormFire_2:{            x:580,      y:114,      zI:2.2,scale:1},

            spiderEmpty_2:{          x:485,      y: -470,   zI:2.5, scale: 1},
            spiderSunglasses_2:{     x:485,      y: -470,   zI:2.5, scale: 1},
            spiderBeachball_2:{      x:520,      y: -470,   zI:2.5, scale: 1},
            spiderRadio_2:{          x:560,      y: -470,   zI:2.5, scale: 1},
            spiderFire_2:{           x:575,      y: -495,   zI:2.5, scale: 1},

            beeEmpty_2:{          x:620,      y: -35,    zI:2.5, scale: 1},
            beeSunglasses_2:{     x:620,      y: -35,   zI:2.5, scale: 1},
            beeBeachball_2:{      x:580,      y: -35,   zI:2.5, scale: 1},
            beeRadio_2:{          x:580,      y: -65,   zI:2.5, scale: 1},
            beeFire_2:{           x:580,      y:  80,   zI:2.5, scale: 1},

            sunglassesEmpty_2:{     x:575,      y:190,      zI:2.2,   scale:1},
            beachballEmpty_2:{      x:575,      y:100,      zI:2.2,   scale:1},
            radioEmpty_2:{          x:560,      y:120,      zI:2.2,   scale:1},
            fireEmpty_2:{           x:580,      y:115,      zI:2.2,   scale:1}
        },
        greenLeaf:{
            ladybugEmpty_2:{        x:445,      y:115,      zI:1.1,scale:1},
            ladybugSunglasses_2:{   x:445,      y:115,      zI:1.1,scale:1},
            ladybugBeachball_2:{    x:340,      y:50,       zI:1.1,scale:1},
            ladybugRadio_2:{        x:385,      y:95,       zI:1.1, scale:1},
            ladybugFire_2:{         x:437,      y:95,       zI:1.1, scale:1},

            wormEmpty_2:{           x:420,      y:115,      zI:1.1,scale:1},
            wormSunglasses_2:{      x:410,      y:108,      zI:1.1,scale:1},
            wormBeachball_2:{       x:340,      y:60,       zI:1.1,scale:1},
            wormRadio_2:{           x:385,      y:60,       zI:1.1,scale:1},
            wormFire_2:{            x:425,      y:84,       zI:1.1,scale:1},
            
            spiderEmpty_2:{          x:340,      y: -490,   zI:2.1, scale: 1},
            spiderSunglasses_2:{     x:340,      y: -490,   zI:2.1, scale: 1},
            spiderBeachball_2:{      x:375,      y: -490,   zI:2.1, scale: 1},
            spiderRadio_2:{          x:415,      y: -490,   zI:2.1, scale: 1},
            spiderFire_2:{           x:430,      y: -515,   zI:2.1, scale: 1},

            beeEmpty_2:{          x:470,      y: -60,    zI:2.1, scale: 1},
            beeSunglasses_2:{     x:470,      y: -60,   zI:2.1, scale: 1},
            beeBeachball_2:{      x:430,      y: -60,   zI:2.1, scale: 1},
            beeRadio_2:{          x:430,      y: -90,   zI:2.1, scale: 1},
            beeFire_2:{           x:430,      y:  55,   zI:2.1, scale: 1},

            sunglassesEmpty_2:{     x:430,      y:170,      zI:1.1,   scale:1},
            beachballEmpty_2:{      x:430,      y:80,      zI:1.1,   scale:1},
            radioEmpty_2:{          x:415,      y:100,      zI:1.1,   scale:1},
            fireEmpty_2:{           x:435,      y:95,      zI:1.1,   scale:1}

        },
        
        float0:{
            spiderEmpty_2:{          x:480,      y: -450,   zI:5.1, scale: -1},
            beeEmpty_2:{             x:365,      y: 15,    zI:5.1, scale: -1},
            spiderSunglasses_2:{          x:480,      y: -450,   zI:5.1, scale: -1},
            beeSunglasses_2:{             x:365,      y: 15,    zI:5.1, scale: -1},
        },

        float1:{
            spiderEmpty_2:{          x:180,      y: -320,   zI:5.2, scale: 1},
            beeEmpty_2:{             x:295,      y: 100,    zI:5.2, scale: 1},
            spiderSunglasses_2:{          x:180,      y: -320,   zI:5.2, scale: 1},
            beeSunglasses_2:{             x:295,      y: 100,    zI:5.2, scale: 1},
        },

        float2:{
            spiderEmpty_2:{          x:675,      y: -425,   zI:5.3, scale: -1},
            beeEmpty_2:{             x:500,      y: 15,    zI:5.3, scale: -1},
            spiderSunglasses_2:{          x:675,      y: -425,   zI:5.3, scale: -1},
            beeSunglasses_2:{             x:500,      y: 15,    zI:5.3, scale: -1},
        },

        float3:{
            spiderEmpty_2:{          x:750,      y: -325,   zI:5.4, scale: -1},
            beeEmpty_2:{             x:635,      y: 115,    zI:5.4, scale: -1},
            spiderSunglasses_2:{          x:750,      y: -325,   zI:5.4, scale: -1},
            beeSunglasses_2:{             x:635,      y: 115,    zI:5.4, scale: -1},
        }

    },

    scene24:{
        ground0:{
            ladybugEmpty_2:{        x:400,      y:265,      zI:4,scale:1},
            ladybugSunglasses_2:{   x:400,      y:265,      zI:4,scale:1},
            ladybugBeachball_2:{    x:295,      y:200,      zI:4,scale:1},
            ladybugRadio_2:{        x:355,      y:265,      zI:4, scale:1},
            ladybugFire_2:{         x:365,      y:265,      zI:4, scale:1},

            wormEmpty_2:{           x:400,      y:300,      zI:4,scale:1},
            wormSunglasses_2:{      x:390,      y:293,      zI:4,scale:1},
            wormBeachball_2:{       x:320,      y:245,       zI:4,scale:1},
            wormRadio_2:{           x:365,      y:245,       zI:4,scale:1},
            wormFire_2:{            x:405,      y:269,       zI:4,scale:1},

            sunglassesEmpty_2:{     x:390,      y:335,      zI:4,   scale:1},
            beachballEmpty_2:{      x:390,      y:245,      zI:4,   scale:1},
            radioEmpty_2:{          x:375,      y:265,      zI:4,   scale:1},
            fireEmpty_2:{           x:395,      y:260,      zI:4,   scale:1}
        },
        ground1:{
            ladybugEmpty_2:{        x:500,      y:125,      zI:2,scale:1},
            ladybugSunglasses_2:{   x:500,      y:125,      zI:2,scale:1},
            ladybugBeachball_2:{    x:450,      y:52,       zI:2,scale:1},
            ladybugRadio_2:{        x:455,      y:100,      zI:2, scale:1},
            ladybugFire_2:{         x:485,      y:100,      zI:2, scale:1},

            wormEmpty_2:{           x:500,      y:145,      zI:2,scale:1},
            wormSunglasses_2:{      x:490,      y:138,      zI:2,scale:1},
            wormBeachball_2:{       x:420,      y:70,       zI:2,scale:1},
            wormRadio_2:{           x:465,      y:70,       zI:2,scale:1},
            wormFire_2:{            x:505,      y:100,       zI:2,scale:1},

            sunglassesEmpty_2:{     x:510,      y:175,      zI:4,   scale:1},
            beachballEmpty_2:{      x:510,      y:85,      zI:4,   scale:1},
            radioEmpty_2:{          x:495,      y:105,      zI:4,   scale:1},
            fireEmpty_2:{           x:515,      y:100,      zI:4,   scale:1}
        },
        redLeaf:{
            ladybugEmpty_2:{        x:445,      y:50,      zI:1, scale:-1, angle:35},
            ladybugSunglasses_2:{   x:445,      y:50,      zI:1, scale:-1, angle:35},

            spiderEmpty_2:{          x:470,      y: -360,   zI:4.1, scale: 1},
            spiderSunglasses_2:{          x:470,      y: -360,   zI:4.1, scale: 1},

            beeEmpty_2:{             x:375,      y: 15,   zI:4.1, scale: 1},
            beeSunglasses_2:{             x:375,      y: 15,   zI:4.1, scale: 1},
            
            
            wormEmpty_2:{               x:290,      y:-5,       zI:1, scale:1,  angle:20},
            wormSunglasses_2:{          x:290,      y:-15,      zI:1, scale:1,  angle:20}
        },
        yellowLeaf:{
            ladybugEmpty_2:{        x:300,      y:125,      zI:2,scale:1},
            ladybugSunglasses_2:{   x:300,      y:125,      zI:2,scale:1},
            ladybugBeachball_2:{    x:250,      y:52,       zI:2,scale:1},
            ladybugRadio_2:{        x:255,      y:100,      zI:2, scale:1},
            ladybugFire_2:{         x:285,      y:100,      zI:2, scale:1},

            wormEmpty_2:{           x:290,      y:138,      zI:2,scale:1},
            wormSunglasses_2:{      x:280,      y:131,      zI:2,scale:1},
            wormBeachball_2:{       x:210,      y:83,       zI:2,scale:1},
            wormRadio_2:{           x:255,      y:83,       zI:2,scale:1},
            wormFire_2:{            x:295,      y:107,       zI:2,scale:1},

            spiderEmpty_2:{          x:200,      y: -475,   zI:2.1, scale: 1},
            spiderSunglasses_2:{     x:200,      y: -475,   zI:2.1, scale: 1},
            spiderBeachball_2:{      x:235,      y: -475,   zI:2.1, scale: 1},
            spiderRadio_2:{          x:275,      y: -475,   zI:2.1, scale: 1},
            spiderFire_2:{           x:290,      y: -500,   zI:2.1, scale: 1},

            beeEmpty_2:{          x:340,      y: -30,    zI:2.5, scale: 1},
            beeSunglasses_2:{     x:340,      y: -30,   zI:2.5, scale: 1},
            beeBeachball_2:{      x:300,      y: -30,   zI:2.5, scale: 1},
            beeRadio_2:{          x:300,      y: -60,   zI:2.5, scale: 1},
            beeFire_2:{           x:300,      y:  75,   zI:2.5, scale: 1},

            sunglassesEmpty_2:{     x:295,      y:185,      zI:4,   scale:1},
            beachballEmpty_2:{      x:295,      y:95,      zI:4,   scale:1},
            radioEmpty_2:{          x:280,      y:115,      zI:4,   scale:1},
            fireEmpty_2:{           x:300,      y:110,      zI:4,   scale:1}
        },
        greenLeaf:{
            ladybugEmpty_2:{        x:650,      y:110,      zI:1, scale:1, angle:-35},
            ladybugSunglasses_2:{   x:650,      y:110,      zI:1, scale:1, angle:-35},

            spiderEmpty_2:{          x:575,      y: -340,   zI:4.1, scale: 1},
            spiderSunglasses_2:{          x:575,      y: -340,   zI:4.1, scale: 1},

            beeEmpty_2:{             x:675,      y: 35,     zI:4.1, scale: 1},
            beeSunglasses_2:{             x:675,      y: 35,     zI:4.1, scale: 1},
            
            wormEmpty_2:{               x:750,      y:40,      zI:1, scale:-1,  angle:-45},
            wormSunglasses_2:{          x:750,      y:30,       zI:1, scale:-1,  angle:-45}
        },

        float0:{
            spiderEmpty_2:{          x:300,      y: -320,   zI:5.1, scale: 1},
            beeEmpty_2:{             x:395,      y: 100,     zI:5.1, scale: 1},
            spiderSunglasses_2:{          x:300,      y: -320,   zI:5.1, scale: 1},
            beeSunglasses_2:{             x:395,      y: 100,     zI:5.1, scale: 1},
        },

        float1:{
            spiderEmpty_2:{          x:360,      y: -425,   zI:5.2, scale: 1},
            beeEmpty_2:{             x:475,      y: 0,    zI:5.2, scale: 1},
            spiderSunglasses_2:{          x:360,      y: -425,   zI:5.2, scale: 1},
            beeSunglasses_2:{             x:475,      y: 0,    zI:5.2, scale: 1},
        },
    },

    scene31:{
        redBlock:{
            beeEmpty_3:{x:270,y:72,zI:6,scale:1},
            beeDrums_3:{x:250,y:75,zI:6,scale:1},
            beeGuitar_3:{x:250,y:75,zI:6,scale:1},
            beeHorn_3:{x:245,y:110,zI:6,scale:1},
            beeKeyboard_3:{x:245,y:85,zI:6,scale:1},

            ladybugEmpty_3:{x:255,y:125,zI:6,scale:1},
            ladybugDrums_3:{x:250,y:92,zI:6,scale:1},
            ladybugGuitar_3:{x:265,y:125,zI:6,scale:1},
            ladybugHorn_3:{x:240,y:125,zI:6,scale:1},
            ladybugKeyboard_3:{x:240,y:111,zI:6,scale:1},

            spiderEmpty_3:    {x:150,   y:-420,     zI:6,scale:1},
            spiderDrums_3:    {x:250,   y:-385,     zI:6,scale:1},
            spiderGuitar_3:   {x:235,   y:-349,     zI:6, scale:1},
            spiderHorn_3:     {x:235,   y:-349,     zI:6, scale:1},
            spiderKeyboard_3: {x:245,   y:-349,     zI:6, scale:1},

            wormEmpty_3:    {x:235,   y:100,     zI:6,scale:1},
            wormDrums_3:    {x:250,   y:70,     zI:6,scale:1},
            wormGuitar_3:   {x:275,   y:135,     zI:6, scale:1},
            wormHorn_3:     {x:245,   y:150,     zI:6, scale:1},
            wormKeyboard_3: {x:245,   y:120,     zI:6, scale:1},

            drumsEmpty_3:    {x:250,    y:130,  zI:6, scale:1},
            guitarEmpty_3:   {x:285,    y:210,  zI:6, scale:1},
            hornEmpty_3:     {x:285,    y:210,  zI:6, scale:1},
            keyboardEmpty_3: {x:245,    y:170,  zI:6, scale:1},

        },

        greenBlock:{
            beeEmpty_3:{x:445,y:-5,zI:4,scale:1},
            beeDrums_3:{x:430,y:8,zI:4,scale:1},
            beeGuitar_3:{x:430,y:8,zI:4,scale:1},
            beeHorn_3:{x:425,y:43,zI:4,scale:1},
            beeKeyboard_3:{x:425,y:18,zI:4,scale:1},

            ladybugEmpty_3:{x:430,y:58,zI:4,scale:1},
            ladybugDrums_3:{x:435,y:25,zI:4,scale:1},
            ladybugGuitar_3:{x:440,y:58,zI:4,scale:1},
            ladybugHorn_3:{x:425,y:58,zI:4,scale:1},
            ladybugKeyboard_3:{x:425,y:44,zI:4,scale:1},

            spiderEmpty_3:    {x:330,   y:-486,     zI:6,scale:1},
            spiderDrums_3:    {x:430,   y:-451,     zI:6,scale:1},
            spiderGuitar_3:   {x:415,   y:-415,     zI:6, scale:1},
            spiderHorn_3:     {x:415,   y:-415,     zI:6, scale:1},
            spiderKeyboard_3: {x:425,   y:-415,     zI:6, scale:1},

            wormEmpty_3:    {x:415,   y:32,     zI:4,scale:1},
            wormDrums_3:    {x:430,   y:2,      zI:4,scale:1},
            wormGuitar_3:   {x:455,   y:67,     zI:4, scale:1},
            wormHorn_3:     {x:425,   y:82,     zI:4, scale:1},
            wormKeyboard_3: {x:425,   y:52,      zI:4, scale:1},

            drumsEmpty_3:    {x:432,    y:60,  zI:4, scale:1},
            guitarEmpty_3:   {x:467,    y:140,  zI:4, scale:1},
            hornEmpty_3:     {x:467,    y:140,  zI:4, scale:1},
            keyboardEmpty_3: {x:427,    y:100,  zI:4, scale:1},
        },

        blueBlock:{
            beeEmpty_3:{x:645,y:72,zI:6,scale:1},
            beeDrums_3:{x:625,y:75,zI:6,scale:1},
            beeGuitar_3:{x:625,y:75,zI:6,scale:1},
            beeHorn_3:{x:620,y:110,zI:6,scale:1},
            beeKeyboard_3:{x:620,y:85,zI:6,scale:1},

            ladybugEmpty_3:{x:625,y:125,zI:6,scale:1},
            ladybugDrums_3:{x:620,y:92,zI:6,scale:1},
            ladybugGuitar_3:{x:635,y:125,zI:6,scale:1},
            ladybugHorn_3:{x:620,y:125,zI:6,scale:1},
            ladybugKeyboard_3:{x:620,y:111,zI:6,scale:1},

            spiderEmpty_3:    {x:527,   y:-420,     zI:6,scale:1},
            spiderDrums_3:    {x:627,   y:-385,     zI:6,scale:1},
            spiderGuitar_3:   {x:612,   y:-349,     zI:6, scale:1},
            spiderHorn_3:     {x:612,   y:-349,     zI:6, scale:1},
            spiderKeyboard_3: {x:622,   y:-349,     zI:6, scale:1},

            wormEmpty_3:    {x:610,   y:100,     zI:6,scale:1},
            wormDrums_3:    {x:625,   y:70,     zI:6,scale:1},
            wormGuitar_3:   {x:650,   y:135,     zI:6, scale:1},
            wormHorn_3:     {x:620,   y:150,     zI:6, scale:1},
            wormKeyboard_3: {x:620,   y:120,     zI:6, scale:1},

            drumsEmpty_3:    {x:620,    y:130,  zI:6, scale:1},
            guitarEmpty_3:   {x:655,    y:210,  zI:6, scale:1},
            hornEmpty_3:     {x:655,    y:210,  zI:6, scale:1},
            keyboardEmpty_3: {x:615,    y:170,  zI:6, scale:1},
        },

        character0:{
            beeEmpty_3:{x:450,y:180,zI:6,scale:1},
            beeDrums_3:{x:430,y:183,zI:6,scale:1},
            beeGuitar_3:{x:430,y:183,zI:6,scale:1},
            beeHorn_3:{x:425,y:218,zI:6,scale:1},
            beeKeyboard_3:{x:425,y:193,zI:6,scale:1},

            ladybugEmpty_3:{x:430,y:233,zI:6,scale:1},
            ladybugDrums_3:{x:425,y:200,zI:6,scale:1},
            ladybugGuitar_3:{x:440,y:233,zI:6,scale:1},
            ladybugHorn_3:{x:425,y:233,zI:6,scale:1},
            ladybugKeyboard_3:{x:425,y:219,zI:6,scale:1},

            spiderEmpty_3:    {x:332,   y:-312,     zI:6,scale:1},
            spiderDrums_3:    {x:432,   y:-277,     zI:6,scale:1},
            spiderGuitar_3:   {x:417,   y:-241,     zI:6, scale:1},
            spiderHorn_3:     {x:417,   y:-241,     zI:6, scale:1},
            spiderKeyboard_3: {x:417,   y:-241,     zI:6, scale:1},

            wormEmpty_3:    {x:415,   y:208,     zI:6,scale:1},
            wormDrums_3:    {x:430,   y:178,      zI:6,scale:1},
            wormGuitar_3:   {x:455,   y:243,     zI:6, scale:1},
            wormHorn_3:     {x:425,   y:268,     zI:6, scale:1},
            wormKeyboard_3: {x:425,   y:228,     zI:6, scale:1},

            drumsEmpty_3:    {x:425,    y:238,  zI:6, scale:1},
            guitarEmpty_3:   {x:460,    y:318,  zI:6, scale:1},
            hornEmpty_3:     {x:460,    y:318,  zI:6, scale:1},
            keyboardEmpty_3: {x:420,    y:278,  zI:6, scale:1},
        }
    },
    scene32:{
        redBlock:{
            beeEmpty_3:{x:645,y:72,zI:6,scale:1},
            beeDrums_3:{x:625,y:75,zI:6,scale:1},
            beeGuitar_3:{x:625,y:75,zI:6,scale:1},
            beeHorn_3:{x:620,y:110,zI:6,scale:1},
            beeKeyboard_3:{x:620,y:85,zI:6,scale:1},

            ladybugEmpty_3:{x:630,y:124,zI:6,scale:1},
            ladybugDrums_3:{x:635,y:93,zI:6,scale:1},
            ladybugGuitar_3:{x:640,y:124,zI:6,scale:1},
            ladybugHorn_3:{x:625,y:124,zI:6,scale:1},
            ladybugKeyboard_3:{x:625,y:110,zI:6,scale:1},

            spiderEmpty_3:    {x:530,   y:-419,     zI:6,scale:1},
            spiderDrums_3:    {x:630,   y:-384,     zI:6,scale:1},
            spiderGuitar_3:   {x:615,   y:-348,     zI:6, scale:1},
            spiderHorn_3:     {x:615,   y:-348,     zI:6, scale:1},
            spiderKeyboard_3: {x:625,   y:-348,     zI:6, scale:1},

            wormEmpty_3:    {x:615,   y:100,     zI:6,scale:1},
            wormDrums_3:    {x:630,   y:70,     zI:6,scale:1},
            wormGuitar_3:   {x:655,   y:135,     zI:6, scale:1},
            wormHorn_3:     {x:625,   y:150,     zI:6, scale:1},
            wormKeyboard_3: {x:625,   y:120,     zI:6, scale:1},

            drumsEmpty_3:    {x:632,    y:130,  zI:6, scale:1},
            guitarEmpty_3:   {x:667,    y:210,  zI:6, scale:1},
            hornEmpty_3:     {x:667,    y:210,  zI:6, scale:1},
            keyboardEmpty_3: {x:627,    y:170,  zI:6, scale:1},
        },

        greenBlock:{
            beeEmpty_3:{x:455,y:-6,zI:4,scale:1},
            beeDrums_3:{x:440,y:7,zI:4,scale:1},
            beeGuitar_3:{x:440,y:7,zI:4,scale:1},
            beeHorn_3:{x:435,y:42,zI:4,scale:1},
            beeKeyboard_3:{x:435,y:17,zI:4,scale:1},

            ladybugEmpty_3:   {x:440,      y:56,  zI:4,scale:1},
            ladybugDrums_3:   {x:445,      y:25,  zI:4,scale:1},
            ladybugGuitar_3:  {x:450,      y:56,  zI:4,scale:1},
            ladybugHorn_3:    {x:435,      y:56,  zI:4,scale:1},
            ladybugKeyboard_3:{x:435,      y:42,  zI:4,scale:1},

            spiderEmpty_3:    {x:345,   y:-489,     zI:6,scale:1},
            spiderDrums_3:    {x:445,   y:-454,     zI:6,scale:1},
            spiderGuitar_3:   {x:430,   y:-418,     zI:6, scale:1},
            spiderHorn_3:     {x:430,   y:-418,     zI:6, scale:1},
            spiderKeyboard_3: {x:440,   y:-418,     zI:6, scale:1},

            wormEmpty_3:    {x:430,   y:30,     zI:4,scale:1},
            wormDrums_3:    {x:445,   y:0,      zI:4,scale:1},
            wormGuitar_3:   {x:470,   y:65,     zI:4, scale:1},
            wormHorn_3:     {x:440,   y:80,     zI:4, scale:1},
            wormKeyboard_3: {x:440,   y:50,     zI:4, scale:1},

            drumsEmpty_3:    {x:438,    y:60,  zI:4, scale:1},
            guitarEmpty_3:   {x:473,    y:140,  zI:4, scale:1},
            hornEmpty_3:     {x:473,    y:140,  zI:4, scale:1},
            keyboardEmpty_3: {x:433,    y:100,  zI:4, scale:1},
        },

        blueBlock:{
            beeEmpty_3:{x:270,y:-6,zI:4,scale:1},
            beeDrums_3:{x:255,y:7,zI:4,scale:1},
            beeGuitar_3:{x:255,y:7,zI:4,scale:1},
            beeHorn_3:{x:250,y:42,zI:4,scale:1},
            beeKeyboard_3:{x:250,y:17,zI:4,scale:1},

            ladybugEmpty_3:   {x:240,      y:56,  zI:4,scale:1},
            ladybugDrums_3:   {x:260,      y:25,  zI:4,scale:1},
            ladybugGuitar_3:  {x:265,      y:56,  zI:4,scale:1},
            ladybugHorn_3:    {x:250,      y:56,  zI:4,scale:1},
            ladybugKeyboard_3:{x:250,      y:42,  zI:4,scale:1},

            spiderEmpty_3:    {x:160,   y:-489,     zI:6,scale:1},
            spiderDrums_3:    {x:260,   y:-454,     zI:6,scale:1},
            spiderGuitar_3:   {x:215,   y:-418,     zI:6, scale:1},
            spiderHorn_3:     {x:215,   y:-418,     zI:6, scale:1},
            spiderKeyboard_3: {x:255,   y:-418,     zI:6, scale:1},

            wormEmpty_3:    {x:230,   y:30,     zI:4,scale:1},
            wormDrums_3:    {x:255,   y:0,      zI:4,scale:1},
            wormGuitar_3:   {x:280,   y:65,     zI:4, scale:1},
            wormHorn_3:     {x:250,   y:80,     zI:4, scale:1},
            wormKeyboard_3: {x:250,   y:50,     zI:4, scale:1},

            drumsEmpty_3:    {x:255,    y:60,  zI:4, scale:1},
            guitarEmpty_3:   {x:260,    y:140,  zI:4, scale:1},
            hornEmpty_3:     {x:290,    y:140,  zI:4, scale:1},
            keyboardEmpty_3: {x:250,    y:100,  zI:4, scale:1},
        },

        character0:{
            beeEmpty_3:{x:450,y:180,zI:6,scale:1},
            beeDrums_3:{x:430,y:183,zI:6,scale:1},
            beeGuitar_3:{x:430,y:183,zI:6,scale:1},
            beeHorn_3:{x:425,y:218,zI:6,scale:1},
            beeKeyboard_3:{x:425,y:193,zI:6,scale:1},

            ladybugEmpty_3:{x:430,y:233,zI:6,scale:1},
            ladybugDrums_3:{x:425,y:200,zI:6,scale:1},
            ladybugGuitar_3:{x:440,y:233,zI:6,scale:1},
            ladybugHorn_3:{x:425,y:233,zI:6,scale:1},
            ladybugKeyboard_3:{x:425,y:219,zI:6,scale:1},

            spiderEmpty_3:    {x:332,   y:-312,     zI:6,scale:1},
            spiderDrums_3:    {x:432,   y:-277,     zI:6,scale:1},
            spiderGuitar_3:   {x:417,   y:-241,     zI:6, scale:1},
            spiderHorn_3:     {x:417,   y:-241,     zI:6, scale:1},
            spiderKeyboard_3: {x:417,   y:-241,     zI:6, scale:1},

            wormEmpty_3:    {x:415,   y:208,     zI:6,scale:1},
            wormDrums_3:    {x:430,   y:178,      zI:6,scale:1},
            wormGuitar_3:   {x:455,   y:243,     zI:6, scale:1},
            wormHorn_3:     {x:425,   y:268,     zI:6, scale:1},
            wormKeyboard_3: {x:425,   y:228,     zI:6, scale:1},

            drumsEmpty_3:    {x:425,    y:238,  zI:6, scale:1},
            guitarEmpty_3:   {x:460,    y:318,  zI:6, scale:1},
            hornEmpty_3:     {x:460,    y:318,  zI:6, scale:1},
            keyboardEmpty_3: {x:420,    y:278,  zI:6, scale:1},
        }
    },
    scene33:{
        redBlock:{
            beeEmpty_3:{x:462,y:60,zI:4,scale:1},
            beeDrums_3:{x:447,y:73,zI:4,scale:1},
            beeGuitar_3:{x:447,y:73,zI:4,scale:1},
            beeHorn_3:{x:442,y:38,zI:4,scale:1},
            beeKeyboard_3:{x:442,y:13,zI:4,scale:1},

            ladybugEmpty_3:   {x:435,      y:52,  zI:4,scale:1},
            ladybugDrums_3:   {x:455,      y:21,  zI:4,scale:1},
            ladybugGuitar_3:  {x:460,      y:52,  zI:4,scale:1},
            ladybugHorn_3:    {x:445,      y:52,  zI:4,scale:1},
            ladybugKeyboard_3:{x:445,      y:38,  zI:4,scale:1},

            spiderEmpty_3:    {x:355,   y:-491,     zI:4, scale:1},
            spiderDrums_3:    {x:455,   y:-456,     zI:4, scale:1},
            spiderGuitar_3:   {x:410,   y:-420,     zI:4, scale:1},
            spiderHorn_3:     {x:410,   y:-420,     zI:4, scale:1},
            spiderKeyboard_3: {x:450,   y:-420,     zI:4, scale:1},

            drumsEmpty_3:    {x:450,    y:55,  zI:4, scale:1},
            guitarEmpty_3:   {x:455,    y:135,  zI:4, scale:1},
            hornEmpty_3:     {x:485,    y:135,  zI:4, scale:1},
            keyboardEmpty_3: {x:445,    y:95,  zI:4, scale:1},

            
        },

        greenBlock:{
            beeEmpty_3:{x:650,y:72,zI:6,scale:1},
            beeDrums_3:{x:630,y:75,zI:6,scale:1},
            beeGuitar_3:{x:630,y:75,zI:6,scale:1},
            beeHorn_3:{x:625,y:110,zI:6,scale:1},
            beeKeyboard_3:{x:625,y:85,zI:6,scale:1},

            ladybugEmpty_3:{x:630,y:125,zI:6,scale:1},
            ladybugDrums_3:{x:635,y:92,zI:6,scale:1},
            ladybugGuitar_3:{x:640,y:125,zI:6,scale:1},
            ladybugHorn_3:{x:625,y:125,zI:6,scale:1},
            ladybugKeyboard_3:{x:625,y:111,zI:6,scale:1},

            spiderEmpty_3:    {x:535,   y:-419,     zI:6, scale:1},
            spiderDrums_3:    {x:635,   y:-384,     zI:6, scale:1},
            spiderGuitar_3:   {x:590,   y:-348,     zI:6, scale:1},
            spiderHorn_3:     {x:590,   y:-348,     zI:6, scale:1},
            spiderKeyboard_3: {x:630,   y:-348,     zI:6, scale:1},

            wormEmpty_3:    {x:610,   y:30,     zI:4,scale:1},
            wormDrums_3:    {x:635,   y:0,      zI:4,scale:1},
            wormGuitar_3:   {x:660,   y:65,     zI:4, scale:1},
            wormHorn_3:     {x:630,   y:80,     zI:4, scale:1},
            wormKeyboard_3: {x:630,   y:120,     zI:4, scale:1},

            drumsEmpty_3:    {x:635,    y:128,  zI:4, scale:1},
            guitarEmpty_3:   {x:640,    y:208,  zI:4, scale:1},
            hornEmpty_3:     {x:670,    y:208,  zI:4, scale:1},
            keyboardEmpty_3: {x:630,    y:168,  zI:4, scale:1},
        },

        blueBlock:{
            beeEmpty_3:{x:270,y:27,zI:6,scale:1},
            beeDrums_3:{x:255,y:40,zI:6,scale:1},
            beeGuitar_3:{x:255,y:40,zI:6,scale:1},
            beeHorn_3:{x:250,y:75,zI:6,scale:1},
            beeKeyboard_3:{x:250,y:50,zI:6,scale:1},

            ladybugEmpty_3:   {x:237,      y:90,  zI:6,scale:1},
            ladybugDrums_3:   {x:257,      y:59,  zI:6,scale:1},
            ladybugGuitar_3:  {x:262,      y:90,  zI:6,scale:1},
            ladybugHorn_3:    {x:247,      y:90,  zI:6,scale:1},
            ladybugKeyboard_3:{x:247,      y:76,  zI:6,scale:1},

            spiderEmpty_3:    {x:155,   y:-454,     zI:6, scale:1},
            spiderDrums_3:    {x:255,   y:-419,     zI:6, scale:1},
            spiderGuitar_3:   {x:210,   y:-383,     zI:6, scale:1},
            spiderHorn_3:     {x:210,   y:-383,     zI:6, scale:1},
            spiderKeyboard_3: {x:250,   y:-383,     zI:6, scale:1},

            wormEmpty_3:    {x:230,   y:65,     zI:6,scale:1},
            wormDrums_3:    {x:255,   y:35,     zI:6,scale:1},
            wormGuitar_3:   {x:280,   y:100,    zI:6, scale:1},
            wormHorn_3:     {x:250,   y:115,    zI:6, scale:1},
            wormKeyboard_3: {x:250,   y:85,     zI:6, scale:1},

            drumsEmpty_3:    {x:255,    y:95,  zI:6, scale:1},
            guitarEmpty_3:   {x:260,    y:175,  zI:6, scale:1},
            hornEmpty_3:     {x:290,    y:175,  zI:6, scale:1},
            keyboardEmpty_3: {x:250,    y:135,  zI:6, scale:1},
        },
        character0:{
            beeEmpty_3:{x:450,y:180,zI:6,scale:1},
            beeDrums_3:{x:430,y:183,zI:6,scale:1},
            beeGuitar_3:{x:430,y:183,zI:6,scale:1},
            beeHorn_3:{x:425,y:218,zI:6,scale:1},
            beeKeyboard_3:{x:425,y:193,zI:6,scale:1},

            ladybugEmpty_3:{x:430,y:233,zI:6,scale:1},
            ladybugDrums_3:{x:425,y:200,zI:6,scale:1},
            ladybugGuitar_3:{x:440,y:233,zI:6,scale:1},
            ladybugHorn_3:{x:425,y:233,zI:6,scale:1},
            ladybugKeyboard_3:{x:425,y:219,zI:6,scale:1},

            spiderEmpty_3:    {x:332,   y:-312,     zI:6,scale:1},
            spiderDrums_3:    {x:432,   y:-277,     zI:6,scale:1},
            spiderGuitar_3:   {x:417,   y:-241,     zI:6, scale:1},
            spiderHorn_3:     {x:417,   y:-241,     zI:6, scale:1},
            spiderKeyboard_3: {x:417,   y:-241,     zI:6, scale:1},

            wormEmpty_3:    {x:415,   y:208,     zI:6,scale:1},
            wormDrums_3:    {x:430,   y:178,      zI:6,scale:1},
            wormGuitar_3:   {x:455,   y:243,     zI:6, scale:1},
            wormHorn_3:     {x:425,   y:268,     zI:6, scale:1},
            wormKeyboard_3: {x:425,   y:228,     zI:6, scale:1},

            drumsEmpty_3:    {x:425,    y:238,  zI:6, scale:1},
            guitarEmpty_3:   {x:460,    y:318,  zI:6, scale:1},
            hornEmpty_3:     {x:460,    y:318,  zI:6, scale:1},
            keyboardEmpty_3: {x:420,    y:278,  zI:6, scale:1},
        }
    },
    scene34:{
        redBlock:{
            beeEmpty_3:{x:650,y:12,zI:4,scale:1},
            beeDrums_3:{x:635,y:25,zI:4,scale:1},
            beeGuitar_3:{x:635,y:25,zI:4,scale:1},
            beeHorn_3:{x:630,y:50,zI:4,scale:1},
            beeKeyboard_3:{x:630,y:35,zI:4,scale:1},

            ladybugEmpty_3:    {x:630,   y:74,zI:4,scale:1},
            ladybugDrums_3:    {x:635,   y:43,zI:4,scale:1},
            ladybugGuitar_3:   {x:640,   y:74,zI:4,scale:1},
            ladybugHorn_3:     {x:625,   y:74,zI:4,scale:1},
            ladybugKeyboard_3: {x:625,   y:60,zI:4,scale:1},

            spiderEmpty_3:    {x:535,   y:-471,     zI:4, scale:1},
            spiderDrums_3:    {x:635,   y:-436,     zI:4, scale:1},
            spiderGuitar_3:   {x:590,   y:-400,     zI:4, scale:1},
            spiderHorn_3:     {x:590,   y:-400,     zI:4, scale:1},
            spiderKeyboard_3: {x:630,   y:-400,     zI:4, scale:1},

            wormEmpty_3:    {x:610,   y:45,     zI:4,scale:1},
            wormDrums_3:    {x:635,   y:15,     zI:4,scale:1},
            wormGuitar_3:   {x:660,   y:80,    zI:4, scale:1},
            wormHorn_3:     {x:630,   y:95,    zI:4, scale:1},
            wormKeyboard_3: {x:630,   y:65,     zI:4, scale:1},

            drumsEmpty_3:    {x:635,    y:75,  zI:4, scale:1},
            guitarEmpty_3:   {x:640,    y:155,  zI:4, scale:1},
            hornEmpty_3:     {x:670,    y:155,  zI:4, scale:1},
            keyboardEmpty_3: {x:630,    y:115,  zI:4, scale:1},
        },

        greenBlock:{
            beeEmpty_3:{x:275,y:12,zI:4,scale:1},
            beeDrums_3:{x:260,y:25,zI:4,scale:1},
            beeGuitar_3:{x:260,y:25,zI:4,scale:1},
            beeHorn_3:{x:255,y:50,zI:4,scale:1},
            beeKeyboard_3:{x:255,y:35,zI:4,scale:1},

            ladybugEmpty_3:    {x:255,   y:74,zI:4,scale:1},
            ladybugDrums_3:    {x:260,   y:43,zI:4,scale:1},
            ladybugGuitar_3:   {x:265,   y:74,zI:4,scale:1},
            ladybugHorn_3:     {x:250,   y:74,zI:4,scale:1},
            ladybugKeyboard_3: {x:250,   y:60,zI:4,scale:1},

            spiderEmpty_3:    {x:157,   y:-471,     zI:4, scale:1},
            spiderDrums_3:    {x:257,   y:-436,     zI:4, scale:1},
            spiderGuitar_3:   {x:212,   y:-400,     zI:4, scale:1},
            spiderHorn_3:     {x:212,   y:-400,     zI:4, scale:1},
            spiderKeyboard_3: {x:252,   y:-400,     zI:4, scale:1},

            wormEmpty_3:    {x:235,   y:46,     zI:4,scale:1},
            wormDrums_3:    {x:260,   y:16,     zI:4,scale:1},
            wormGuitar_3:   {x:285,   y:81,    zI:4, scale:1},
            wormHorn_3:     {x:255,   y:96,    zI:4, scale:1},
            wormKeyboard_3: {x:255,   y:66,     zI:4, scale:1},

            drumsEmpty_3:    {x:260,    y:75,  zI:4, scale:1},
            guitarEmpty_3:   {x:265,    y:155,  zI:4, scale:1},
            hornEmpty_3:     {x:295,    y:155,  zI:4, scale:1},
            keyboardEmpty_3: {x:255,    y:115,  zI:4, scale:1},
        },

        blueBlock:{
            beeEmpty_3:{x:455,y:12,zI:4,scale:1},
            beeDrums_3:{x:440,y:25,zI:4,scale:1},
            beeGuitar_3:{x:440,y:25,zI:4,scale:1},
            beeHorn_3:{x:435,y:50,zI:4,scale:1},
            beeKeyboard_3:{x:435,y:35,zI:4,scale:1},

            ladybugEmpty_3:    {x:442,   y:74,zI:4,scale:1},
            ladybugDrums_3:    {x:447,   y:43,zI:4,scale:1},
            ladybugGuitar_3:   {x:452,   y:74,zI:4,scale:1},
            ladybugHorn_3:     {x:437,   y:74,zI:4,scale:1},
            ladybugKeyboard_3: {x:437,   y:60,zI:4,scale:1},

            spiderEmpty_3:    {x:345,   y:-471,     zI:4, scale:1},
            spiderDrums_3:    {x:445,   y:-436,     zI:4, scale:1},
            spiderGuitar_3:   {x:400,   y:-400,     zI:4, scale:1},
            spiderHorn_3:     {x:400,   y:-400,     zI:4, scale:1},
            spiderKeyboard_3: {x:440,   y:-400,     zI:4, scale:1},

            wormEmpty_3:    {x:420,   y:46,     zI:4,scale:1},
            wormDrums_3:    {x:445,   y:16,     zI:4,scale:1},
            wormGuitar_3:   {x:470,   y:81,    zI:4, scale:1},
            wormHorn_3:     {x:440,   y:96,    zI:4, scale:1},
            wormKeyboard_3: {x:440,   y:66,     zI:4, scale:1},

            drumsEmpty_3:    {x:450,    y:75,  zI:4, scale:1},
            guitarEmpty_3:   {x:455,    y:155,  zI:4, scale:1},
            hornEmpty_3:     {x:485,    y:155,  zI:4, scale:1},
            keyboardEmpty_3: {x:445,    y:115,  zI:4, scale:1},
        },

        character0:{
            beeEmpty_3:{x:650,y:180,zI:6,scale:1},
            beeDrums_3:{x:630,y:183,zI:6,scale:1},
            beeGuitar_3:{x:630,y:183,zI:6,scale:1},
            beeHorn_3:{x:625,y:218,zI:6,scale:1},
            beeKeyboard_3:{x:625,y:193,zI:6,scale:1},

            ladybugEmpty_3:{x:630,y:233,zI:6,scale:1},
            ladybugDrums_3:{x:625,y:200,zI:6,scale:1},
            ladybugGuitar_3:{x:640,y:233,zI:6,scale:1},
            ladybugHorn_3:{x:625,y:233,zI:6,scale:1},
            ladybugKeyboard_3:{x:625,y:219,zI:6,scale:1},

            spiderEmpty_3:    {x:532,   y:-312,     zI:6,scale:1},
            spiderDrums_3:    {x:632,   y:-277,     zI:6,scale:1},
            spiderGuitar_3:   {x:617,   y:-241,     zI:6, scale:1},
            spiderHorn_3:     {x:617,   y:-241,     zI:6, scale:1},
            spiderKeyboard_3: {x:617,   y:-241,     zI:6, scale:1},

            wormEmpty_3:    {x:615,   y:208,     zI:6,scale:1},
            wormDrums_3:    {x:630,   y:178,      zI:6,scale:1},
            wormGuitar_3:   {x:655,   y:243,     zI:6, scale:1},
            wormHorn_3:     {x:625,   y:268,     zI:6, scale:1},
            wormKeyboard_3: {x:625,   y:228,     zI:6, scale:1},

            drumsEmpty_3:    {x:625,    y:238,  zI:6, scale:1},
            guitarEmpty_3:   {x:660,    y:318,  zI:6, scale:1},
            hornEmpty_3:     {x:660,    y:318,  zI:6, scale:1},
            keyboardEmpty_3: {x:620,    y:278,  zI:6, scale:1},
        }
    }


};

RampsToReading.UI.buttonList = [
    'beachballButton',
    'beeButton',
    'boatButton',
    'clockButton',
    'cocoaButton',
    'cookieButton',
    'cupcakeButton',
    'dayButton',
    'drumsetButton',
    'fireButton',
    'guitarButton',
    'gumballButton',
    'keyboardButton',
    'ladybugButton',
    'lightsButton',
    'nightButton',
    'radioButton',
    'spiderButton',
    'sunglassesButton',
    'hornButton',
    'wormButton',
    'greenCrayonButton',
    'orangeCrayonButton',
    'purpleCrayonButton',
    'blueFlowersButton',
    'purpleFlowersButton',
    'redFlowersButton',
    'blueMushroomButton',
    'orangeMushroomButton',
    'redMushroomButton'
];