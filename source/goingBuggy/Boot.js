var GoingBuggy = {};
RampsToReading = {};
GoingBuggy.Boot = function (game) {
};

GoingBuggy.Boot.prototype = {

    preload: function () {

        //  Here we load the assets required for our preloader (in this case a background and a loading bar)
        this.load.image('preloaderBackground', 'games/goingbuggy/media/img/menu/preloader_background.png');
        this.load.image('preloaderBar', 'games/goingbuggy/media/img/menu/preloader_bar.png');
        this.load.image('splash', 'games/goingbuggy/media/img/menu/splash.png');
        this.load.image('mobileHand', 'games/goingbuggy/media/img/sprites/mobile/hand.png');
        this.load.image('transition', 'games/goingbuggy/media/img/bg/transition.png');
        
    },

    create: function () {

        /*Unless you specifically know your game needs to support multi-touch I would recommend setting this to 1*/
        this.input.maxPointers = 1;

        /*Phaser will automatically pause if the browser tab the game is in loses focus. You can disable that here: */
        this.stage.disableVisibilityChange = true;

        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        // this.scale.minWidth = 480;
        // this.scale.minHeight = 260;
        this.scale.maxWidth = 1024;
        this.scale.maxHeight = 768;
             
        //this.scale.forceLandscape = true;
        this.scale.pageAlignHorizontally = true;
        this.scale.pageAlignVertically = false;
        this.scale.setScreenSize(true);

        this.state.start('Preloader');

    }

};
